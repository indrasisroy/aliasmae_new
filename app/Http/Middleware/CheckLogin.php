<?php
namespace App\Http\Middleware;

use Session;
use Redirect;
use Closure;
use Auth;
use Request;
use Illuminate\Support\Facades\DB;
class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
        $segMent = Request::segments('2');
        //echo "<pre>";
        //print_r($segMent);//die;
        if(Session::has('is_loggedin'))
        {
            
        }
        if(!Session::has('userId') && !Session::has('is_loggedin'))
        {
            Session::put('error_msg','Please login first!');
            Session::save();
            return redirect('/oms');
        }
        else
        {
            $checkLogin = DB::table('StoreManager')->where(array('StoreManagerId'=>Session::get('loginUserId')))->get();
            //echo "<pre>";
        //print_r($checkLogin);echo count($checkLogin);die;
            if(count($checkLogin) == 0)
            {
                return redirect('/oms/logout');            
            }
        }   
        return $next($request);
    }
}
?>