<?php

namespace App\Http\Middleware;
use Session;
use Redirect;
use Closure;
use Auth;
use Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\IpUtils;

class IpCheck
{
    protected $ips = [
        '42.104.76.11',
        '42.104.76.14',
        '111.93.181.158',
        '144.139.86.182',
        '14.201.208.228',
        '203.196.159.35',
        '182.66.186.129',
        '42.110.129.141',
        '182.66.55.119',
    ];

    protected $ipRanges = [
        '10.11.3.1',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        foreach ($request->getClientIps() as $ip) {
            if (! $this->isValidIp($ip) ) //&& ! $this->isValidIpRange($ip)
            {
                // echo $ip; die('hello there');
                return redirect('/page-error');
            }
            
        }

        return $next($request);
    }

    protected function isValidIp($ip)
    {
        return in_array($ip, $this->ips);
    }

    protected function isValidIpRange($ip)
    {
        return IpUtils::checkIp($ip, $this->ipRanges);
    }
}