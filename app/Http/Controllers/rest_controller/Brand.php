<?php
    namespace App\Http\Controllers\rest_controller;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use App\Http\Controllers\rest_controller\MyController as APIBaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Contracts\Filesystem\Filesystem;
    use Illuminate\Pagination\Paginator;  /*for pagination*/
    use Illuminate\Http\Request;
    use App\Mail\sendMyMail;  /*send mail class*/
    use Illuminate\Support\Facades\Mail;  /*for sending mail*/
    use Illuminate\Support\Facades\DB;
    use Image;
    
    class Brand extends APIBaseController{
       public function __construct(){
        
        }
        //public function index(Request $request){
        //    $mode = $request->post('mode');
        //    $userId = $request->post('userId');
        //    $UDID = $request->post('UDID');
        //    
        //}
        
        //==========function for  brand==========//
        public function index(Request $request)
        {
            $userId = $request->post('userId');
            //$UDID = $request->post('UDID');
            $mode = $request->post('mode');
            
            //=====================insert or update brand details=========================//
            if($mode == 'insert_brand_details')
            {
                
                $brandid=$request->post('brandid'); // if brandid send then update , if it is blank then insert

                if(!empty($userId) && $userId != 0)
                {
                    
                    $brandName=trim(strip_tags($request->post('brandName')));
                    
                    $displayInMenulist=$request->post('displayInMenulist');
                    $IsActive=$request->post('IsActive'); //brand status (1: active, 0: blocked ,2 :Deleted)
                    $displaylevel=$request->post('displaylevel');                 
                    $store = $request->post('Store');                 
                                
                    //echo $brandid;die();
                    if($brandid == '')
                    {
                                //insert
                                //return 'insert';
                        $data_to_store=array(
                                    'brandName'=>$brandName,
                                    'displayInMenulist'=>(int)$displayInMenulist,
                                    'displayLevel'=>(int)$displaylevel,
                                    'status'=>(int)$IsActive,
                                    'Store'=> (string)$store                                       
                                     );
                              //echo '<pre>';
                            //print_r($data_to_store);
                            //die;
                        $addCustomerDetls = $this->insertDatafunc($data_to_store,'brand');
                        if(!empty($addCustomerDetls))
                        {
                                    $msg = 'Thank you! Brand added';
                                    $returnArr = array();
                                    return $this->sendResponse($msg,$returnArr);
                        }
                        else
                        {
                            $msg = 'Please try after sometime!';
                            $returnArr = array();
                            return $this->sendError($msg,$returnArr);
                        }
                    }
                    else
                    {
                              
                        $data_to_store=array(
                                'brandName'=>$brandName,
                                'displayInMenulist'=>(int)$displayInMenulist,
                                'displayLevel'=>(int)$displaylevel,
                                'status'=>(int)$IsActive,
                                'Store'=>(string)$store
                                 );

                        $whereArr=array('brandId'=>$brandid);
                        //echo "<pre>";print_r($data_to_store);print_r($whereArr);die();
                        $updateDetails=$this->updateDatafunc($data_to_store,'brand',$whereArr);
                        if(!empty($updateDetails))
                        {
                            $msg = 'Thank you! Brand Updated';
                            $returnArr = array();
                            return $this->sendResponse($msg,$returnArr);
                        }
                        else
                        {
                            $msg = 'No Changes made to update!';
                            $returnArr = array();
                            return $this->sendError($msg,$returnArr);
                        }
                    }
                        
                    //}
                   
                }
                else
                {
                    $msg = ' userid is required!';
                    $returnArr = array();
                    return $this->sendError($msg,$returnArr);
                    
                }
                
                        
            }
            
            //===============================get brand list and brand detials=================================//
            else if($mode == 'get_brand_list')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $brandid=trim($request->post('brandid'));
                $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                
                if($brandid !='' && $storeid === '')
                {
                    $whereCondition['brandId']=$brandid;
                    $brand = $this->getAllData('brand',$whereCondition,'brandId','DESC',$lastlimit,$lastRow);
                    $allbrand = $this->getAllData('brand',$whereCondition,'brandId','DESC');
                }
                elseif ($storeid != '')
                {
                    if($brandid !='')
                    {
                        $whereCondition['brandId']=$brandid;
                    }
                    else
                    {
                        $whereCondition=array();  
                    }
                    $brand = $this->getAllFindInSetData('brand',$whereCondition,$storeid,'Store','brandId','DESC',$lastlimit,$lastRow);
                    $allbrand = $this->getAllFindInSetData('brand',$whereCondition,$storeid,'Store','brandId','DESC');
                }
                else
                {
                    $brand = $this->getAllData('brand',$whereCondition,'brandId','DESC',$lastlimit,$lastRow);
                    $allbrand = $this->getAllData('brand',$whereCondition,'brandId','DESC');
                }
                
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allbrand;
                }
                else
                {
                    $finalarray=$brand;
                }
                
                //echo "<pre>";
                //print_r($banner);
                //die;                
                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['brand'][$rowsCount]['brandId']            = (string)$rows['brandId'];
                            $returnArr['brand'][$rowsCount]['brandmarkdown']            = (string)$rows['markdown'];
                            $returnArr['brand'][$rowsCount]['brandName']          = $rows['brandName'];
                            $returnArr['brand'][$rowsCount]['displayInMenulist']  = $rows['displayInMenulist'];
                            $returnArr['brand'][$rowsCount]['numberofHits']       = $rows['numberofHits'];
                            $returnArr['brand'][$rowsCount]['displayLevel']       = $rows['displayLevel'];
                            $returnArr['brand'][$rowsCount]['Store']              = $rows['Store'];
                            $returnArr['brand'][$rowsCount]['status']             = $rows['status'];
                           
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $brand_pegi    = $this->getAllData('brand',$whereCondition,'brandId','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($brand_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['brand'])){
                    
                    return $this->sendResponse('All brand listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('Sorry!No brand found!',array());
                    
                }
            }
            
            
        }
       
    }
?>