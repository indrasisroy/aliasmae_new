<?php
    namespace App\Http\Controllers\rest_controller;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use App\Http\Controllers\rest_controller\MyController as APIBaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Contracts\Filesystem\Filesystem;
    use Illuminate\Pagination\Paginator;  /*for pagination*/
    use Illuminate\Http\Request;
    use App\Mail\sendMyMail;  /*send mail class*/
    use Illuminate\Support\Facades\Mail;  /*for sending mail*/
    use Illuminate\Support\Facades\DB;
    use Image;
    
    class Allcustomer extends APIBaseController{
       public function __construct(){
        
        }
        //public function index(Request $request){
        //    $mode = $request->post('mode');
        //    $userId = $request->post('userId');
        //    $UDID = $request->post('UDID');
        //    
        //}
        
        //==========function for  article==========//
        public function index(Request $request)
        {
            $userId = $request->post('userId');
            //$UDID = $request->post('UDID');
            $mode = $request->post('mode');
            
            //=====================insert or update product details=========================//
            if($mode == 'insert_customer_manager_details') //product 
            {

                //return $_REQUEST();die;
              $MemberID = trim($request->post('MemberID'));
              // return '9999'.$MemberID;
              // die;
              $CustomerType= trim($request->post('CustomerType'));
              $UserName= trim($request->post('UserName'));
              $UserPassword= trim($request->post('UserPassword'));
              $FirstName= trim($request->post('FirstName'));
              $LastName= trim($request->post('LastName'));
              $BusinessName= trim($request->post('BusinessName'));
              $ABN= trim($request->post('ABN'));
              $StreetAddress1= trim($request->post('StreetAddress1'));
              $StreetAddress2= trim($request->post('StreetAddress2'));
              $Suburb= trim($request->post('Suburb'));
              $State= trim($request->post('State'));
              $Country= trim($request->post('Country'));
              $Zip= trim($request->post('Zip'));
              $Phone= trim($request->post('Phone'));
              $Mobile= trim($request->post('Mobile'));
              $Fax= trim($request->post('Fax'));
              $IsActive= trim($request->post('IsActive'));
              $ReceiveUpdates= trim($request->post('ReceiveUpdates'));

              $DiscountPercent= trim($request->post('DiscountPercent'));
              $AdminApproved= trim($request->post('AdminApproved'));
              $AdminNotes= trim($request->post('AdminNotes'));


              //$InsertionDate= trim($request->post('InsertionDate'));

              $HowHear= trim($request->post('HowHear'));
              $HowHearOther= trim($request->post('HowHearOther'));
              
              $today = date("Y-m-d H:i:s");
              $InsertionDate = strtotime($today);
              //echo $InsertionDate;die;
              $data_to_store=array(
                            'CustomerType'=> $CustomerType,
                            'UserName'=> $UserName,
                            'UserPassword'=> $UserPassword,
                            'FirstName'=> $FirstName,
                            'LastName'=> $LastName,
                            'BusinessName'=> $BusinessName,
                            'ABN'=> $ABN,
                            'StreetAddress1'=>  $StreetAddress1,
                            'StreetAddress2'=> $StreetAddress2,
                            'Suburb'=> $Suburb,
                            'State'=> $State,
                            'Country'=> $Country,
                            'Zip'=> $Zip,
                            'Phone'=> $Phone,
                            'Mobile'=> $Mobile,
                            'Fax'=> $Fax,
                            'IsActive'=> $IsActive,
                            'ReceiveUpdates'=> $ReceiveUpdates,
                            'DiscountPercent'=> $DiscountPercent,
                            'AdminApproved'=> $AdminApproved,
                            'AdminNotes'=> $AdminNotes,
                            'InsertionDate'=> $InsertionDate,
                            'HowHear'=> $HowHear,
                            'HowHearOther'=> $HowHearOther
                );

                // return $data_to_store;
                // die;

                

                if($MemberID === '')
                {
                        
                       //============================== check duplicate user name =======================
                        $whereConditionUsernamecheck['UserName']=$UserName;                    
                        $allmembers = $this->getAllData('members',$whereConditionUsernamecheck,'MemberID','DESC');
                        if(empty($allmembers))
                        {

                            $addCustomerDetls = $this->insertDatafunc($data_to_store,'members');

                           // return 'insert';
                           // die;
                                if(!empty($addCustomerDetls))
                                {
                                    $msg = 'Thank you! Customer Manager added';
                                    $returnArr = array();
                                    return $this->sendResponse($msg,$returnArr);
                                }
                                else
                                {
                                    $msg = 'Please try after sometime!';
                                    $returnArr = array();
                                    return $this->sendError($msg,$returnArr);
                                }

                        }
                        else
                        {
                                    $msg = 'Duplicate User Name!';
                                    $returnArr = array();
                                    return $this->sendError($msg,$returnArr);
                        }
                }else{
                       
                         //============================== check duplicate user name =======================
                        $whereConditionUsernamecheck['UserName']=$UserName;                    
                        $allmembers = $this->getAllData('members',$whereConditionUsernamecheck,'MemberID','DESC');
                         // echo "<pre>";
                         // print_r($allmembers);
                         // echo $allmembers[0]['MemberID'];die;
                        //echo $MemberID;die;
                        if(empty($allmembers))
                        {
                                    $whereArr=array('MemberID'=>$MemberID);                                   
                                    $updateDetails=$this->updateDatafunc($data_to_store,'members',$whereArr);
                                    if(!empty($updateDetails))
                                    {
                                        $msg = 'Thank you! Customer Manager Updated';
                                        $returnArr = array();
                                        return $this->sendResponse($msg,$returnArr);
                                    }
                                    else
                                    {
                                        $msg = 'No Changes made to update!';
                                        $returnArr = array();
                                        return $this->sendError($msg,$returnArr);
                                    }
                               
                        }else
                        {
                            if($allmembers[0]['MemberID'] == $MemberID)
                                {                                 
                                    $whereArr=array('MemberID'=>$MemberID);                                   
                                    $updateDetails=$this->updateDatafunc($data_to_store,'members',$whereArr);
                                    if(!empty($updateDetails))
                                    {
                                        $msg = 'Thank you! Customer Manager Updated';
                                        $returnArr = array();
                                        return $this->sendResponse($msg,$returnArr);
                                    }
                                    else
                                    {
                                        $msg = 'No Changes made to update!';
                                        $returnArr = array();
                                        return $this->sendError($msg,$returnArr);
                                    }
                                    
                                }else{
                                        $msg = 'Duplicate User Name!';
                                        $returnArr = array();
                                        return $this->sendError($msg,$returnArr);           
                                }
                        }
                }
              
            }
            
            //=============================== get customer list and customer detials =================================//
            else if($mode == 'get_customer_manager_list')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));
                $MemberID=trim($request->post('MemberID'));

                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();

                if($MemberID !='')
                {
                    $whereCondition['MemberID']=$MemberID;
                    $members = $this->getAllData('members',$whereCondition,'MemberID','DESC',$lastlimit,$lastRow);
                    $allmembers = $this->getAllData('members',$whereCondition,'MemberID','DESC');
                }else{
                    $members = $this->getAllData('members',$whereCondition,'MemberID','DESC',$lastlimit,$lastRow);
                    $allmembers = $this->getAllData('members',$whereCondition,'MemberID','DESC');                    
                }

                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allmembers;
                }
                else
                {
                    $finalarray=$members;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['customer'][$rowsCount]['MemberID']             = (string)$rows['MemberID'];//
                            $returnArr['customer'][$rowsCount]['CustomerType']         = $rows['CustomerType'];//
                            $returnArr['customer'][$rowsCount]['UserName']             = $rows['UserName'];//
                            $returnArr['customer'][$rowsCount]['email_address']        = $rows['email_address'];//
                            $returnArr['customer'][$rowsCount]['UserPassword']         = $rows['UserPassword']; //
                            $returnArr['customer'][$rowsCount]['FirstName']            = $rows['FirstName']; //
                            $returnArr['customer'][$rowsCount]['LastName']             = $rows['LastName'];//
                            $returnArr['customer'][$rowsCount]['BusinessName']         = $rows['BusinessName'];//
                            $returnArr['customer'][$rowsCount]['ABN']                  = $rows['ABN'];//
                            $returnArr['customer'][$rowsCount]['StreetAddress1']       = $rows['StreetAddress1']; //
                            $returnArr['customer'][$rowsCount]['StreetAddress2']       = $rows['StreetAddress2'];//
                            $returnArr['customer'][$rowsCount]['Suburb']               = $rows['Suburb'];//
                            $returnArr['customer'][$rowsCount]['State']                = $rows['State'];
                            $returnArr['customer'][$rowsCount]['Country']              = $rows['Country'];
                            $returnArr['customer'][$rowsCount]['Zip']                  = $rows['Zip'];
                            $returnArr['customer'][$rowsCount]['Phone']                = $rows['Phone'];
                            $returnArr['customer'][$rowsCount]['Mobile']               = $rows['Mobile'];
                            $returnArr['customer'][$rowsCount]['Fax']                  = $rows['Fax'];
                            $returnArr['customer'][$rowsCount]['IsActive']             = $rows['IsActive'];
                            $returnArr['customer'][$rowsCount]['ReceiveUpdates']       = $rows['ReceiveUpdates'];
                            $returnArr['customer'][$rowsCount]['DiscountPercent']      = $rows['DiscountPercent'];
                            $returnArr['customer'][$rowsCount]['AdminApproved']        = $rows['AdminApproved']; //
                            $returnArr['customer'][$rowsCount]['AdminNotes']           = $rows['AdminNotes'];
                            $returnArr['customer'][$rowsCount]['InsertionDate']        = $rows['InsertionDate']; //
                            $returnArr['customer'][$rowsCount]['HowHear']              = $rows['HowHear']; //
                            $returnArr['customer'][$rowsCount]['HowHearOther']         = $rows['HowHearOther']; //
                            $returnArr['customer'][$rowsCount]['reset_token']          = $rows['reset_token']; //
                            $returnArr['customer'][$rowsCount]['request_time']         = $rows['request_time']; //

                           
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $article_pegi    = $this->getAllData('members',$whereCondition,'MemberID','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($article_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['customer'])){
                    
                    return $this->sendResponse('All Customer listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No Customer found!',array());
                    
                }
            }
            else if($mode == 'update_customer_manager_list')
            {
                echo 'ok';
            }
            
        }
       
    }
?>