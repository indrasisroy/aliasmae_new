<?php

namespace App\Http\Controllers\rest_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Pagination\Paginator;  /*for pagination*/
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Request;
use Image;
class MyController extends BaseController
{
    public function __construct()
    {
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //---------------------------------------- ALL THE API RESPONSES (SUCCESS AND FAILOUR) ----------------------------------------------//
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //=========== send response for success msg ===============//
    public function sendResponse($message='',$result=array())
    {
        $response = [
            'status' => 1,
            'message' => $message,
           // 'data'    => $result,
        ];
        if(!empty($result))
        {
            $response['data'] = $result;
        }

        return response()->json($response, 200);
    }

    //=========== send response for error msg ===============//
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'status' => 0,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
    
    
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //----------------------------------- FIRE CURL TO GET DATA FROM SIGNIFY VISION CMS -------------------------------------------------//
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function fireCurl($mode,$data=array(),$type ="GET")
		{
			$curlUrl = "http://bae660cc-a53e-4db6-b857-e607b6061115:@cloudpay.signifivision.com/api/".$mode;
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => $curlUrl,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $type,
			//CURLOPT_POSTFIELDS => json_encode($curlPostfields),
			//CURLOPT_HTTPHEADER => array("content-type: application/json"),
			CURLOPT_HTTPHEADER => array("content-type: multipart/form-data"), 
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			$responseArr = json_decode($response);
			return $response;
		}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //--------------------------------------------- UPLOAD IMAGE TO STORAGE ------------------------------------------------------//
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function uploadImage($disk,$f_path,$image)
	{
		$storage = \Storage::disk($disk);//specify the storage like s3 or local
		$storage->put($f_path,$image,'public');//store image to storage
	}
    
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //---------------------------------------------  MAKE THUMBNAIL AND UPLOAD IMAGE TO STORAGE -----------------------------------------//
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function uploadThumbnail($disk,$fileThumb,$image,$Twidth,$Theight)
	{
		$storage = \Storage::disk($disk);//specify the storage like s3 or local
		$storage->put($fileThumb,$image,'public');//store image to storage
		
		$url = \Config::get('app.url');
		list($width, $height, $type, $attr) = getimagesize($url.'storage/'.$fileThumb);
		$per = 75;
		$thumb_w = $width;
		$thumb_h = $height;
		if($thumb_w > $Twidth || $thumb_h > $Theight)
		{
			while($per >= 25)
			{
				if($thumb_w > $thumb_h)
				{
					$chkVarThumb = $Twidth;
					$chkVarOriginal = $thumb_w;
				}
				else
				{
					$chkVarThumb = $Theight;
					$chkVarOriginal = $thumb_h;
				}
				
				//if($thumb_w > $Twidth || $thumb_h > $Theight)
				if($chkVarOriginal > $chkVarThumb)
				{
					$thumb_w = intval(($per * $width) / 100);
					$thumb_h = intval(($per * $height) / 100);
				}
				$per = $per - 25;
			}
		}
		$image_t = Image::make(\Storage::disk('public')->get($fileThumb))->resize($thumb_w,$thumb_h)->encode();
        $this->uploadImage('public',$fileThumb,$image_t);
	}
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //----------------------------------- ALL QUERIES FOR MYSQL TABLES WITH LARAVEL QUIRY BUILDER--------------------------------------//
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //================ SEND RESPONSE FOR GET ALL MYSQL ROW WITH CONDITION ============//
    public function getAllData($table,$whereCondition=array(),$order_by='',$order_type='DESC',$limit='',$skip='',$group_by='',$select='*')
    {
        $query = DB::table($table)->select($select);
        if(!empty($whereCondition))
        {
            $query = $query->where($whereCondition);
        }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        if($limit!='')
        {
            $query = $query->offset($skip)->limit($limit);
        }
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        $result = $query->get();
        return json_decode($result,1);
    }
    //================ SEND RESPONSE FOR GET ALL MYSQL ROW WITH find_in_set CONDITION  ============//
    public function getAllFindInSetData($table,$whereCondition=array(),$storeId='',$storeColumn='',$order_by='',$order_type='DESC',$limit='',$skip='',$group_by='',$select='*')
    {
        $query = DB::table($table)->select($select);
        
        if(!empty($storeId) && !empty($storeColumn))
        {
            $query =$query->whereRaw("find_in_set('".$storeId."',".$storeColumn.")");        
        }
        if(!empty($whereCondition))
        {
            $query = $query->where($whereCondition);
           
        }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        if($limit!='')
        {
            $query = $query->offset($skip)->limit($limit);
        }
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        $result = $query->get();
        return json_decode($result,1);
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //----------------------------- ALL QUERIES FOR MONGODB COLLECTIONS WITH LARAVEL QUIRY BUILDER--------------------------------------//
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////================ SEND RESPONSE FOR GET ALL DOCUMENT WITH CONDITION ============/////
    public function getAllDocument($collection,$whereCondition=array(),$order_by='',$order_type='DESC',$limit=10,$skip='',$group_by='',$select='*')
    {
        $query = DB::connection('mongodb')->collection($collection)->select($select);
        if(!empty($whereCondition))
        {
            $query = $query->where($whereCondition);
        }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        //if(!empty($skip))
        //{
            $query = $query->offset($skip)->limit($limit);
			
        //}
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        return $result = $query->get()->toArray();
        //return json_decode($result,1);
    }
	
	public function getAllDocumentAll($collection,$whereCondition=array(),$order_by='',$order_type='DESC',$limit=10,$skip='',$group_by='')
    {
        $query = DB::connection('mongodb')->collection($collection);
        if(!empty($whereCondition))
        {
            $query = $query->where($whereCondition);
        }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        //if(!empty($skip))
        //{
            //$query = $query->offset($skip)->limit($limit);
			
        //}
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        return $result = $query->get()->toArray();
        //return json_decode($result,1);
    }
	
	//////================ SEND RESPONSE FOR GET ALL DOCUMENT WITH INBUILD PAGINATION CONDITION ============/////
	 public function getAllDocumentPagination($collection,$whereCondition=array(),$order_by='',$order_type='DESC',$limit='',$skip='',$group_by='')
    {
        $query = DB::connection('mongodb')->collection($collection);
        if(!empty($whereCondition))
        {
            $query = $query->where($whereCondition);
        }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        //if(!empty($skip))
        //{
        //    $query = $query->offset($skip)->limit($limit);
        //}
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        return $result = $query->paginate($limit);
        //return json_decode($result,1);
    }
    
    //////================ INSERT A DOCUMENT TO MONGODB COLLECTION ============/////
    public function insertDocument($collection,$document)
    {
        return DB::connection('mongodb')->collection($collection)->insertGetId($document);
    }
	// =================== UPDATE DATA TO MONGODB COLLECTION ================= //
	public function updateDocument($collection,$document,$condition){
		return DB::connection('mongodb')->collection($collection)->where($condition)->update($document);
	}
	

    public function insertDatafunc($insertArr,$table){

                        $id = DB::table($table)->insertGetId($insertArr);
                        // return 1;
                         return $id;

      
    }

    public function updateDatafunc($updateArr,$table,$whereCondition){

       

        $id = DB::table($table)->where($whereCondition)->update($updateArr);
                        // return 1;
        return $id;


    }
	
	
	
	
	 public function getAllDocumentAnalytics($collection,$whereCondition=array(),$order_by='',$order_type='DESC',$limit=10,$skip='',$group_by='')
    {
        $query = DB::connection('mongodb')->collection($collection);
        if(!empty($whereCondition))
        {
            $query = $query->where($whereCondition)->where('added_str','>','1522091030');
        }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        //if(!empty($skip))
        //{
            $query = $query->offset($skip)->limit($limit);
			
        //}
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        return $result = $query->get()->toArray();
        //return json_decode($result,1);
    }

    //***********************************  pageination for where array starts here ****************************

     public function getAllDocumentwhereinpagination($collection,$whereCondition=array(),$order_by='',$order_type='DESC',$limit='',$skip='',$group_by='')
    {
        $query = DB::connection('mongodb')->collection($collection);
        if(!empty($whereCondition))
        {
            // $query = $query->where($whereCondition);
            $query->where_in('kiosk', array_unique($whereCondition));
        }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        //if(!empty($skip))
        //{
        //    $query = $query->offset($skip)->limit($limit);
        //}
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        return $result = $query->paginate($limit);
        //return json_decode($result,1);
    }

    //***********************************  pagination for where array ends here *******************************

}
?>