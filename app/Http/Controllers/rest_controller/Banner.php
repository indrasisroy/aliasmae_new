<?php
    namespace App\Http\Controllers\rest_controller;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use App\Http\Controllers\rest_controller\MyController as APIBaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Contracts\Filesystem\Filesystem;
    use Illuminate\Pagination\Paginator;  /*for pagination*/
    use Illuminate\Http\Request;
    use App\Mail\sendMyMail;  /*send mail class*/
    use Illuminate\Support\Facades\Mail;  /*for sending mail*/
    use Illuminate\Support\Facades\DB;
    use Image;
    
    class Banner extends APIBaseController{
       public function __construct(){
        
        }
        //public function index(Request $request){
        //    $mode = $request->post('mode');
        //    $userId = $request->post('userId');
        //    $UDID = $request->post('UDID');
        //    
        //}
        
        //==========function for  customer==========//
        public function index(Request $request)
        {
            $userId = $request->post('userId');
            //$UDID = $request->post('UDID');
            $mode = $request->post('mode');
            
            //=====================insert or update banner details=========================//
            if($mode == 'insert_banner_details'){
                
                $bannerid=$request->post('bannerid'); // if bannerid send then update , if it is blank then insert
                
                if(!empty($userId) && $userId != 0){
                    
                                    $BannerFor=$request->post('BannerFor');
                                    $BannerName=$request->post('BannerName');
                                    $BannerText=$request->post('BannerText');
                                    $BannerURL=$request->post('BannerURL');
                                    $BannerOtherAttributes=$request->post('BannerOtherAttributes');
                                    $login_device_type=$request->post('login_device_type'); //login device type (1: android, 2: IOS ,3 :web)
                                    $IsActive=$request->post('IsActive');
                                    $DisplayOrder=$request->post('DisplayOrder');                 
                                    $store = $request->post('Store');                 
                                    $currentTime = date('Y-m-d H:i:s');
                                    $InsertionDate = strtotime($currentTime);
                                    
                                    $start_date=$request->post('StartDate');
                                    $start_date_time=$request->post('StartTime');
                                    $end_date=$request->post('EndDate');
                                    $end_date_time=$request->post('EndTime');
                    


                    //$file=$request->file('image');
                    if($login_device_type == 1 || $login_device_type == 2)
                                {
                                    $file=$request->file('image');
                                    
                                    if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
                                    {
                                        
                                        $Image1FileName = $file->getClientOriginalName();
                                        $fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
                                        list($width,$height) = getimagesize($file);
                                        //echo $width.'       '.$height;
                                       // $imagesfile= $file->getClientSize();
                                        $Image1FileSize = filesize($file);//$imagesfile;//round(($imagesfile/1024),2);
                                        $image_content = file_get_contents($file);
                                        $path = 'uploads/banner_images/';
                                        $thumb_path = 'uploads/banner_images/thumbnail/';
                                        $fileNormal = $path.$fileName;
                                        $fileThumb = $thumb_path.$fileName;
                                       
                                        $this->uploadImage('public',$fileNormal,$image_content);
                                        $this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
                                        
                                        $Image1SavedFileName=$fileName;
                                       
                                    }
                                    else
                                    {
                                        $Image1SavedFileName='';
                                        $Image1FileName = '';
                                        $Image1FileSize='';
                                    }
                                }
                                else
                                {
                                    $Image1SavedFileName=$request->post('image'); //after upload new image name
                                    $Image1FileName=$request->post('Image1FileName'); // original image name
                                    $Image1FileSize=$request->post('Image1FileSize');
                                }
                                
                    
                   
                    //echo $promocodeid;die();
                    if($bannerid == ''){
                                //insert
                                //return 'insert';
                             $data_to_store=array(
                                        'BannerFor'=>$BannerFor,
                                        'BannerName'=>$BannerName,
                                        'BannerText'=>$BannerText,
                                        'BannerURL'=>$BannerURL,
                                        'BannerOtherAttributes'=>$BannerOtherAttributes,
                                        'Image1FileName'=>$Image1FileName,
                                        'Image1SavedFileName'=>$Image1SavedFileName,
                                        'Image1FileSize' => $Image1FileSize,
                                        'IsActive'=>$IsActive,
                                        'DisplayOrder'=>$DisplayOrder,
                                        'InsertionStrtotime'=> $InsertionDate,
                                        'InsertionDate'=> $currentTime,
                                        'Store'=> $store,
                                        'StartDate'=>$start_date,
                                        'StartTime'=>$start_date_time,
                                        'EndDate'=>$end_date,
                                        'EndTime'=>$end_date_time                                         
                                         );
                             // echo '<pre>';
                             // print_r($data_to_store);
                             // die;
                        $addCustomerDetls = $this->insertDatafunc($data_to_store,'banners');
                        if(!empty($addCustomerDetls)){
                                    $msg = 'Thank you! Banner added';
                                    $returnArr = array();
                                    return $this->sendResponse($msg,$returnArr);
                                }else{
                                    $msg = 'Please try after sometime!';
                                    $returnArr = array();
                                    return $this->sendError($msg,$returnArr);
                                }
                            }else{
                                //update
                              // return 'update';

                                if($Image1SavedFileName != '' && $Image1FileName != '' && $Image1FileSize !=''){
                                    $data_to_store=array(
                                        'BannerFor'=>$BannerFor,
                                        'BannerName'=>$BannerName,
                                        'BannerText'=>$BannerText,
                                        'BannerURL'=>$BannerURL,
                                        'BannerOtherAttributes'=>$BannerOtherAttributes,
                                        'Image1FileName'=>$Image1FileName,
                                        'Image1SavedFileName'=>$Image1SavedFileName,
                                        'Image1FileSize' => $Image1FileSize,
                                        'IsActive'=>$IsActive,
                                        'InsertionStrtotime'=> $InsertionDate,
                                        'InsertionDate'=> $currentTime,
                                        'Store'=> $store,
                                        'StartDate'=>$start_date,
                                        'StartTime'=>$start_date_time,
                                        'EndDate'=>$end_date,
                                        'EndTime'=>$end_date_time
                                         );

                                    //============================== unlink previous image ============================
                                        // $this->unlinkimage($bannerid);  
                                        // $id= $request;
                                                            $ConditionBanner=array('BannerID'=>$bannerid);
                                                            $fetchbanner=$this->getAllData('banners',$ConditionBanner,'BannerID','DESC');
                                                            //echo "<pre>";print_r($fetchbanner);die();
                                                            if(!empty($fetchbanner))
                                                            {
                                                                $previmage=$fetchbanner[0]['Image1SavedFileName'];
                                                                
                                                                
                                                                    $normalpath=storage_path().'/app/'.'public/uploads/banner_images/'.$previmage;
                                                                    $thumbnailpath=storage_path().'/app/'.'public/uploads/banner_images/thumbnail/'.$previmage;
                                                                    
                                                                    if(file_exists($normalpath))
                                                                    {
                                                                        unlink(storage_path().'/app/'.'public/uploads/banner_images/'.$previmage);
                                                                    }
                                                                    if(file_exists($thumbnailpath))
                                                                    {
                                                                        unlink(storage_path().'/app/'.'public/uploads/banner_images/thumbnail/'.$previmage);
                                                                    }
                                                                
                                                                
                                                            }                                       
                                    //============================== unlink previous image ============================
                                }else{
                                 $data_to_store=array(
                                        'BannerFor'=>$BannerFor,
                                        'BannerName'=>$BannerName,
                                        'BannerText'=>$BannerText,
                                        'BannerURL'=>$BannerURL,
                                        'BannerOtherAttributes'=>$BannerOtherAttributes,
                                        //'Image1FileName'=>$Image1FileName,
                                        //'Image1SavedFileName'=>$Image1SavedFileName,
                                        //'Image1FileSize' => $Image1FileSize,
                                        'IsActive'=>$IsActive,
                                        'DisplayOrder'=>$DisplayOrder,
                                        'InsertionStrtotime'=> $InsertionDate,
                                        'InsertionDate'=> $currentTime,
                                        'Store'=> (string)$store,
                                        'StartDate'=>$start_date,
                                        'StartTime'=>$start_date_time,
                                        'EndDate'=>$end_date,
                                        'EndTime'=>$end_date_time
                                         );
                                    }

                                    $whereArr=array('BannerID'=>$bannerid);
                                     $updateDetails=$this->updateDatafunc($data_to_store,'banners',$whereArr);
                                     if(!empty($updateDetails)){
                                            $msg = 'Thank you! Banner Updated';
                                            $returnArr = array();
                                            return $this->sendResponse($msg,$returnArr);
                                        }else{
                                            $msg = 'No Changes made to update!';
                                            $returnArr = array();
                                            return $this->sendError($msg,$returnArr);
                                        }
                            }
                        
                    //}
                   
                }
                else
                {
                    $msg = ' userid is required!';
                    $returnArr = array();
                    return $this->sendError($msg,$returnArr);
                    
                }
                
                        
            }
            
            //===============================get banner list and banner detials=================================//
            else if($mode == 'get_banner_list')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $bannerid=trim($request->post('bannerid'));
                $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                
                if($bannerid !='' && $storeid === '')
                {
                    $whereCondition['BannerID']=$bannerid;
                    $banner = $this->getAllData('banners',$whereCondition,'BannerID','DESC',$lastlimit,$lastRow);
                    $allbanner = $this->getAllData('banners',$whereCondition,'BannerID','DESC');
                }
                elseif ($storeid != '')
                {
                    if($bannerid !='')
                    {
                        $whereCondition['BannerID']=$bannerid;
                    }
                    else
                    {
                        $whereCondition=array();  
                    }
                    $banner = $this->getAllFindInSetData('banners',$whereCondition,$storeid,'Store','BannerID','DESC',$lastlimit,$lastRow);
                    $allbanner = $this->getAllFindInSetData('banners',$whereCondition,$storeid,'Store','BannerID','DESC');
                }
                else{
                    $banner = $this->getAllData('banners',$whereCondition,'BannerID','DESC',$lastlimit,$lastRow);
                    $allbanner = $this->getAllData('banners',$whereCondition,'BannerID','DESC');
                }
                
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allbanner;
                }
                else
                {
                    $finalarray=$banner;
                }
                
                //echo "<pre>";
                //print_r($banner);
                //die;                
                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['banner'][$rowsCount]['BannerID']              = (string)$rows['BannerID'];
                            $returnArr['banner'][$rowsCount]['BannerFor']             = $rows['BannerFor'];
                            $returnArr['banner'][$rowsCount]['BannerName']            = $rows['BannerName'];
                            $returnArr['banner'][$rowsCount]['BannerText']            = $rows['BannerText'];
                            $returnArr['banner'][$rowsCount]['BannerURL']             =$rows['BannerURL'];
                            $returnArr['banner'][$rowsCount]['BannerOtherAttributes'] = $rows['BannerOtherAttributes'];
                            $returnArr['banner'][$rowsCount]['Image1FileName']        = $rows['Image1FileName'];
                            $returnArr['banner'][$rowsCount]['Image1SavedFileName']   = $rows['Image1SavedFileName'];
                            $returnArr['banner'][$rowsCount]['Image1FileSize']        = $rows['Image1FileSize'];
                            $returnArr['banner'][$rowsCount]['IsActive']              = $rows['IsActive'];
                            $returnArr['banner'][$rowsCount]['InsertionDate']         = $rows['InsertionDate'];
                            $returnArr['banner'][$rowsCount]['DisplayOrder']          = $rows['DisplayOrder'];
                            $returnArr['banner'][$rowsCount]['Store']                 = $rows['Store'];

                            $returnArr['banner'][$rowsCount]['StartDate']             = $rows['StartDate'];
                            $returnArr['banner'][$rowsCount]['StartTime']             = $rows['StartTime'];
                            $returnArr['banner'][$rowsCount]['EndDate']               = $rows['EndDate'];
                            $returnArr['banner'][$rowsCount]['EndTime']               = $rows['EndTime'];


                            
                           
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $banner_pegi    = $this->getAllData('banners',$whereCondition,'BannerID','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($banner_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['banner'])){
                    
                    return $this->sendResponse('All banner listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('Sorry!No banner found!',array());
                    
                }
            }
            
            
        }
       
    }
?>