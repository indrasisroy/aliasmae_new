<?php
    namespace App\Http\Controllers\rest_controller;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use App\Http\Controllers\rest_controller\MyController as APIBaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Contracts\Filesystem\Filesystem;
    use Illuminate\Pagination\Paginator;  /*for pagination*/
    use Illuminate\Http\Request;
    use App\Mail\sendMyMail;  /*send mail class*/
    use Illuminate\Support\Facades\Mail;  /*for sending mail*/
    use Illuminate\Support\Facades\DB;
    use Image;
    
    class ProductType extends APIBaseController{
       public function __construct(){
        
        }
        //public function index(Request $request){
        //    $mode = $request->post('mode');
        //    $userId = $request->post('userId');
        //    $UDID = $request->post('UDID');
        //    
        //}
        
        //==========function for  brand==========//
        public function index(Request $request)
        {
            $userId = $request->post('userId');
            //$UDID = $request->post('UDID');
            $mode = $request->post('mode');

            // $msg='123';
            //     $returnArr = array();
            //     return $this->sendError($msg,$returnArr);
            //      die('hello there');
            
            //=====================insert or update brand details=========================//
            //  if($mode == 'insert_producttype_details1')
            // {
            //     $producttypeName=trim(strip_tags($request->post('producttypeName')));
            //     $msg = $producttypeName;
            //     $returnArr = array();
            //     return $this->sendError($msg,$returnArr);
            // }
            if($mode == 'insert_producttype_details')
            {
                // print_r($_REQUEST);
                // $msg = ' userid is required here!';
                // $returnArr = array();
                // return $this->sendError($msg,$returnArr);
                // die('hello inside rest controller');

                $producttypeid=$request->post('producttypeid'); // if producttypeid send then update , if it is blank then insert

                if(!empty($userId) && $userId != 0)
                {                    
                    $producttypeName      =trim(strip_tags($request->post('producttypeName')));
                    
                    $displayInMenulist    = $request->post('displayInMenulist');
                    $IsActive             = $request->post('IsActive'); //brand status (1: active, 0: blocked ,2 :Deleted)
                    $displaylevel         = $request->post('displaylevel');                 
                    $store                = $request->post('Store');  
                    //$measureUnitId        = $request->post('measureUnitId');
                    $colourAccess         = $request->post('colourAccess'); 
                    $materialAccess       = $request->post('materialAccess'); 

                    $sizeEUAccess           = $request->post('sizeEUAccess'); 
                    $sizeUSAAccess          = $request->post('sizeUSAAccess'); 
                    $sizeASIAAccess         = $request->post('sizeASIAAccess'); 
                    $sizeUnitEUAccess       = $request->post('sizeUnitEUAccess'); 
                    $sizeUnitUSAAccess      = $request->post('sizeUnitUSAAccess'); 
                    $sizeUnitASIAAccess     = $request->post('sizeUnitASIAAccess'); 


                    //echo $producttypeid;die();
                    if($producttypeid == '')
                    {
                                //insert
                                //return 'insert';
                        $data_to_store=array(
                                    'producttypeName'=>$producttypeName,
                                    'displayInMenulist'=>(int)$displayInMenulist,
                                    'displayLevel'=>(int)$displaylevel,
                                    'SeoName'=>str_slug($producttypeName,'-'),
                                    'status'=>(int)$IsActive,
                                    'Store'=> (string)$store
                                    //'measureUnitId'=> $measureUnitId,
                                    //'colourAccess'=>$colourAccess,
                                    //'materialAccess'=>$materialAccess                                        
                                     );
                            //echo '<pre>';
                            //print_r($data_to_store);
                            //die;
                        $addCustomerDetls = $this->insertDatafunc($data_to_store,'ProductType');

                        //echo $addCustomerDetls; die;
                        if($addCustomerDetls != '')
                        {

                            //====== insert product color data Table name(productTypeColor)=====
                            if(!empty($colourAccess))
                            {
                                $colourAccessData = explode(',',$colourAccess);
                                foreach ($colourAccessData as $colordata) {
                                    # code...
                                    $data_to_store_color = array(
                                                                'name'=> $colordata,
                                                                'productTypeId'=>$addCustomerDetls
                                                                );     
                                    $add_data_to_store_color = $this->insertDatafunc($data_to_store_color,'productTypeColor');
                                }
                                
                            }

                            // die;
                            //====== insert product material data Table name(productTypeMaterial) ======
                             if(!empty($materialAccess))
                            {
                                $materialAccessData = explode(',',$materialAccess);
                                foreach ($materialAccessData as $materialdata) {
                                    # code...
                                    $data_to_store_material = array(
                                                                'name'=> $materialdata,
                                                                'productTypeId'=>$addCustomerDetls
                                                                );     
                                    $add_data_to_store_material = $this->insertDatafunc($data_to_store_material,'productTypeMaterial');
                                }
                                
                            }


                            //====== store all data in to productTypeSizeMerge table for showing in the add product table ======
                            
                             if(!empty($sizeUSAAccess) || !empty($sizeEUAccess) || !empty($sizeASIAAccess))
                            {                            
                                $sizeUSAAccessData  = explode(',',$sizeUSAAccess);
                                $sizeEUAccessData   = explode(',',$sizeEUAccess);
                                $sizeASIAAccessData = explode(',',$sizeASIAAccess);
                                
                                $sizeUnitUSAAccessData  = explode(',',$sizeUnitUSAAccess);
                                $sizeUnitEUAccessData   = explode(',',$sizeUnitEUAccess);
                                $sizeUnitASIAAccessData = explode(',',$sizeUnitASIAAccess);

                                $i = 0;
                                foreach ($sizeUSAAccessData as $sizeUsadata) {
                                    # code...
                                    // =================== fetch size unit name ====================    step 1                               
                                    $whereCondition['id'] = $sizeUnitEUAccessData[$i];
                                    $unitNameEu = $this->getAllData('measureUnit',$whereCondition,'id','DESC','','');

                                    $whereCondition['id'] = $sizeUnitUSAAccessData[$i];
                                    $unitNameUSA = $this->getAllData('measureUnit',$whereCondition,'id','DESC','','');

                                    $whereCondition['id'] = $sizeUnitASIAAccessData[$i];
                                    $unitNameASIA = $this->getAllData('measureUnit',$whereCondition,'id','DESC','','');
                                    // =================== fetch size unit name ====================

                                    $insertDataValue = $sizeEUAccessData[$i].'('.$unitNameEu['0']['unitName'].')/'.$sizeUSAAccessData[$i].'('.$unitNameUSA['0']['unitName'].')/'.$sizeASIAAccessData[$i].'('.$unitNameASIA['0']['unitName'].')';

                                    $data_to_store_size = array(
                                                                'sizeAll'=> $insertDataValue,
                                                                'productTypeId'=> $addCustomerDetls,
                                                                'status'=>'1'
                                                                ); 

                                    $add_data_to_store_merge = $this->insertDatafunc($data_to_store_size,'productTypeSizeMerge');



                                    //===================== now insert the data to product type size table ================== step 2


                                    $data_to_store_size = array(
                                                                'sizeinusa'=> $sizeUSAAccessData[$i],
                                                                'sizeinusaunit'=>$sizeUnitUSAAccessData[$i],
                                                                'sizeineu'=> $sizeEUAccessData[$i],
                                                                'sizeineuunit'=>$sizeUnitEUAccessData[$i],
                                                                'sizeinasia'=> $sizeASIAAccessData[$i],
                                                                'sizeinasiaunit'=>$sizeUnitASIAAccessData[$i],


                                                                'productTypeSizeMergeId'=> $add_data_to_store_merge,                                                                
                                                                'productTypeId'=>$addCustomerDetls
                                                                );     
                                    $add_data_to_store_size = $this->insertDatafunc($data_to_store_size,'productTypeSize');


                                    $i++;
                                }
                                


                            }

                            //====== insert product size data ======
                            //  if(!empty($sizeUSAAccess))
                            // {
                            //     $sizeUSAAccessData = explode(',',$sizeUSAAccess);
                            //     $i = 0;
                            //     foreach ($sizeUSAAccessData as $sizeUsadata) {
                            //         # code...
                            //         $sizeUnitUSAAccessData = explode(',',$sizeUnitUSAAccess);

                            //         $data_to_store_size = array(
                            //                                     'sizeUnit'=> $sizeUsadata,
                            //                                     'measureUnit'=>$sizeUnitUSAAccessData[$i],
                            //                                     'storesizeId'=>'1',
                            //                                     'productTypeId'=>$addCustomerDetls
                            //                                     );     
                            //         $add_data_to_store_size = $this->insertDatafunc($data_to_store_size,'productTypeSize');

                            //         $i++;
                            //     }
                                
                            // }
                            //  if(!empty($sizeEUAccess))
                            // {
                            //     $sizeEUAccessData = explode(',',$sizeEUAccess);
                            //     $i = 0;
                            //     foreach ($sizeEUAccessData as $sizeEUdata) {
                            //         # code...
                            //         $sizeUnitEUAccessData = explode(',',$sizeUnitEUAccess);

                            //         $data_to_store_size = array(
                            //                                     'sizeUnit'=> $sizeEUdata,
                            //                                     'measureUnit'=>$sizeUnitEUAccessData[$i],
                            //                                     'storesizeId'=>'2',
                            //                                     'productTypeId'=>$addCustomerDetls
                            //                                     );     
                            //         $add_data_to_store_size = $this->insertDatafunc($data_to_store_size,'productTypeSize');

                            //         $i++;
                            //     }
                                
                            // }
                            //  if(!empty($sizeASIAAccess))
                            // {
                            //     $sizeASIAAccessData = explode(',',$sizeASIAAccess);
                            //     $i = 0;
                            //     foreach ($sizeASIAAccessData as $sizeASIAdata) {
                            //         # code...
                            //         $sizeUnitASIAAccessData = explode(',',$sizeUnitASIAAccess);

                            //         $data_to_store_size = array(
                            //                                     'sizeUnit'=> $sizeASIAdata,
                            //                                     'measureUnit'=>$sizeUnitASIAAccessData[$i],
                            //                                     'storesizeId'=>'3',
                            //                                     'productTypeId'=>$addCustomerDetls
                            //                                     );     
                            //         $add_data_to_store_size = $this->insertDatafunc($data_to_store_size,'productTypeSize');

                            //         $i++;
                            //     }
                                
                            // }

                            //die;
                        }

                        if(!empty($addCustomerDetls))
                        {
                                    $msg = 'Thank you! Product Type added';
                                    $returnArr = array();
                                    return $this->sendResponse($msg,$returnArr);
                        }
                        else
                        {
                            $msg = 'Please try after sometime!';
                            $returnArr = array();
                            return $this->sendError($msg,$returnArr);
                        }
                    }
                    else
                    {
                              
                        $data_to_store=array(
                                'producttypeName'=>$producttypeName,
                                'displayInMenulist'=>(int)$displayInMenulist,
                                'displayLevel'=>(int)$displaylevel,
                                'SeoName'=>str_slug($producttypeName,'-'),
                                'status'=>(int)$IsActive,
                                'Store'=>(string)$store
                                //'measureUnitId'=> $measureUnitId,
                                //'colourAccess'=>$colourAccess,
                                //'materialAccess'=>$materialAccess 
                                 );

                        $whereArr=array('producttypeid'=>$producttypeid);
                        //echo "<pre>";print_r($data_to_store);print_r($whereArr);die();
                        $updateDetails=$this->updateDatafunc($data_to_store,'ProductType',$whereArr);
                        if(!empty($updateDetails))
                        {
                            $msg = 'Thank you! Product Type Updated';
                            $returnArr = array();
                            return $this->sendResponse($msg,$returnArr);
                        }
                        else
                        {
                            $msg = 'No Changes made to update!';
                            $returnArr = array();
                            return $this->sendError($msg,$returnArr);
                        }
                    }
                        
                    //}
                   
                }
                else
                {
                    $msg = ' userid is required!';
                    $returnArr = array();
                    return $this->sendError($msg,$returnArr);
                    
                }
                
                        
            }
            
            //===============================get brand list and brand detials=================================//
            else if($mode == 'get_producttype_list')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $producttypeid=trim($request->post('producttypeid'));
                $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                
                if($producttypeid !='' && $storeid === '')
                {
                    $whereCondition['producttypeid']=$producttypeid;
                    $brand = $this->getAllData('ProductType',$whereCondition,'producttypeid','DESC',$lastlimit,$lastRow);
                    $allbrand = $this->getAllData('ProductType',$whereCondition,'producttypeid','DESC');
                }
                elseif ($storeid != '')
                {
                    if($producttypeid !='')
                    {
                        $whereCondition['producttypeid']=$producttypeid;
                    }
                    else
                    {
                        $whereCondition=array();  
                    }
                    $brand = $this->getAllFindInSetData('ProductType',$whereCondition,$storeid,'Store','producttypeid','DESC',$lastlimit,$lastRow);
                    $allbrand = $this->getAllFindInSetData('ProductType',$whereCondition,$storeid,'Store','producttypeid','DESC');
                }
                else
                {
                    $brand = $this->getAllData('ProductType',$whereCondition,'producttypeid','DESC',$lastlimit,$lastRow);
                    $allbrand = $this->getAllData('ProductType',$whereCondition,'producttypeid','DESC');
                }
                
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allbrand;
                }
                else
                {
                    $finalarray=$brand;
                }
                
                //echo "<pre>";
                //print_r($banner);
                //die;                
                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['producttype'][$rowsCount]['producttypeid']      = (string)$rows['producttypeid'];
                            $returnArr['producttype'][$rowsCount]['producttypeName']    = $rows['producttypeName'];
                            $returnArr['producttype'][$rowsCount]['displayInMenulist']  = $rows['displayInMenulist'];
                            $returnArr['producttype'][$rowsCount]['measureUnitId']      = $rows['measureUnitId'];
                            $returnArr['producttype'][$rowsCount]['numberofHits']       = $rows['numberofHits'];
                            $returnArr['producttype'][$rowsCount]['displayLevel']       = $rows['displayLevel'];
                            $returnArr['producttype'][$rowsCount]['Store']              = $rows['Store'];
                            $returnArr['producttype'][$rowsCount]['colourAccess']       = $rows['colourAccess'];
                            $returnArr['producttype'][$rowsCount]['materialAccess']     = $rows['materialAccess'];
                            $returnArr['producttype'][$rowsCount]['status']             = $rows['status'];
                           
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $brand_pegi    = $this->getAllData('ProductType',$whereCondition,'producttypeid','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($brand_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['producttype'])){
                    
                    return $this->sendResponse('All product type listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('Sorry!No product type found!',array());
                    
                }
            }
            
            
        }
       
    }
?>