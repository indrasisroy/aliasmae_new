<?php
    namespace App\Http\Controllers\rest_controller;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use App\Http\Controllers\rest_controller\MyController as APIBaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Contracts\Filesystem\Filesystem;
    use Illuminate\Pagination\Paginator;  /*for pagination*/
    use Illuminate\Http\Request;
    use App\Mail\sendMyMail;  /*send mail class*/
    use Illuminate\Support\Facades\Mail;  /*for sending mail*/
    use Illuminate\Support\Facades\DB;
    use Image;
    
    class StoreManagement extends APIBaseController{
       public function __construct(){
        
        }
         public function index(Request $request)
        {

            $userId     = trim($request->post('userId'));
            $mode       = trim($request->post('mode'));

            if(!empty($userId) && $userId != 0){

                    //======================= Add new store start=========================
                    if($mode == 'Add_new_store'){

                                      $storeName                = trim(strip_tags($request->post('storeName')));                          
                                      $storeLocation            = trim(strip_tags($request->post('storeLocation')));                          
                                      $storeCountryId           = trim($request->post('storeCountryId'));

                                      $storeSize           = trim($request->post('StoreSize')); 
                                      $StoreCurrency           = trim($request->post('StoreCurrency'));
                                      $StoreEmail           = trim($request->post('StoreEmail'));                         
                                      $status                   = trim($request->post('status'));

                                      $storeAddress=trim($request->post('storeAddress'));
                                        $StoreUnitNumber=trim($request->post('StoreUnitNumber'));
                                        $StoreState=trim($request->post('StoreState'));
                                        $StoreZipCode=trim($request->post('StoreZipCode'));       
                                        $storePhoneNumber=trim($request->post('storePhoneNumber'));



                        //check duplicate value with respect to Store name and country id
                                if($storeName!='' && $storeLocation!='' && $storeCountryId!=''){

                                    $data_to_check['StoreName']         = $storeName;
                                    //$data_to_check['StoreLocation']     = $storeLocation;
                                    $data_to_check['CountryId']         = $storeCountryId;
                                    $Check1 = $this->getAllData('Store',$data_to_check,'storeid','DESC');
                                    if(empty($Check1)){
                                                          $data_to_store            = array(
                                                                                        'StoreName'     => $storeName,
                                                                                        'StoreLocation' => $storeLocation, 
                                                                                        'CountryId'     => $storeCountryId,
                                                                                        'StoreCurrency' =>  $StoreCurrency,
                                                                                        'StoreEmail'    =>$StoreEmail,
                                                                                        'StoreSize'     =>$storeSize,
                                                                                        'Status'        => $status,
                                                                                        'storeAddress'  =>$storeAddress,
                                                                                        'StoreUnitNumber'=>$StoreUnitNumber,
                                                                                        'StoreState'    =>$StoreState,
                                                                                        'StoreZipCode'  =>$StoreZipCode,
                                                                                        'storePhoneNumber'=>$storePhoneNumber
                                                                                        );
                                                          $addStoreDetls = $this->insertDatafunc($data_to_store,'Store');
                                                          //echo $addStoreDetls;die();
                                                          if(!empty($addStoreDetls) && $addStoreDetls > 0)
                                                                    {
                                                                        $msg = 'Thank you! new store added';
                                                                        $returnArr = array('insertId'=>$addStoreDetls);
                                                                        return $this->sendResponse($msg,$returnArr);
                                                                    }else{
                                                                        $msg = 'Please try after sometime!';
                                                                        $returnArr = array();
                                                                        return $this->sendError($msg,$returnArr);
                                                                    }
                                                    }else{
                                                        $msg = 'Duplicate Value!';
                                                                        $returnArr = array();
                                                                        return $this->sendError($msg,$returnArr);
                                                    }
                                                }else{
                                                    $msg = 'Please Send All Details!';
                                                    $returnArr = array();
                                                    return $this->sendError($msg,$returnArr);
                                                }
                                        
                    }  
                    //======================= Add new store end ============================       

                    //======================= Edit new store start =========================
                     else if($mode == 'Edit_new_store')
                    {
                                      $storeId                  = trim($request->post('storeId'));                          
                                      $storeName                = trim(strip_tags($request->post('storeName')));                          
                                      $storeLocation            = trim(strip_tags($request->post('storeLocation')));                          
                                      $storeCountryId           = trim($request->post('storeCountryId'));
                                      $StoreCurrency           = trim($request->post('StoreCurrency'));
                                      $StoreEmail           = trim($request->post('StoreEmail')); 
                                      $storeSize           = trim($request->post('StoreSize'));
                                      $status                   = trim($request->post('status'));

                                        $storeAddress=trim($request->post('storeAddress'));
                                        $StoreUnitNumber=trim($request->post('StoreUnitNumber'));
                                        $StoreState=trim($request->post('StoreState'));
                                        $StoreZipCode=trim($request->post('StoreZipCode'));       
                                        $storePhoneNumber=trim($request->post('storePhoneNumber'));




                        if($storeName !='' && $storeLocation !='' && $storeCountryId !='' && $storeId != '')
                        {

                            $whereCheckStoreName=[
                                                    ['StoreName', $storeName],
                                                    ['CountryId', $storeCountryId],
                                                    ['storeid', '<>', $storeId]
                                                ]; 
                                    //$Check1 = $this->getAllData('Store',$data_to_check,'storeid','DESC');                                   
                            $StoreCheck = $this->getAllData('Store',$whereCheckStoreName,'storeid','DESC');
                            //echo '<pre>';print_r($EmailCheck);print_r($UserNameCheck);print_r( $UserNameEmailCheck);die;
                            if(empty($StoreCheck))
                            { 
                                $data_to_store            = array(
                                                            'StoreName'     => $storeName,
                                                            'StoreLocation' => $storeLocation, 
                                                            'CountryId'     => $storeCountryId,
                                                            'StoreCurrency' =>  $StoreCurrency,
                                                            'StoreEmail'    =>$StoreEmail,
                                                            'StoreSize'     =>$storeSize,
                                                            'Status'        => $status,
                                                            'storeAddress'  =>$storeAddress,
                                                            'StoreUnitNumber'=>$StoreUnitNumber,
                                                            'StoreState'    =>$StoreState,
                                                            'StoreZipCode'  =>$StoreZipCode,
                                                            'storePhoneNumber'=>$storePhoneNumber
                                                            );
                              
                                $whereArr=array('storeid'=>$storeId);
                                $updateDetails=$this->updateDatafunc($data_to_store,'Store',$whereArr);
                             /* if(!empty($updateDetails))
                                        {*/
                                            $msg = 'Thank you!  store updated';
                                            $returnArr = array();
                                            return $this->sendResponse($msg,$returnArr);
                                        /*}else{
                                            $msg = 'No changes made to be updated!';
                                            $returnArr = array();
                                            return $this->sendError($msg,$returnArr);
                                        }*/
                            }
                            else
                            {
                                $msg = 'Duplicate Data cannot be updated!';
                                $returnArr = array();
                                return $this->sendError($msg,$returnArr);
                            }
                        }
                        else
                        {
                            $msg = 'Please Send All Details!';
                            $returnArr = array();
                            return $this->sendError($msg,$returnArr);
                        }
                               

                    }
                    //======================= Edit new store end =========================
                    //======================= Status Change store start =========================
                    else if($mode == 'chnage_status_store')
                    {
                        $status                   = trim($request->post('status'));
                        $storeId                  = trim($request->post('storeId')); 
                        $data_to_store            = array(
                                                         'Status'     => $status,
                                                        );
                         $whereArr=array('storeid'=>$storeId);
                         $updateDetails=$this->updateDatafunc($data_to_store,'Store',$whereArr);
                         if(!empty($updateDetails))
                                                {
                                                    $msg = 'Thank you!  store updated';
                                                    $returnArr = array();
                                                    return $this->sendResponse($msg,$returnArr);
                                                }else{
                                                    $msg = 'Sorry!Please try after sometime!';
                                                    $returnArr = array();
                                                    return $this->sendError($msg,$returnArr);
                                                }
                    }
                    //======================= Status Change store end =========================
                    //======================= Fetch store Start =========================
                     else if($mode == 'fetch_store')
                    {
                         $lastRow   =(int)$request->post('lastRow');
                         $flag      = trim($request->post('flag'));  
                         $storeid   =trim($request->post('storeid'));

                        $lastlimit=10;
                                
                                if($lastRow == '')
                                {
                                    $lastRow = 0;
                                }
                
                                $whereCondition=array();
                                
                                if($storeid !='')
                                {
                                    $whereCondition['storeid']=$storeid;
                                }

                                $store = $this->getAllData('Store',$whereCondition,'storeid','ASC',$lastlimit,$lastRow);
                                $allstore = $this->getAllData('Store',$whereCondition,'storeid','ASC');
                
                                $finalarray=array();
                                if($flag == '1')
                                {
                                    $finalarray=$allstore;
                                }
                                else
                                {
                                    $finalarray=$store;
                                }


                                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            // fetch country details
                            $cCode = $rows['CountryId'];
                            $whereConditioncCode['CountryID']=$cCode;
                            $allcountryDetails = $this->getAllData('CountryDetails',$whereConditioncCode,'CountryID','DESC');
                            // fetch country details end
                            if(!empty($allcountryDetails)){
                                $cName      = $allcountryDetails[0]['CountryName']; 
                                $ccurrency  = $allcountryDetails[0]['CurrencyName'].'('.$allcountryDetails[0]['CurrencySymbol'].')';
                            }else{
                                $cName = '';
                                $ccurrency = '';
                            }
                            // fetch country currency details
                            if(isset($rows['StoreCurrency']))
                            {
                                $whereConditionCurrencyCode['CurrencyCode']=$rows['StoreCurrency'];
                            }
                            else{
                                $whereConditionCurrencyCode=array();
                            }
                            $getcountryDetailswithcCode = $this->getAllData('CountryDetails',$whereConditionCurrencyCode,'CountryID','DESC');
                            if(!empty($getcountryDetailswithcCode))
                            {
                                $Storecurrencydtls  = $getcountryDetailswithcCode[0]['CurrencyName'].' ( '.$getcountryDetailswithcCode[0]['CurrencyCode'].' - '.$getcountryDetailswithcCode[0]['CurrencySymbol'].')';
                                $StoreCurrencycode=$getcountryDetailswithcCode[0]['CurrencyCode'];
                            }
                            else
                            {
                                $Storecurrencydtls  ='';
                                $StoreCurrencycode ='';
                            }
                            // fetch country currency details end

                            $returnArr['store'][$rowsCount]['StoreId']                   = (string)$rows['storeid'];
                            $returnArr['store'][$rowsCount]['StoreName']                 = $rows['StoreName'];
                            $returnArr['store'][$rowsCount]['StoreLocation']             = $rows['StoreLocation'];
                            $returnArr['store'][$rowsCount]['CountryId']                 = $rows['CountryId'];
                            $returnArr['store'][$rowsCount]['StoreCurrency']             = $rows['StoreCurrency'];
                             $returnArr['store'][$rowsCount]['StoreEmail']               = $rows['StoreEmail'];
                            $returnArr['store'][$rowsCount]['CountryName']               = $cName;
                            $returnArr['store'][$rowsCount]['Currency']                  = $ccurrency;
                            $returnArr['store'][$rowsCount]['CurrencyDetails']           = $Storecurrencydtls;
                            $returnArr['store'][$rowsCount]['CurrencyCode']              = $StoreCurrencycode;
                            $returnArr['store'][$rowsCount]['Status']                    = $rows['Status'];
                            $returnArr['store'][$rowsCount]['StoreSize']                 = $rows['StoreSize'];

                            $returnArr['store'][$rowsCount]['storePhoneNumber']          =$rows['storePhoneNumber'];
                            $returnArr['store'][$rowsCount]['StoreZipCode']                 =$rows['StoreZipCode'];
                            $returnArr['store'][$rowsCount]['StoreState']                 =$rows['StoreState'];
                            $returnArr['store'][$rowsCount]['StoreUnitNumber']                 =$rows['StoreUnitNumber'];
                            $returnArr['store'][$rowsCount]['StoreAddress']                 =$rows['StoreAddress'];
                            
                           
                            $rowsCount++;

                    }
                //die();
                    $returnArr['pagination']['hasMore']          = 0;
                    $returnArr['pagination']['totaldata']        =count($finalarray);
                    $totalpage                                   =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']        =$totalpage;
                    
                    
                    if($flag == '1')
                    {
                        $lastRow = $lastRow + $lastlimit;
                        $banner_pegi = $this->getAllData('Store',$whereCondition,'storeid','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($banner_pegi))
                        {
                            $returnArr['pagination']['hasMore']     = 1;
                            $returnArr['pagination']['lastRow']     = $lastRow;
                            $returnArr['pagination']['lastLimit']   =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['store'])){
                    
                    return $this->sendResponse('All store listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('Sorry!No store found!',array());
                    
                }

                    }
                    //======================= Fetch store end =========================
                     else if($mode == 'fetch_country_details')
                    {
                         $lastRow   =(int)$request->post('lastRow');
                         $flag      = trim($request->post('flag'));  
                         $countryid   =trim($request->post('countryid'));

                        $lastlimit=10;
                                
                                if($lastRow == '')
                                {
                                    $lastRow = 0;
                                }
                
                                $whereCondition=array();
                                $whereCondition['Status']= '1';
                                if($countryid !='')
                                {
                                    $whereCondition['CountryID']=$countryid;
                                }

                                $store = $this->getAllData('CountryDetails',$whereCondition,'CountryID','ASC',$lastlimit,$lastRow);
                                $allstore = $this->getAllData('CountryDetails',$whereCondition,'CountryID','ASC');
                
                                $finalarray=array();
                                if($flag == '1')
                                {
                                    $finalarray=$allstore;
                                }
                                else
                                {
                                    $finalarray=$store;
                                }


                                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                             $returnArr['country'][$rowsCount]['CountryID']                     = (string)$rows['CountryID'];
                             $returnArr['country'][$rowsCount]['CountryName']                   = $rows['CountryName']; 
                             $returnArr['country'][$rowsCount]['CurrencyName']                   = $rows['CurrencyName']; 
                             $returnArr['country'][$rowsCount]['CurrencyCode']                   = $rows['CurrencyCode']; 
                             $returnArr['country'][$rowsCount]['CurrencySymbol']                   = $rows['CurrencySymbol']; 
                             $returnArr['country'][$rowsCount]['PhoneCode']                   = $rows['PhoneCode']; 
                             $returnArr['country'][$rowsCount]['TimeZone']                   = $rows['TimeZone']; 
                             $returnArr['country'][$rowsCount]['CountryCode']                   = $rows['CountryCode']; 
                             $returnArr['country'][$rowsCount]['UTCoffset']                   = $rows['UTCoffset']; 
                            
                            
                            
                           
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']          = 0;
                    $returnArr['pagination']['totaldata']        =count($finalarray);
                    $totalpage                                   =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']        =$totalpage;
                    
                    
                    if($flag == '1')
                    {
                        $lastRow = $lastRow + $lastlimit;
                        $banner_pegi = $this->getAllData('CountryDetails',$whereCondition,'CountryID','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($banner_pegi))
                        {
                            $returnArr['pagination']['hasMore']     = 1;
                            $returnArr['pagination']['lastRow']     = $lastRow;
                            $returnArr['pagination']['lastLimit']   =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['country'])){
                    
                    return $this->sendResponse('All country listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('Sorry!No country found!',array());
                    
                }


                    }else{
                        return $this->sendError('Sorry! mode blank',array());        
                    }

                }else{
                return $this->sendError('Sorry! userid blank',array());
             }

        }//index end

    }
    ?>