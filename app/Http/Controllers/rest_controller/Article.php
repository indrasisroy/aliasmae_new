<?php
    namespace App\Http\Controllers\rest_controller;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use App\Http\Controllers\rest_controller\MyController as APIBaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Contracts\Filesystem\Filesystem;
    use Illuminate\Pagination\Paginator;  /*for pagination*/
    use Illuminate\Http\Request;
    use App\Mail\sendMyMail;  /*send mail class*/
    use Illuminate\Support\Facades\Mail;  /*for sending mail*/
    use Illuminate\Support\Facades\DB;
    use Image;
    
    class Article extends APIBaseController{
       public function __construct(){
        
        }
        //public function index(Request $request){
        //    $mode = $request->post('mode');
        //    $userId = $request->post('userId');
        //    $UDID = $request->post('UDID');
        //    
        //}
        
        //==========function for  article==========//
        public function index(Request $request)
        {
            $userId = $request->post('userId');
            //$UDID = $request->post('UDID');
            $mode = $request->post('mode');
            
            //=====================insert or update article details=========================//
            if($mode == 'insert_article_details')
            {
                
                $articleid=$request->post('articleid'); // if articleid send then update , if it is blank then insert
                if(!empty($userId) && $userId != 0)
                {
                    
                    $ArticleName=trim(strip_tags($request->post('ArticleName')));
                    $ArticleContent =trim(strip_tags($request->post('ArticleContent')));
                    $IsActive=$request->post('IsActive');
                    $IsFeatured =$request->post('IsFeatured');                 
                    $store = $request->post('Store');                 
                    $currentTime = date('Y-m-d H:i:s');
                    $InsertionDate = strtotime($currentTime);
                    
                    $PageTitle=$request->post('PageTitle');
                    $PageKeywords=$request->post('PageKeywords');
                    $PageDescription=$request->post('PageDescription');
                
                    //echo $promocodeid;die();
                    if($articleid == '')
                    {
                                //insert
                                //return 'insert';
                        $data_to_store=array(
                                    'ArticleName'=>$ArticleName,
                                    'ArticleContent'=>$ArticleContent,
                                    'IsActive'=>$IsActive,
                                    'IsFeatured'=>$IsFeatured,
                                    'InsertionDate'=> $InsertionDate,
                                    'Store'=> $store,
                                    'PageTitle'=>$PageTitle,
                                    'PageKeywords'=>$PageKeywords,
                                    'PageDescription'=>$PageDescription
                                     );
                             // echo '<pre>';
                             // print_r($data_to_store);
                             // die;
                        $addArticleDetls = $this->insertDatafunc($data_to_store,'articles');
                        if(!empty($addArticleDetls))
                        {
                            $msg = 'Thank you! Article added';
                            $returnArr = array();
                            return $this->sendResponse($msg,$returnArr);
                        }
                        else
                        {
                            $msg = 'Please try after sometime!';
                            $returnArr = array();
                            return $this->sendError($msg,$returnArr);
                        }
                    }
                    else
                    {
                        //update
                      // return 'update';
                        $data_to_store=array(
                                'ArticleName'=>$ArticleName,
                                'ArticleContent'=>$ArticleContent,
                                'IsActive'=>$IsActive,
                                'IsFeatured'=>$IsFeatured,
                                'InsertionDate'=> $InsertionDate,
                                'Store'=> $store,
                                'PageTitle'=>$PageTitle,
                                'PageKeywords'=>$PageKeywords,
                                'PageDescription'=>$PageDescription
                             );
                        $whereArr=array('ArticleID'=>$articleid);
                        $updateDetails=$this->updateDatafunc($data_to_store,'articles',$whereArr);
                        if(!empty($updateDetails))
                        {
                            $msg = 'Thank you! Article Updated';
                            $returnArr = array();
                            return $this->sendResponse($msg,$returnArr);
                        }
                        else
                        {
                            $msg = 'No Changes made to update!';
                            $returnArr = array();
                            return $this->sendError($msg,$returnArr);
                        }
                    }
                        
                    //}   
                }
                else
                {
                    $msg = ' Please provide valid details!';
                    $returnArr = array();
                    return $this->sendError($msg,$returnArr);
                    
                }
                
                        
            }
            
            //===============================get article list and article detials=================================//
            else if($mode == 'get_article_list')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $articleid=trim($request->post('articleid'));
                $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                
                if($articleid !='' && $storeid === '')
                {
                    $whereCondition['ArticleID']=$articleid;
                    $article = $this->getAllData('articles',$whereCondition,'ArticleID','DESC',$lastlimit,$lastRow);
                    $allarticle = $this->getAllData('articles',$whereCondition,'ArticleID','DESC');
                }
                elseif ($storeid != '')
                {
                    if($articleid !='')
                    {
                        $whereCondition['ArticleID']=$articleid;
                    }
                    else
                    {
                        $whereCondition=array();  
                    }
                    $article = $this->getAllFindInSetData('articles',$whereCondition,$storeid,'Store','ArticleID','DESC',$lastlimit,$lastRow);
                    $allarticle = $this->getAllFindInSetData('articles',$whereCondition,$storeid,'Store','ArticleID','DESC');
                }
                else
                {
                    $article = $this->getAllData('articles',$whereCondition,'ArticleID','DESC',$lastlimit,$lastRow);
                    $allarticle = $this->getAllData('articles',$whereCondition,'ArticleID','DESC');
                }
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allarticle;
                }
                else
                {
                    $finalarray=$article;
                }
                
                //echo "<pre>";
                //print_r($promocode);
                //die;                
                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['article'][$rowsCount]['ArticleID']             = (string)$rows['ArticleID'];
                            $returnArr['article'][$rowsCount]['ArticleName']           = $rows['ArticleName'];
                            $returnArr['article'][$rowsCount]['ArticleContent']        = $rows['ArticleContent'];
                            $returnArr['article'][$rowsCount]['IsActive']              = $rows['IsActive'];
                            $returnArr['article'][$rowsCount]['InsertionDate']         =$rows['InsertionDate'];
                            $returnArr['article'][$rowsCount]['IsFeatured']            = $rows['IsFeatured'];
                            $returnArr['article'][$rowsCount]['Store']                 = $rows['Store'];
                            $returnArr['article'][$rowsCount]['PageTitle']             = $rows['PageTitle'];
                            $returnArr['article'][$rowsCount]['PageKeywords']          = $rows['PageKeywords'];
                            $returnArr['article'][$rowsCount]['PageDescription']       = $rows['PageDescription'];
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $article_pegi    = $this->getAllData('articles',$whereCondition,'ArticleID','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($article_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['article'])){
                    
                    return $this->sendResponse('All article listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No article found!',array());
                    
                }
            }
            
            
        }
       
    }
?>