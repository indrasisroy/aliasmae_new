<?php
namespace App\Http\Controllers\rest_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\rest_controller\MyController as APIBaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Contracts\Filesystem\Filesystem;
use App\Mail\sendMyMail;  /*send mail class*/
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\DB;

//use Request;
use Image;
class Home extends APIBaseController
{
    public function __construct()
    {
        
    }
    public function index(Request $request)
    {


        $mode = $request->post('mode');
         // die;
         //echo 'aassd'.$mode;die;
        
        
        
        if($mode == 'userLogin')
        {

            $username = trim($request->post('username'));
            $password = trim($request->post('password'));

            $returnArr['userid'] = '';
            if($username != '' && $password != '')
            {
                $whereCondition['UserName'] = $username;
                $whereCondition['password'] = base64_encode($password);
                //$whereCondition['CompanySettingID'] = '1';

                $allUser = $this->getAllData('StoreManager',$whereCondition,'StoreManagerId','DESC');                
                //echo '<pre>'; print_r($allUser);die;
                if(!empty($allUser))
                {

                    $returnArr['userdetails'] = $allUser[0];
                    return $this->sendResponse('Success! user found',$returnArr);
                }
                else
                {
                    return $this->sendError('Sorry! User not found',array());
                }
                
            }
            else
            {
               return $this->sendError('Sorry! Wrong Input!',array());
            }
            
        }
        // get category list starts here
        else if($mode == 'get_forntend_category_list')
        {

                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $categoryid=trim($request->post('categoryid'));
                $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                $whereCondition['DisplayCategory']='1';
                if($categoryid !='' && $categoryid === '')
                {
                    $whereCondition['CategoryID']=$categoryid;
                    $category = $this->getAllData('categories',$whereCondition,'CategoryID','DESC',$lastlimit,$lastRow);
                    $allcategory = $this->getAllData('categories',$whereCondition,'CategoryID','DESC');
                }
                elseif ($storeid != '')
                {
                    if($categoryid !='')
                    {
                        $whereCondition['CategoryID']=$categoryid;
                    }
                    else
                    {
                        $whereCondition=array();  
                    }
                    $category = $this->getAllFindInSetData('categories',$whereCondition,$storeid,'Store','CategoryID','DESC',$lastlimit,$lastRow);
                    $allcategory = $this->getAllFindInSetData('categories',$whereCondition,$storeid,'Store','CategoryID','DESC');
                }
                else{
                    $category = $this->getAllData('categories',$whereCondition,'CategoryID','DESC',$lastlimit,$lastRow);
                    $allcategory = $this->getAllData('categories',$whereCondition,'CategoryID','DESC');
                }
                
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allcategory;
                }
                else
                {
                    $finalarray=$category;
                }
                
                //echo "<pre>";
                //print_r($banner);
                //die;                
                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['category'][$rowsCount]['CategoryID']            = (string)$rows['CategoryID'];
                            $returnArr['category'][$rowsCount]['CategoryName']          = $rows['CategoryName'];
                            $returnArr['category'][$rowsCount]['Description']           = $rows['Description'];
                            $returnArr['category'][$rowsCount]['NumberHits']            = $rows['NumberHits'];
                            $returnArr['category'][$rowsCount]['CategoryLevel']         = $rows['CategoryLevel'];
                            $returnArr['category'][$rowsCount]['PageTitle']             = $rows['PageTitle'];
                            $returnArr['category'][$rowsCount]['PageKeywords']          = $rows['PageKeywords'];
                            $returnArr['category'][$rowsCount]['PageDescription']       = $rows['PageDescription'];
                            $returnArr['category'][$rowsCount]['MenuImage']             = $rows['MenuImage'];
                            $returnArr['category'][$rowsCount]['MenuRollOverImage']     = $rows['MenuRollOverImage'];
                            $returnArr['category'][$rowsCount]['URLFriendlyName']       = $rows['URLFriendlyName'];
                            $returnArr['category'][$rowsCount]['CategoryDiscount']      = $rows['CategoryDiscount'];
                            $returnArr['category'][$rowsCount]['Store']                 = $rows['Store'];

                            $returnArr['category'][$rowsCount]['BannerImage']           = $rows['BannerImage'];
                            $returnArr['category'][$rowsCount]['BannerImageText']       = $rows['BannerImageText'];
                            $returnArr['category'][$rowsCount]['salecategory']          = $rows['salecategory'];
                            $returnArr['category'][$rowsCount]['DisplayCategory']       = $rows['DisplayCategory'];


                            
                           
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $category_pegi    = $this->getAllData('categories',$whereCondition,'CategoryID','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($category_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['category'])){
                    
                    return $this->sendResponse('All category listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('Sorry!No category found!',array());
                    
                }
        }
        //get category list end here
        else if($mode == 'getadmin_manager_details')
        {

            $adminId = trim($request->post('adminId'));
            $whereCondition['StoreManagerId'] = $adminId;
            $companyadminmanager_details = $this->getAllData('StoreManager',$whereCondition,'StoreManagerId','DESC');
             if(!empty($companyadminmanager_details))
            {
                  $returnArr['Admin_manager_details'] = $companyadminmanager_details;
                  $returnArr['Status'] = '1';
                  return $this->sendResponse('Success! Admin Details Found',$returnArr);

            }else{
                    return $this->sendError('Sorry! Wrong userId!',array());
            }
            

        }
        else if($mode == 'retreivePassword')
        {
           // echo 'ok';
                $userEmail = '';
                $userId = ''; $FirstName="";
                $receiver = trim($request->post('admin_email_or_username'));
                $userId  = trim($request->post('userId'));

                $whereCondition1['UserName'] = $receiver;
                $chkuser = $this->getAllData('StoreManager',$whereCondition1,'StoreManagerId','DESC');
                // echo  $chkuser;
                // die;
                if(!empty($chkuser))
                {

                    $adminmemberid = $chkuser[0]['StoreManagerId'];
                    /*$wherememberCondition['MemberID'] = $adminmemberid;
                    $chkusermember = $this->getAllData('members',$wherememberCondition,'MemberID','DESC');

                    if(!empty($chkusermember))
                    {*/
                        $userEmail = $chkuser[0]['email'];
                        $isactibe =  $chkuser[0]['status'];
                        $userId =  $chkuser[0]['StoreManagerId'];
                        $FirstName = $chkuser[0]['firstname'];

                    //}
                }
                else
                {
                    $adminmemberEmail = $receiver;
                    $whereCondition2['email'] = $adminmemberEmail;
                    $chkusermember = $this->getAllData('StoreManager',$whereCondition2,'StoreManagerId','DESC');

                    if(!empty($chkusermember))
                    {
                        $userEmail = $chkusermember[0]['email'];
                        $isactibe =  $chkusermember[0]['status'];
                        $userId =  $chkusermember[0]['StoreManagerId'];
                        $FirstName = $chkusermember[0]['firstname'];

                    }   
                }

                if($userEmail != ''){
                    
                        if($isactibe !== '1')
                        {
                                $currentTime = date('Y-m-d H:i:s');
                                $currentTime_str = strtotime($currentTime);

                                $alphabet = time()."abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
                                $pass = array(); //remember to declare $pass as an array
                                $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                                for ($i = 0; $i < 5; $i++) {
                                    $n = rand(0, $alphaLength);
                                    $pass[] = $alphabet[$n];
                                }
                                $userToken = implode($pass); //turn the array into a string
                                
                                $updatearray['reset_token']= $userToken;
                                $updatearray['request_time']= $currentTime_str;
                                $whereConditionUpdate['email']= $userEmail;
                                $mem_update =$this->updateDatafunc($updatearray,'StoreManager',$whereConditionUpdate);

                                //*****************************mail send start************************//
                $whereCondition11['id'] = '2';
                $whereCondition11['status'] = 'Y';                
                $email_template = $this->getAllData('email_template',$whereCondition11,'id','DESC');

                $whereCondition12['id'] = '1';
                $whereCondition12['status'] = '1';                
                $sitesettings = $this->getAllData('site_settings',$whereCondition12,'id','DESC');
                // echo '<pre>';
                // print_r($sitesettings);
                // die;
                 $objEmail = new \stdClass();
                    $objEmail->sender = "Aliasmae";
                    $objEmail->receiver_email = $userEmail;
                    $objEmail->subject='Retreive Password';
                               

                $urlTo = $sitesettings[0]['website_url'];
                $logo_url = $urlTo."/images/logo-blk.png";
                $logo='<a href="javascript:void(0)" class="navbar-brand">
                                <svg viewBox="0 0 24 24" height="28" width="28" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 0h24v24H0z" fill="none"/>
                                    <path d="M19.51 3.08L3.08 19.51c.09.34.27.65.51.9.25.24.56.42.9.51L20.93 4.49c-.19-.69-.73-1.23-1.42-1.41zM11.88 3L3 11.88v2.83L14.71 3h-2.83zM5 3c-1.1 0-2 .9-2 2v2l4-4H5zm14 18c.55 0 1.05-.22 1.41-.59.37-.36.59-.86.59-1.41v-2l-4 4h2zm-9.71 0h2.83L21 12.12V9.29L9.29 21z" fill="#fff" class="fill-theme"/>
                                </svg>
                                
                                <h3><span class="hidden-folded d-inline">Aliasmae</span></h3>
                            </a>';
                
                $link = $urlTo.'oms/reset-password/'.base64_encode($userId).'/'.$userToken;
                $email_details = array("[LINK]","[LOGO]","[BASEURL]","[RECEIVER]","[PASSWORD]","[USERNAME]","[SITENAME]","[SITEMAIL]","[SITEPHONE]");
                $email_details_rplc = array($link,$logo,$urlTo,ucfirst($FirstName),'','',$sitesettings[0]['site_name'],$sitesettings[0]['email'],'');
                $content=str_replace($email_details,$email_details_rplc,$email_template[0]['email_desc']);


                    // echo '<pre>';
                    // print_r($content);
                    // echo '</pre>';
                    // die;
                                $objEmail->email_content = $content;
                               
                                $mail_send=Mail::to($userEmail)->send(new sendMyMail($objEmail));

                               // echo $mail_send;

                                $returnArr['Status'] = '1';
                                return $this->sendResponse('Success! Reset Email Send to - '.$userEmail,$returnArr);
                            }
                            else{
                                return $this->sendError('Sorry! You are not a active User !',array());
                            }

                }else{
                    return $this->sendError('Sorry! Invalid username or mail!',array());
                }
                

        }
        else if($mode == 'ChangePasswordAdmin')
        {
            $adminuserID = trim($request->post('userId'));
            $password = trim($request->post('password'));
            if( $password != '' &&  $adminuserID !='')
            {
                $reqId = base64_decode($adminuserID);
                //if($reqId == '1'){
                //$whereConditionadminupdate1['StoreManagerId'] = $reqId;

                //before update password we need to check the token and time
                $whereCondition100['StoreManagerId'] = $reqId;
                //$whereCondition100['reset_token'] = '';
               $ChekToken = $this->getAllData('StoreManager',$whereCondition100,'StoreManagerId','DESC');
               
               // echo $ChekToken;
               // die;
               if(!empty($ChekToken) && $ChekToken[0]['reset_token']!='')
               {
                    $updatearray['password']= base64_encode($password);
                    $updatearray['reset_token']= '';
                    $updatearray['request_time']= '';
                  $res =$this->updateDatafunc($updatearray,'StoreManager',$whereCondition100);
                  //update member table 
                 /* $updatearray1['reset_token']= '';
                  $updatearray1['request_time']= '';
                  $whereConditionUpdate12['MemberID'] = $reqId;
                  $mem_update =$this->updateDatafunc($updatearray1,'members',$whereConditionUpdate12);*/


                  $returnArr['status'] ='1';
                  return $this->sendResponse('Success! password changed. Please Login to continue.',$returnArr);
                }
                else
                {
                    return $this->sendError('Sorry! Invalid Token',array());       
                }
            }
            else
            {
               return $this->sendError('Sorry! Wrong Input!',array());
            }

        }
        else if($mode == 'updatePassword')
        {
            $username = trim($request->post('username'));
            $password = trim($request->post('password'));
            $userId = trim($request->post('userId'));
            if($userId != ''){
            if($username != '' && $password != '')
            {
                $whereCondition['StoreManagerId'] = $userId;


                $updatearray['UserName']=$username;
                $updatearray['password']= base64_encode($password);
                  $res =$this->updateDatafunc($updatearray,'StoreManager',$whereCondition);
                  $returnArr['status'] ='1';
                  return $this->sendResponse('Success! password and username hass been changed',$returnArr);
            }
            else
            {
               return $this->sendError('Sorry! Wrong Input!',array());
            }
        }else{
            return $this->sendError('Sorry! Please provide userId!',array());
        }

        }else if($mode == 'getuserdetails')
        {
            $userid = trim($request->post('userId'));

            if($userid !='')
            {

                $whereCondition['StoreManagerId'] = $userid;

                $UserDetails = $this->getAllData('StoreManager',$whereCondition,'StoreManagerId','DESC');

                if(!empty($UserDetails))
                {

                    $returnArr['userDetails'] = $UserDetails;
                    $userStoreId=$UserDetails[0]['AccessStore'];
                    if($userStoreId != '')
                    {
                        $storeId=explode(',', $userStoreId);
                        if(!empty($storeId)) 
                        {
                            $whereConditionStore=array('storeid' => $storeId[0]);
                            $StoreDetails = $this->getAllData('Store',$whereConditionStore,'storeid','DESC');
                            if(!empty($StoreDetails))
                            {
                                $returnArr['storeLocation'] = $StoreDetails[0]['StoreLocation'];
                                $countryId= $StoreDetails[0]['CountryId'];
                                if($countryId != '')
                                {
                                    $whereConditionCountry=array('CountryID' => $countryId);
                                    $CountryDetails = $this->getAllData('CountryDetails',$whereConditionCountry,'CountryID','DESC');
                                    if(!empty($CountryDetails))
                                    {
                                        $CountryName=$CountryDetails[0]['CountryName'];
                                        $returnArr['CountryName'] = $CountryName;
                                    }
                                    else
                                    {
                                        $returnArr['CountryName'] = '';
                                    }
                                }
                                else
                                {
                                    $returnArr['CountryName'] = '';
                                }
                            }
                            else
                            {
                                $returnArr['storeLocation'] ='';
                                $returnArr['CountryName'] = '';
                            }
                        }
                        else
                        {
                            $returnArr['storeLocation'] ='';
                            $returnArr['CountryName'] = '';
                        }
                    }
                    else{
                        $storeId=array(); 
                        $returnArr['storeLocation'] ='';
                        $returnArr['CountryName'] = '';
                    }
                    return $this->sendResponse('Success! user found',$returnArr);
                }
                else
                {
                    return $this->sendError('Sorry! User not found',array());
                }
            }
            else
            {
                return $this->sendError('Sorry! Please provide userId',array());
            }



        }
        else if($mode == 'shipping_options')
        {
            // Shipping
            $userId = trim($request->post('userId'));
            $fspnzealand = trim($request->post('Flat-Shipping-Price-New-Zealand'));
            $fspUS = trim($request->post('Flat-Shipping-Price-Us'));
            $fspUK = trim($request->post('Flat-Shipping-Price-Uk'));
            $fspSINGAPORE = trim($request->post('Flat-Shipping-Price-Singapore'));
            $fspuCANADA = trim($request->post('Flat-Shipping-Price-Canada'));
            $fspuINTERNATIONAL = trim($request->post('Flat-Shipping-Price-International'));
            
            // Flat Shipping Australia
            $fspAUS = trim($request->post('Flat-Shipping-Price-Australia'));
            $AifspuAUS = trim($request->post('Additional-Item-Flat-Shipping-Price-Australia'));

            // Flat Shipping on Total purchase value
            $MOA = trim($request->post('Max-Order-Amount'));
            $SA = trim($request->post('Shipping-Amount'));
            // Shipping Discount Australia

                // Min Order Amount
                $MIAone = trim($request->post('Min-Order-Amount-one'));
                $MIAtwo = trim($request->post('Min-Order-Amount-two'));
                $MIAthree = trim($request->post('Min-Order-Amount-three'));
                // Shipping Discount
                $SDone = trim($request->post('Shipping-Discount-one'));
                $SDtwo = trim($request->post('Shipping-Discount-two'));
                $SDthree = trim($request->post('Shipping-Discount-three'));

            if($userId != ''){
            
                if($fspnzealand != '' || $fspUS != '' || $fspUK != '' || $fspSINGAPORE != '' || $fspuCANADA != '' || $fspuINTERNATIONAL != '' || $fspAUS != '' || $AifspuAUS != '' || $MOA != '' || $SA != '' || $MIAone != '' || $MIAtwo != '' || $MIAthree != '' || $SDone != '' || $SDtwo != '' || $SDthree != '')
                {
                    $whereCondition['CompanySettingID'] = $userId;

                    if($fspnzealand != ''){
                        $updatearray['FlatShippingChargeNZ']= $fspnzealand;    
                    }
                    
                if($fspUS != ''){
                    $updatearray['FlatShippingChargeUS']= $fspUS;
                }
                if($fspUK != ''){
                    $updatearray['FlatShippingChargeUK']= $fspUK;
                }
                if($fspSINGAPORE !=''){
                    $updatearray['FlatShippingChargeSG']= $fspSINGAPORE;
                }
                if($fspuCANADA != ''){
                    $updatearray['FlatShippingChargeCN']= $fspuCANADA;
                }
                if($fspuINTERNATIONAL != ''){
                    $updatearray['FlatShippingChargeInt']= $fspuINTERNATIONAL;
                }
                if($fspAUS !=''){

                    $updatearray['FlatShippingCharge']= $fspAUS;
                }
                if($AifspuAUS != ''){
                    $updatearray['IncrementalShippingCharge']= $AifspuAUS;
                }

                if($MOA != ''){
                    $updatearray['MaxOrderAmountShippingTotal']= $MOA;
                }
                if( $SA != ''){
                    $updatearray['FlatShippingChargeTotal']= $SA;
                }
                if($MIAone != ''){
                    $updatearray['MinOrderAmountShippingDiscount1']= $MIAone;
                }
                if($MIAtwo != ''){
                    $updatearray['MinOrderAmountShippingDiscount2']= $MIAtwo;
                }
                if($MIAthree != ''){
                    $updatearray['MinOrderAmountShippingDiscount3']= $MIAthree;
                }
                if($SDone != ''){
                    $updatearray['MinOrderPercentShippingDiscount1']= $SDone;
                }
                if($SDtwo != ''){
                    $updatearray['MinOrderPercentShippingDiscount2']= $SDtwo;
                }
                if($SDthree != ''){
                    $updatearray['MinOrderPercentShippingDiscount3']= $SDthree;
                }


                      $res =$this->updateDatafunc($updatearray,'company_settings',$whereCondition);
                      $returnArr['status'] ='1';
                      return $this->sendResponse('Success! Manage Shipping Options successfully updated',$returnArr);
                }
                else
                {
                   return $this->sendError('Sorry! Wrong Input!',array());
                }
            }else{
                return $this->sendError('Sorry! Please provide userId!',array());
            }

        }
        else if($mode == 'discount_options')
        {
            // Pair Discount
            $userId = trim($request->post('userId'));
            $EnablePairDiscount         = trim($request->post('Activate'));
            $NumberOfPairsToPurchase    = trim($request->post('Minimum-Number-of-Pairs-To-Purchase'));
            $DiscountForCheapestPair    = trim($request->post('Discount-Applied-on-the-Cheapest-Pair'));

            if($userId != ''){

                    if($EnablePairDiscount != '' || $NumberOfPairsToPurchase != '' || $DiscountForCheapestPair != '')
                    {
                        $whereCondition['CompanySettingID'] = $userId;

                        if($EnablePairDiscount != ''){
                            $updatearray['EnablePairDiscount']= $EnablePairDiscount;    
                        }
                        if($NumberOfPairsToPurchase != '')
                        {
                            $updatearray['NumberOfPairsToPurchase']= $NumberOfPairsToPurchase;
                        }
                        if($DiscountForCheapestPair != '')         
                        {
                            $updatearray['DiscountForCheapestPair']= $DiscountForCheapestPair;    
                        }
                                  

                        $res =$this->updateDatafunc($updatearray,'company_settings',$whereCondition);
                        $returnArr['status'] ='1';
                        return $this->sendResponse('Success! Manage Pair Discount successfully updated',$returnArr);
                    }
                    else{
                        return $this->sendError('Sorry! Wrong Input!',array());
                    }
            }
            else{
                    return $this->sendError('Sorry! Please provide userId!',array());
                }
             

        }
        else if($mode == 'order_sale_report_email')
        {
            $ordersaleemailid         = trim($request->post('ordersaleemailid'));
            $userId = trim($request->post('userId'));
            if($userId != ''){

                        if($ordersaleemailid != ''){
                                $whereCondition['CompanySettingID'] = '1';
                                $updatearray['ordersaleemailid']= $ordersaleemailid;
                                $res =$this->updateDatafunc($updatearray,'company_settings',$whereCondition);
                                $returnArr['status'] ='1';
                                return $this->sendResponse('Success! Order Sales successfully updated',$returnArr);
                        }else{
                            return $this->sendError('Sorry! Wrong Input!',array());
                        }
                    }
                    else{
                            return $this->sendError('Sorry! Please provide userId!',array());
                        }
            
        }
        else if($mode == 'totalhitcount')
        {
            $userId = trim($request->post('userId'));//userId
            $whereCondition['StoreManagerID'] =  $userId ;
            $allhitcountData = $this->getAllData('StoreManager',$whereCondition,'StoreManagerID','DESC');

            // echo '<pre>';
            // print_r($allhitcountData);
            // echo '</pre>';die;
             if(!empty($allhitcountData))
                {
                    $returnArr['totalhitcount'] =  $allhitcountData[0]['TotalHits'];
                    return $this->sendResponse('Success! data found',$returnArr);
                }
                else{
                    //$returnArr['totalhitcount'] = '';
                    return $this->sendError('Sorry! Wrong Input!',array());     
                }

        }





       
    }
    
    
    
    
    
    
}
?>