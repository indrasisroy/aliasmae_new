<?php
    namespace App\Http\Controllers\rest_controller;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use App\Http\Controllers\rest_controller\MyController as APIBaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Contracts\Filesystem\Filesystem;
    use Illuminate\Pagination\Paginator;  /*for pagination*/
    use Illuminate\Http\Request;
    use App\Mail\sendMyMail;  /*send mail class*/
    use Illuminate\Support\Facades\Mail;  /*for sending mail*/
    use Illuminate\Support\Facades\DB;
    use Image;
    
    class Trend extends APIBaseController{
       public function __construct(){
        
        }
        public function index(Request $request)
        {
            $userId = $request->post('userId');
            $mode = $request->post('mode');
            //return  $userId;die;
             if(!empty($userId) && $userId != 0){
                    if($mode == 'fetch_trends_data'){

                                $lastRow=(int)$request->post('lastRow');
                                $flag= trim($request->post('flag'));  
                                $trendid=trim($request->post('trendid'));
                                $storeid=trim($request->post('storeId'));
                                $lastlimit=10;
                                
                                if($lastRow == '')
                                {
                                    $lastRow = 0;
                                }
                
                                $whereCondition=array();
                                
                                /*if($trendid !='')
                                {
                                    $whereCondition['TrendID']=$trendid;
                                }*/
                                if($trendid !='' && $storeid === '')
                                {
                                    $whereCondition['TrendID']=$trendid;
                                    $trend = $this->getAllData('trends',$whereCondition,'TrendID','DESC',$lastlimit,$lastRow);
                                    $alltrend = $this->getAllData('trends',$whereCondition,'TrendID','DESC');
                                }
                                elseif ($storeid != '')
                                {
                                    if($trendid !='')
                                    {
                                        $whereCondition['TrendID']=$trendid;
                                    }
                                    else
                                    {
                                        $whereCondition=array();  
                                    }
                                    $trend = $this->getAllFindInSetData('trends',$whereCondition,$storeid,'Store','TrendID','DESC',$lastlimit,$lastRow);
                                    $alltrend = $this->getAllFindInSetData('trends',$whereCondition,$storeid,'Store','TrendID','DESC');
                                }
                                else
                                {
                                    $trend = $this->getAllData('trends',$whereCondition,'TrendID','DESC',$lastlimit,$lastRow);
                                    $alltrend = $this->getAllData('trends',$whereCondition,'TrendID','DESC');
                                }
                                /*$banner = $this->getAllData('trends',$whereCondition,'TrendID','DESC',$lastlimit,$lastRow);
                                $allbanner = $this->getAllData('trends',$whereCondition,'TrendID','DESC');*/
                
                                $finalarray=array();
                                if($flag == '1')
                                {
                                    $finalarray=$alltrend;
                                }
                                else
                                {
                                    $finalarray=$trend;
                                }
                
               // echo "<pre>";
                //print_r($trend);
                //die;                
                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['trend'][$rowsCount]['TrendID']                  = (string)$rows['TrendID'];
                            $returnArr['trend'][$rowsCount]['TrendName']                = $rows['TrendName'];
                            $returnArr['trend'][$rowsCount]['Image1SavedFileName']      = $rows['Image1SavedFileName'];
                            $returnArr['trend'][$rowsCount]['BannerImageText']          = $rows['BannerImageText'];
                            $returnArr['trend'][$rowsCount]['PageTitle']                =$rows['PageTitle'];
                            $returnArr['trend'][$rowsCount]['PageKeywords']             = $rows['PageKeywords'];
                            $returnArr['trend'][$rowsCount]['PageDescription']          = $rows['PageDescription'];
                            $returnArr['trend'][$rowsCount]['IsActive']                 = $rows['IsActive'];
                            $returnArr['trend'][$rowsCount]['DisplayOrder']             = $rows['DisplayOrder'];                            
                            $returnArr['trend'][$rowsCount]['InsertionDate']            = $rows['InsertionDate'];
                            $returnArr['trend'][$rowsCount]['Store']                    = $rows['Store'];
                            
                           
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']          = 0;
                    $returnArr['pagination']['totaldata']        =count($finalarray);
                    $totalpage                                   =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']        =$totalpage;
                    
                    
                    if($flag == '1')
                    {
                        $lastRow = $lastRow + $lastlimit;
                        $banner_pegi = $this->getAllData('trends',$whereCondition,'TrendID','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($banner_pegi))
                        {
                            $returnArr['pagination']['hasMore']     = 1;
                            $returnArr['pagination']['lastRow']     = $lastRow;
                            $returnArr['pagination']['lastLimit']   =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['trend'])){
                    
                    return $this->sendResponse('All trends listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('Sorry!No trends found!',array());
                    
                }

                }
                //============================= start trends value insert and update ==================================================================
                    else if($mode == 'addedittrend')
                    {

                        $trendid=$request->post('trendid'); // if trendid send then update , if it is blank then insert

                        //$trendid=$request->post('trendid');
                        $TrendName=$request->post('TrendName');
                       // $Image1SavedFileName=$request->post('Image1SavedFileName');
                        $BannerImageText=$request->post('BannerImageText');
                        $PageTitle=$request->post('PageTitle');
                        $PageKeywords=$request->post('PageKeywords');
                        $PageDescription=$request->post('PageDescription');
                        $IsActive=$request->post('IsActive');
                        $DisplayOrder=$request->post('DisplayOrder');
                        $login_device_type=$request->post('login_device_type'); //login device type (1: android, 2: IOS ,3 :web)
                        $store = $request->post('Store');  
                        //$InsertionDate=$request->post('InsertionDate');

                         $currentTime = date('Y-m-d H:i:s');
                         $InsertionDate = strtotime($currentTime);

                          if($login_device_type == 1 || $login_device_type == 2)
                                {
                                    $file=$request->file('image');
                                    
                                    if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
                                    {
                                        
                                        $Image1FileName = $file->getClientOriginalName();
                                        $fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
                                        list($width,$height) = getimagesize($file);
                                        //echo $width.'       '.$height;
                                       // $imagesfile= $file->getClientSize();
                                        $Image1FileSize = filesize($file);//$imagesfile;//round(($imagesfile/1024),2);
                                        $image_content = file_get_contents($file);
                                        $path = 'uploads/trend_images/';
                                        $thumb_path = 'uploads/trend_images/thumbnail/';
                                        $fileNormal = $path.$fileName;
                                        $fileThumb = $thumb_path.$fileName;
                                       
                                        $this->uploadImage('public',$fileNormal,$image_content);
                                        $this->uploadThumbnail('public',$fileThumb,$image_content,200,200);
                                        
                                        $Image1SavedFileName=$fileName;
                                       
                                    }
                                    else
                                    {
                                        $Image1SavedFileName='';
                                        $Image1FileName = '';
                                        $Image1FileSize='';
                                    }
                                }
                                else
                                {
                                    $Image1SavedFileName=$request->post('image'); //after upload new image name
                                    $Image1FileName=$request->post('Image1FileName'); // original image name
                                    $Image1FileSize=$request->post('Image1FileSize');
                                }


                        if($trendid == ''){
                                //insert
                                //return 'insert';
                                 $data_to_store=array(
                                        //'Image1SavedFileName'=> $Image1SavedFileName,
                                        'Image1FileName'=> $Image1FileName,
                                        'Image1FileSize'=> $Image1FileSize,
                                        'TrendName'=>$TrendName,
                                        'Image1SavedFileName'=>$Image1SavedFileName,
                                        'BannerImageText'=>$BannerImageText,
                                        'PageTitle'=>$PageTitle,
                                        'PageKeywords'=>$PageKeywords,
                                        'PageDescription'=>$PageDescription,                                        
                                        'IsActive'=>$IsActive,
                                        'DisplayOrder'=>$DisplayOrder,
                                        'InsertionStrtotime'=> $InsertionDate,
                                        'InsertionDate'=> $currentTime,
                                        'Store'=> $store
                                         );

                                        $addDetls = $this->insertDatafunc($data_to_store,'trends');
                                        if(!empty($addDetls)){
                                                    $msg = 'Thank you! Trend added';
                                                    $returnArr = array();
                                                    return $this->sendResponse($msg,$returnArr);
                                                }else{
                                                    $msg = 'Sorry!Please try after sometime!';
                                                    $returnArr = array();
                                                    return $this->sendError($msg,$returnArr);
                                                }

                                

                            }else{
                                //update
                              // return 'update';
                                if($Image1SavedFileName != '' || $Image1FileName != '' || $Image1FileSize !=''){
                                        $data_to_store=array(
                                            //'Image1SavedFileName'=> $Image1SavedFileName,
                                            'Image1FileName'=> $Image1FileName,
                                            'Image1FileSize'=> $Image1FileSize,
                                            'TrendName'=>$TrendName,
                                            'Image1SavedFileName'=>$Image1SavedFileName,
                                            'BannerImageText'=>$BannerImageText,
                                            'PageTitle'=>$PageTitle,
                                            'PageKeywords'=>$PageKeywords,
                                            'PageDescription'=>$PageDescription,                                        
                                            'IsActive'=>$IsActive,
                                            'DisplayOrder'=>$DisplayOrder,
                                            'InsertionStrtotime'=> $InsertionDate,
                                            'InsertionDate'=> $currentTime,
                                            'Store'=> $store
                                             );
                                }
                                else{
                                            $data_to_store=array(
                                                'TrendName'=>$TrendName,
                                                'BannerImageText'=>$BannerImageText,
                                                'PageTitle'=>$PageTitle,
                                                'PageKeywords'=>$PageKeywords,
                                                'PageDescription'=>$PageDescription,    
                                                'IsActive'=>$IsActive,
                                                'DisplayOrder'=>$DisplayOrder,
                                                'InsertionStrtotime'=> $InsertionDate,
                                                'InsertionDate'=> $currentTime,
                                                'Store'=> $store
                                                 );
                                }
                                 $whereArr=array('TrendID'=>$trendid);
                                     $updateDetails=$this->updateDatafunc($data_to_store,'trends',$whereArr);
                                     if(!empty($updateDetails)){
                                            $msg = 'Thank you! Trends Updated';
                                            $returnArr = array();
                                            return $this->sendResponse($msg,$returnArr);
                                        }else{
                                            $msg = 'Sorry!Please try after sometime!';
                                            $returnArr = array();
                                            return $this->sendError($msg,$returnArr);
                                        }
                            }

                    }
                    //=================================================== fetch trends product start================================================
                     else if($mode == 'fetch_trend_product')
                    {
                        $trendid=trim($request->post('trendid'));
                        if($trendid != ''){
                                $lastRow=(int)$request->post('lastRow');
                                $flag= trim($request->post('flag')); 
                                $lastlimit=10;
                
                                if($lastRow == '')
                                {
                                    $lastRow = 0;
                                }
                                
                                $whereCondition=array();
                                
                                if($trendid !='')
                                {
                                    $whereCondition['TrendID']=$trendid;
                                }

                                $trendproduct = $this->getAllData('trend_products',$whereCondition,'TrendProductID','DESC',$lastlimit,$lastRow);
                                $alltrendproduct = $this->getAllData('trend_products',$whereCondition,'TrendProductID','DESC');
                                
                                $finalarray=array();
                                if($flag == '1')
                                {
                                    $finalarray=$alltrendproduct;
                                }
                                else
                                {
                                    $finalarray=$trendproduct;
                                }
                                 if(!empty($finalarray))
                                    {
                                        $rowsCount = 0;

                                        foreach($finalarray as $rows)
                                            {
                                                    $returnArr['trendproduct'][$rowsCount]['TrendProductID']            = (string)$rows['TrendProductID'];
                                                    $returnArr['trendproduct'][$rowsCount]['ProductID']                 = $rows['ProductID'];
                                                    $returnArr['trendproduct'][$rowsCount]['ProductAttributeID']        = $rows['ProductAttributeID'];
                                                    

                                                   
                                                    $rowsCount++;
                                            }
                                            
                                            $returnArr['pagination']['hasMore']         = 0;
                                            $returnArr['pagination']['totaldata']       =count($finalarray);
                                            $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                                            $returnArr['pagination']['totalpage']       =$totalpage;
                                            
                                            
                                            if($flag == '1')
                                            {
                                                $lastRow        = $lastRow + $lastlimit;
                                                $banner_pegi    = $this->getAllData('trend_products',$whereCondition,'TrendProductID','DESC',$lastlimit,$lastRow);
                                                
                                                if(!empty($banner_pegi))
                                                {
                                                    $returnArr['pagination']['hasMore']      = 1;
                                                    $returnArr['pagination']['lastRow']      = $lastRow;
                                                    $returnArr['pagination']['lastLimit']    =$lastlimit;
                                                }
                                                
                                            }





                                    }
                                    else
                                    {
                                        $returnArr=array();
                                    }

                                            if(!empty($finalarray) && !empty($returnArr['trendproduct'])){
                    
                                                return $this->sendResponse('All trend product listing!',$returnArr);
                                               
                                            }
                                            else
                                            {
                                                return $this->sendError('Sorry!No trend product found!',array());
                                                
                                            }

                        }else{
                            return $this->sendError('Sorry! Trend Id blank',array());
                        }



                    }
                    //=================================================== fetch trends product end================================================

             }
             else{
                return $this->sendError('Sorry! userid blank',array());
             }

        }
        
    }
?>