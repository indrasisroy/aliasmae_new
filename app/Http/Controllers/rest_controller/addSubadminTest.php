<?php

namespace App\Mail;

//use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class addSubadminTest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    //protected $price;
    //protected $ordername;
    protected $chkevent;
    
    public function __construct($event)
    {
        $this->chkevent = $event;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        // echo 'here in subadmin test '.$this->chkevent->receiver;die;
       $subject="Test mail laravel 5.6";



        // return $this->subject($subject2)
        //         ->from('xy@x.de')
        //         ->view('email.contact');
       if(isset($this->chkevent->sender))
       {
        $subject="Test mail laravel 5.6 check";
        
        return $this->view('emails.subadmintestemail')
                    ->with([
                        'demo1' =>  $this->chkevent->demo_one,
                        'demo2' => $this->chkevent->demo_two,
                        'sender'=>'kolkata',
                        'receiver'=>$this->chkevent->receiver,
                        'image'=>$this->chkevent->file,
                    ])
                    ->attach($this->chkevent->file)
                    ->attach($this->chkevent->document, [
                        'as' => 'mydocument.docx',
                        ])
                    ->from('debapriya.dutta@esolzmail.com', $this->chkevent->sender)
                    ->subject($subject);
       }
       else
       {
        
       return $this->view('emails.subadmintestemail')
                    ->with([
                        'demo1' =>  $this->chkevent->demo_one,
                        'demo2' => $this->chkevent->demo_two,
                        'sender'=>'kolkata',
                        'receiver'=>$this->chkevent->receiver,
                        'image'=>$this->chkevent->file,
                    ])
                    ->attach($this->chkevent->file)
                    ->attach($this->chkevent->document, [
                        'as' => 'mydocument.docx',
                        ])
                        // ->from('indrasis.roy@esolzmail.com', 'Indrasis Roy')
                    ->subject($subject);
       }
                
    }
}
