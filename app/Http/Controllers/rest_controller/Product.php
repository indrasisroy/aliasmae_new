<?php
    namespace App\Http\Controllers\rest_controller;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use App\Http\Controllers\rest_controller\MyController as APIBaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Contracts\Filesystem\Filesystem;
    use Illuminate\Pagination\Paginator;  /*for pagination*/
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Image;
    
    class Product extends APIBaseController{
       public function __construct(){
        
        }
        //public function index(Request $request){
        //    $mode = $request->post('mode');
        //    $userId = $request->post('userId');
        //    $UDID = $request->post('UDID');
        //    
        //}
        
        //==========function for  customer==========//
        public function index(Request $request)
        {
            $userId = $request->post('userid');
            $UDID = $request->post('UDID');
            $mode = $request->post('mode');
            $returnArr=array();
            //=====================insert or update customer details=========================//
            if($mode == 'get_product_list'){
                
                if(!empty($userId) && $userId != 0){
                    $productDetls = $this->getAllDocument('Products');
                    
                    $userDetails=$this->getAllData('users',array('id'=>1)); 
                    
                    if(!empty($productDetls))
                    {
                        $product_counter=0;
                        foreach($productDetls as $allProduct)
                        {
                            $returnArr['product_list'][$product_counter]['addedBy']=$allProduct['addedBy'];
                            $returnArr['product_list'][$product_counter]['categoryId']=$allProduct['categoryId'];
                            $returnArr['product_list'][$product_counter]['subCategoryId']=$allProduct['subCategoryId'];
                            $returnArr['product_list'][$product_counter]['productName']=$allProduct['productName'];
                            $returnArr['product_list'][$product_counter]['productDesc']=$allProduct['productDesc'];
                            $returnArr['product_list'][$product_counter]['productPrice']=$allProduct['productPrice'];
                            $product_counter++;
                        }
                    }
                    
                    if(!empty($userDetails))
                    {
                        $returnArr['user_details']['addedBy']=$userDetails[0]['id'];
                        $returnArr['user_details']['categoryId']=$userDetails[0]['firstName'];
                        $returnArr['user_details']['subCategoryId']=$userDetails[0]['lastName'];
                    }
                    
                    if(!empty($returnArr['product_list'] || !empty($userDetails))){
                        $msg = 'Product details';
                        return $this->sendResponse($msg,$returnArr);
                    }else{
                        $msg = 'No data';
                        $returnArr = array();
                        return $this->sendError($msg,$returnArr);
                    }
                    
                 
                }
                else
                {
                    $msg = 'Sorry! userid is required!';
                    $returnArr = array();
                    return $this->sendError($msg,$returnArr);
                    
                }
                
                        
            }
            
            //===============================get customer list and customer detials=================================//
            
        }
        
    }
?>