<?php
    namespace App\Http\Controllers\rest_controller;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use App\Http\Controllers\rest_controller\MyController as APIBaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Contracts\Filesystem\Filesystem;
    use Illuminate\Pagination\Paginator;  /*for pagination*/
    use Illuminate\Http\Request;
    use App\Mail\sendMyMail;  /*send mail class*/
    use Illuminate\Support\Facades\Mail;  /*for sending mail*/
    use Illuminate\Support\Facades\DB;
    use Image;
    
    class Category extends APIBaseController{
       public function __construct(){
        
        }
        //public function index(Request $request){
        //    $mode = $request->post('mode');
        //    $userId = $request->post('userId');
        //    $UDID = $request->post('UDID');
        //    
        //}
        
        //==========function for  category==========//
        public function index(Request $request)
        {
            $userId = $request->post('userId');
            //$UDID = $request->post('UDID');
            $mode = $request->post('mode');
            
            //=====================insert or update Category details=========================//
            if($mode == 'insert_category_details'){
                
                $categoryid=$request->post('categoryid'); // if categoryid send then update , if it is blank then insert
                
                if(!empty($userId) && $userId != 0)
                {
                    
                    $categoryName      =    trim(strip_tags($request->post('categoryName')));
                    $urlName           =    trim(strip_tags($request->post('urlName')));
                    $pageTitle         =    trim(strip_tags($request->post('pageTitle')));
                    $pageKeyword       =    trim(strip_tags($request->post('pageKeyword')));
                    $login_device_type =    $request->post('login_device_type'); //login device type (1: android, 2: IOS ,3 :web)
                    $IsActive          =    $request->post('IsActive');
                    $DisplayOrder      =    $request->post('DisplayOrder');                 
                    $store             =    $request->post('Store');                 
                    $currentTime       =    date('Y-m-d H:i:s');
                    $InsertionDate     =    strtotime($currentTime);
                    
                    $pageDesc          =    trim(strip_tags($request->post('pageDesc')));
                    $categoryDesc      =    trim(strip_tags($request->post('categoryDesc')));
                    $bannerText        =    trim(strip_tags($request->post('bannerText')));
                    $discount          =    $request->post('discount');
                    $salecategory      =    $request->post('salecategory');
                    
                    $productTypeId1     =    $request->post('productTypeId');

                    $productTypeId    =    isset($productTypeId1) ? $productTypeId1 : '';

                    //echo $productTypeId1; die;

                    //$file=$request->file('image');
                    if($login_device_type == 1 || $login_device_type == 2)
                    {
                        $file=$request->file('image');
                        
                        if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
                        {
                            
                            $Image1FileName = $file->getClientOriginalName();
                            $fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
                            list($width,$height) = getimagesize($file);
                            //echo $width.'       '.$height;
                           // $imagesfile= $file->getClientSize();
                            $Image1FileSize = filesize($file);//$imagesfile;//round(($imagesfile/1024),2);
                            $image_content = file_get_contents($file);
                            $path = 'uploads/category_images/';
                            $thumb_path = 'uploads/category_images/thumbnail/';
                            $fileNormal = $path.$fileName;
                            $fileThumb = $thumb_path.$fileName;
                           
                            $this->uploadImage('public',$fileNormal,$image_content);
                            $this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
                            
                            $bannerImage=$fileName;
                           
                        }
                        else
                        {
                            $bannerImage='';
                            $Image1FileName = '';
                            $Image1FileSize='';
                        }
                    }
                    else
                    {
                        $bannerImage=$request->post('bannerImage'); //after upload new image name
                        //$Image1FileName=$request->post('Image1FileName'); // original image name
                        //$Image1FileSize=$request->post('Image1FileSize');
                    }
                                
                    
                   
                    //echo $promocodeid;die();
                    if($categoryid == '')
                    {
                        if($categoryName!='')
                        {

                            $data_to_check['CategoryName']         = $categoryName;
                            $CheckCategory = $this->getAllData('categories',$data_to_check,'CategoryID','DESC');
                        }
                        else
                        {
                            $CheckCategory =array('CategoryName' => $categoryName);
                        }
                        if(empty($CheckCategory))
                        {
                        //insert//return 'insert';                
                            $data_to_store=array(
                                'CategoryName'=>$categoryName,
                                'Description'=>$categoryDesc,
                                'PageTitle'=>$pageTitle,
                                'PageKeywords'=>$pageKeyword,
                                'PageDescription'=>$pageDesc,
                                'URLFriendlyName'=>$urlName,
                                'BannerImage'=>$bannerImage,
                                'BannerImageText' => $bannerText,
                                'DisplayCategory'=>$IsActive,
                                'CategoryLevel'=>$DisplayOrder,
                                'salecategory'=> $salecategory,
                                'Store'=> $store,
                                'productTypeId'=> $productTypeId,
                                'CategoryDiscount'=>$discount                                         
                                 );
                             // echo '<pre>';
                             // print_r($data_to_store);
                             // die;
                            $addCategoryDetls = $this->insertDatafunc($data_to_store,'categories');
                            if(!empty($addCategoryDetls))
                            {
                                $msg = 'Thank you! Category added';
                                $returnArr = array();
                                return $this->sendResponse($msg,$returnArr);
                            }
                            else
                            {
                                $msg = 'Please try after sometime!';
                                $returnArr = array();
                                return $this->sendError($msg,$returnArr);
                            }
                        }
                        else
                        {
                            $msg = 'Duplicate Category Name!';
                            $returnArr = array();
                            return $this->sendError($msg,$returnArr);
                        }

                    }
                    else
                    {
                        //check duplicate category
                        $whereCheckCategory = [
                                            ['CategoryName', $categoryName],
                                            ['CategoryID', '<>', $categoryid]
                                        ];
                        $CategoryCheck = $this->getAllData('categories',$whereCheckCategory,'CategoryID','DESC');
                        if(empty($CategoryCheck))
                        { 
                            if($bannerImage != '')
                            {
                                $data_to_store=array(
                                    'CategoryName'=>$categoryName,
                                    'Description'=>$categoryDesc,
                                    'PageTitle'=>$pageTitle,
                                    'PageKeywords'=>$pageKeyword,
                                    'PageDescription'=>$pageDesc,
                                    'URLFriendlyName'=>$urlName,
                                    'BannerImage'=>$bannerImage,
                                    'BannerImageText' => $bannerText,
                                    'DisplayCategory'=>$IsActive,
                                    'CategoryLevel'=>$DisplayOrder,
                                    'salecategory'=> $salecategory,
                                    'Store'=> $store,
                                    'productTypeId'=> $productTypeId,
                                    'CategoryDiscount'=>$discount 
                                     );

                                //============================== unlink previous image ============================
                                    // $this->unlinkimage($bannerid);  
                                    // $id= $request;
                                    $ConditionCategory=array('CategoryID'=>$categoryid);
                                    $fetchcategory=$this->getAllData('categories',$ConditionCategory,'CategoryID','DESC');
                                    //echo "<pre>";print_r($fetchbanner);die();
                                    if(!empty($fetchcategory))
                                    {
                                        $previmage=$fetchcategory[0]['BannerImage'];
                                        
                                        if($previmage !='')
                                        {
                                            $normalpath=storage_path().'/app/'.'public/uploads/category_images/'.$previmage;
                                            $thumbnailpath=storage_path().'/app/'.'public/uploads/category_images/thumbnail/'.$previmage;
                                            
                                            if(file_exists($normalpath))
                                            {
                                                unlink(storage_path().'/app/'.'public/uploads/category_images/'.$previmage);
                                            }
                                            if(file_exists($thumbnailpath))
                                            {
                                                unlink(storage_path().'/app/'.'public/uploads/category_images/thumbnail/'.$previmage);
                                            }
                                        }
                                            
                                        
                                        
                                    }                                       
                                //============================== unlink previous image ============================
                            }
                            else
                            {
                                $data_to_store=array(
                                   'CategoryName'=>$categoryName,
                                    'Description'=>$categoryDesc,
                                    'PageTitle'=>$pageTitle,
                                    'PageKeywords'=>$pageKeyword,
                                    'PageDescription'=>$pageDesc,
                                    'URLFriendlyName'=>$urlName,
                                    //'BannerImage'=>$bannerImage,
                                    'BannerImageText' => $bannerText,
                                    'DisplayCategory'=>$IsActive,
                                    'CategoryLevel'=>$DisplayOrder,
                                    'salecategory'=> $salecategory,
                                    'Store'=> $store,
                                    'productTypeId'=> $productTypeId,
                                    'CategoryDiscount'=>$discount 
                                     );
                            }
                            $whereArr=array('CategoryID'=>$categoryid);
                            $updateDetails=$this->updateDatafunc($data_to_store,'categories',$whereArr);
                            if(!empty($updateDetails))
                            {
                                $msg = 'Thank you! Category Updated';
                                $returnArr = array();
                                return $this->sendResponse($msg,$returnArr);
                            }
                            else
                            {
                                $msg = 'No Changes made to update!';
                                $returnArr = array();
                                return $this->sendError($msg,$returnArr);
                            }
                        }
                        else
                        {
                            $msg = 'Duplicate data cannot be updated.';
                            $returnArr = array();
                            return $this->sendError($msg,$returnArr);
                        }
                    }
                
            //}
           
                }
                else
                {
                    $msg = 'Please Login First!';
                    $returnArr = array();
                    return $this->sendError($msg,$returnArr);
                    
                }
                
                        
            }
            
            //===============================get banner list and banner detials=================================//
            else if($mode == 'get_category_list')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $categoryid=trim($request->post('categoryid'));
                $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                //$whereCondition=array('Status' => '2');

                
                if($categoryid !='' && $storeid === '')
                {
                    $whereCondition['CategoryID']=$categoryid;
                    $category = $this->getAllData('categories',$whereCondition,'CategoryID','DESC',$lastlimit,$lastRow);
                    $allcategory = $this->getAllData('categories',$whereCondition,'CategoryID','DESC');
                }
                elseif ($storeid != '')
                {
                    if($categoryid !='')
                    {
                        $whereCondition['CategoryID']=$categoryid;
                    }
                    else
                    {
                        $whereCondition=array();  
                    }
                    $category = $this->getAllFindInSetData('categories',$whereCondition,$storeid,'Store','CategoryID','DESC',$lastlimit,$lastRow);
                    $allcategory = $this->getAllFindInSetData('categories',$whereCondition,$storeid,'Store','CategoryID','DESC');
                }
                else{
                    $category = $this->getAllData('categories',$whereCondition,'CategoryID','DESC',$lastlimit,$lastRow);
                    $allcategory = $this->getAllData('categories',$whereCondition,'CategoryID','DESC');
                }
                
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allcategory;
                }
                else
                {
                    $finalarray=$category;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;                
                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['category'][$rowsCount]['CategoryID']            = (string)$rows['CategoryID'];
                            $returnArr['category'][$rowsCount]['CategoryName']          = $rows['CategoryName'];
                            $returnArr['category'][$rowsCount]['Description']           = $rows['Description'];
                            $returnArr['category'][$rowsCount]['NumberHits']            = $rows['NumberHits'];
                            $returnArr['category'][$rowsCount]['CategoryLevel']         = $rows['CategoryLevel'];
                            $returnArr['category'][$rowsCount]['PageTitle']             = $rows['PageTitle'];
                            $returnArr['category'][$rowsCount]['PageKeywords']          = $rows['PageKeywords'];
                            $returnArr['category'][$rowsCount]['PageDescription']       = $rows['PageDescription'];
                            $returnArr['category'][$rowsCount]['MenuImage']             = $rows['MenuImage'];
                            $returnArr['category'][$rowsCount]['MenuRollOverImage']     = $rows['MenuRollOverImage'];
                            $returnArr['category'][$rowsCount]['URLFriendlyName']       = $rows['URLFriendlyName'];
                            $returnArr['category'][$rowsCount]['CategoryDiscount']      = $rows['CategoryDiscount'];
                            $returnArr['category'][$rowsCount]['Store']                 = $rows['Store'];

                            $returnArr['category'][$rowsCount]['BannerImage']           = $rows['BannerImage'];
                            $returnArr['category'][$rowsCount]['BannerImageText']       = $rows['BannerImageText'];
                            $returnArr['category'][$rowsCount]['salecategory']          = $rows['salecategory'];
                            $returnArr['category'][$rowsCount]['DisplayCategory']       = $rows['DisplayCategory'];
                            $returnArr['category'][$rowsCount]['productTypeId']         = $rows['productTypeId'];
                            $returnArr['category'][$rowsCount]['Status']                = $rows['Status'];


                            
                           
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $category_pegi    = $this->getAllData('categories',$whereCondition,'CategoryID','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($category_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['category'])){
                    
                    return $this->sendResponse('All category listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('Sorry!No category found!',array());
                    
                }
            }
            
            
        }
       
    }
?>