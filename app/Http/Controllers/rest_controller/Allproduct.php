<?php
    namespace App\Http\Controllers\rest_controller;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use App\Http\Controllers\rest_controller\MyController as APIBaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Contracts\Filesystem\Filesystem;
    use Illuminate\Pagination\Paginator;  /*for pagination*/
    use Illuminate\Http\Request;
    use App\Mail\sendMyMail;  /*send mail class*/
    use Illuminate\Support\Facades\Mail;  /*for sending mail*/
    use Illuminate\Support\Facades\DB;
    use Image;
    
    class Allproduct extends APIBaseController{
       public function __construct(){
        
        }
        //public function index(Request $request){
        //    $mode = $request->post('mode');
        //    $userId = $request->post('userId');
        //    $UDID = $request->post('UDID');
        //    
        //}
        
        //==========function for  article==========//
        public function index(Request $request)
        {
            $userId = $request->post('userId');
            //$UDID = $request->post('UDID');
            $mode = $request->post('mode');
            
            //=====================insert or update product details=========================//
            if($mode == 'insert_article_details') //product 
            {
                
                // $articleid=$request->post('articleid'); // if articleid send then update , if it is blank then insert
                // if(!empty($userId) && $userId != 0)
                // {
                    
                //     $ArticleName    =trim(strip_tags($request->post('ArticleName')));
                //     $ArticleContent =trim(strip_tags($request->post('ArticleContent')));
                //     $IsActive       =$request->post('IsActive');
                //     $IsFeatured     =$request->post('IsFeatured');                 
                //     $storeid = $request->post('Store');                 
                //     $currentTime = date('Y-m-d H:i:s');
                //     $InsertionDate = strtotime($currentTime);
                    
                //     $PageTitle=$request->post('PageTitle');
                //     $PageKeywords=$request->post('PageKeywords');
                //     $PageDescription=$request->post('PageDescription');
                
                //     //echo $promocodeid;die();
                //     if($articleid == '')
                //     {
                //                 //insert
                //                 //return 'insert';
                //         $data_to_store=array(
                //                     'ArticleName'=>$ArticleName,
                //                     'ArticleContent'=>$ArticleContent,
                //                     'IsActive'=>$IsActive,
                //                     'IsFeatured'=>$IsFeatured,
                //                     'InsertionDate'=> $InsertionDate,
                //                     'Store'=> $store,
                //                     'PageTitle'=>$PageTitle,
                //                     'PageKeywords'=>$PageKeywords,
                //                     'PageDescription'=>$PageDescription
                //                      );
                //              // echo '<pre>';
                //              // print_r($data_to_store);
                //              // die;
                //         $addArticleDetls = $this->insertDatafunc($data_to_store,'articles');
                //         if(!empty($addArticleDetls))
                //         {
                //             $msg = 'Thank you! Article added';
                //             $returnArr = array();
                //             return $this->sendResponse($msg,$returnArr);
                //         }
                //         else
                //         {
                //             $msg = 'Please try after sometime!';
                //             $returnArr = array();
                //             return $this->sendError($msg,$returnArr);
                //         }
                //     }
                //     else
                //     {
                //         //update
                //       // return 'update';
                //         $data_to_store=array(
                //                 'ArticleName'=>$ArticleName,
                //                 'ArticleContent'=>$ArticleContent,
                //                 'IsActive'=>$IsActive,
                //                 'IsFeatured'=>$IsFeatured,
                //                 'InsertionDate'=> $InsertionDate,
                //                 'Store'=> $store,
                //                 'PageTitle'=>$PageTitle,
                //                 'PageKeywords'=>$PageKeywords,
                //                 'PageDescription'=>$PageDescription
                //              );
                //         $whereArr=array('ArticleID'=>$articleid);
                //         $updateDetails=$this->updateDatafunc($data_to_store,'articles',$whereArr);
                //         if(!empty($updateDetails))
                //         {
                //             $msg = 'Thank you! Article Updated';
                //             $returnArr = array();
                //             return $this->sendResponse($msg,$returnArr);
                //         }
                //         else
                //         {
                //             $msg = 'No Changes made to update!';
                //             $returnArr = array();
                //             return $this->sendError($msg,$returnArr);
                //         }
                //     }
                        
                //     //}   
                // }
                // else
                // {
                //     $msg = ' Please provide valid details!';
                //     $returnArr = array();
                //     return $this->sendError($msg,$returnArr);
                    
                // }
                
                        
            }
            
            //===============================get article list and article detials=================================//
            else if($mode == 'get_product_list')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $productid=trim($request->post('productid'));
                $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                
                if($productid !='' && $storeid === '')
                {
                    $whereCondition['storeid']=$articleid;
                    $product = $this->getAllData('products',$whereCondition,'DisplayLevel','DESC',$lastlimit,$lastRow);
                    $allproduct = $this->getAllData('products',$whereCondition,'DisplayLevel','DESC');
                }
                elseif ($storeid != '')
                {
                    if($productid !='')
                    {
                        $whereCondition['ProductID']=$productid;
                    }
                    else
                    {
                        $whereCondition=array();  
                    }
                    $product = $this->getAllFindInSetData('products',$whereCondition,$storeid,'storeid','DisplayLevel','DESC',$lastlimit,$lastRow);
                    $allproduct = $this->getAllFindInSetData('products',$whereCondition,$storeid,'storeid','DisplayLevel','DESC');
                }
                else
                {
                    $product = $this->getAllData('products',$whereCondition,'DisplayLevel','DESC',$lastlimit,$lastRow);
                    $allproduct = $this->getAllData('products',$whereCondition,'DisplayLevel','DESC');
                }
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allproduct;
                }
                else
                {
                    $finalarray=$product;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['product'][$rowsCount]['ProductID']             = (string)$rows['ProductID'];//
                            $returnArr['product'][$rowsCount]['CategoryID']            = $rows['CategoryID'];//
                            $returnArr['product'][$rowsCount]['SecondaryCategoryID']   = $rows['SecondaryCategoryID'];//
                            $returnArr['product'][$rowsCount]['Name']                  = $rows['Name']; //
                            $returnArr['product'][$rowsCount]['FactoryReferenceID']    = $rows['FactoryReferenceID']; //
                            $returnArr['product'][$rowsCount]['PageTitle']             = $rows['PageTitle'];//
                            $returnArr['product'][$rowsCount]['PageDescription']       = $rows['PageDescription'];//
                            $returnArr['product'][$rowsCount]['PageKeywords']          = $rows['PageKeywords'];//
                            $returnArr['product'][$rowsCount]['RetailPrice']           = $rows['RetailPrice']; //
                            $returnArr['product'][$rowsCount]['DiscountedRetailPrice'] = $rows['DiscountedRetailPrice'];//
                            $returnArr['product'][$rowsCount]['CostPrice']             = $rows['CostPrice'];//
                            $returnArr['product'][$rowsCount]['Code']                  = $rows['Code'];
                            $returnArr['product'][$rowsCount]['Weight']                = $rows['Weight'];
                            $returnArr['product'][$rowsCount]['Height']                = $rows['Height'];
                            $returnArr['product'][$rowsCount]['Width']                 = $rows['Width'];
                            $returnArr['product'][$rowsCount]['Length']                = $rows['Length'];
                            $returnArr['product'][$rowsCount]['Description']           = $rows['Description'];
                            $returnArr['product'][$rowsCount]['ImagePath1']            = $rows['ImagePath1'];
                            $returnArr['product'][$rowsCount]['ImagePath2']            = $rows['ImagePath2'];
                            $returnArr['product'][$rowsCount]['TechDoc']               = $rows['TechDoc'];
                            $returnArr['product'][$rowsCount]['NumberHits']            = $rows['NumberHits']; //
                            $returnArr['product'][$rowsCount]['DisplayProduct']        = $rows['DisplayProduct'];
                            $returnArr['product'][$rowsCount]['DisplayLevel']          = $rows['DisplayLevel']; //

                            $returnArr['product'][$rowsCount]['CurrentStock']         = $rows['CurrentStock']; //
                            $returnArr['product'][$rowsCount]['RelatedProducts']      = $rows['RelatedProducts'];
                            $returnArr['product'][$rowsCount]['IsFeatured']           = $rows['IsFeatured'];//
                            
                            // $returnArr['product'][$rowsCount]['']       = $rows[''];

                            // $returnArr['product'][$rowsCount]['']       = $rows[''];
                            // $returnArr['product'][$rowsCount]['']       = $rows[''];
                            // $returnArr['product'][$rowsCount]['']       = $rows[''];
                            // $returnArr['product'][$rowsCount]['']       = $rows[''];

                            // $returnArr['product'][$rowsCount]['']       = $rows[''];
                            // $returnArr['product'][$rowsCount]['']       = $rows[''];
                            // $returnArr['product'][$rowsCount]['']       = $rows[''];
                            // $returnArr['product'][$rowsCount]['']       = $rows[''];

                            // $returnArr['product'][$rowsCount]['']       = $rows[''];
                            // $returnArr['product'][$rowsCount]['']       = $rows[''];
                            // $returnArr['product'][$rowsCount]['']       = $rows[''];
                            // $returnArr['product'][$rowsCount]['']       = $rows[''];
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $article_pegi    = $this->getAllData('products',$whereCondition,'ProductID','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($article_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    =$lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['product'])){
                    
                    return $this->sendResponse('All product listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No product found!',array());
                    
                }
            }
            else if($mode == 'get_variants_colour')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                // $productid=trim($request->post('productid'));
                // $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array('Status' => '1');
                
                
                   $colour = $this->getAllData('VariantsColour',$whereCondition,'Id','DESC',$lastlimit,$lastRow);
                   $allcolour = $this->getAllData('VariantsColour',$whereCondition,'Id','DESC');
                
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allcolour;
                }
                else
                {
                    $finalarray=$colour;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['colour'][$rowsCount]['Id']             = (string)$rows['Id'];
                            $returnArr['colour'][$rowsCount]['Name']            = $rows['Name'];
                            
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $colour_pegi    = $this->getAllData('VariantsColour',$whereCondition,'Id','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($colour_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    = $lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['colour'])){
                    
                    return $this->sendResponse('All Variants Colour listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No Variants Colour found!',array());
                    
                }

            }
            else if($mode == 'get_variants_materials')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                // $productid=trim($request->post('productid'));
                // $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array('Status' => '1');
                
                
                   $Material = $this->getAllData('VariantsMaterial',$whereCondition,'Id','DESC',$lastlimit,$lastRow);
                   $allMaterial = $this->getAllData('VariantsMaterial',$whereCondition,'Id','DESC');
                
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allMaterial;
                }
                else
                {
                    $finalarray=$Material;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['Material'][$rowsCount]['Id']             = (string)$rows['Id'];
                            $returnArr['Material'][$rowsCount]['Name']            = $rows['Name'];
                            
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $colour_pegi    = $this->getAllData('VariantsMaterial',$whereCondition,'Id','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($colour_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    = $lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['Material'])){
                    
                    return $this->sendResponse('All Variants Material listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No Variants Material found!',array());
                    
                }

            }
             else if($mode == 'get_season_List')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                // $productid=trim($request->post('productid'));
                // $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array('Status'=> '1');
                
                
                   $Season = $this->getAllData('Season',$whereCondition,'Id','DESC',$lastlimit,$lastRow);
                   $allSeason = $this->getAllData('Season',$whereCondition,'Id','DESC');
                
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allSeason;
                }
                else
                {
                    $finalarray=$Season;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['Season'][$rowsCount]['Id']             = (string)$rows['Id'];
                            $returnArr['Season'][$rowsCount]['Name']            = $rows['Name'];
                            
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $colour_pegi    = $this->getAllData('Season',$whereCondition,'Id','DESC',$lastlimit,$lastRow);
                        
                        if(!empty($colour_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    = $lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['Season'])){
                    
                    return $this->sendResponse('All  Season listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No  Season found!',array());
                    
                }

            }
             else if($mode == 'get_productType_List')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                // $productid=trim($request->post('productid'));
                // $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array('status'=> '1');
                
                
                   $ProductType = $this->getAllData('ProductType',$whereCondition,'producttypeid','ASC',$lastlimit,$lastRow);
                   $allProductType = $this->getAllData('ProductType',$whereCondition,'producttypeid','ASC');
                
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allProductType;
                }
                else
                {
                    $finalarray=$ProductType;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['ProductType'][$rowsCount]['producttypeid']             = (string)$rows['producttypeid'];
                            $returnArr['ProductType'][$rowsCount]['producttypeName']            = $rows['producttypeName'];
                            
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $colour_pegi    = $this->getAllData('ProductType',$whereCondition,'producttypeid','ASC',$lastlimit,$lastRow);
                        
                        if(!empty($colour_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    = $lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['ProductType'])){
                    
                    return $this->sendResponse('All  Product Type listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No  Product Type found!',array());
                    
                }

            }
            else if($mode == 'get_measureunit_List')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $measureunitid=trim($request->post('measureunitid'));
                // $storeid=trim($request->post('storeId'));
                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array('status'=> '1');
                
                if($measureunitid !='')
                    {
                        $whereCondition['id']=$measureunitid;
                    }

                
                   $measureUnit = $this->getAllData('measureUnit',$whereCondition,'id','ASC',$lastlimit,$lastRow);
                   $allmeasureUnit = $this->getAllData('measureUnit',$whereCondition,'id','ASC');
                
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allmeasureUnit;
                }
                else
                {
                    $finalarray=$measureUnit;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['measureUnit'][$rowsCount]['id']                  = (string)$rows['id'];
                            $returnArr['measureUnit'][$rowsCount]['unitName']            = $rows['unitName'];

                            


                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $colour_pegi    = $this->getAllData('measureUnit',$whereCondition,'id','ASC',$lastlimit,$lastRow);
                        
                        if(!empty($colour_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    = $lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['measureUnit'])){
                    
                    return $this->sendResponse('All  Measure Unit listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No  Measure Unit found!',array());
                    
                }

            }
               else if($mode == 'get_category_List_by_ptypeid')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $producttypeid=trim($request->post('producttypeid'));
                //$storeid=trim($request->post('storeId'));

                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                $whereCondition=array('Status' => '1');
                if($producttypeid !='')
                    {
                        $whereCondition['productTypeId']=$producttypeid;
                    }

                
                   $productTypeData = $this->getAllData('categories',$whereCondition,'CategoryID','ASC',$lastlimit,$lastRow);
                   $allproductTypeData = $this->getAllData('categories',$whereCondition,'CategoryID','ASC');
                
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allproductTypeData;
                }
                else
                {
                    $finalarray=$productTypeData;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['producttype'][$rowsCount]['CategoryID']                  = (string)$rows['CategoryID'];
                            $returnArr['producttype'][$rowsCount]['CategoryName']            = $rows['CategoryName'];

                            


                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $colour_pegi    = $this->getAllData('categories',$whereCondition,'CategoryID','ASC',$lastlimit,$lastRow);
                        
                        if(!empty($colour_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    = $lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['producttype'])){
                    
                    return $this->sendResponse('All  Product Type listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No   Product Type found!',array());
                    
                }

            }
            else if($mode == 'get_colour_List_by_ptypeid')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $producttypeid=trim($request->post('producttypeid'));
                //$storeid=trim($request->post('storeId'));

                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                
                if($producttypeid !='')
                    {
                        $whereCondition['productTypeId']=$producttypeid;
                    }

                
                   $productTypeData = $this->getAllData('productTypeColor',$whereCondition,'id','ASC',$lastlimit,$lastRow);
                   $allproductTypeData = $this->getAllData('productTypeColor',$whereCondition,'id','ASC');
                
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allproductTypeData;
                }
                else
                {
                    $finalarray=$productTypeData;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['producttypeColour'][$rowsCount]['ColourId']              = (string)$rows['id'];
                            $returnArr['producttypeColour'][$rowsCount]['ColourName']            = $rows['name'];
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $colour_pegi    = $this->getAllData('productTypeColor',$whereCondition,'id','ASC',$lastlimit,$lastRow);
                        
                        if(!empty($colour_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    = $lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['producttypeColour'])){
                    
                    return $this->sendResponse('All  Product Type wise Colour listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No   Product Type wise colour found!',array());
                    
                }

            }
            else if($mode == 'get_material_List_by_ptypeid')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $producttypeid=trim($request->post('producttypeid'));
                //$storeid=trim($request->post('storeId'));

                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                
                if($producttypeid !='')
                    {
                        $whereCondition['productTypeId']=$producttypeid;
                    }

                
                   $productTypeData = $this->getAllData('productTypeMaterial',$whereCondition,'id','ASC',$lastlimit,$lastRow);
                   $allproductTypeData = $this->getAllData('productTypeMaterial',$whereCondition,'id','ASC');
                
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allproductTypeData;
                }
                else
                {
                    $finalarray=$productTypeData;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['producttypeMaterial'][$rowsCount]['MaterialId']              = (string)$rows['id'];
                            $returnArr['producttypeMaterial'][$rowsCount]['MaterialName']            = $rows['name'];
                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $colour_pegi    = $this->getAllData('productTypeMaterial',$whereCondition,'id','ASC',$lastlimit,$lastRow);
                        
                        if(!empty($colour_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    = $lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['producttypeMaterial'])){
                    
                    return $this->sendResponse('All  Product Type wise Material listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No   Product Type wise Material found!',array());
                    
                }

            }

        else if($mode == 'get_size_List_by_ptypeid')
            {
                $lastRow=(int)$request->post('lastRow');
                $flag= trim($request->post('flag'));  
                $producttypeid=trim($request->post('producttypeid'));
                //$storeid=trim($request->post('storeId'));

                $lastlimit=10;
                
                if($lastRow == '')
                {
                    $lastRow = 0;
                }
                
                $whereCondition=array();
                
                if($producttypeid !='')
                    {
                        $whereCondition['productTypeId']=$producttypeid;
                        //$whereCondition['storesizeId'] = '2';
                    }

                
                   $productTypeData = $this->getAllData('productTypeSize',$whereCondition,'id','ASC',$lastlimit,$lastRow);
                   $allproductTypeData = $this->getAllData('productTypeSize',$whereCondition,'id','ASC');
                
               
                
                $finalarray=array();
                if($flag == '1')
                {
                    $finalarray=$allproductTypeData;
                }
                else
                {
                    $finalarray=$productTypeData;
                }
                
                // echo "<pre>";
                // print_r($finalarray);
                // die;   

                if(!empty($finalarray))
                {
                    $rowsCount = 0;
                    foreach($finalarray as $rows)
                    {
                            $returnArr['producttypeSize'][$rowsCount]['id']              = (string)$rows['id'];
                            $returnArr['producttypeSize'][$rowsCount]['sizeinusa']            = $rows['sizeinusa'];
                            $returnArr['producttypeSize'][$rowsCount]['sizeineu']            = $rows['sizeineu'];
                            $returnArr['producttypeSize'][$rowsCount]['sizeinasia']            = $rows['sizeinasia'];
                            
                            $returnArr['producttypeSize'][$rowsCount]['sizeinusaunit']            = $rows['sizeinusaunit'];
                            $returnArr['producttypeSize'][$rowsCount]['sizeineuunit']            = $rows['sizeineuunit'];
                            $returnArr['producttypeSize'][$rowsCount]['sizeinasiaunit']            = $rows['sizeinasiaunit'];


                            $rowsCount++;
                    }
                    
                    $returnArr['pagination']['hasMore']         = 0;
                    $returnArr['pagination']['totaldata']       =count($finalarray);
                    $totalpage                                  =ceil(count($finalarray)/$lastlimit);
                    $returnArr['pagination']['totalpage']       =$totalpage;
                    
                    if($flag == '1')
                    {
                        $lastRow        = $lastRow + $lastlimit;
                        $colour_pegi    = $this->getAllData('productTypeSize',$whereCondition,'id','ASC',$lastlimit,$lastRow);
                        
                        if(!empty($colour_pegi))
                        {
                            $returnArr['pagination']['hasMore']      = 1;
                            $returnArr['pagination']['lastRow']      = $lastRow;
                            $returnArr['pagination']['lastLimit']    = $lastlimit;
                        }
                        
                    }
                    
                    //echo 'count=>'.count($allcustomer);
                    
                }
                else
                {
                    $returnArr=array();
                }
                if(!empty($finalarray) && !empty($returnArr['producttypeSize'])){
                    
                    return $this->sendResponse('All  Product Type wise Size listing!',$returnArr);
                   
                }
                else
                {
                    return $this->sendError('No   Product Type wise Size found!',array());
                    
                }

            }


            
        }
       
    }
?>