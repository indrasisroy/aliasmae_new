<?php

namespace App\Http\Controllers\site_controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;  //adedd for test auth
//use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;  /*for auth user*/
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use App\Mail\sendMyMail;  /*send mail class*/
use Redirect;
use Cookie;
use Session;
class Cart extends MyController
{
    public function __construct(Request $request)
    {
       
    }
    public function index()
    {   
        $pageName='category';
        return view('front/cart/cart',compact('pageName'));
    }
    public function Checkout()
    {   
        $pageName='category';
        return view('front/cart/checkOut',compact('pageName'));
    }
}
?>