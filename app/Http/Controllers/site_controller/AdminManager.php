<?php

namespace App\Http\Controllers\site_controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
//use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mail\sendMyMail;  /*send mail class*/
use Redirect;
use Session;
class AdminManager extends MyController
{
    
    //====== load Password Manager page =========//
    public function index()
    {
        $data['title'] = 'Password Manager';
        
        //userdetails fetch
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        
        if($userId != '')
        {
            $curlUserUrl = 'api/connectSite';
            $curlUserPostData = array(
                            'mode' => 'getadmin_manager_details',                           
                            'adminId' => $userId
                            );
            $Userurl = $baseUrl.$curlUserUrl;
            $user = $this->fireCurl($Userurl,$curlUserPostData);
            $user_array = json_decode($user,1);
            if($user_array['status'] == 1)
            {
                $data['username']=$user_array['data']['Admin_manager_details'][0]['UserName'];
                $data['password']=base64_decode($user_array['data']['Admin_manager_details'][0]['password']);
                
            }
            else{
                $data['username']='';
                $data['password']='';                
            }
             //echo "<pre>";
             //print_r($data);
            // die();
           
            $data['viewPage'] = 'control/adminmanager/adminmanager';
            //$data['js'] ='adminmanager.js';
            return view('control/includes/master_view',compact('data'));
            
        }
        else{
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }	
        
    }
    //====== load Shipping Manager page =========//
    public function manageShipping()
    {
        $data['title'] = 'Shipping Manager';
        
        //userdetails fetch
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        
        
        
        if($userId != '')
        {
            $curlUserUrl = 'api/connectSite';
            $curlUserPostData = array(
                            'mode' => 'getadmin_manager_details',                           
                            'adminId' => $userId
                            );
            $Userurl = $baseUrl.$curlUserUrl;
            $user = $this->fireCurl($Userurl,$curlUserPostData);
            $user_array = json_decode($user,1);
            if($user_array['status'] == 1)
            {
                $data['FlatShippingChargeNZ']=$user_array['data']['Admin_manager_details'][0]['FlatShippingChargeNZ'];
                $data['FlatShippingChargeUS']=$user_array['data']['Admin_manager_details'][0]['FlatShippingChargeUS'];
                $data['FlatShippingChargeUK']=$user_array['data']['Admin_manager_details'][0]['FlatShippingChargeUK'];
                $data['FlatShippingChargeSG']=$user_array['data']['Admin_manager_details'][0]['FlatShippingChargeSG'];
                $data['FlatShippingChargeCN']=$user_array['data']['Admin_manager_details'][0]['FlatShippingChargeCN'];
                $data['FlatShippingChargeInt']=$user_array['data']['Admin_manager_details'][0]['FlatShippingChargeInt'];
                $data['FlatShippingCharge']=$user_array['data']['Admin_manager_details'][0]['FlatShippingCharge'];
                $data['IncrementalShippingCharge']=$user_array['data']['Admin_manager_details'][0]['IncrementalShippingCharge'];
                $data['MaxOrderAmountShippingTotal']=$user_array['data']['Admin_manager_details'][0]['MaxOrderAmountShippingTotal'];
                $data['FlatShippingChargeTotal']=$user_array['data']['Admin_manager_details'][0]['FlatShippingChargeTotal'];
                $data['MinOrderAmountShippingDiscount1']=$user_array['data']['Admin_manager_details'][0]['MinOrderAmountShippingDiscount1'];
                $data['MinOrderPercentShippingDiscount1']=$user_array['data']['Admin_manager_details'][0]['MinOrderPercentShippingDiscount1'];
                $data['MinOrderAmountShippingDiscount2']=$user_array['data']['Admin_manager_details'][0]['MinOrderAmountShippingDiscount2'];
                $data['MinOrderPercentShippingDiscount2']=$user_array['data']['Admin_manager_details'][0]['MinOrderPercentShippingDiscount2'];
                $data['MinOrderAmountShippingDiscount3']=$user_array['data']['Admin_manager_details'][0]['MinOrderAmountShippingDiscount3'];
                $data['MinOrderPercentShippingDiscount3']=$user_array['data']['Admin_manager_details'][0]['MinOrderPercentShippingDiscount3'];
            }
            else{
                $data['FlatShippingChargeNZ']='';
                $data['FlatShippingChargeUS']='';    
                $data['FlatShippingChargeUK']=''; 
                $data['FlatShippingChargeSG']='';   
                $data['FlatShippingChargeCN']='';
                $data['FlatShippingChargeInt']='';
                $data['FlatShippingCharge']='';
                $data['IncrementalShippingCharge']='';
                $data['MaxOrderAmountShippingTotal']='';
                $data['FlatShippingChargeTotal']='';
                $data['MinOrderAmountShippingDiscount1']='';
                $data['MinOrderPercentShippingDiscount1']='';
                $data['MinOrderAmountShippingDiscount2']='';
                $data['MinOrderPercentShippingDiscount2']='';
                $data['MinOrderAmountShippingDiscount3']='';
                $data['MinOrderPercentShippingDiscount3']='';        
            }
            // echo "<pre>";
            // print_r($user_array);
            // die();
           
            $data['viewPage'] = 'control/adminmanager/shippingmanager';
            //$data['js'] ='adminmanager.js';
            return view('control/includes/master_view',compact('data'));
            
        }
        else{
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }   
        
    }
    //======================Update Shipping Management==================
    public function updateShipping(Request $request)
    {
        $data['title'] = 'Shipping Manager';
        
        //userdetails fetch
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        
        
        
        if($userId != '')
        {
            $curlUrl = 'api/connectSite';
            $curlPostData = array(
                            'mode' => 'shipping_options', 
                            'userId' =>  $userId,                       
                            'Flat-Shipping-Price-New-Zealand' => $request->post('nzprice'),
                            'Flat-Shipping-Price-Us' => $request->post('usprice'),
                            'Flat-Shipping-Price-Uk' => $request->post('ukprice'),
                            'Flat-Shipping-Price-Singapore' => $request->post('singaporeprice'),
                            'Flat-Shipping-Price-Canada' => $request->post('canadaprice'),
                            'Flat-Shipping-Price-International' => $request->post('intprice'),
                            'Flat-Shipping-Price-Australia' => $request->post('flatprice'),
                            'Additional-Item-Flat-Shipping-Price-Australia' => $request->post('addflatprice'),
                            'Max-Order-Amount' => $request->post('maxorderprice'),
                            'Shipping-Amount' => $request->post('maxshippingprice'),
                            'Min-Order-Amount-one' => $request->post('minorderprice1'),
                            'Min-Order-Amount-two' => $request->post('minorderprice2'),
                            'Min-Order-Amount-three' => $request->post('minorderprice3'),
                            'Shipping-Discount-one' => $request->post('minshippingprice1'),
                            'Shipping-Discount-two' => $request->post('minshippingprice2'),
                            'Shipping-Discount-three' => $request->post('minshippingprice3'),
                            );
            $url = $baseUrl.$curlUrl;
            $updateShipping = $this->fireCurl($url,$curlPostData);
            $updateShipping_array = json_decode($updateShipping,1);
            if($updateShipping_array['status'] == 1)
            {
                $request->session()->put('success_msg',"Data Updated Successfully");
               return Redirect::to('oms/adminmanager/manageshipping');
            }
            else{
                $request->session()->put('error_msg',"Something went wrong. Please try again!");
                return Redirect::to('oms/adminmanager/manageshipping');
            }

            /*echo "<pre>";
            print_r($user_array);
            die();*/    
        }
        else{
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }   
        
    }
    //======================================== Fetching manage pair discount start ================================
    public function managepairdiscount()
    {
        $data['title'] = 'Manage Pair Discount';
        
        //userdetails fetch
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        
        if($userId != '')
        {
            $curlUserUrl = 'api/connectSite';
            $curlUserPostData = array(
                            'mode' => 'getadmin_manager_details',                           
                            'adminId' => $userId
                            );
            $Userurl = $baseUrl.$curlUserUrl;
            $user = $this->fireCurl($Userurl,$curlUserPostData);
            $user_array = json_decode($user,1);
            if($user_array['status'] == 1)
            {
                $data['EnablePairDiscount']=$user_array['data']['Admin_manager_details'][0]['EnablePairDiscount'];
                $data['NumberOfPairsToPurchase']=$user_array['data']['Admin_manager_details'][0]['NumberOfPairsToPurchase'];
                $data['DiscountForCheapestPair']=$user_array['data']['Admin_manager_details'][0]['DiscountForCheapestPair'];
                
            }
            else{
                $data['EnablePairDiscount']='';
                $data['NumberOfPairsToPurchase']='';    
                $data['DiscountForCheapestPair']='';                 
            }
            // echo "<pre>";
            // print_r($user_array);
            // die();
           
            $data['viewPage'] = 'control/adminmanager/managepairdiscount';
            //$data['js'] ='adminmanager.js';
            return view('control/includes/master_view',compact('data'));
            
        }
        else{
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }   
        
    }
    public function updatepassword(Request $request)
    {
        $data['title'] = 'Update password Manager';
         $username = $request->post('username');
        $password = $request->post('password');
        //userdetails fetch
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');

        $baseUrl = \Config::get('app.url');
        $curlUrl = 'api/connectSite';
        $curlPostData = array(
                        'mode'=>'updatePassword',
                        'userId' =>  $userId, 
                        'username'=>$username,
                        'password'=>$password
                        );
        $url = $baseUrl.$curlUrl;
        $userupdate = $this->fireCurl($url,$curlPostData);//die();
        $userupdate_array = json_decode($userupdate,1);
       if($userupdate_array['status'] == 1)
        {
            $request->session()->put('success_msg',"Username and Password changed");
           return Redirect::to('oms/adminmanager/settingsmanager');
        }
        else{
            $request->session()->put('error_msg',"Username and Password has not been changed. Please try again!");
            return Redirect::to('oms/adminmanager/settingsmanager');
        }


    }
    //======================================== update manage pair discount start ================================
    public function updatemanagepairdiscount(Request $request){
            $EnablePairDiscountvalue = '0';
            $NumberOfPairsToPurchase = $request->post('NumberOfPairsToPurchase');
            $DiscountForCheapestPair = $request->post('DiscountForCheapestPair');
            $EnablePairDiscount = $request->post('EnablePairDiscount');

            //echo $EnablePairDiscount;
            //die;

            //echo $DiscountForCheapestPair;

           // echo $NumberOfPairsToPurchase;
            if($EnablePairDiscount != '')
            {
                $EnablePairDiscountvalue = (int)$EnablePairDiscount;
            }

                    $baseUrl = \Config::get('app.url');    //this is the base url defined in config.php file
                    $userId = Session::get('userId');

                    $baseUrl = \Config::get('app.url');
                    $curlUrl = 'api/connectSite';
                    $curlPostData = array(
                                    'mode'=>'discount_options',
                                    'userId' =>  $userId,  
                                    'Activate'=> $EnablePairDiscountvalue,
                                    'Minimum-Number-of-Pairs-To-Purchase'=>$NumberOfPairsToPurchase,
                                    'Discount-Applied-on-the-Cheapest-Pair'=>$DiscountForCheapestPair
                                    );
                    $url = $baseUrl.$curlUrl;
                    $userupdate = $this->fireCurl($url,$curlPostData);//die();
                    $userupdate_array = json_decode($userupdate,1);
                   if($userupdate_array['status'] == 1)
                    {
                        $request->session()->put('success_msg',"Data Updated Successfully.");
                       return Redirect::to('oms/adminmanager/managepairdiscount');
                    }
                    else{
                        $request->session()->put('error_msg',"Data has not been Updated. Please try again!");
                        return Redirect::to('oms/adminmanager/managepairdiscount');
                    }
            


    }
    //======================================== update manage pair discount end================================
    //======================================== Fetching Send Order report mail start ================================
    public function manageReportMail()
    {
        $data['title'] = 'Manage Report Mail';
        
        //userdetails fetch
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        
        if($userId != '')
        {
            $curlUserUrl = 'api/connectSite';
            $curlUserPostData = array(
                            'mode' => 'getadmin_manager_details',                           
                            'adminId' => $userId
                            );
            $Userurl = $baseUrl.$curlUserUrl;
            $user = $this->fireCurl($Userurl,$curlUserPostData);
            $user_array = json_decode($user,1);
            if($user_array['status'] == 1)
            {
                $data['ordersaleemailid']=$user_array['data']['Admin_manager_details'][0]['ordersaleemailid'];       
            }
            else{
                $data['ordersaleemailid']='';                
            }
            // echo "<pre>";
            // print_r($user_array);
            // die();
           
            $data['viewPage'] = 'control/adminmanager/reportMail';
            //$data['js'] ='adminmanager.js';
            return view('control/includes/master_view',compact('data'));
            
        }
        else{
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }   
        
    }
    //======================Update SAle order Report Mail Management==================
    public function updateReportMail(Request $request)
    {
        $data['title'] = 'Order Sale Report Mail Manager';
        
        //userdetails fetch
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        
        
        
        if($userId != '')
        {
            $curlUrl = 'api/connectSite';
            $curlPostData = array(
                            'mode' => 'order_sale_report_email', 
                            'userId' =>  $userId,                       
                            'ordersaleemailid' => $request->post('emailId'),
                        );
            $url = $baseUrl.$curlUrl;
            $updateReportMail = $this->fireCurl($url,$curlPostData);
            $updateReportMail_array = json_decode($updateReportMail,1);
            if($updateReportMail_array['status'] == 1)
            {
                $request->session()->put('success_msg',"Data Updated Successfully");
               return Redirect::to('oms/adminmanager/reportmail');
            }
            else{
                $request->session()->put('error_msg',"Something went wrong. Please try again!");
                return Redirect::to('oms/adminmanager/reportmail');
            }

            /*echo "<pre>";
            print_r($user_array);
            die();*/    
        }
        else{
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        } 
    }  
   
}
?>