<?php
namespace App\Http\Controllers\site_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Mail\addSubadminTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\Storage;  /*for file upload*/
use Redirect;
use Session;
use Excel;
use Image;

class TrendManager extends MyController
{
	//================Trend details==============//
    public function getDetails(Request $request)
    {
		//echo $id;die();
		$trendid=base64_decode($request->segment(4));
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		$Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		
		$curlUrl = 'api/trend';
		$lastRow='';
		
		$curlPostData = array(
							'mode' => 'fetch_trends_data',
							'lastRow' => $lastRow,
							'userId'=>$userId,
							'trendid'=>$trendid,
							'storeId' =>$userStoretoreId,
							'flag'=>'0'
						);
		$url = $baseUrl.$curlUrl;
		$trend = $this->fireCurl($url,$curlPostData);
		$trend_array = json_decode($trend,1);
		//echo "<pre>";print_r($banner_array);die();
		//======== GET STORE DETAILS FROM API =========//
            $curlstoreUrl = 'api/StoreManagement';
            $curlstorePostData = array(
                            'mode' => 'fetch_store',
                            'lastrow' => $lastRow,
                            'flag' => 1,
                            'userId' => $userId,
                            'storeid'=>$userStoretoreId
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlstoreUrl;
			$store = $this->fireCurl($url,$curlstorePostData);
            $store_array = json_decode($store,1);
			 //echo "<pre>";
           //print_r($store_array);die;
            if(!empty($store_array) && $store_array['status'] == 1)
            {
                $data['store'] = $store_array['data']['store'];
            }else
            {
            	$data['store'] =array();
            }
		$trendarr=array();
		if(!empty($trend_array['data']) && $trend_array['status'] == 1)
		{
			$trendarr=$trend_array['data']['trend'][0];
			$storeaccess=$trend_array['data']['trend'][0]['Store'];
				if($storeaccess != '')
				{
					$strAccess=explode(',', $storeaccess);
                	$data['storeaccess'] = $strAccess;
				}
				else{
					$data['storeaccess'] = array();
				}
			$data['title'] = 'Trend Details';
			$data['viewPage'] = 'control/trend/edit_trend';
			return view('control/includes/master_view',compact('data','trendarr'));
		}
		else
		{
			return redirect('/oms/trend/trendlist');
			
		}
        
    }
    
	//=======================trend list===========================//
    public function getList()
    {
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$trend_array=array();
        if($userId != '')
        {
				//======== GET TREND LIST FROM API =========//
				$curlUrl = 'api/trend';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'fetch_trends_data',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'trendid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$trend = $this->fireCurl($url,$curlPostData);
				$trend_array = json_decode($trend,1);
		//echo "<pre>";
		//print_r($trend_array);
		//die();
            if(!empty($trend_array) && $trend_array['status'] == 1)
            {
	            $data['trendlist'] = $trend_array['data']['trend'];
				$data['title'] = 'Trend';
				$data['viewPage'] = 'control/trend/trend_list';
				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['trendlist'] = array();
				$data['title'] = 'Trend';
				$data['viewPage'] = 'control/trend/trend_list';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/');
        }
    }
	
	
	//=================edit trend details=========================//
	public function updateTrendDtls(Request $request)
	{
		$trendName=$request->post('trendName');
		$trendText=$request->post('trendText');
		$pageTitle=$request->post('pageTitle');
		$pageKeyword=$request->post('pageKeyword');
		$pageDescription=$request->post('pageDescription');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$trendId=$request->post('trendId');
		$store=$request->post('store');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}
		$file=$request->file('image');
		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal=0;
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== update trend =========//
		$curlUrl = 'api/trend';
	//echo "<pre>";echo $userId.'=='.$trendName.'=='.$displayOrder.'=='.$IsActiveVal;die();
		//echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $trendName!='' && $displayOrder!='' && $IsActiveVal!='')
		{
			if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
			{
				$imagesfile= $file->getClientSize();
				$Image1FileSize=filesize($file);//$imagesfile;
				$Image1FileName = $file->getClientOriginalName();//['filename'];//$request->file('input_pdf')->getClientOriginalName();
				//echo $Image1FileSize.'@@@@'.$Image1FileName;die();
				$fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
				list($width,$height) = getimagesize($file);
				//echo $width.'       '.$height;die();
				$image_content = file_get_contents($file);
				$path = 'uploads/trend_images/';
				$thumb_path = 'uploads/trend_images/thumbnail/';
				$fileNormal = $path.$fileName;
				$fileThumb = $thumb_path.$fileName;
			   
				$this->uploadImage('public',$fileNormal,$image_content);
				$this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
			   
			}
			else
			{
				$fileName='';
				$Image1FileSize='';
				$Image1FileName ='';
			}	
				$curlPostData = array(
							'mode' => 'addedittrend',
							'userId'=>$userId,
							'trendid'=>$trendId,
							'login_device_type'=>3,
							'TrendName'=>$trendName,
							'BannerImageText'=>$trendText,
							'PageTitle'=>$pageTitle,
							'PageKeywords'=>$pageKeyword,
							'PageDescription'=>$pageDescription,
							'IsActive'=>$IsActiveVal,
							'DisplayOrder' => $displayOrder,
							'image'=>$fileName,
							'Image1FileSize'=>$Image1FileSize,
							'Image1FileName'=>$Image1FileName,
							'Store' =>$strAccess
							);
				$url = $baseUrl.$curlUrl;
				
				//echo "<pre>";
				//print_r($curlPostData);
				//die();
				$banner = $this->fireCurl($url,$curlPostData);
				$banner_array = json_decode($banner,1);
				//echo "<pre>";print_r($banner_array);die();
				if($banner_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$banner_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$banner_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/trend/trendlist');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/trend/trendlist');
		}
	}
	//==============================load add trend view================================//
	public function addTrend()
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		if($userId != '')
        {
			$lastRow='';
			//======== GET STORE DETAILS FROM API =========//
	            $curlUrl = 'api/StoreManagement';
	            $curlPostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$store = $this->fireCurl($url,$curlPostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
			$data['title'] = 'Trend';
	        $data['viewPage'] = 'control/trend/add_trend';
	        return view('control/includes/master_view',compact('data'));
    	}
    	else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/');
        }

	}
	

	//================================insert trend details===========================//
	public function insertTrendDtls(Request $request)
	{
		
		$trendName=$request->post('trendName');
		$trendText=$request->post('trendText');
		$pageTitle=$request->post('pageTitle');
		$pageKeyword=$request->post('pageKeyword');
		$pageDescription=$request->post('pageDescription');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$trendId=$request->post('trendId');
		$store=$request->post('store');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}
		$file=$request->file('image');
		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		
		if($IsActive =='')
		{
			$IsActiveVal=0;
		}
		else{
			$IsActiveVal=$IsActive;
		}
		//======== insert trend =========//
		$curlUrl = 'api/trend';
		//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActive;echo "</pre>";
		// echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $trendName!='' && $displayOrder!='' && $IsActiveVal!=''  && !empty($file))
		{
			if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
			{
				$imagesfile= $file->getClientSize();
				$Image1FileSize=filesize($file);//$imagesfile;
				$Image1FileName = $file->getClientOriginalName();//['filename'];//$request->file('input_pdf')->getClientOriginalName();
				//echo $Image1FileSize.'@@@@'.$Image1FileName;die();
				$fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
				list($width,$height) = getimagesize($file);
				//echo $width.'       '.$height;die();
				$image_content = file_get_contents($file);
				$path = 'uploads/trend_images/';
				$thumb_path = 'uploads/trend_images/thumbnail/';
				$fileNormal = $path.$fileName;
				$fileThumb = $thumb_path.$fileName;
			   
				$this->uploadImage('public',$fileNormal,$image_content);
				$this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
			   
			}
			else
			{
				$fileName='';
				$Image1FileSize='';
				$Image1FileName ='';
			}	
				$curlPostData = array(
							'mode' => 'addedittrend',
							'userId'=>$userId,
							'trendid'=>$trendId,
							'login_device_type'=>3,
							'TrendName'=>$trendName,
							'BannerImageText'=>$trendText,
							'PageTitle'=>$pageTitle,
							'PageKeywords'=>$pageKeyword,
							'PageDescription'=>$pageDescription,
							'IsActive'=>$IsActiveVal,
							'DisplayOrder' => $displayOrder,
							'image'=>$fileName,
							'Image1FileSize'=>$Image1FileSize,
							'Image1FileName'=>$Image1FileName,
							'Store' =>$strAccess
							);
				$url = $baseUrl.$curlUrl;
				
				//echo "<pre>";
				//print_r($curlPostData);
				//die();
				$trend = $this->fireCurl($url,$curlPostData);
				$trend_array = json_decode($trend,1);
				
				if($trend_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$trend_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$trend_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/trend/trendlist');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/trend/trendlist');
		}
		
		
	}
	
	//================================change status===================================//
	public function changeStatus(Request $request)
	{
		$bannerid=$request->segment(4);
		$status=$request->segment(5);
		
		$whereArr=array('TrendID'=>$bannerid);
		$updatearr=array('IsActive'=>$status);
		
        $updatebannerDetails=$this->update('trends',$whereArr,$updatearr);
		
		$request->session()->put('success_msg','Status updated successfully!');
		
		 return Redirect::to('/oms/trend/trendlist');
	}
	

	///=============== delete a trend ====================//
	public function deletetrend(Request $request)
	{
		$id = $request->segment(4);
		if($id !='')
		{
			$ConditionBanner=array('TrendID'=>$id);
			$fetchtrend=$this->getAllData('trends',$ConditionBanner,'TrendID','DESC');
			//echo "<pre>";print_r($fetchbanner);die();
			if(!empty($fetchtrend))
			{
				$previmage=$fetchtrend[0]['Image1SavedFileName'];
				$delete = $this->delete('trends',array('TrendID'=>$id));
				if($delete)
				{
					$normalpath=storage_path().'/app/'.'public/uploads/trend_images/'.$previmage;
					$thumbnailpath=storage_path().'/app/'.'public/uploads/trend_images/thumbnail/'.$previmage;
					
					if(file_exists($normalpath))
					{
						unlink(storage_path().'/app/'.'public/uploads/trend_images/'.$previmage);
					}
					if(file_exists($thumbnailpath))
					{
						unlink(storage_path().'/app/'.'public/uploads/trend_images/thumbnail/'.$previmage);
					}
					$request->session()->put('success_msg',"Trend deleted.");
				}
				else
				{
					$request->session()->put('error_msg',"Failed to delete trend.");
				}
				
			}
			else
			{
				$request->session()->put('error_msg',"Please provide valid details.");
			}
		}
		else
		{
			$request->session()->put('error_msg',"Please provide valid details.");
		}
		return Redirect::to('/oms/trend/trendlist');
	}
}
   
   
?>