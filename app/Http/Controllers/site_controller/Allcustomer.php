<?php
namespace App\Http\Controllers\site_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Mail\addSubadminTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\Storage;  /*for file upload*/
use Redirect;
use Session;
use Excel;
use Image;

class Allcustomer extends MyController
{
	public function getList(Request $request)
    {

        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }

		if($userId != '')
        {

        		//======== GET Customer LIST FROM API =========//
				$curlUrl = 'api/allcustomer';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_customer_manager_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'MemberID'=>'',									
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$customer = $this->fireCurl($url,$curlPostData);
				$customer_array = json_decode($customer,1);
						
				// 		echo "<pre>";
				// 	print_r($customer_array);
				// die();

				$data['customer'] = $customer_array['data']['customer'];
				$data['title'] = 'Customers';
				$data['viewPage'] = 'control/customer/customer_list';
				return view('control/includes/master_view',compact('data'));

        }
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

    }
    public function addCustomer(Request $request)
    {

        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }

        if($userId != '')
        {
			$lastRow='';


			$data['title'] = 'Add Customer';
	        $data['viewPage'] = 'control/customer/add_customer';
	        return view('control/includes/master_view',compact('data'));

        }
	    else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

    }
    public function insertCustomerDtls(Request $request)
    {
    	// echo "<pre>";
    	// print_r($_REQUEST);
    	// die;
    		  $Memberid = trim($request->post('memberid'));
    		  if(isset($Memberid))
    		  {
    		  	$MemberID = $Memberid;
    		  }else{
    		  	$MemberID = '';
    		  }
              $CustomerType= ($request->post('ctype'));

              if(!empty($CustomerType))
				{
					$CustomerTypeAccess=implode(',', $CustomerType);
				}
				else{
					$CustomerTypeAccess='';
				}
              $UserName= trim($request->post('email'));
              $UserPassword= trim($request->post('password'));
              $FirstName= trim($request->post('firstname'));
              $LastName= trim($request->post('lastname'));
              $BusinessName= trim($request->post('businessname'));
              $ABN= trim($request->post('abnacn'));
              $StreetAddress1= trim($request->post('address1'));
              $StreetAddress2= trim($request->post('address2'));
              $Suburb= trim($request->post('suburb'));
              $State= trim($request->post('state'));
              $Country= trim($request->post('country'));
              $Zip= trim($request->post('postcode'));
              $Phone= trim($request->post('phone'));
              $Mobile= trim($request->post('mobile'));
              $Fax= '';//trim($request->post('Fax'));


              $IsActive= trim($request->post('IsActiveNew'));
              $ReceiveUpdates= trim($request->post('ReceiveNewslettersNew'));

              $DiscountPercent= '0';//trim($request->post('DiscountPercent'));
              $AdminApproved= trim($request->post('adminapprovedNew'));
              $AdminNotes= trim($request->post('adminnotes'));


              //$InsertionDate= trim($request->post('InsertionDate'));

              $HowHearNew= ($request->post('howhear'));
              $HowHearOther= trim($request->post('other'));

              if(!empty($HowHearNew)){
              	$HowHear = implode(',', $HowHearNew) ;
              }else{
              	$HowHear = '';
              }

             	$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        		$userId = Session::get('userId');
		
				$url = \Config::get('app.url');

				//======== insert customer Data =========//
				$curlUrl = 'api/allcustomer';
				if($CustomerTypeAccess !='' && $UserPassword!='' && $UserName!='')
				{

								$curlPostData = array(
										'mode'=>'insert_customer_manager_details',
										'MemberID'=>$MemberID,
										'userId'=>$userId,
										'CustomerType'=>$CustomerTypeAccess,										
                            'UserName'=> $UserName,
                            'UserPassword'=> $UserPassword,
                            'FirstName'=> $FirstName,
                            'LastName'=> $LastName,
                            'BusinessName'=> $BusinessName,
                            'ABN'=> $ABN,
                            'StreetAddress1'=>  $StreetAddress1,
                            'StreetAddress2'=> $StreetAddress2,
                            'Suburb'=> $Suburb,
                            'State'=> $State,
                            'Country'=> $Country,
                            'Zip'=> $Zip,
                            'Phone'=> $Phone,
                            'Mobile'=> $Mobile,
                            'Fax'=> $Fax,
                            'IsActive'=> $IsActive,
                            'ReceiveUpdates'=> $ReceiveUpdates,
                            'DiscountPercent'=> $DiscountPercent,
                            'AdminApproved'=> $AdminApproved,
                            'AdminNotes'=> $AdminNotes,                           
                            'HowHear'=> $HowHear,
                            'HowHearOther'=> $HowHearOther
										);

// echo "<pre>";
// print_r($curlPostData);
// die;
						$url = $baseUrl.$curlUrl;
						$brand1 = $this->fireCurl($url,$curlPostData); 
							$brand1_array = json_decode($brand1,1);

							// echo "<pre>";
							// print_r($brand1_array );die;
							if($brand1_array['status'] == 1)
							{	
								$request->session()->put('success_msg',$brand1_array['message']);
							}
							else
							{
								$request->session()->put('error_msg',$brand1_array['message']);
							}
							//echo $banner_array['message'].'@@@'.$promo_array['status'];
							return Redirect::to('/oms/customer/customer-list');


				}else{
					$request->session()->put('error_msg','Please provide valid details.');
						//echo 'provide valid details'.'@@@'.'0';
					return Redirect::to('/oms/customer/add-customer');
				}



    }


}
?>