<?php
namespace App\Http\Controllers\site_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Mail\addSubadminTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\Storage;  /*for file upload*/
use Redirect;
use Session;
use Excel;
use Image;

class BannerManager extends MyController
{
	//================Banner details==============//
    public function getDetails(Request $request)
    {
		//echo $id;die();
		$bannerid=base64_decode($request->segment(4));
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		if($userId != '')
        {
		
			$curlUrl = 'api/banner';
			$lastRow='';
			
			$curlPostData = array(
								'mode' => 'get_banner_list',
								'lastRow' => $lastRow,
								'userId'=>$userId,
								'bannerid'=>$bannerid,
								'storeId' =>$userStoretoreId,
								'flag'=>'0'
							);
			$url = $baseUrl.$curlUrl;
			$banner = $this->fireCurl($url,$curlPostData);
			$banner_array = json_decode($banner,1);
			//echo "<pre>";print_r($banner_array);die();
			//======== GET STORE DETAILS FROM API =========//
	            $curlstoreUrl = 'api/StoreManagement';
	            $curlstorePostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlstoreUrl;
				$store = $this->fireCurl($url,$curlstorePostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
			$bannerarr=array();
			if(!empty($banner_array['data']) && $banner_array['status'] == 1)
			{
				$bannerarr=$banner_array['data']['banner'][0];
				$storeaccess=$banner_array['data']['banner'][0]['Store'];
				if($storeaccess != '')
				{
					$strAccess=explode(',', $storeaccess);
                	$data['storeaccess'] = $strAccess;
				}
				else{
					$data['storeaccess'] = array();
				}
               
				$data['title'] = 'Banner Details';
				$data['viewPage'] = 'control/banner/edit_banner';
				return view('control/includes/master_view',compact('data','bannerarr','storearr'));
			}
			else
			{
				return redirect('/oms/banner/bannerlist');
				
			}
		}
		else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
        
    }
    
	//=======================banner list===========================//
    public function getList()
    {
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$banner_array=array();
        if($userId != '')
        {
				//======== GET PROMO LIST FROM API =========//
				$curlUrl = 'api/banner';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_banner_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'bannerid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$banner = $this->fireCurl($url,$curlPostData);
				$banner_array = json_decode($banner,1);
			//echo "<pre>";
			//print_r($banner_array);
			//die();
            if(!empty($banner_array) && $banner_array['status'] == 1)
            {
	            $data['bannerlist'] = $banner_array['data']['banner'];
				$data['title'] = 'Banner';
				$data['viewPage'] = 'control/banner/banner_list';
				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['bannerlist'] = array();
				$data['title'] = 'Banner';
				$data['viewPage'] = 'control/banner/banner_list';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }
	
	
	//=================edit banner details=========================//
	public function updateBannerDtls(Request $request)
	{
		$bannerFor=$request->post('bannerfor');
		$bannerName=$request->post('bannerName');
		$bannerText=$request->post('bannerText');
		$bannerOtherAttributes=$request->post('bannerOtherAttributes');
		$bannerURL=$request->post('bannerURL');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$bannerId=$request->post('bannerId');
		$store=$request->post('store');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}

		$startdate=$request->post('startDate');
		$startDate=date('Y-m-d',strtotime($startdate));
		$startTime=$request->post('startTime');
		$end_date=$request->post('endDate');
		$endDatenew=date('Y-m-d',strtotime($end_date));
		//$endDatenew=date('Y-m-d',strtotime($enddate));
		$endTime=$request->post('endTime');
		$file=$request->file('image');
		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== update banner =========//
		$curlUrl = 'api/banner';
	//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActiveVal;//die();
		//echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $bannerFor!='' && $bannerName!='' && $bannerText!='' && $bannerURL!='' && $displayOrder!='' && $IsActiveVal!='' && $startDate!='' && $startTime!='' && $endDatenew!='' && $endTime!='')
		{
			if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
			{
				$imagesfile= $file->getClientSize();
				$Image1FileSize=filesize($file);//$imagesfile;
				$Image1FileName = $file->getClientOriginalName();//['filename'];//$request->file('input_pdf')->getClientOriginalName();
				//echo $Image1FileSize.'@@@@'.$Image1FileName;die();
				$fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
				list($width,$height) = getimagesize($file);
				//echo $width.'       '.$height;die();
				$image_content = file_get_contents($file);
				$path = 'uploads/banner_images/';
				$thumb_path = 'uploads/banner_images/thumbnail/';
				$fileNormal = $path.$fileName;
				$fileThumb = $thumb_path.$fileName;
			   
				$this->uploadImage('public',$fileNormal,$image_content);
				$this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
			   
			}
			else
			{
				$fileName='';
				$Image1FileSize='';
				$Image1FileName ='';
			}	
				$curlPostData = array(
							'mode' => 'insert_banner_details',
							'userId'=>$userId,
							'bannerid'=>$bannerId,
							'login_device_type'=>3,
							'BannerFor'=>$bannerFor,
							'BannerName'=>$bannerName,
							'BannerText'=>$bannerText,
							'BannerURL'=>$bannerURL,
							'BannerOtherAttributes'=>$bannerOtherAttributes,
							'IsActive'=>$IsActiveVal,
							'DisplayOrder' => $displayOrder,
							'image'=>$fileName,
							'Image1FileSize'=>$Image1FileSize,
							'Image1FileName'=>$Image1FileName,
							'Store' =>(string)$strAccess,
							'StartDate'=>$startDate,
							'StartTime'=>$startTime,
							'EndDate'=>$endDatenew,
							'EndTime'=>$endTime
							);
				$url = $baseUrl.$curlUrl;
				
				//echo "<pre>";
				//print_r($curlPostData);
				//die();
				$banner = $this->fireCurl($url,$curlPostData);
				$banner_array = json_decode($banner,1);
				//echo "<pre>";print_r($banner_array);die();
				if($banner_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$banner_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$banner_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/banner/bannerlist');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/banner/bannerlist');
		}
	}
	//==============================load add banner view================================//
	public function addBanner()
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
        //echo $userId.'+++'.$Is_superadmin.'+++'. $storeId;die();
		if($userId != '')
        {
			$lastRow='';
			//======== GET STORE DETAILS FROM API =========//
	            $curlUrl = 'api/StoreManagement';
	            $curlPostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent storeid = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$store = $this->fireCurl($url,$curlPostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
			$data['title'] = 'Banner';
	        $data['viewPage'] = 'control/banner/add_banner';
	        return view('control/includes/master_view',compact('data'));
	    }
	    else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

	}
	

	//================================insert banner details===========================//
	public function insertBannerDtls(Request $request)
	{
		
		$bannerFor=$request->post('bannerfor');
		$bannerName=$request->post('bannerName');
		$bannerText=$request->post('bannerText');
		$bannerOtherAttributes=$request->post('bannerOtherAttributes');
		$bannerURL=$request->post('bannerURL');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$bannerId=$request->post('bannerId');
		$store=$request->post('store');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}
		$startdate=$request->post('startDate');
		$startDate=date('Y-m-d',strtotime($startdate));
		$startTime=$request->post('startTime');
		$end_date=$request->post('endDate');
		$endDatenew=date('Y-m-d',strtotime($end_date));
		//$endDatenew=date('Y-m-d',strtotime($enddate));
		$endTime=$request->post('endTime');
		$file=$request->file('image');

		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== insert banner =========//
		$curlUrl = 'api/banner';
		//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActive;echo "</pre>";
		// echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $bannerFor!='' && $bannerName!='' && $bannerText!='' && $bannerURL!='' && $displayOrder!='' && $IsActiveVal!='' && $startDate!='' && $startTime!='' && $endDatenew!='' && $endTime!=''  && !empty($file))
		{
			if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
			{
				$imagesfile= $file->getClientSize();
				$Image1FileSize=filesize($file);//$imagesfile;
				$Image1FileName = $file->getClientOriginalName();//['filename'];//$request->file('input_pdf')->getClientOriginalName();
				//echo $Image1FileSize.'@@@@'.$Image1FileName;die();
				$fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
				list($width,$height) = getimagesize($file);
				//echo $width.'       '.$height;die();
				$image_content = file_get_contents($file);
				$path = 'uploads/banner_images/';
				$thumb_path = 'uploads/banner_images/thumbnail/';
				$fileNormal = $path.$fileName;
				$fileThumb = $thumb_path.$fileName;
			   
				$this->uploadImage('public',$fileNormal,$image_content);
				$this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
			   
			}
			else
			{
				$fileName='';
				$Image1FileSize='';
				$Image1FileName ='';
			}	
				$curlPostData = array(
							'mode' => 'insert_banner_details',
							'userId'=>$userId,
							'bannerid'=>$bannerId,
							'login_device_type'=>3,
							'BannerFor'=>$bannerFor,
							'BannerName'=>$bannerName,
							'BannerText'=>$bannerText,
							'BannerURL'=>$bannerURL,
							'BannerOtherAttributes'=>$bannerOtherAttributes,
							'IsActive'=>$IsActiveVal,
							'DisplayOrder' => $displayOrder,
							'image'=>$fileName,
							'Image1FileSize'=>$Image1FileSize,
							'Image1FileName'=>$Image1FileName,
							'Store' =>$strAccess,
							'StartDate'=>$startDate,
							'StartTime'=>$startTime,
							'EndDate'=>$endDatenew,
							'EndTime'=>$endTime
							);
				$url = $baseUrl.$curlUrl;
				
			//echo "<pre>";
			//print_r($curlPostData);
			//die();
				$banner = $this->fireCurl($url,$curlPostData);
				$banner_array = json_decode($banner,1);
				
				if($banner_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$banner_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$banner_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/banner/bannerlist');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/banner/bannerlist');
		}
		
		
	}
	
	//================================change status===================================//
	public function changeStatus(Request $request)
	{
		$bannerid=$request->segment(4);
		$status=$request->segment(5);
		
		$whereArr=array('BannerID'=>$bannerid);
		$updatearr=array('IsActive'=>$status);
		
        $updatebannerDetails=$this->update('banners',$whereArr,$updatearr);
		
		$request->session()->put('success_msg','Status updated successfully!');
		
		 return Redirect::to('/oms/banner/bannerlist');
	}
	

	///=============== delete a banner ====================//
	public function deletebanner(Request $request)
	{
		$id = $request->segment(4);
		if($id !='')
		{
			$ConditionBanner=array('BannerID'=>$id);
			$fetchbanner=$this->getAllData('banners',$ConditionBanner,'BannerID','DESC');
			//echo "<pre>";print_r($fetchbanner);die();
			if(!empty($fetchbanner))
			{
				$previmage=$fetchbanner[0]['Image1SavedFileName'];
				$delete = $this->delete('banners',array('BannerID'=>$id));
				if($delete)
				{
					$normalpath=storage_path().'/app/'.'public/uploads/banner_images/'.$previmage;
					$thumbnailpath=storage_path().'/app/'.'public/uploads/banner_images/thumbnail/'.$previmage;
					
					if(file_exists($normalpath))
					{
						unlink(storage_path().'/app/'.'public/uploads/banner_images/'.$previmage);
					}
					if(file_exists($thumbnailpath))
					{
						unlink(storage_path().'/app/'.'public/uploads/banner_images/thumbnail/'.$previmage);
					}
					$request->session()->put('success_msg',"Banner deleted.");
				}
				else
				{
					$request->session()->put('error_msg',"Failed to delete banner.");
				}
				
			}
			else
			{
				$request->session()->put('error_msg',"Please provide valid details.");
			}
		}
		else
		{
			$request->session()->put('error_msg',"Please provide valid details.");
		}
		return Redirect::to('/oms/banner/bannerlist');
	}
}
   
   
?>