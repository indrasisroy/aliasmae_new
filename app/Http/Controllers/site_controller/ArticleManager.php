<?php
namespace App\Http\Controllers\site_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Mail\addSubadminTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\Storage;  /*for file upload*/
use Redirect;
use Session;
use Excel;
use Image;

class ArticleManager extends MyController
{
	//================Article details==============//
    public function getDetails(Request $request)
    {
		//echo $id;die();
		$bannerid=base64_decode($request->segment(4));
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		if($userId != '')
        {
		
			$curlUrl = 'api/article';
			$lastRow='';
			
			$curlPostData = array(
								'mode' => 'get_article_list',
								'lastRow' => $lastRow,
								'userId'=>$userId,
								'articleid'=>$bannerid,
								'storeId' =>$userStoretoreId,
								'flag'=>'0'
							);
			$url = $baseUrl.$curlUrl;
			$article = $this->fireCurl($url,$curlPostData);
			$article_array = json_decode($article,1);
			//echo "<pre>";print_r($banner_array);die();
			//======== GET STORE DETAILS FROM API =========//
	            $curlstoreUrl = 'api/StoreManagement';
	            $curlstorePostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlstoreUrl;
				$store = $this->fireCurl($url,$curlstorePostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
			$articlearr=array();
			if(!empty($article_array['data']) && $article_array['status'] == 1)
			{
				$articlearr=$article_array['data']['article'][0];
				$storeaccess=$article_array['data']['article'][0]['Store'];
				if($storeaccess != '')
				{
					$strAccess=explode(',', $storeaccess);
                	$data['storeaccess'] = $strAccess;
				}
				else{
					$data['storeaccess'] = array();
				}
               
				$data['title'] = 'Page Details';
				$data['viewPage'] = 'control/article/edit_article';
				return view('control/includes/master_view',compact('data','articlearr','storearr'));
			}
			else
			{
				return redirect('/oms/page/page-list');
				
			}
		}
		else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
        
    }
    
	//=======================Article list===========================//
    public function getList()
    {
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$banner_array=array();
        if($userId != '')
        {
				//======== GET Article LIST FROM API =========//
				$curlUrl = 'api/article';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_article_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'articleid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$article = $this->fireCurl($url,$curlPostData);
				$article_array = json_decode($article,1);
			//echo "<pre>";
			//print_r($banner_array);
			//die();
            if(!empty($article_array) && $article_array['status'] == 1)
            {
	            $data['articlelist'] = $article_array['data']['article'];
				$data['title'] = 'Page';
				$data['viewPage'] = 'control/article/article_list';
				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['articlelist'] = array();
				$data['title'] = 'Page';
				$data['viewPage'] = 'control/article/article_list';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }
	
	
	//=================edit Article details=========================//
	public function updateArticleDtls(Request $request)
	{
		$articleName=$request->post('articleName');
		$articlecontent=$request->post('body_html');
		/*$bannerText=$request->post('bannerText');
		$bannerOtherAttributes=$request->post('bannerOtherAttributes');
		$bannerURL=$request->post('bannerURL');*/
		//$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$articleId=$request->post('articleId');
		$store=$request->post('store');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}

		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== update article =========//
		$curlUrl = 'api/article';
	//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActiveVal;//die();
		//echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $articleName!='' && $articleId!='' && $articlecontent!='' && $IsActiveVal!='' )
		{
				$curlPostData = array(
							'mode' => 'insert_article_details',
							'userId'=>$userId,
							'articleid'=>$articleId,
							'ArticleName'=>$articleName,
							'ArticleContent'=>$articlecontent,
							'IsActive'=>$IsActiveVal,
							'IsFeatured' => '0',
							'Store' =>(string)$strAccess,
							'PageTitle'=>'',
							'PageKeywords'=>'',
							'PageDescription'=>''
							);
				$url = $baseUrl.$curlUrl;
				
				//echo "<pre>";
				//print_r($curlPostData);
				//die();
				$article = $this->fireCurl($url,$curlPostData);
				$article_array = json_decode($article,1);
				//echo "<pre>";print_r($banner_array);die();
				if($article_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$article_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$article_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/page/page-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/page/page-list');
		}
	}
	//==============================load add Article view================================//
	public function addArticle()
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		if($userId != '')
        {
			$lastRow='';
			//======== GET STORE DETAILS FROM API =========//
	            $curlUrl = 'api/StoreManagement';
	            $curlPostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$store = $this->fireCurl($url,$curlPostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
			$data['title'] = 'Page';
	        $data['viewPage'] = 'control/article/add_article';
	        return view('control/includes/master_view',compact('data'));
	    }
	    else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

	}
	

	//================================insert Article details===========================//
	public function insertArticleDtls(Request $request)
	{
		
		$articleName=$request->post('articleName');
		$articlecontent=$request->post('body_html');
		/*$bannerText=$request->post('bannerText');
		$bannerOtherAttributes=$request->post('bannerOtherAttributes');
		$bannerURL=$request->post('bannerURL');*/
		//$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$articleId=$request->post('articleId');
		$store=$request->post('store');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}

		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== insert article =========//
		$curlUrl = 'api/article';
		//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActive;echo "</pre>";
		// echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $articleName!='' && $articlecontent!='' && $IsActiveVal!=''  )
		{
				$curlPostData = array(
							'mode' => 'insert_article_details',
							'userId'=>$userId,
							'articleid'=>'',
							'ArticleName'=>$articleName,
							'ArticleContent'=>$articlecontent,
							'IsActive'=>$IsActiveVal,
							'IsFeatured' => '0',
							'Store' =>(string)$strAccess,
							'PageTitle'=>'',
							'PageKeywords'=>'',
							'PageDescription'=>''
							);
				$url = $baseUrl.$curlUrl;
				
			//echo "<pre>";
			//print_r($curlPostData);
			//die();
				$article = $this->fireCurl($url,$curlPostData);
				$article_array = json_decode($article,1);
				//echo "<pre>";
			//print_r($article_array);
			//die();
				if($article_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$article_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$article_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/page/page-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/page/page-list');
		}
		
		
	}
	
	//================================change status===================================//
	public function changeStatus(Request $request)
	{
		$bannerid=$request->segment(4);
		$status=$request->segment(5);
		
		$whereArr=array('ArticleID'=>$bannerid);
		$updatearr=array('IsActive'=>$status);
		
        $updatebannerDetails=$this->update('articles',$whereArr,$updatearr);
		
		$request->session()->put('success_msg','Status updated successfully!');
		
		 return Redirect::to('/oms/page/page-list');
	}
	

	///=============== delete a Article ====================//
	public function deleteArticle(Request $request)
	{
		$id = $request->segment(4);
		if($id !='')
		{
			$ConditionBanner=array('ArticleID'=>$id);
			$fetcharticle=$this->getAllData('articles',$ConditionBanner,'ArticleID','DESC');
			//echo "<pre>";print_r($fetchbanner);die();
			if(!empty($fetcharticle))
			{
				$delete = $this->delete('articles',array('ArticleID'=>$id));
				if($delete)
				{
					$request->session()->put('success_msg',"Page deleted.");
				}
				else
				{
					$request->session()->put('error_msg',"Failed to delete article.");
				}
				
			}
			else
			{
				$request->session()->put('error_msg',"Please provide valid details.");
			}
		}
		else
		{
			$request->session()->put('error_msg',"Please provide valid details.");
		}
		return Redirect::to('/oms/page/page-list');
	}
}
   
   
?>