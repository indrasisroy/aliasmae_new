<?php
namespace App\Http\Controllers\site_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Mail\addSubadminTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\Storage;  /*for file upload*/
use Redirect;
use Session;
use Excel;
use Image;

class TypeManager extends MyController
{
	//================Brand details==============//
    public function getDetails(Request $request)
    {
		//echo $id;die();
		$producttypeid=base64_decode($request->segment(4));
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		if($userId != '')
        {
		
			$curlUrl = 'api/ProductType';
			$lastRow='';
			
			$curlPostData = array(
								'mode' => 'get_producttype_list',
								'lastRow' => $lastRow,
								'userId'=>$userId,
								'producttypeid'=>$producttypeid,
								'storeId' =>$userStoretoreId,
								'flag'=>'0'
							);
			$url = $baseUrl.$curlUrl;
			$brand = $this->fireCurl($url,$curlPostData);
			$brand_array = json_decode($brand,1);
			//echo "<pre>";print_r($brand_array);die();

			//======== GET MEASURE UNIT DETAILS FROM API =========//
	            $curlUrl = 'api/allproduct';
	            $curlPostData = array(
	                            'mode' => 'get_measureunit_List',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'measureunitid' => ''	                            
	                            );
	            $url = $baseUrl.$curlUrl;
				$measureunit = $this->fireCurl($url,$curlPostData);
	            $measureunit_array = json_decode($measureunit,1);
				 //echo "<pre>";
	           //print_r($measureunit_array);die;
	            if(!empty($measureunit_array) && $measureunit_array['status'] == 1)
	            {
	                $data['measureUnit'] = $measureunit_array['data']['measureUnit'];
	            }else{
	            	$data['measureUnit'] =array();
	            }
			//======== GET CATEGORY DETAILS FROM API =========//
	            $curlstoreUrl = 'api/StoreManagement';
	            $curlstorePostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlstoreUrl;
				$store = $this->fireCurl($url,$curlstorePostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
	        //=============== GET VARIANT COLOUR DATA =================//    	            
	            $curlUrl = 'api/allproduct';
	            $curlPostData = array(
	                            'mode' => 'get_variants_colour',
	                            'lastrow' => '',
	                            'flag' => 1	                            	                            
	                            );
	            $url = $baseUrl.$curlUrl;
				$measureunit = $this->fireCurl($url,$curlPostData);
	            $colour_array = json_decode($measureunit,1);
				 //echo "<pre>";
	           //print_r($colour_array);die;
	            if(!empty($colour_array) && $colour_array['status'] == 1)
	            {
	                $data['colourData'] = $colour_array['data']['colour'];
	            }else{
	            	$data['colourData'] =array();
	            }
	        //================ GET VARIANTS MATERIAL ===================//
	             $curlUrl = 'api/allproduct';
	            $curlPostData = array(
	                            'mode' => 'get_variants_materials',
	                            'lastrow' => '',
	                            'flag' => 1
	                            );
	            $url = $baseUrl.$curlUrl;
				$measureunit = $this->fireCurl($url,$curlPostData);
	            $material_array = json_decode($measureunit,1);
				 //echo "<pre>";
	        	 //print_r($material_array);die;
	            if(!empty($material_array) && $material_array['status'] == 1)
	            {
	                $data['MaterialData'] = $material_array['data']['Material'];
	            }else{
	            	$data['MaterialData'] =array();
	            }    
	        //=========================================================    
			$producttype=array();
			if(!empty($brand_array['data']) && $brand_array['status'] == 1)
			{
				$producttype      =$brand_array['data']['producttype'][0];
				$storeaccess      =$brand_array['data']['producttype'][0]['Store'];
				$measureUnitaccess=$brand_array['data']['producttype'][0]['measureUnitId'];
				$colourAccess     =$brand_array['data']['producttype'][0]['colourAccess'];
				$materialAccess   =$brand_array['data']['producttype'][0]['materialAccess'];

				if($storeaccess != '')
				{
					$strAccess=explode(',', $storeaccess);
                	$data['storeaccess'] = $strAccess;
				}
				else{
					$data['storeaccess'] = array();
				}

				if($measureUnitaccess != '')
				{
					$measureaccess=explode(',', $measureUnitaccess);
                	$data['measureUnitaccess'] = $measureaccess;
				}
				else{
					$data['measureUnitaccess'] = array();
				}

				if($colourAccess != '')
				{
					$colourAccessData=explode(',', $colourAccess);
                	$data['colourAccess'] = $colourAccessData;
				}
				else{
					$data['colourAccess'] = array();
				}

				if($materialAccess != '')
				{
					$materialAccessData=explode(',', $materialAccess);
                	$data['materialAccess'] = $materialAccessData;
				}
				else{
					$data['materialAccess'] = array();
				}
               
				$data['title'] = 'Product Type Details';
				$data['viewPage'] = 'control/producttype/edit_producttype';
				return view('control/includes/master_view',compact('data','producttype','storearr'));
			}
			else
			{
				return Redirect::to('/oms/producttype/type-list');
				
			}
		}
		else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
        
    }
    
	//=======================Brand list===========================//
    public function getList()
    {
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$banner_array=array();
        if($userId != '')
        {
				//======== GET BRAND LIST FROM API =========//
				$curlUrl = 'api/ProductType';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_producttype_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'producttypeid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				// echo "<pre>";
				// print_r($brand_array);
				// die();
            if(!empty($brand_array) && $brand_array['status'] == 1)
            {
	            $data['producttype'] = $brand_array['data']['producttype'];
				$data['title'] = 'Product Type';
				$data['viewPage'] = 'control/producttype/producttype_list';
				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['producttype'] = array();
				$data['title'] = 'Product Type ';
				$data['viewPage'] = 'control/producttype/producttype_list';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }
	
	
	//=================edit Type details=========================//
	public function updateTypeDtls(Request $request)
	{
		$brandName=$request->post('brandName');
		$displayInMenulist=$request->post('displayInMenulist');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$brandId=$request->post('brandId');
		$store=$request->post('store');
		$unitNameData =  $request->post('unitName');
		$colour = $request->post('colour');
		$material = $request->post('material');
		if(!empty($colour))
		{
			$colourAccess=implode(',', $colour);
		}
		else{
			$colourAccess='';
		}

		if(!empty($material))
		{
			$materialAccess=implode(',', $material);
		}
		else{
			$materialAccess='';
		}
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}
		if(!empty($unitNameData))
		{
			$unitNameDataAccess=implode(',', $unitNameData);
		}
		else{
			$unitNameDataAccess='';
		}

		 


		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		if($displayInMenulist =='')
		{
			$displaymenulist='0';
		}
		else{
			$displaymenulist=$displayInMenulist;
		}
		
		//======== update Product Type =========//
		$curlUrl = 'api/ProductType';
	//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActiveVal;//die();
		//echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $brandName!='' && $IsActiveVal!='' && $brandId!='')
		{	
			$curlPostData = array(
							'mode' => 'insert_producttype_details',
							'userId'=>(int)$userId,
							'producttypeid'=>(int)$brandId,
							'producttypeName'=>$brandName,
							'displayInMenulist'=>(int)$displaymenulist,
							'IsActive'=>(int)$IsActiveVal,
							'displaylevel' => (int)$displayOrder,
							'Store' =>$strAccess,
							'measureUnitId'=>$unitNameDataAccess,
							'colourAccess'=>$colourAccess,
							'materialAccess'=>$materialAccess
							);
			
			$url = $baseUrl.$curlUrl;
				
			// echo "<pre>";
			// print_r($curlPostData);
			// die();
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
			// echo "<pre>";
			// print_r($brand_array);
			// die();
				if($brand_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$brand_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$brand_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/producttype/type-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/producttype/type-list');
		}
	}
	//==============================load add brand view================================//
	public function addType()
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
        //echo $userId.'+++'.$Is_superadmin.'+++'. $storeId;die();
		if($userId != '')
        {
			$lastRow='';
			//======== GET STORE DETAILS FROM API =========//
	            $curlUrl = 'api/StoreManagement';
	            $curlPostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent storeid = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$store = $this->fireCurl($url,$curlPostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
	        //======== GET MEASURE UNIT DETAILS FROM API =========//
	            $curlUrl = 'api/allproduct';
	            $curlPostData = array(
	                            'mode' => 'get_measureunit_List',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'measureunitid' => ''	                            
	                            );
	            $url = $baseUrl.$curlUrl;
				$measureunit = $this->fireCurl($url,$curlPostData);
	            $measureunit_array = json_decode($measureunit,1);
				 //echo "<pre>";
	           //print_r($measureunit_array);die;
	            if(!empty($measureunit_array) && $measureunit_array['status'] == 1)
	            {
	                $data['measureUnit'] = $measureunit_array['data']['measureUnit'];
	            }else{
	            	$data['measureUnit'] =array();
	            }
	        //=============== GET VARIANT COLOUR DATA =================//    	            
	            $curlUrl = 'api/allproduct';
	            $curlPostData = array(
	                            'mode' => 'get_variants_colour',
	                            'lastrow' => '',
	                            'flag' => 1	                            	                            
	                            );
	            $url = $baseUrl.$curlUrl;
				$measureunit = $this->fireCurl($url,$curlPostData);
	            $colour_array = json_decode($measureunit,1);
				 //echo "<pre>";
	           //print_r($colour_array);die;
	            if(!empty($colour_array) && $colour_array['status'] == 1)
	            {
	                $data['colourData'] = $colour_array['data']['colour'];
	            }else{
	            	$data['colourData'] =array();
	            }
	        //================ GET VARIANTS MATERIAL ===================//
	             $curlUrl = 'api/allproduct';
	            $curlPostData = array(
	                            'mode' => 'get_variants_materials',
	                            'lastrow' => '',
	                            'flag' => 1
	                            );
	            $url = $baseUrl.$curlUrl;
				$measureunit = $this->fireCurl($url,$curlPostData);
	            $material_array = json_decode($measureunit,1);
				 //echo "<pre>";
	        	 //print_r($material_array);die;
	            if(!empty($material_array) && $material_array['status'] == 1)
	            {
	                $data['MaterialData'] = $material_array['data']['Material'];
	            }else{
	            	$data['MaterialData'] =array();
	            }
	        //====================================================================================
			$data['title'] = 'Add Product Type';
	        $data['viewPage'] = 'control/producttype/add_producttype';
	        return view('control/includes/master_view',compact('data'));
	    }
	    else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

	}
	

	//================================insert Brand details===========================//
	public function insertTypeDtls(Request $request)
	{
		
		// echo "<pre>";
		// print_r($_REQUEST);
		// die;
		$brandName=$request->post('productTypeName');
		$displayInMenulist=$request->post('displayInMenulist');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$brandId=$request->post('productTypeId');
		$store=$request->post('store');
		//$unitName = $request->post('unitName');
		$colour = $request->post('colour');
		$material = $request->post('material');

		$sizeEU = $request->post('size0'); 
		$sizeUSA = $request->post('size1'); 
		$sizeASIA = $request->post('size2');

		$sizeUnitEU = $request->post('size_unit0'); 
		$sizeUnitUSA = $request->post('size_unit00'); 
		$sizeUnitASIA = $request->post('size_unit000');
 		//======================= size data =====================	
 		if(!empty($sizeEU))
		{
			$sizeEUAccess=implode(',', $sizeEU);
		}
		else{
			$sizeEUAccess='';
		}

		if(!empty($sizeUSA))
		{
			$sizeUSAAccess=implode(',', $sizeUSA);
		}
		else{
			$sizeUSAAccess='';
		}

		if(!empty($sizeASIA))
		{
			$sizeASIAAccess=implode(',', $sizeASIA);
		}
		else{
			$sizeASIAAccess='';
		}

		//=================== size unit ==============
		if(!empty($sizeUnitEU))
		{
			$sizeUnitEUAccess=implode(',', $sizeUnitEU);
		}
		else{
			$sizeUnitEUAccess='';
		}

		if(!empty($sizeUnitUSA))
		{
			$sizeUnitUSAAccess=implode(',', $sizeUnitUSA);
		}
		else{
			$sizeUnitUSAAccess='';
		}
		
		if(!empty($sizeASIA))
		{
			$sizeUnitASIAAccess=implode(',', $sizeUnitASIA);
		}
		else{
			$sizeUnitASIAAccess='';
		}
		//================== size unit end ================

		if(!empty($colour))
		{
			$colourAccess=implode(',', $colour);
		}
		else{
			$colourAccess='';
		}

		if(!empty($material))
		{
			$materialAccess=implode(',', $material);
		}
		else{
			$materialAccess='';
		}


		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}

		// if(!empty($unitName))
		// {
		// 	$unitNameData=implode(',', $unitName);
		// }
		// else{
		// 	$unitNameData='';
		// }

		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		if($displayInMenulist =='')
		{
			$displaymenulist='0';
		}
		else{
			$displaymenulist=$displayInMenulist;
		}
		//======== insert brand =========//
		$curlUrl = 'api/ProductType';
		//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActive;echo "</pre>";
		if($userId !='' && $brandName!='' && $IsActiveVal!='')
		{
			$curlPostData = array(
							'mode'=>'insert_producttype_details',
							'userId'=>$userId,
							'producttypeid'=>'',
							'producttypeName'=>$brandName,
							'displayInMenulist'=>$displaymenulist,
							'IsActive'=>$IsActiveVal,
							'displaylevel' => $displayOrder,
							'Store' =>$strAccess,
							//'measureUnitId'=>$unitNameData,
							'colourAccess'=>$colourAccess,
							'materialAccess'=>$materialAccess,
							'sizeEUAccess'=>$sizeEUAccess,
							'sizeUSAAccess'=>$sizeUSAAccess,
							'sizeASIAAccess'=>$sizeASIAAccess,
							'sizeUnitEUAccess'=>$sizeUnitEUAccess,
							'sizeUnitUSAAccess'=>$sizeUnitUSAAccess,
							'sizeUnitASIAAccess'=>$sizeUnitASIAAccess
							);
			$url = $baseUrl.$curlUrl;
				
			// echo "<pre>";
			// print_r($curlPostData);
			// echo $url;
			// die();
			
				$brand1 = $this->fireCurl($url,$curlPostData); 
				$brand1_array = json_decode($brand1,1);
			// echo "<pre> hello there";
			// print_r($brand_array);
			// echo 'status is =>'.$brand_array['msg'];
		
			// die('hello there');
				if($brand1_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$brand1_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$brand1_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/producttype/type-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/producttype/type-list');
		}
		
		
	}
	
	//================================change status===================================//
	public function changeStatus(Request $request)
	{
		$producttypeid=$request->segment(4);
		$status=$request->segment(5);
		
		$whereArr=array('producttypeid'=>$producttypeid);
		$updatearr=array('status'=>$status);
		
        $updatebannerDetails=$this->update('ProductType',$whereArr,$updatearr);
		
		$request->session()->put('success_msg','Status updated successfully!');
		
		 return Redirect::to('/oms/producttype/type-list');
	}
	

	///=============== delete a brand ====================//
	public function deleteType(Request $request)
	{
		$id = $request->segment(4);
		if($id !='')
		{
			$ConditionCategory=array('producttypeid'=>$id);
			$fetchbrand=$this->getAllData('ProductType',$ConditionCategory,'producttypeid','DESC');
			//echo "<pre>";print_r($fetchbanner);die();
			if(!empty($fetchbrand))
			{
				$status=2;
				//$delete = $this->delete('brand',array('brandId'=>$id));
				$whereArr=array('producttypeid'=>$id);
				$updatearr=array('status'=>$status);
				
		        $updatebannerDetails=$this->update('ProductType',$whereArr,$updatearr);
				if($updatebannerDetails)
				{
					$request->session()->put('success_msg',"Product Type deleted.");
				}
				else
				{
					$request->session()->put('error_msg',"Failed to delete Product Type.");
				}
				
			}
			else
			{
				$request->session()->put('error_msg',"Please provide valid details.");
			}
		}
		else
		{
			$request->session()->put('error_msg',"Please provide valid details.");
		}
		return Redirect::to('/oms/producttype/type-list');
	}
}
   
   
?>