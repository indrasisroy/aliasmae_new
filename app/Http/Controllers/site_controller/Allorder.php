<?php
namespace App\Http\Controllers\site_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Mail\addSubadminTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\Storage;  /*for file upload*/
use Redirect;
use Session;
use Excel;
use Image;

class Allorder extends MyController
{
	public function getList(Request $request)
    {

        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }

		if($userId != '')
        {

        		//======== GET Order LIST FROM API =========//
				// $curlUrl = 'api/allorder';
				// $lastRow='';
				
				// $curlPostData = array(
				// 					'mode' => '',
				// 					'lastRow' => $lastRow,
				// 					'userId'=>$userId,
				// 					'MemberID'=>'',									
				// 					'flag'=>'1'
				// 				);
					
				// $url = $baseUrl.$curlUrl;
				// $customer = $this->fireCurl($url,$curlPostData);
				// $customer_array = json_decode($customer,1);
						
				// 		echo "<pre>";
				// 	print_r($customer_array);
				// die();

				$data['customer'] = array();//$customer_array['data']['customer'];
				$data['title'] = 'Order';
				$data['viewPage'] = 'control/order/order_list';
				return view('control/includes/master_view',compact('data'));

        }
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

    }
 




 }
?>