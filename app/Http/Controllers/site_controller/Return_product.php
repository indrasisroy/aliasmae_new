<?php

namespace App\Http\Controllers\site_controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;  //adedd for test auth
//use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;  /*for auth user*/
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use App\Mail\sendMyMail;  /*send mail class*/
use Redirect;
use Cookie;
use Session;
class Return_product extends MyController
{
    public function __construct(Request $request)
    {
        //print_r(Session::all());die();
        /*if(Session::get('userId') != null && Session::has('is_loggedin') != null)
        {
            Redirect::to('admin/dashboard')->send();
        }*/
    }
    
    public function index()
    {
        /*if(isset($_REQUEST['dev']) && $_REQUEST['dev'] == '1')
        {
            echo "<pre>";
            print_r($_SERVER);die;
        }*/
        $pageName='return';
        return view('front/return/return', compact('pageName'));
    }

}
?>