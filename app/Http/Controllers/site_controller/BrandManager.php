<?php
namespace App\Http\Controllers\site_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Mail\addSubadminTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\Storage;  /*for file upload*/
use Redirect;
use Session;
use Excel;
use Image;

class BrandManager extends MyController
{
	//================Brand details==============//
    public function getDetails(Request $request)
    {
		//echo $id;die();
		$brandid=base64_decode($request->segment(4));
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		if($userId != '')
        {
		
			$curlUrl = 'api/brand';
			$lastRow='';
			
			$curlPostData = array(
								'mode' => 'get_brand_list',
								'lastRow' => $lastRow,
								'userId'=>$userId,
								'brandid'=>$brandid,
								'storeId' =>$userStoretoreId,
								'flag'=>'0'
							);
			$url = $baseUrl.$curlUrl;
			$brand = $this->fireCurl($url,$curlPostData);
			$brand_array = json_decode($brand,1);
			//echo "<pre>";print_r($banner_array);die();
			//======== GET CATEGORY DETAILS FROM API =========//
	            $curlstoreUrl = 'api/StoreManagement';
	            $curlstorePostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlstoreUrl;
				$store = $this->fireCurl($url,$curlstorePostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
			$brandarr=array();
			if(!empty($brand_array['data']) && $brand_array['status'] == 1)
			{
				$brandarr=$brand_array['data']['brand'][0];
				$storeaccess=$brand_array['data']['brand'][0]['Store'];
				if($storeaccess != '')
				{
					$strAccess=explode(',', $storeaccess);
                	$data['storeaccess'] = $strAccess;
				}
				else{
					$data['storeaccess'] = array();
				}
               
				$data['title'] = 'Brand Details';
				$data['viewPage'] = 'control/brand/edit_brand';
				return view('control/includes/master_view',compact('data','brandarr','storearr'));
			}
			else
			{
				return redirect('/oms/brand/brand-list');
				
			}
		}
		else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
        
    }
    
	//=======================Brand list===========================//
    public function getList()
    {
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$banner_array=array();
        if($userId != '')
        {
				//======== GET BRAND LIST FROM API =========//
				$curlUrl = 'api/brand';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_brand_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'brandid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
			//echo "<pre>";
		//print_r($category_array);
	//die();
            if(!empty($brand_array) && $brand_array['status'] == 1)
            {
	            $data['brandlist'] = $brand_array['data']['brand'];
				$data['title'] = 'Brand';
				$data['viewPage'] = 'control/brand/brand_list';
				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['brandlist'] = array();
				$data['title'] = 'Brand ';
				$data['viewPage'] = 'control/brand/brand_list';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }
	
	
	//=================edit brand details=========================//
	public function updateBrandDtls(Request $request)
	{
		$brandName=$request->post('brandName');
		$displayInMenulist=$request->post('displayInMenulist');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$brandId=$request->post('brandId');
		$store=$request->post('store');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}

		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		if($displayInMenulist =='')
		{
			$displaymenulist='0';
		}
		else{
			$displaymenulist=$displayInMenulist;
		}
		
		//======== update brand =========//
		$curlUrl = 'api/brand';
	//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActiveVal;//die();
		//echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $brandName!='' && $IsActiveVal!='' && $brandId!='')
		{	
			$curlPostData = array(
							'mode' => 'insert_brand_details',
							'userId'=>$userId,
							'brandid'=>$brandId,
							'brandName'=>$brandName,
							'displayInMenulist'=>$displaymenulist,
							'IsActive'=>$IsActiveVal,
							'displaylevel' => $displayOrder,
							'Store' =>$strAccess
							);
			$url = $baseUrl.$curlUrl;
				
			//echo "<pre>";
			//print_r($curlPostData);
			//die();
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
			//echo "<pre>";
			//print_r($brand_array);
			//die();
				if($brand_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$brand_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$brand_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/brand/brand-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/brand/brand-list');
		}
	}
	//==============================load add brand view================================//
	public function addBrand()
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
        //echo $userId.'+++'.$Is_superadmin.'+++'. $storeId;die();
		if($userId != '')
        {
			$lastRow='';
			//======== GET STORE DETAILS FROM API =========//
	            $curlUrl = 'api/StoreManagement';
	            $curlPostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent storeid = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$store = $this->fireCurl($url,$curlPostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
			$data['title'] = 'Brand';
	        $data['viewPage'] = 'control/brand/add_brand';
	        return view('control/includes/master_view',compact('data'));
	    }
	    else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

	}
	

	//================================insert Brand details===========================//
	public function insertBrandDtls(Request $request)
	{
		
		$brandName=$request->post('brandName');
		$displayInMenulist=$request->post('displayInMenulist');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$brandId=$request->post('brandId');
		$store=$request->post('store');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}

		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		if($displayInMenulist =='')
		{
			$displaymenulist='0';
		}
		else{
			$displaymenulist=$displayInMenulist;
		}
		//======== insert brand =========//
		$curlUrl = 'api/brand';
		//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActive;echo "</pre>";
		if($userId !='' && $brandName!='' && $IsActiveVal!='')
		{
			$curlPostData = array(
							'mode' => 'insert_brand_details',
							'userId'=>$userId,
							'brandid'=>'',
							'brandName'=>$brandName,
							'displayInMenulist'=>$displaymenulist,
							'IsActive'=>$IsActiveVal,
							'displaylevel' => $displayOrder,
							'Store' =>$strAccess
							);
			$url = $baseUrl.$curlUrl;
				
			//echo "<pre>";
			//print_r($curlPostData);
			//die();
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
			//echo "<pre>";
			//print_r($brand_array);
			//die();
				if($brand_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$brand_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$brand_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/brand/brand-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/brand/brand-list');
		}
		
		
	}
	
	//================================change status===================================//
	public function changeStatus(Request $request)
	{
		$categoryid=$request->segment(4);
		$status=$request->segment(5);
		
		$whereArr=array('brandId'=>$categoryid);
		$updatearr=array('status'=>$status);
		
        $updatebannerDetails=$this->update('brand',$whereArr,$updatearr);
		
		$request->session()->put('success_msg','Status updated successfully!');
		
		 return Redirect::to('/oms/brand/brand-list');
	}
	

	///=============== delete a brand ====================//
	public function deleteBrand(Request $request)
	{
		$id = $request->segment(4);
		if($id !='')
		{
			$ConditionCategory=array('brandId'=>$id);
			$fetchbrand=$this->getAllData('brand',$ConditionCategory,'brandId','DESC');
			//echo "<pre>";print_r($fetchbanner);die();
			if(!empty($fetchbrand))
			{
				$status=2;
				//$delete = $this->delete('brand',array('brandId'=>$id));
				$whereArr=array('brandId'=>$id);
				$updatearr=array('status'=>$status);
				
		        $updatebannerDetails=$this->update('brand',$whereArr,$updatearr);
				if($updatebannerDetails)
				{
					$request->session()->put('success_msg',"Brand deleted.");
				}
				else
				{
					$request->session()->put('error_msg',"Failed to delete brand.");
				}
				
			}
			else
			{
				$request->session()->put('error_msg',"Please provide valid details.");
			}
		}
		else
		{
			$request->session()->put('error_msg',"Please provide valid details.");
		}
		return Redirect::to('/oms/brand/brand-list');
	}
}
   
   
?>