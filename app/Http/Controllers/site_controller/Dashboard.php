<?php

namespace App\Http\Controllers\site_controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
//use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mail\sendMyMail;  /*send mail class*/
use Redirect;
use Session;
class Dashboard extends MyController
{
    
    //====== load dashboard page =========//
    public function index()
    {
        $data['title'] = 'dashboard';
        $product_count = $kiosk_count =0;
        //userdetails fetch
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $loginUserId = Session::get('loginUserId'); //loginUserIdIs_superadmin
        $lastRow='';
        //echo $userId.'+++'.$loginUserId ;die();
        if($userId != '')
        {
            $curlUserUrl = 'api/connectSite';
            $curlUserPostData = array(
                            'mode' => 'getuserdetails',
                            //'lastRow' => $lastRow,
                            'userId' => $userId
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $Userurl = $baseUrl.$curlUserUrl;
            $user = $this->fireCurl($Userurl,$curlUserPostData);
            $user_array = json_decode($user,1);
            if($user_array['status'] == 1)
            {
                $data['Username']=$user_array['data']['userDetails'][0]['email'];
                $data['FirstName']=$user_array['data']['userDetails'][0]['firstname'];
                if(!empty($user_array['data']['storeLocation']) && !empty($user_array['data']['CountryName']))
                {
                    $seperate=','; 
                }
                else{
                    $seperate='';
                }
                $data['Location']=$user_array['data']['storeLocation'].$seperate.$user_array['data']['CountryName'];
            }
            else{
                $data['Username']='';
                $data['FirstName']='';
                $data['Location']='';
            }
            /*echo "<pre>";
            print_r($user_array);
            die();*/
            $curlUrl = 'api/connectSite';
            $curlPostData = array(
                            'mode' => 'totalhitcount',
                            'userId'=>$userId 
                            );
            $url = $baseUrl.$curlUrl;
            $totalHits = $this->fireCurl($url,$curlPostData);
            $totalHits_array = json_decode($totalHits,1);
            //echo "<pre>";print_r($totalHits_array);die();
            if($totalHits_array['status'] == 1)
            {
                $data['totalHits']=$totalHits_array['data']['totalhitcount'];
            }
            else{
                $data['totalHits']='';
            }
            //$product_count = DB::connection('mongodb')->collection('cbd_products')->where(array('userId'=>(string)$userId))->count();//all product count 

            $data['viewPage'] = 'control/home/home';
            return view('control/includes/master_view',compact('data'));
            
        }
        else{
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }	
        
    }
   /////========= logout form anytime =========/////////
    public function logout(Request $request)
    {
        //$this->update('admin',array('id'=>Session::get('loginUserId')),array('islogin'=>'0'));
        Session::flush();
        $request->session()->put('success_msg',"Logged out successfully!");
        Redirect::to('/oms')->send();
    }
    public function changestoretype(Request $request){

        // echo '<pre>';
        // print_r($_REQUEST);
        $storeid = $request->input('storename');
        
        $request->session()->put('storeId',$storeid);
        // die;

    }
}
?>