<?php

namespace App\Http\Controllers\site_controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
//use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Pagination\Paginator;  /*for pagination*/
use Illuminate\Support\Facades\DB;
use App\Mail\sendMyMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Redirect;
use Session;


class Store extends MyController
{
    
    //===================== LOAD ALL STORE LISTING ===================//
    public function index()
    {
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        if($userId != '')
        {
            $page='';
			$lastRow='';
			$limit=10;
			
			/*if(isset($_REQUEST['page']) && $_REQUEST['page'] != '')
			{
				$page=$_REQUEST['page'];
			}
			
            if($page == '' || $page == 0)
			{
				$page=1;
			}
			$lastRow = ($page-1) * $limit;*/

            // echo 
            
            //======== GET STORE LIST FROM API =========//
            $curlUrl = 'api/StoreManagement';
            $curlPostData = array(
                            'mode' => 'fetch_store',
                            'lastrow' => $lastRow,
                            'flag' => 1,
                            'userId' => $userId,
                            'storeid'=>''
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
			$store = $this->fireCurl($url,$curlPostData);
            $store_array = json_decode($store,1);
			// echo "<pre>";
           //print_r($store_array);die;
            if(!empty($store_array) && $store_array['status'] == 1)
            {
                $data['store'] = $store_array['data']['store'];
				$hasMore=$store_array['data']['pagination']['hasMore'];
				
				$data['pagination']=$store_array['data']['pagination']['totalpage'];
				$numOfTotal=$store_array['data']['pagination']['totaldata'];
				
				$pegi= $this->myPagination($numOfTotal,$limit,$page,$url='?',"");
				$data['pegi']=html_entity_decode($pegi);
                
            }
            else
            {
                $data['store'] = array();
                $data['pegi']='';
                $data['pagination']=array();
                
            }
            
            // print_r($data);
            
            $data['title'] = 'Store';
            $data['viewPage'] = 'control/store/store_list';
            return view('control/includes/master_view',compact('data'));
        }
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }
    
    //================ LOAD ADD NEW STORE PAGE =======================//
    public function addStore()
    {
    	$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        if($userId != '')
        {
        	$lastRow='';
        //================= get all pages =============//
        //$pages = $this->getAllData('page_management',array('status'=>'1'),'page_name','asc');
        //$data['pages'] = $pages;
        //======== GET STORE LIST FROM API =========//
            $curlUrl = 'api/StoreManagement';
            $curlPostData = array(
                            'mode' => 'fetch_country_details',
                            'lastrow' => $lastRow,
                            'userId' => $userId,
                            'countryid'=>'',
                            'flag' =>1
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
			$country = $this->fireCurl($url,$curlPostData);
            $country_array = json_decode($country,1);
            if(!empty($country_array) && $country_array['status'] == 1)
            {
            	$data['CountryDetails'] = $country_array['data']['country'];
            }
            else{

                $data['CountryDetails'] =array();
            }
        $data['title'] = 'add-store';
        $data['viewPage'] = 'control/store/add_store';
        return view('control/includes/master_view',compact('data'));
    	}
    	else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

    }
	
	//================================insert store details===========================//
	public function storeInsert(Request $request)
	{
		
		$storeName=$request->post('storeName');
		$storeLocation=$request->post('storeLocation');
		$country=$request->post('country');
		$IsActive=$request->post('IsActive');
		$StoreSize=$request->post('store');
		$currency=$request->post('currency');
		$storeEmail=$request->post('storeEmail');

		$storeAddress=$request->post('storeAddress');
		$StoreUnitNumber=$request->post('StoreUnitNumber');
		$StoreState=$request->post('StoreState');
		$StoreZipCode=$request->post('StoreZipCode');		
		$storePhoneNumber=$request->post('storePhoneNumber');




		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== insert store =========//
		$curlUrl = 'api/StoreManagement';
		if($userId !='' && $storeName!='' && $storeLocation!='' && $country!='' && $IsActiveVal!='')
		{	
			$curlPostData = array(
						'mode' => 'Add_new_store',
						'userId'=> $userId,
						'storeName'=> $storeName,
						'storeLocation'=> $storeLocation,
						'storeCountryId'=> $country,
						'StoreCurrency' => $currency,
						'StoreEmail'    => $storeEmail,
						'StoreSize'=> $StoreSize,
						'status'=> $IsActiveVal,
						'storeAddress'=>$storeAddress,
						'StoreUnitNumber'=>$StoreUnitNumber,
						'StoreState'=>$StoreState,
						'StoreZipCode'=>$StoreZipCode,
						'storePhoneNumber'=>$storePhoneNumber
						);
			$url = $baseUrl.$curlUrl;	
			//echo "<pre>";print_r($curlPostData);die();

				$store = $this->fireCurl($url,$curlPostData);
				$store_array = json_decode($store,1);
				//echo "<pre>";print_r($store_array);die();
	
				if($store_array['status'] == 1)
				{	
					$whereCondition=array();
					$allmenshoesize = $this->getAllData('MenShoeSize',$whereCondition,'SizeId','ASC');
			        $allwomenshoesize = $this->getAllData('WomenShoeSize',$whereCondition,'SizeId','ASC');
		            if(!empty($allmenshoesize) &&  !empty($allwomenshoesize))
					{
						$mensizecount=count($allmenshoesize);
						$womensizecount=count($allwomenshoesize);
						if($mensizecount > $womensizecount)
						{
							$totalcount=$mensizecount;
						}
						elseif($womensizecount > $mensizecount)
						{
							$totalcount=$womensizecount;
						}
						else{
							$totalcount=0;
						}
					}
					else{
						$totalcount=0;
					}
					for($i=0;$i<$totalcount;$i++)
					{
						$mensizeininch=$request->post('mensizeininch'.$i);
						$womensizeininch=$request->post('womensizeininch'.$i);
						$mensizeinmm=$request->post('mensizeinmm'.$i);
						$womensizeinmm=$request->post('womensizeinmm'.$i);
						$menshoesize=$request->post('menshoesize'.$i);
						$womenshoesize=$request->post('womenshoesize'.$i);
						$StoreId=$store_array['data']['insertId'];
						$Status=1;
						$dataToInsert=array(
											'StoreId' => $StoreId,
											'MenFootIninches' => $mensizeininch,
											'WomenFootIninches' => $womensizeininch,
											'MenFootInmm' => $mensizeinmm,
											'WomenFootInmm' => $womensizeinmm,
											'MenShoeSize' => $menshoesize,
											'WomenShoeSize' => $womenshoesize,
											'Status' => $Status
										);
						$addStoreShoeDetls = $this->insertDatafunc($dataToInsert,'StoreShoeSize');
						//echo "<pre>";print_r($dataToInsert);	
					}
					//die();
					$request->session()->put('success_msg',$store_array['message']);
					// $databaseStoreId='';
					$dataresp['resp']=$store_array['data']['insertId'];

					return view('control/includes/storeadddatabase',compact('dataresp'));
				}
				else
				{
					$request->session()->put('error_msg',$store_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/store/store-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/store/store-list');
		}
		
		
	}
	//================ LOAD EDIT STORE PAGE =======================//
    public function editStore(Request $request)
    {
		//echo $id;die();
		$storeid=base64_decode($request->segment(4));
    	$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        if($userId != '')
        {
        	$lastRow='';
        //================= get all pages =============//
        //$pages = $this->getAllData('page_management',array('status'=>'1'),'page_name','asc');
        //$data['pages'] = $pages;
        //======== GET STORE LIST FROM API =========//
            $curlUrl = 'api/StoreManagement';
            $curlPostData = array(
                            'mode' => 'fetch_country_details',
                            'lastrow' => $lastRow,
                            'userId' => $userId,
                            'countryid'=>'',
                            'flag' =>1
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
			$country = $this->fireCurl($url,$curlPostData);
            $country_array = json_decode($country,1);
            if(!empty($country_array) && $country_array['status'] == 1)
            {
            	$data['CountryDetails'] = $country_array['data']['country'];
            }
            else{

                $data['CountryDetails'] =array();
            }
             //======== GET STORE DETAILS FROM API =========//
            $curlUrl = 'api/StoreManagement';
            $curlPostData = array(
                            'mode' => 'fetch_store',
                            'lastrow' => $lastRow,
                            'flag' => 0,
                            'userId' => $userId,
                            'storeid'=>$storeid
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
			$store = $this->fireCurl($url,$curlPostData);
            $store_array = json_decode($store,1);
			 //echo "<pre>";
           //print_r($store_array);die;
            if(!empty($store_array) && $store_array['status'] == 1)
            {
                $data['store'] = $store_array['data']['store'][0];
            }else{
            	$data['store'] =array();
            }
            //======== GET STORE SIZE DETAILS FROM API =========//
        $whereCondition=array('StoreId' => $storeid);
        $allstoreshoesize = $this->getAllData('StoreShoeSize',$whereCondition,'storeSizeId','ASC');
        if(!empty($allstoreshoesize))
        {
        	$data['storesize'] =$allstoreshoesize;
        }
        else{
        	$data['storesize'] =array();
        }
        //echo "<pre>";
        //print_r($allstoreshoesize);die;
        $data['title'] = 'edit-store';
        $data['viewPage'] = 'control/store/edit_store';
        //echo "<pre>";
         //print_r($data);die;
        return view('control/includes/master_view',compact('data'));
    	}
    	else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

    }
    //================================Update store details===========================//
	public function storeUpdate(Request $request)
	{
		$storeId=$request->post('storeId');
		$storeName=$request->post('storeName');
		$storeLocation=$request->post('storeLocation');
		$country=$request->post('country');
		$IsActive=$request->post('IsActive');
		$StoreSize=$request->post('store');
		$currency=$request->post('currency');
		$storeEmail=$request->post('storeEmail');

		$storeAddress=$request->post('storeAddress');
		$StoreUnitNumber=$request->post('StoreUnitNumber');
		$StoreState=$request->post('StoreState');
		$StoreZipCode=$request->post('StoreZipCode');		
		$storePhoneNumber=$request->post('storePhoneNumber');		



		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== insert store =========//
		$curlUrl = 'api/StoreManagement';
		//echo "<pre>";echo $userId.'=='.$storeName.'=='.$storeLocation.'=='.$country.'=='.$IsActiveVal.'=='.$IsActive;//die();
		// echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $storeId!='' && $storeName!='' && $storeLocation!='' && $country!='' && $IsActiveVal!='')
		{	
			$curlPostData = array(
						'mode' => 'Edit_new_store',
						'userId'=>$userId,
						'storeId'=>$storeId,
						'storeName'=>$storeName,
						'storeLocation'=>$storeLocation,
						'storeCountryId'=>$country,
						'StoreCurrency' => $currency,
						'StoreEmail'    => $storeEmail,
						'StoreSize'=>$StoreSize,
						'status'=>$IsActiveVal,
						'storeAddress'=>$storeAddress,
						'StoreUnitNumber'=>$StoreUnitNumber,
						'StoreState'=>$StoreState,
						'StoreZipCode'=>$StoreZipCode,
						'storePhoneNumber'=>$storePhoneNumber
						);
			$url = $baseUrl.$curlUrl;
				
			//echo "<pre>";
			//print_r($curlPostData);
			//die();
				$store = $this->fireCurl($url,$curlPostData);
				$store_array = json_decode($store,1);
			//



				//echo "<pre>";
			//print_r($store_array);
			//die();
				if($store_array['status'] == 1)
				{	
					$whereCondition=array('StoreId' => $storeId);
        			$allstoreshoesize = $this->getAllData('StoreShoeSize',$whereCondition,'storeSizeId','ASC');
		            if(!empty($allstoreshoesize))
					{
						$totalcount=count($allstoreshoesize);
					
						for($i=0;$i<$totalcount;$i++)
						{
							$storeSizeinputID=$request->post('storesizeId'.$i);
							if(isset($storeSizeinputID))
							{
								$storeSizeID=$storeSizeinputID;
							}
							else
							{
								$storeSizeID=$allstoreshoesize[$i]['storeSizeId'];
							}
							$mensizeininch=$request->post('mensizeininch'.$i);
							$womensizeininch=$request->post('womensizeininch'.$i);
							$mensizeinmm=$request->post('mensizeinmm'.$i);
							$womensizeinmm=$request->post('womensizeinmm'.$i);
							$menshoesize=$request->post('menshoesize'.$i);
							$womenshoesize=$request->post('womenshoesize'.$i);
							//$StoreId=$store_array['data']['insertId'];
							$Status=1;
							$dataToInsert=array(
												'StoreId' => $storeId,
												'MenFootIninches' => $mensizeininch,
												'WomenFootIninches' => $womensizeininch,
												'MenFootInmm' => $mensizeinmm,
												'WomenFootInmm' => $womensizeinmm,
												'MenShoeSize' => $menshoesize,
												'WomenShoeSize' => $womenshoesize,
												'Status' => $Status
											);
							//echo "<pre>DataToinsert=";print_r($dataToInsert);
							$whereStoreSizeCondition=array('StoreId' => $storeId,'storeSizeId' => $storeSizeID);
	        				$getstoreshoesize = $this->getAllData('StoreShoeSize',$whereStoreSizeCondition,'storeSizeId','ASC');
	        				//echo "<pre>GETData=";print_r($getstoreshoesize);
	        				if(!empty($getstoreshoesize) && count($getstoreshoesize) == 1)
	        				{
	        					$whereArr=array('StoreId' => $storeId,'storeSizeId' => $storeSizeID);
	                            $updateDetails=$this->update('StoreShoeSize',$whereArr,$dataToInsert);

	        				}
	        				else
	        				{
	        					$addStoreShoeDetls = $this->insertDatafunc($dataToInsert,'StoreShoeSize');
	        				}
							//echo "<pre>";print_r($dataToInsert);	
						}
					}
					else
					{
						$whereCondition=array();
						$allmenshoesize = $this->getAllData('MenShoeSize',$whereCondition,'SizeId','ASC');
				        $allwomenshoesize = $this->getAllData('WomenShoeSize',$whereCondition,'SizeId','ASC');
			            if(!empty($allmenshoesize) &&  !empty($allwomenshoesize))
						{
							$mensizecount=count($allmenshoesize);
							$womensizecount=count($allwomenshoesize);
							if($mensizecount > $womensizecount)
							{
								$totalcount=$mensizecount;
							}
							elseif($womensizecount > $mensizecount)
							{
								$totalcount=$womensizecount;
							}
							else{
								$totalcount=0;
							}
						}
						else{
							$totalcount=0;
						}
						for($i=0;$i<$totalcount;$i++)
						{
							//$storeSizeID=$request->post('storesizeId'.$i);
							$mensizeininch=$request->post('mensizeininch'.$i);
							$womensizeininch=$request->post('womensizeininch'.$i);
							$mensizeinmm=$request->post('mensizeinmm'.$i);
							$womensizeinmm=$request->post('womensizeinmm'.$i);
							$menshoesize=$request->post('menshoesize'.$i);
							$womenshoesize=$request->post('womenshoesize'.$i);
							//$StoreId=$store_array['data']['insertId'];
							$Status=1;
							$dataToInsert=array(
												'StoreId' => $storeId,
												'MenFootIninches' => $mensizeininch,
												'WomenFootIninches' => $womensizeininch,
												'MenFootInmm' => $mensizeinmm,
												'WomenFootInmm' => $womensizeinmm,
												'MenShoeSize' => $menshoesize,
												'WomenShoeSize' => $womenshoesize,
												'Status' => $Status
											);
							$addStoreShoeDetls = $this->insertDatafunc($dataToInsert,'StoreShoeSize');
						}
					}
					//die();
					$request->session()->put('success_msg',$store_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$store_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/store/store-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/store/store-list');
		}
		
		
	}
	//================================change status===================================//
	public function changeStatus(Request $request)
	{
		$storeid=$request->segment(4);
		$status=$request->segment(5);
		
		$whereArr=array('storeid'=>$storeid);
		$updatearr=array('Status'=>$status);
		
        $updatebannerDetails=$this->update('Store',$whereArr,$updatearr);
		
		$request->session()->put('success_msg','Status updated successfully!');
		
		 return Redirect::to('/oms/store/store-list');
	}
	
	//=======================================Fetching Shoe Size with respect to country=====================//
	public function fetchShoeSize(Request $request)
	{
		$shoeValue=$request->post('shoevalue');
		//echo $shoeValue;die();
		$mensizecount=0;$womensizecount=0;
		$whereCondition=array();
        $allmenshoesize = $this->getAllData('MenShoeSize',$whereCondition,'SizeId','ASC');
        $allwomenshoesize = $this->getAllData('WomenShoeSize',$whereCondition,'SizeId','ASC');
        //echo "<pre>";print_r($allmenshoesize );print_r( $allwomenshoesize);die();
            if(!empty($allmenshoesize) &&  !empty($allwomenshoesize))
			{
				if(!empty($allmenshoesize))
				{
					$mensizecount=count($allmenshoesize);
				}
				if(!empty($allwomenshoesize))
				{
					$womensizecount=count($allwomenshoesize);
				}
				if($mensizecount > $womensizecount)
				{
					$totalcount=$mensizecount;
				}
				elseif($womensizecount > $mensizecount)
				{
					$totalcount=$womensizecount;
				}
				else{
					$totalcount=0;
				}
				if($shoeValue == 1)
				{ 
					$countrySize = '(US)';
				}
				if($shoeValue == 2)
				{ 
					$countrySize = '(EU)'; 
				} 
				if($shoeValue == 3) 
				{ 
					$countrySize = '(ASIA)';
				}
				$html1='<table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						 	
						 	<tr>
						 		<td class="titleRow borderR" align="center">
						 			<p><strong><input type="text" id="menInch" value="Men Foot (inches)" readonly></strong></p>
						 		</td>
						 		<td class="titleRow borderR" align="center">
						 			<p><strong><input type="text" id="menInch" value="Women Foot (inches)" readonly></strong></p>
						 		</td>
						 		<td class="titleRow borderR" align="center">
						 			<p><strong><input type="text" id="menmm" value="Men Foot (mm)" readonly></strong></p>
						 		</td>
						 		<td class="titleRow borderR" align="center">
						 			<p><strong><input type="text" id="womenmm" value="Women Foot (mm)" readonly></strong></p>
						 		</td>
						 		<td class="titleRow paddingL" align="center">
						 			<p><strong><input type="text" id="menshoe" value="Men Shoe Size '.$countrySize.'" readonly></strong></p>
						 		</td>
						 		<td class="titleRow paddingL" align="center">
						 			<p><strong><input type="text" id="womenshoe" value="Women Shoe Size '.$countrySize.'" readonly></strong></p>
						 		</td>
						 	</tr>';
						 	$html2 ='';
						 	for($i=0;$i< $totalcount;$i++)
						 	{
						 		if(isset($allmenshoesize[$i]['SizeInInches'])){$mensizeininch = $allmenshoesize[$i]['SizeInInches']; }else{$mensizeininch ='';}
						 		if(isset($allwomenshoesize[$i]['SizeInInches'])){$womensizeininch = $allwomenshoesize[$i]['SizeInInches']; }else{ $womensizeininch = ''; }
						 		if(isset($allmenshoesize[$i]['SizeInmm'])){$mensizeinmm = $allmenshoesize[$i]['SizeInmm']; }else{ $mensizeinmm = '';}
						 		if(isset($allwomenshoesize[$i]['SizeInmm']))
						 		{
						 			$womensizeinmm = $allwomenshoesize[$i]['SizeInmm']; 
						 		}
						 		else
						 		{ 
						 			$womensizeinmm = '';
						 		}
						 		if($shoeValue == 1)
						 		{ 
						 			if(isset($allmenshoesize[$i]['SizeInUS']))
					 				{
					 					$menshoesize = $allmenshoesize[$i]['SizeInUS']; 
					 				}
					 				else
						 			{ 
						 				$menshoesize =''; 
						 			}  
						 		}
						 		elseif($shoeValue == 2) 
						 		{ 
						 			if(isset($allmenshoesize[$i]['SizeInEU']))
						 			{
						 				$menshoesize = $allmenshoesize[$i]['SizeInEU']; 
						 			}
						 			else
						 			{ 
						 				$menshoesize =''; 
						 			}  
						 		}
						 		else
						 		{ 
						 			if(isset($allmenshoesize[$i]['SizeInAsia']))
						 			{
						 				$menshoesize = $allmenshoesize[$i]['SizeInAsia']; 
						 			}
						 			else
						 			{ 
						 				$menshoesize =''; 
						 			} 
						 		}
						 		if($shoeValue == 1){ if(isset($allwomenshoesize[$i]['SizeInUS'])){$womenshoesize = $allwomenshoesize[$i]['SizeInUS']; }else{ $womenshoesize =''; }  }elseif($shoeValue == 2) { if(isset($allwomenshoesize[$i]['SizeInEU'])){$womenshoesize = $allwomenshoesize[$i]['SizeInEU']; }else{ $womenshoesize =''; } }else{ if(isset($allwomenshoesize[$i]['SizeInAsia'])){$womenshoesize = $allwomenshoesize[$i]['SizeInAsia']; }else{ $womenshoesize = '';} }
						 		$html2.='<tr>
									 		<td class="borderR" align="center" width="20%">
									 			<p><input type="text" class="isNumberKey" id="mensizeininch'.$i.'" name="mensizeininch'.$i.'" value="'.$mensizeininch.'"></p>
									 		</td>
									 		<td class="borderR" align="center" width="20%">
									 			<p><input type="text" class="isNumberKey" id="womensizeininch'.$i.'" name="womensizeininch'.$i.'" value="'.$womensizeininch.'"></p>
									 		</td>
									 		<td class="borderR" align="center" width="20%">
									 			<p><input type="text" class="isNumberKey"  id="mensizeinmm'.$i.'" name="mensizeinmm'.$i.'" value="'.$mensizeinmm.'"></p>
									 		</td>
									 		<td class="borderR" align="center" width="20%">
									 			<p><input type="text" class="isNumberKey" id="womensizeinmm'.$i.'" name="womensizeinmm'.$i.'" value="'.$womensizeinmm.'"></p>
									 		</td>
									 		<td class="paddingL" align="center" width="20%">
									 			<p><input type="text" class="isNumberKey" id="menshoesize'.$i.'" name="menshoesize'.$i.'" value="'.$menshoesize.'"></p>
									 		</td>
									 		<td class="paddingL" align="center" width="20%">
									 			<p><input type="text" class="isNumberKey" id="womenshoesize'.$i.'" name="womenshoesize'.$i.'" value="'.$womenshoesize.'"></p>
									 		</td>
									 	</tr>';
						 	}
				$html3='</tbody></table>';
				$HTml=$html1.$html2.$html3;
				echo $HTml;
			}
			else{
				$HTml='';
				echo $HTml;
			}

	}
}
?>