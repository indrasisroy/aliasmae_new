<?php
namespace App\Http\Controllers\site_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Mail\addSubadminTest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\Storage;  /*for file upload*/
use Redirect;
use Session;
use Excel;
use Image;
use View;

class ProductManager extends MyController
{
	//================Product details==============//
    public function getDetails(Request $request)
    {
		//echo $id;die();
		$bannerid=base64_decode($request->segment(4));
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		if($userId != '')
        {
		
			$curlUrl = 'api/product';
			$lastRow='';
			
			$curlPostData = array(
								'mode' => 'get_product_list',
								'lastRow' => $lastRow,
								'userId'=>$userId,
								'productid'=>$bannerid,
								'storeId' =>$userStoretoreId,
								'flag'=>'0'
							);
			$url = $baseUrl.$curlUrl;
			$product = $this->fireCurl($url,$curlPostData);
			$product_array = json_decode($banner,1);
			//echo "<pre>";print_r($banner_array);die();
			//======== GET STORE DETAILS FROM API =========//
	            $curlstoreUrl = 'api/StoreManagement';
	            $curlstorePostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlstoreUrl;
				$store = $this->fireCurl($url,$curlstorePostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
			$productarr=array();
			if(!empty($banner_array['data']) && $banner_array['status'] == 1)
			{
				$productarr=$banner_array['data']['banner'][0];
				$storeaccess=$banner_array['data']['banner'][0]['Store'];
				if($storeaccess != '')
				{
					$strAccess=explode(',', $storeaccess);
                	$data['storeaccess'] = $strAccess;
				}
				else{
					$data['storeaccess'] = array();
				}
               
				$data['title'] = 'Product Details';
				$data['viewPage'] = 'control/product/edit_product';
				return view('control/includes/master_view',compact('data','bannerarr','storearr'));
			}
			else
			{
				return redirect('/oms/product/product-list');
				
			}
		}
		else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
        
    }
    
	//=======================Product list===========================//
    public function getList()
    {
    	// echo 1; die('hello there 1');
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$banner_array=array();
        if($userId != '')
        {
				//======== GET PRODUCT LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_product_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'bannerid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$product = $this->fireCurl($url,$curlPostData);
				$product_array = json_decode($product,1);
			// echo "<pre>";
			// print_r($product_array);
			// die();
				//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['categorylist'] = $category_array['data']['category'];
				}
				else
				{
					$data['categorylist'] = array();
				}
				

				
			//echo "<pre>";
				$product = array();
				//$product_array = array();product_array
            if(!empty($product_array) && $product_array['status'] == 1)
            {
	            $data['productlist'] = $product_array['data']['product'];
				$data['title'] = 'Product';
				$data['viewPage'] = 'control/product/product_list';				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['productlist'] = array();
				$data['title'] = 'Product';
				$data['viewPage'] = 'control/product/product_list';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }
	

	// copy 1
	//=======================Product list===========================//
    public function getList2_curl()
    {
    	// echo 2; die('hello there 2');
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$banner_array=array();
        if($userId != '')
        {
				//======== GET PRODUCT LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_product_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'bannerid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$product = $this->fireCurl($url,$curlPostData);
				$product_array = json_decode($product,1);
			// echo "<pre>";
			// print_r($product_array);
			// die();
				//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['categorylist'] = $category_array['data']['category'];
				}
				else
				{
					$data['categorylist'] = array();
				}
				




					// get search list parameters starts here 
					//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['categorylist'] = $category_array['data']['category'];
				}
				else
				{
					$data['categorylist'] = array();
				}
				//======== GET BRAND LIST FROM API =========//
				$curlUrl = 'api/brand';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_brand_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'brandid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['brandlist'] = $brand_array['data']['brand'];
				}
				else
				{
					$data['brandlist'] = array();
				}

				//============= GET VARIANTS COLOURS ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_variants_colour',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['variants_colour'] = $brand_array['data']['colour'];
				}
				else
				{
					$data['variants_colour'] = array();
				}
				//============= GET VARIANTS MATERIAL ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_variants_materials',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['variants_materials'] = $brand_array['data']['Material'];
				}
				else
				{
					$data['variants_materials'] = array();
				}
				//============= GET SEASON DATA ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_season_List',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['season'] = $brand_array['data']['Season'];
				}
				else
				{
					$data['season'] = array();
				}
				//======== GET PRODUCT TYPE LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_productType_List',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'categoryid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['ProductType'] = $category_array['data']['ProductType'];
				}
				else
				{
					$data['ProductType'] = array();
				}

				
				
				//==========================================================
				// get search list parameters ends here






				
			//echo "<pre>";
				$product = array();
				//$product_array = array();product_array
            if(!empty($product_array) && $product_array['status'] == 1)
            {
	            $data['productlist'] = $product_array['data']['product'];
				$data['title'] = 'Product';
				$data['viewPage'] = 'control/product/product_list1';				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['productlist'] = array();
				$data['title'] = 'Product';
				$data['viewPage'] = 'control/product/product_list1';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }

    // copy 2

    //=======================Product list===========================//
    public function getList3()
    {
    	// echo 3; die('hello there 3');
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$banner_array=array();
        if($userId != '')
        {
				//======== GET PRODUCT LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_product_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'bannerid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$product = $this->fireCurl($url,$curlPostData);
				$product_array = json_decode($product,1);
			// echo "<pre>";
			// print_r($product_array);
			// die();
				//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['categorylist'] = $category_array['data']['category'];
				}
				else
				{
					$data['categorylist'] = array();
				}
				

				
			//echo "<pre>";
				$product = array();
				//$product_array = array();product_array
            if(!empty($product_array) && $product_array['status'] == 1)
            {
	            $data['productlist'] = $product_array['data']['product'];
				$data['title'] = 'Product';
				$data['viewPage'] = 'control/product/product_list2';				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['productlist'] = array();
				$data['title'] = 'Product';
				$data['viewPage'] = 'control/product/product_list2';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }
	
	public function insertProductDtls()
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');

        echo $storeId;
        echo '<br>'.$Is_superadmin;
        echo '<br>'.$baseUrl;

        echo "<pre>";
        print_r($_REQUEST);
        print_r($_FILES);
        die;
        $body_html		=$request->post('body_html');
        $subBrand		=$request->post('subBrand');
		$producttype    =$request->post('producttype');
		$styleName      =$request->post('styleName');
		$factoryId      =$request->post('factoryId');
		$category       =$request->post('category');
		$subCategory    =$request->post('subCategory');
		$season 		=$request->post('season');
		$IsActive       =$request->post('IsActive');
		$IsNew          =$request->post('IsNew');
		$displayOrder   =$request->post('displayOrder');
		$barcode        =$request->post('barcode');
		$colour         =$request->post('colour');
		$colour2        =$request->post('colour2');
		$material       =$request->post('material');
		$material2      =$request->post('material2');
		$size           =$request->post('size');

		if($IsActive =='')
		{
			$IsActiveVal=	0;
		}
		else{
			$IsActiveVal=$IsActive;
		}

		if(!empty($barcode)){	$barcodeData	=	implode(',', $barcode);		} else{ $barcodeData	=	''; } //barcode data

		if(!empty($colour)){	$colourData	=	implode(',', $colour);		} else{ $colourData	=	''; } //color one data

		if(!empty($colour2)){	$colour2Data	=	implode(',', $colour2);		} else{ $colour2Data	=	''; } // color two data

		if(!empty($material)){	$materialData	=	implode(',', $material);		} else{ $materialData	=	''; } // material one data

		if(!empty($material2)){	$material2Data	=	implode(',', $material2);		} else{ $material2Data	=	''; } //material two data

		if(!empty($size)){	$sizeData	=	implode(',', $size);		} else{ $sizeData	=	''; } // size data

		
		//======== insert Product Data =========//
		$curlUrl = 'api/Allproduct';

		if($userId !='' && $factoryId!='' && $displayOrder!='' && $IsActiveVal!='' && $subBrand!='' && $producttype!= '' && $styleName!=''   && !empty($file))
		{
			// if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
			// {
			// 	$imagesfile= $file->getClientSize();
			// 	$Image1FileSize=filesize($file);//$imagesfile;
			// 	$Image1FileName = $file->getClientOriginalName();//['filename'];//$request->file('input_pdf')->getClientOriginalName();
			// 	//echo $Image1FileSize.'@@@@'.$Image1FileName;die();
			// 	$fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
			// 	list($width,$height) = getimagesize($file);
			// 	//echo $width.'       '.$height;die();
			// 	$image_content = file_get_contents($file);
			// 	$path = 'uploads/product_images/';
			// 	$thumb_path = 'uploads/product_images/thumbnail/';
			// 	$fileNormal = $path.$fileName;
			// 	$fileThumb = $thumb_path.$fileName;
			   
			// 	$this->uploadImage('public',$fileNormal,$image_content);
			// 	$this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
			   
			// }
			// else
			// {
			// 	$fileName='';
			// 	$Image1FileSize='';
			// 	$Image1FileName ='';
			// }

				$curlPostData = array(
							'mode' => 'addproductData',
							'userId'=>$userId,

							
							);
				$url = $baseUrl.$curlUrl;
				
				//echo "<pre>";
				//print_r($curlPostData);
				//die();
				$trend = $this->fireCurl($url,$curlPostData);
				$trend_array = json_decode($trend,1);
				
				if($trend_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$trend_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$trend_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/product/product-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/product/add-product');
		}





	}

	//=======================Shoe list===========================//
    public function getShoeList()
    {
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$banner_array=array();
        if($userId != '')
        {
				//======== GET PRODUCT LIST FROM API =========//
				/*$curlUrl = 'api/product';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_product_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'bannerid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$banner = $this->fireCurl($url,$curlPostData);
				$banner_array = json_decode($banner,1);*/
			//echo "<pre>";
			//print_r($banner_array);
			//die();
				//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['categorylist'] = $category_array['data']['category'];
				}
				else
				{
					$data['categorylist'] = array();
				}
			//echo "<pre>";
				$product = array();
				$product_array = array();
            if(!empty($product_array) && $product_array['status'] == 1)
            {
	            $data['productlist'] = $product_array['data']['product'];
				$data['title'] = 'Shoes';
				$data['viewPage'] = 'control/product/shoe_list';
				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['productlist'] = array();
				$data['title'] = 'Shoes';
				$data['viewPage'] = 'control/product/shoe_list';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }
	
	//=================edit banner details=========================//
	public function updateBannerDtls(Request $request)
	{
		$bannerFor=$request->post('bannerfor');
		$bannerName=$request->post('bannerName');
		$bannerText=$request->post('bannerText');
		$bannerOtherAttributes=$request->post('bannerOtherAttributes');
		$bannerURL=$request->post('bannerURL');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$bannerId=$request->post('bannerId');
		$store=$request->post('store');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}

		$startdate=$request->post('startDate');
		$startDate=date('Y-m-d',strtotime($startdate));
		$startTime=$request->post('startTime');
		$end_date=$request->post('endDate');
		$endDatenew=date('Y-m-d',strtotime($end_date));
		//$endDatenew=date('Y-m-d',strtotime($enddate));
		$endTime=$request->post('endTime');
		$file=$request->file('image');
		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== update banner =========//
		$curlUrl = 'api/banner';
	//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActiveVal;//die();
		//echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $bannerFor!='' && $bannerName!='' && $bannerText!='' && $bannerURL!='' && $displayOrder!='' && $IsActiveVal!='' && $startDate!='' && $startTime!='' && $endDatenew!='' && $endTime!='')
		{
			if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
			{
				$imagesfile= $file->getClientSize();
				$Image1FileSize=filesize($file);//$imagesfile;
				$Image1FileName = $file->getClientOriginalName();//['filename'];//$request->file('input_pdf')->getClientOriginalName();
				//echo $Image1FileSize.'@@@@'.$Image1FileName;die();
				$fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
				list($width,$height) = getimagesize($file);
				//echo $width.'       '.$height;die();
				$image_content = file_get_contents($file);
				$path = 'uploads/banner_images/';
				$thumb_path = 'uploads/banner_images/thumbnail/';
				$fileNormal = $path.$fileName;
				$fileThumb = $thumb_path.$fileName;
			   
				$this->uploadImage('public',$fileNormal,$image_content);
				$this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
			   
			}
			else
			{
				$fileName='';
				$Image1FileSize='';
				$Image1FileName ='';
			}	
				$curlPostData = array(
							'mode' => 'insert_banner_details',
							'userId'=>$userId,
							'bannerid'=>$bannerId,
							'login_device_type'=>3,
							'BannerFor'=>$bannerFor,
							'BannerName'=>$bannerName,
							'BannerText'=>$bannerText,
							'BannerURL'=>$bannerURL,
							'BannerOtherAttributes'=>$bannerOtherAttributes,
							'IsActive'=>$IsActiveVal,
							'DisplayOrder' => $displayOrder,
							'image'=>$fileName,
							'Image1FileSize'=>$Image1FileSize,
							'Image1FileName'=>$Image1FileName,
							'Store' =>(string)$strAccess,
							'StartDate'=>$startDate,
							'StartTime'=>$startTime,
							'EndDate'=>$endDatenew,
							'EndTime'=>$endTime
							);
				$url = $baseUrl.$curlUrl;
				
				//echo "<pre>";
				//print_r($curlPostData);
				//die();
				$banner = $this->fireCurl($url,$curlPostData);
				$banner_array = json_decode($banner,1);
				//echo "<pre>";print_r($banner_array);die();
				if($banner_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$banner_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$banner_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/banner/bannerlist');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/banner/bannerlist');
		}
	}
	//==============================load add Product view================================//
	public function addProduct()
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
        //echo $userId.'+++'.$Is_superadmin.'+++'. $storeId;die();
		if($userId != '')
        {
			$lastRow='';
			//======== GET STORE DETAILS FROM API =========//
	            $curlUrl = 'api/StoreManagement';
	            $curlPostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent storeid = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$store = $this->fireCurl($url,$curlPostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
	            //======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['categorylist'] = $category_array['data']['category'];
				}
				else
				{
					$data['categorylist'] = array();
				}
				//======== GET BRAND LIST FROM API =========//
				$curlUrl = 'api/brand';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_brand_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'brandid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['brandlist'] = $brand_array['data']['brand'];
				}
				else
				{
					$data['brandlist'] = array();
				}

				//============= GET VARIANTS COLOURS ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_variants_colour',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['variants_colour'] = $brand_array['data']['colour'];
				}
				else
				{
					$data['variants_colour'] = array();
				}
				//============= GET VARIANTS MATERIAL ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_variants_materials',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['variants_materials'] = $brand_array['data']['Material'];
				}
				else
				{
					$data['variants_materials'] = array();
				}
				//============= GET SEASON DATA ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_season_List',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['season'] = $brand_array['data']['Season'];
				}
				else
				{
					$data['season'] = array();
				}
				//======== GET PRODUCT TYPE LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_productType_List',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'categoryid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['ProductType'] = $category_array['data']['ProductType'];
				}
				else
				{
					$data['ProductType'] = array();
				}

				// echo "<pre>";
				// print_r($data['variants_materials']);
				// die;
				
				//==========================================================



				if($userStoretoreId != '')
				{
					$selectStoreId=$userStoretoreId;
				}
				else
				{
					$selectStoreId=$store_array['data']['store'][0]['StoreId'];
				}
			$data['title'] = 'Product';
			$data['js'] = 'product.js';
	        $data['viewPage'] = 'control/product/add_product';
	        return view('control/includes/master_view',compact('data','selectStoreId'));
	    }
	    else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

	}
	
//==============================load add Shoe Style view================================//
	public function addShoeProduct()
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
        //echo $userId.'+++'.$Is_superadmin.'+++'. $storeId;die();
		if($userId != '')
        {
			$lastRow='';
			//======== GET STORE DETAILS FROM API =========//
	            $curlUrl = 'api/StoreManagement';
	            $curlPostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent storeid = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$store = $this->fireCurl($url,$curlPostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
	            //======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['categorylist'] = $category_array['data']['category'];
				}
				else
				{
					$data['categorylist'] = array();
				}
				//======== GET BRAND LIST FROM API =========//
				$curlUrl = 'api/brand';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_brand_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'brandid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['brandlist'] = $brand_array['data']['brand'];
				}
				else
				{
					$data['brandlist'] = array();
				}
				if($userStoretoreId != '')
				{
					$selectStoreId=$userStoretoreId;
				}
				else
				{
					$selectStoreId=$store_array['data']['store'][0]['StoreId'];
				}
			$data['title'] = 'Shoe';
			$data['js'] = 'product.js';
	        $data['viewPage'] = 'control/product/add_shoe';
	        return view('control/includes/master_view',compact('data','selectStoreId'));
	    }
	    else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

	}
	//================================insert banner details===========================//
	public function insertBannerDtls(Request $request)
	{
		
		$bannerFor=$request->post('bannerfor');
		$bannerName=$request->post('bannerName');
		$bannerText=$request->post('bannerText');
		$bannerOtherAttributes=$request->post('bannerOtherAttributes');
		$bannerURL=$request->post('bannerURL');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$bannerId=$request->post('bannerId');
		$store=$request->post('store');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}
		$startdate=$request->post('startDate');
		$startDate=date('Y-m-d',strtotime($startdate));
		$startTime=$request->post('startTime');
		$end_date=$request->post('endDate');
		$endDatenew=date('Y-m-d',strtotime($end_date));
		//$endDatenew=date('Y-m-d',strtotime($enddate));
		$endTime=$request->post('endTime');
		$file=$request->file('image');

		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== insert banner =========//
		$curlUrl = 'api/banner';
		//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActive;echo "</pre>";
		// echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $bannerFor!='' && $bannerName!='' && $bannerText!='' && $bannerURL!='' && $displayOrder!='' && $IsActiveVal!='' && $startDate!='' && $startTime!='' && $endDatenew!='' && $endTime!=''  && !empty($file))
		{
			if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
			{
				$imagesfile= $file->getClientSize();
				$Image1FileSize=filesize($file);//$imagesfile;
				$Image1FileName = $file->getClientOriginalName();//['filename'];//$request->file('input_pdf')->getClientOriginalName();
				//echo $Image1FileSize.'@@@@'.$Image1FileName;die();
				$fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
				list($width,$height) = getimagesize($file);
				//echo $width.'       '.$height;die();
				$image_content = file_get_contents($file);
				$path = 'uploads/banner_images/';
				$thumb_path = 'uploads/banner_images/thumbnail/';
				$fileNormal = $path.$fileName;
				$fileThumb = $thumb_path.$fileName;
			   
				$this->uploadImage('public',$fileNormal,$image_content);
				$this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
			   
			}
			else
			{
				$fileName='';
				$Image1FileSize='';
				$Image1FileName ='';
			}	
				$curlPostData = array(
							'mode' => 'insert_banner_details',
							'userId'=>$userId,
							'bannerid'=>$bannerId,
							'login_device_type'=>3,
							'BannerFor'=>$bannerFor,
							'BannerName'=>$bannerName,
							'BannerText'=>$bannerText,
							'BannerURL'=>$bannerURL,
							'BannerOtherAttributes'=>$bannerOtherAttributes,
							'IsActive'=>$IsActiveVal,
							'DisplayOrder' => $displayOrder,
							'image'=>$fileName,
							'Image1FileSize'=>$Image1FileSize,
							'Image1FileName'=>$Image1FileName,
							'Store' =>$strAccess,
							'StartDate'=>$startDate,
							'StartTime'=>$startTime,
							'EndDate'=>$endDatenew,
							'EndTime'=>$endTime
							);
				$url = $baseUrl.$curlUrl;
				
			//echo "<pre>";
			//print_r($curlPostData);
			//die();
				$banner = $this->fireCurl($url,$curlPostData);
				$banner_array = json_decode($banner,1);
				
				if($banner_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$banner_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$banner_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/banner/bannerlist');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/banner/bannerlist');
		}
		
		
	}
	
	//================================change status===================================//
	public function changeStatus(Request $request)
	{
		$bannerid=$request->segment(4);
		$status=$request->segment(5);
		
		$whereArr=array('BannerID'=>$bannerid);
		$updatearr=array('IsActive'=>$status);
		
        $updatebannerDetails=$this->update('banners',$whereArr,$updatearr);
		
		$request->session()->put('success_msg','Status updated successfully!');
		
		 return Redirect::to('/oms/banner/bannerlist');
	}
	

	///=============== delete a banner ====================//
	public function deletebanner(Request $request)
	{
		$id = $request->segment(4);
		if($id !='')
		{
			$ConditionBanner=array('BannerID'=>$id);
			$fetchbanner=$this->getAllData('banners',$ConditionBanner,'BannerID','DESC');
			//echo "<pre>";print_r($fetchbanner);die();
			if(!empty($fetchbanner))
			{
				$previmage=$fetchbanner[0]['Image1SavedFileName'];
				$delete = $this->delete('banners',array('BannerID'=>$id));
				if($delete)
				{
					$normalpath=storage_path().'/app/'.'public/uploads/banner_images/'.$previmage;
					$thumbnailpath=storage_path().'/app/'.'public/uploads/banner_images/thumbnail/'.$previmage;
					
					if(file_exists($normalpath))
					{
						unlink(storage_path().'/app/'.'public/uploads/banner_images/'.$previmage);
					}
					if(file_exists($thumbnailpath))
					{
						unlink(storage_path().'/app/'.'public/uploads/banner_images/thumbnail/'.$previmage);
					}
					$request->session()->put('success_msg',"Banner deleted.");
				}
				else
				{
					$request->session()->put('error_msg',"Failed to delete banner.");
				}
				
			}
			else
			{
				$request->session()->put('error_msg',"Please provide valid details.");
			}
		}
		else
		{
			$request->session()->put('error_msg',"Please provide valid details.");
		}
		return Redirect::to('/oms/banner/bannerlist');
	}

	//=======================================Fetching store Shoe Size =====================//
	public function fetchStoreShoeSize(Request $request)
	{
		$shoeValue=$request->post('shoevalue');
		$varcount=$request->post('varcount');
		//
		// print_r($shoeValue);
		// print_r($varcount);
		// die();
		$mensizecount=0;$womensizecount=0;
		$whereCondition=array('StoreId' => $shoeValue);
		$query = DB::table('StoreShoeSize')->distinct()->whereIn('StoreId', $shoeValue)->get(['WomenFootIninches']);
		//echo "<pre>";print_r($query); echo "</pre>";die();
		$allsize_array=array();$html='';$i=1;$allsizeId_array=array();
		foreach($query as $shoesize)
		{
			$storeWiseSize=array();$allsize_array=array();$allsizeId_array=array();
			if(!empty($shoesize->WomenFootIninches))
			{
				foreach($shoeValue as $shoestoreId)
				{
					// $storeWiseSize=array();
					$whereStoreSizeCondition=array('StoreId' =>$shoestoreId,'WomenFootIninches'=>$shoesize->WomenFootIninches);
					$getstoresizedetails = DB::table('StoreShoeSize')->select('WomenShoeSize','StoreId','storeSizeId')->where($whereStoreSizeCondition)->get();

					//print_r($getstoresizedetails);die;

					if(!empty($getstoresizedetails))
					{
						$storeWiseSize[]=array('StoreId'=>$getstoresizedetails[0]->StoreId,'Size'=>$getstoresizedetails[0]->WomenShoeSize,'Id'=>$getstoresizedetails[0]->storeSizeId);

					}
					
				}

				foreach($storeWiseSize as $shoesizedtls)
				{
					$allsize_array[]	=$shoesizedtls['Size'];	
					$allsizeId_array[]	=$shoesizedtls['Id'];	
				}
				
				$allsizeHtml=implode('/',$allsize_array);

				$allsizeId=implode('/',$allsizeId_array);

				
				
			}
			$html.='<div class="form-group col-sm-6">
                        <label for="sku5" class="ad_prd_lbl">Barcode ('.$allsizeHtml.') *</label>
                        <input type="text" class="form-control varneed" id="barcode'.$i.'_'.$varcount.'" name="barcode'.$i.'[]" placeholder="">
                        <input type="hidden" class="form-control" id="barcodeId'.$i.'_'.$varcount.'" name="barcodeId'.$i.'[]" placeholder="" value="'.$allsizeId.'">
                        </div>';
                        
			
				
			$i++;
		}
		//echo "<pre>";
				//print_r($html);die();
		// print_r($storeWiseSize);
		// die();
        //$allstoreshoesize = $this->getAllData('StoreShoeSize',$whereCondition,'storeSizeId','ASC');
        //SELECT DISTINCT `WomenFootIninches` FROM `StoreShoeSize` WHERE `StoreId` IN(2,14)
        //$allwomenshoesize = $this->getAllData('WomenShoeSize',$whereCondition,'SizeId','ASC');
        //echo "<pre>";print_r($allmenshoesize );print_r( $allwomenshoesize);die();
        /*if(!empty($allstoreshoesize) )
		{
			$html1='<select class="form-control combobox">
	            <option value="">Select Size</option>';
	            $html2='';
	            foreach($allstoreshoesize as $Storeshoesize)
	            {
	            	if($Storeshoesize['WomenShoeSize'] !='')
	            	{
	            		$html2.='<option value="'.$Storeshoesize['WomenShoeSize'].'">'.$Storeshoesize['WomenShoeSize'].'</option>';
	            	}
	                
	            }
	        $html3='</select>'; 
	        $html=$html1.$html2.$html3;
		}
		else{
			$html='';
		}*/
		echo $html;
    }
	//=======================================Fetching Category Data ajax =====================//
	public function fetchCategoryDta(Request $request)
	{
		 $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
		$userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');

		$producttype=$request->post('producttype');
		$html ='';
		 	//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_List_by_ptypeid',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'producttypeid'=>$producttype,
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            //$data['producttypelist'] = $category_array['data']['producttype'];
		            $html.= '<option value="">Select one...</option>';
                                      if(!empty($category_array['data']['producttype'])){
                                        foreach($category_array['data']['producttype'] as $category){
                                       $html.= '<option value="'. $category['CategoryID'] .'">'. $category['CategoryName'] .'</option>';
                                        }
                                      }
				}
				else
				{
					//$data['producttypelist'] = array();
					$html.= '<option value="">Select one...</option>';
				}


		echo $html;
    }
    //========================================= fetch colour value by product type id ================================
    public function fetchColourDta(Request $request)
	{
			 $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
		$userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');

		$producttype=$request->post('producttype');
		$html ='';
		 	//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_colour_List_by_ptypeid',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'producttypeid'=>$producttype,
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$categorycolour = $this->fireCurl($url,$curlPostData);
				$categorycolour_array = json_decode($categorycolour,1);
				if(!empty($categorycolour_array) && $categorycolour_array['status'] == 1)
	            {
		            //$data['producttypelist'] = $category_array['data']['producttype'];
		            $html.= '<option value="">Select one...</option>';
                                      if(!empty($categorycolour_array['data']['producttypeColour'])){
                                        foreach($categorycolour_array['data']['producttypeColour'] as $category){
                                       $html.= '<option value="'. $category['ColourId'] .'">'. $category['ColourName'] .'</option>';
                                        }
                                      }
				}
				else
				{
					//$data['producttypelist'] = array();
					$html.= '<option value="">Select one...</option>';
				}


		echo $html;


	}

	 //========================================= fetch Material value by product type id ================================
    public function fetchMaterialDta(Request $request)
	{

						 $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
		$userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');

		$producttype=$request->post('producttype');
		$html ='';
		 	//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_material_List_by_ptypeid',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'producttypeid'=>$producttype,
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$categorymaterial = $this->fireCurl($url,$curlPostData);
				$categorymaterial_array = json_decode($categorymaterial,1);
				if(!empty($categorymaterial_array) && $categorymaterial_array['status'] == 1)
	            {
		            //$data['producttypelist'] = $category_array['data']['producttype'];
		            $html.= '<option value="">Select one...</option>';
                                      if(!empty($categorymaterial_array['data']['producttypeMaterial'])){
                                        foreach($categorymaterial_array['data']['producttypeMaterial'] as $category){
                                       $html.= '<option value="'. $category['MaterialId'] .'">'. $category['MaterialName'] .'</option>';
                                        }
                                      }
				}
				else
				{
					//$data['producttypelist'] = array();
					$html.= '<option value="">Select one...</option>';
				}


		echo $html;

	}
	 //========================================= fetch Size value by product type id ================================
    public function fetchSizeDtaEU(Request $request)
	{

						 $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
		$userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');

		$producttype=$request->post('producttype');
		$html ='';
		 	//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_size_List_by_ptypeid',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'producttypeid'=>$producttype,
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$categorymaterial = $this->fireCurl($url,$curlPostData);
				$categorymaterial_array = json_decode($categorymaterial,1);
				if(!empty($categorymaterial_array) && $categorymaterial_array['status'] == 1)
	            {
		            //$data['producttypelist'] = $category_array['data']['producttype'];
		            $html.= '<option value="">Select one...</option>';
                                      if(!empty($categorymaterial_array['data']['producttypeSize'])){
                                        foreach($categorymaterial_array['data']['producttypeSize'] as $category){
                                       $html.= '<option value="'. $category['id'] .'">'. $category['sizeineu'].' (EU)/'. $category['sizeinusa'].' (USA)'.'</option>';
                                        }
                                      }
				}
				else
				{
					//$data['producttypelist'] = array();
					$html.= '<option value="">Select one...</option>';
				}


		echo $html;

	}
	
public function fetchSizeDtaUSA(Request $request)
	{

						 $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
		$userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');

		$producttype=$request->post('producttype');
		$html ='';
		 	//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_size_List_by_ptypeid',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'producttypeid'=>$producttype,
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$categorymaterial = $this->fireCurl($url,$curlPostData);
				$categorymaterial_array = json_decode($categorymaterial,1);
				if(!empty($categorymaterial_array) && $categorymaterial_array['status'] == 1)
	            {
		            //$data['producttypelist'] = $category_array['data']['producttype'];
		            $html.= '<option value="">Select one...</option>';
                                      if(!empty($categorymaterial_array['data']['producttypeSize'])){
                                        foreach($categorymaterial_array['data']['producttypeSize'] as $category){
                                       $html.= '<option value="'. $category['id'] .'">'. $category['sizeinusa'] .'</option>';
                                        }
                                      }
				}
				else
				{
					//$data['producttypelist'] = array();
					$html.= '<option value="">Select one...</option>';
				}


		echo $html;

	}
	


	// edit product starts here
	public function editProduct(Request $request){

		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
        // echo $userId.'+++'.$Is_superadmin.'+++'. $storeId;die();
		if($userId != '')
        {
			$lastRow='';
			//======== GET STORE DETAILS FROM API =========//
	            $curlUrl = 'api/StoreManagement';
	            $curlPostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent storeid = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$store = $this->fireCurl($url,$curlPostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
	            //======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['categorylist'] = $category_array['data']['category'];
				}
				else
				{
					$data['categorylist'] = array();
				}
				//======== GET BRAND LIST FROM API =========//
				$curlUrl = 'api/brand';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_brand_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'brandid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['brandlist'] = $brand_array['data']['brand'];
				}
				else
				{
					$data['brandlist'] = array();
				}

				//============= GET VARIANTS COLOURS ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_variants_colour',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['variants_colour'] = $brand_array['data']['colour'];
				}
				else
				{
					$data['variants_colour'] = array();
				}
				//============= GET VARIANTS MATERIAL ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_variants_materials',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['variants_materials'] = $brand_array['data']['Material'];
				}
				else
				{
					$data['variants_materials'] = array();
				}
				//============= GET SEASON DATA ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_season_List',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['season'] = $brand_array['data']['Season'];
				}
				else
				{
					$data['season'] = array();
				}
				//======== GET PRODUCT TYPE LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_productType_List',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'categoryid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['ProductType'] = $category_array['data']['ProductType'];
				}
				else
				{
					$data['ProductType'] = array();
				}

				// echo "<pre>";
				// print_r($data['variants_materials']);
				// die;
				
				//==========================================================



				if($userStoretoreId != '')
				{
					$selectStoreId=$userStoretoreId;
				}
				else
				{
					$selectStoreId=$store_array['data']['store'][0]['StoreId'];
				}
			$data['title'] = 'Product';
			$data['js'] = 'product.js';
			$data['storeadminorsiteadmin'] = $Is_superadmin;
	        $data['viewPage'] = 'control/product/edit_product';
	        return view('control/includes/master_view',compact('data','selectStoreId'));
	    }
	    else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }


	}

	public function insertproductdata(Request $request){
		
		// echo '<pre>';
		// print_r($_REQUEST);

		$subBrand = $request->input('subBrand');
		$producttype = $request->input('producttype');
		$styleName = $request->input('styleName');
		$factoryId = $request->input('factoryId');
		$category = $request->input('category');
		$subCategory = $request->input('subCategory');
		$season = $request->input('season');
		$insertarray = array(

			'style_name' =>$styleName,
			'brand' =>$subBrand,
			'productType' =>$producttype,
			'factory_id' =>$factoryId,
			'catagoryId1' =>$category,
			'catagoryId2' =>$subCategory,
			'session' =>$season,
			'added_on' => date('Y-m-d H:i:s'),
			'modified_on' => date('Y-m-d H:i:s'),
			'status' =>1		

		);

		// echo '<pre>';
		// print_r($insertarray);
		// die('hello there');

		DB::table('productMaster')->insert($insertarray);
		$request->session()->put('success_msg',"Product added successfully");
		return view('control/includes/insert_product',compact('insertarray'));
		// return 1;


	}


	 public function getList2()
    {
    	// echo 2; die('hello there 2');
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$banner_array=array();
        if($userId != '')
        {
				//======== GET PRODUCT LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_product_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'bannerid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$product = $this->fireCurl($url,$curlPostData);
				$product_array = json_decode($product,1);
			// echo "<pre>";
			// print_r($product_array);
			// die();
				//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['categorylist'] = $category_array['data']['category'];
				}
				else
				{
					$data['categorylist'] = array();
				}
				




					// get search list parameters starts here 
					//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['categorylist'] = $category_array['data']['category'];
				}
				else
				{
					$data['categorylist'] = array();
				}
				//======== GET BRAND LIST FROM API =========//
				$curlUrl = 'api/brand';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_brand_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'brandid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['brandlist'] = $brand_array['data']['brand'];
				}
				else
				{
					$data['brandlist'] = array();
				}

				//============= GET VARIANTS COLOURS ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_variants_colour',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['variants_colour'] = $brand_array['data']['colour'];
				}
				else
				{
					$data['variants_colour'] = array();
				}
				//============= GET VARIANTS MATERIAL ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_variants_materials',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['variants_materials'] = $brand_array['data']['Material'];
				}
				else
				{
					$data['variants_materials'] = array();
				}
				//============= GET SEASON DATA ====================
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_season_List',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'brandid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$brand = $this->fireCurl($url,$curlPostData);
				$brand_array = json_decode($brand,1);
				if(!empty($brand_array) && $brand_array['status'] == 1)
	            {
		            $data['season'] = $brand_array['data']['Season'];
				}
				else
				{
					$data['season'] = array();
				}
				//======== GET PRODUCT TYPE LIST FROM API =========//
				$curlUrl = 'api/allproduct';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_productType_List',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									//'categoryid'=>'',
									//'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
				if(!empty($category_array) && $category_array['status'] == 1)
	            {
		            $data['ProductType'] = $category_array['data']['ProductType'];
				}
				else
				{
					$data['ProductType'] = array();
				}

				
				
				//==========================================================
				// get search list parameters ends here
				//echo "<pre>";
				$product = array();
				//$product_array = array();product_array


				// get product list starts here

				$productmasterdata = DB::table('productMaster')->get();
				// echo '<pre>';
				// print_r($productmasterdata);
				// die('hello there');
				// get product list ends here 
				if(!empty($productmasterdata)){
					$data['productdata'] = $productmasterdata;
				}else{
					$data['productdata'] = array();
				}
            if(!empty($product_array) && $product_array['status'] == 1)
            {
	            $data['productlist'] = $product_array['data']['product'];
				$data['title'] = 'Product';
				$data['viewPage'] = 'control/product/product_list1';				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['productlist'] = array();
				$data['title'] = 'Product';
				$data['viewPage'] = 'control/product/product_list1';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }


    public function searchproduct(Request $request){
    		
    			

    			$categoryidval = $request->input('categoryidval');
    			$subcategoryid = $request->input('subcategoryid');
    			$sessionid = $request->input('sessionid');
    			$producttype = $request->input('producttype');
    			$brand = $request->input('brand');
    			$manufacturedyear =  $request->input('manufacturedyear');
    			
				$product = array();
				//$product_array = array();product_array
				$wherearray =  array();
				if($categoryidval!=''){
					$wherearray['catagoryId1']= $categoryidval;
				}
				if($subcategoryid!=''){
					$wherearray['catagoryId2']= $subcategoryid;
				}
				if($sessionid!=''){
					$wherearray['session']= $sessionid;
				}
				if($producttype!=''){
					$wherearray['productType']= $producttype;
				}
				if($brand!='')
				{
					$wherearray['brand']= $brand;
				}
				if($manufacturedyear  !='')
				{
					$wherearray['manufacturedYear']= $manufacturedyear;
				}
				
				// echo '<pre>';
				// print_r($wherearray);
				// die('hello there');

				// get product list starts here
				$productmasterdata = DB::table('productMaster')->where($wherearray)->get();
				if(!empty($productmasterdata)){
					$data['productdata'] = $productmasterdata;
				}else{
					$data['productdata'] = array();
				}

				// echo '<pre>';
				// print_r($productmasterdata);
				// die('hello there');

				$view_obj = View::make('control/product/ajaxdatatableresp',$data);

				$ep_view_contents = $view_obj->render(); //echo $ep_view_contents;
				echo $ep_view_contents;
    }
     
    public function productexcelimport(Request $request){
    	
    	 if ($_FILES['fileimport']['name'] != "")
        {
           
            $extAr=explode('.',strtolower($_FILES['fileimport']["name"]));
            $ext='';
            if(!empty($extAr))
            {
                if(count($extAr)>1){
                    $ext=end($extAr);
                     $ext=strtolower($ext);
                }

            }
            $validExt=array('xls','xlsx');
            if(!in_array($ext,$validExt))
            {
                $request->session()->put('error_msg','Upoaded file should be  *.xlsx or *.xls file type');

                // $request->session()->put('success_msg','Data imported successfully');
				return Redirect::to('/oms/product/product-list');
                // return Redirect::to('/ingredientsSettings');
            }

            $DIR_DOC = base_path() . '/public/uploadmyuserlistfile/';

           // $fileNormal = $DIR_DOC.time().'.'.$ext;
            $fileNormal = $DIR_DOC.'productlistlistdata-'.rand(10,100).'.'.$ext;
            $file = $_FILES['fileimport']['tmp_name'];
            $result = move_uploaded_file($file, $fileNormal);
           

            if($result)
            {

                $uploadrel_msg='';$errmsgdata='';
                $p=0;
                //**** read file and save thenm to database starts *****

                    $filefullpath=$fileNormal;

	                Excel::selectSheetsByIndex(0)->load($filefullpath, function($reader) use (&$uploadrel_msg,&$errmsgdata) {

	                $objtoarrData= $reader->toArray(); // convert to array

	                // Excel Sheet

	                	  if(!empty($objtoarrData))
		                {
		                	$recivingidarray = array();$p=0;
		                	$recivingsub_array = array();
		                	$recivingcolor_array = array();
		                	$recivingmaterial_array = array();
		                	$recivingcost_array = array();
		                	$recivingavg_cost_array = array();
		                	$recivingnotes_array = array();
		                	$receivinglastupdate_array = array();
		                	$receivingstocklocation_array =array();


		                	// $receivingsub_arraycolor
		                	$recivingidarray['factoryid']='';
		                	$mainarray = array();
							foreach($objtoarrData as $k=>$newarrdata)
							{	
								echo '<pre> foreach section '.$k;
								print_r($newarrdata);
								echo '</pre>';


								if(isset($newarrdata['factoryid']) && $newarrdata['factoryid'] == 'Barcode')
								{
										$recivingidarray = array();$p=0;
										$recivingsub_array = array();
										$recivingcolor_array = array();
										$recivingmaterial_array= array();
										$recivingcost_array =array();
										$recivingavg_cost_array = array();
										$recivingnotes_array = array();
										$receivinglastupdate_array=array();
										$receivingstocklocation_array = array();
								}

								if(isset($newarrdata['factoryid'])  && $newarrdata['factoryid'] != 'Barcode' && $newarrdata['factoryid']!='')
								{
									// echo "!= 'Barcode'";
									// echo '<br>';
									// check this is a factory id or not starts here
									 $factoryidchk = substr_count($newarrdata['factoryid'], '-'); // --> 2
									// check this is a factory id or not ends here 

									if($factoryidchk >0 && $factoryidchk!='')
									{
										// this a factory id
										// echo $newarrdata['factoryid'];
										// check duplicate factory id starts here

										// $factoryidchkqry = DB::table('productMaster')->where('factoryId',$newarrdata['factoryid'])->first();
										// if(empty($factoryidchkqry))
										// {

										// check duplicate factory id ends here

											$recivingidarray['factoryId']= $newarrdata['factoryid'];
											$uploadrel_msg.=$recivingidarray['factoryId'].',';
										// }else{
										// 	$errmsgdata.=$newarrdata['factoryid'].',';
										// 	$recivingidarray['factoryId']='';
											
										// }
										// echo $errmsgdata;

										$recivingidarray['styleName']= $newarrdata['style_name'];
										$recivingidarray['rrp']= $newarrdata['rrp'];
										$recivingidarray['averageCost']= $newarrdata['avg_cost'];
										$recivingidarray['notes']= $newarrdata['notes'];
										$recivingidarray['manufacturedYear']= $newarrdata['intro_year'];
										
										if($newarrdata['class']!='')
											{
												$category_val= strtolower($newarrdata['class']);
												$categoryexistsqry = DB::table('categories')->where('CategoryNameLower',$category_val)->first();
												if(!empty($categoryexistsqry))
												{
													$categoryID = $categoryexistsqry->CategoryID;
													$recivingidarray['catagoryId1']= $categoryID;
												
												}else
												{
													$flag = 1;
												}
											}
										
										

										if($newarrdata['season']!='')
											{
												$season_val= strtolower($newarrdata['season']);
												$seasonexistsqry = DB::table('Season')->where('NameLower',$season_val)->first();
												if(!empty($seasonexistsqry))
												{
													$seasonID = $seasonexistsqry->Id;
													$recivingidarray['session']= $seasonID;
												
												}else
												{
													$flag = 1;
												}
											}
											if($newarrdata['brand']!='')
											{
												$brand_val= strtolower($newarrdata['brand']);
												$brandexistsqry = DB::table('brand')->where('brandNameLower',$brand_val)->first();
												if(!empty($brandexistsqry))
												{
													$brandID = $brandexistsqry->brandId;
													$recivingidarray['brand']= $brandID;
												}else
												{
													$flag = 1;
												}
											}

											// if($newarrdata['Class']!='')
											// {
											// 	$categoryval= strtolower($newarrdata['Class']);

											// 	$categoryexistsqry = DB::table('categories')->where('CategoryNameLower',$categoryval)->first();
											// 	if(!empty($categoryexistsqry))
											// 	{
											// 		$categoryidID = $categoryexistsqry->brandId;
											// 		$recivingidarray['catagoryId1']= $categoryidID;
											// 	}else
											// 	{
											// 		$flag = 1;
											// 	}
											// }


										// insert data into master table starts here
										// if(empty($factoryidchkqry))
										// {
											DB::table('productMaster')->insert($recivingidarray);
										// }

										// insert data into master table ends here



									} if($factoryidchk == 0)
									{
									
										$recivingsub_array[]= $newarrdata['factoryid']; // for barcode
										$recivingcolor_array[]= $newarrdata['style_name']; // for barcode
										$recivingmaterial_array[]= $newarrdata['rrp']; // for material
										$recivingcost_array[]= $newarrdata['manufacturer']; // for cost
										$recivingavg_cost_array[] = $newarrdata['avg_cost']; // for average cost
										$recivingnotes_array[] = $newarrdata['notes']; // for average cost
										$receivinglastupdate_array[] = $newarrdata['intro_year'];
										$receivingstocklocation_array[] = $newarrdata['season'];


										

										$p++;
									}

									// echo '<pre>';
									// echo 'hello';
									// print_r($recivingidarray);
									// echo '<pre>';


								}

								// die();
							

								// if($newarrdata['factoryid'] != 'Barcode' && $newarrdata['factoryid']!='')
								// {
								// 	// echo $newarrdata['factoryid'];
								// 	$recivingidarray['factoryid']='123';
								// 	// array_push($recivingidarray['factoryid'],'123');
									
								// }
								if(!empty($recivingsub_array))
								{
									
									$recivingidarray['barcode'] = $recivingsub_array;
								}
								if(!empty($recivingcolor_array))
								{
									
									$recivingidarray['color'] = $recivingcolor_array;
								}
								if(!empty($recivingmaterial_array)){
									$recivingidarray['material'] = $recivingmaterial_array;
								}
								if(!empty($recivingcost_array)){
									$recivingidarray['cost'] = $recivingcost_array;
								}
								if(!empty($recivingavg_cost_array)){
									$recivingidarray['Size'] = $recivingavg_cost_array;
								}
								if(!empty($recivingnotes_array)){
									$recivingidarray['on Hand'] =$recivingnotes_array;
								}
								if(!empty($receivinglastupdate_array)){
									$recivingidarray['LastUpdate']=$receivinglastupdate_array;
								}
								if(!empty($receivingstocklocation_array)){
									$recivingidarray['StockLoc']=$receivingstocklocation_array;
								}



								// avg_cost
								// echo '<pre> recivingidarray ';
								// print_r($recivingidarray);
								// die('hello there');
									

									if(isset($recivingidarray['factoryId']))
									{
											// echo 'hello there';
											// echo 'factory id exists';

										$productmasterarray = array();
										$flag=0;

										// inserting data starts here

										// checking factory is already present or not starts here 
										$factoryid_val =  $recivingidarray['factoryId'];
										// DB::table('productMaster')
										// $factoryqrycheck = DB::table('productMaster')->where('factoryId',$factoryid_val)->first();
										// if(empty($factoryqrycheck)){
											// insert starts here
											// echo '<pre> ';
											// print_r($recivingidarray);
											// echo '</pre>';

											//****************** season condition checking  starts here 
											if($recivingidarray['session']!='')
											{
												$season_val= strtolower($recivingidarray['session']);
												$seasonexistsqry = DB::table('Season')->where('NameLower',$season_val)->first();
												if(!empty($seasonexistsqry))
												{
													$seasonID = $seasonexistsqry->Id;
													$productmasterdata['session'] = $seasonID;
												}else
												{
													$flag = 1;
												}
											}
											//****************** season condition checking ends here

											//****************** brand checking starts here
											if($recivingidarray['brand']!='')
											{
												$brand_val= strtolower($recivingidarray['brand']);
												$brandexistsqry = DB::table('brand')->where('brandNameLower',$brand_val)->first();
												if(!empty($brandexistsqry))
												{
													$brandID = $brandexistsqry->brandId;
													$productmasterdata['brand'] = $brandID;
												}else
												{
													$flag = 1;
												}
											}
											
											//******************  brand checking ends here

											// category checking starts here
											// if($recivingidarray['class']!='')
											// {
											// 	$category_val= strtolower($recivingidarray['class']);
											// 	$categoryexistsqry = DB::table('ProductType')->where('producttypeName',$category_val)->first();
											// 	if(!empty($categoryexistsqry))
											// 	{
											// 		$categoryID = $categoryexistsqry->producttypeName;
											// 		$productmasterdata['producttypeName'] = $categoryID;
											// 	}else
											// 	{
											// 		$flag = 1;
											// 	}
											// }

											// caregory checking ends here
											// barcode checking starts here
											$barcodearr = array();
											if(!empty($recivingidarray['barcode']))
											{
												

												
											}

											// barcode cheking ends here

											// color checking starts here
											$colorarr = array();
											if(!empty($recivingidarray['color']))
											{
												// echo 'barcode';
												// print_r($recivingidarray['barcode']);
												for($i=0;$i<count($recivingidarray['color']);$i++)
												{
													// get color data starts here
													$colorname = strtolower($recivingidarray['color'][$i]);
													$colornameqry = DB::table('productTypeColor')->where('nameLower',$colorname)->first();
													if(!empty($colornameqry))
													{
														$colorarr[] = $colornameqry->id;
													}
													// get color data ends here 
													
												}
												
											}
											// color checking ends here
											// material checking starts here
											$materialarr = array();
											if(!empty($recivingidarray['material']))
											{
												
												for($i=0;$i<count($recivingidarray['material']);$i++)
												{
													// get color data starts here
													 $materialname = strtolower($recivingidarray['material'][$i]);
													 // die('hello there');
													$materialnameqry = DB::table('productTypeMaterial')->where('nameLower',$materialname)->first();
													if(!empty($materialnameqry))
													{
														$materialarr[] = $materialnameqry->id;
													}
													// get color data ends here 
													
												}
												
											}

											// material cheking ends here
											
											// inserting data into variants table starts here
											// echo '<pre> hello there';
											// print_r($recivingidarray);
											// echo '</pre>';
											// die('hello there');

											$countdata_barcode = count($recivingidarray['barcode']);
											$insertarray_variants = array();
											for($t=1;$t<=$countdata_barcode;$t++)
											{
												$tt = $t-1;
												$insertarray_variants['FactoryId'] =$recivingidarray['factoryId'];
												$insertarray_variants['BarCode'] = $recivingidarray['barcode'][$tt];
												$insertarray_variants['Color'] = $colorarr[$tt];
												$insertarray_variants['Cost'] = $recivingidarray['cost'][$tt];
												$insertarray_variants['Size'] = $recivingidarray['Size'][$tt];
												$insertarray_variants['OnHand'] = $recivingidarray['on Hand'][$tt];
												$insertarray_variants['LastUpdate'] = $recivingidarray['LastUpdate'][$tt];
												$insertarray_variants['StockLoc'] = $recivingidarray['StockLoc'][$tt];
												$insertarray_variants['Material'] = $materialarr[$tt];
												// echo ' insert variants <pre>';
												// print_r($insertarray_variants);
												// echo '</pre>';

												DB::table('productMasterVariant')->insert($insertarray_variants);

											} 

											// inserting data into variants table ends here

											// echo '<pre>';
											// print_r($productmasterdata);
											// echo '</pre>';

											// insert ends here
										// }
										// checking factory is already present or not ends here 
										// inserting data ends here
									}
									
										

									
								// if($newarrdata['factoryid'] != 'Barcode' && ){

								// }
									
								$recivingidarray = array();
								
							}
						}

	                // Excel Sheet



						});
// echo $uploadrel_msg;
	            }

	            // die('hello there');
				$data = array();
				$data['factoryid'] = $uploadrel_msg;
	           	echo" errmsgdata ".$errmsgdata;
				// die('Hello there');
				$view_obj = View::make('control/includes/excelimportajax',$data);

				$ep_view_contents = $view_obj->render(); //echo $ep_view_contents;
				echo $ep_view_contents;
				
				
				$errordata = rtrim($errmsgdata,",");
				if($errmsgdata !=''){
					$request->session()->put('error_msg',$errordata.' Factory Id already exists');
				}else
				{
					$request->session()->put('success_msg','Data inserted successfully');
				}

				return Redirect::to('/oms/product/product-list');
			


							
    }
}
}
   
   
?>