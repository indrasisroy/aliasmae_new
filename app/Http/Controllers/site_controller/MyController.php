<?php

namespace App\Http\Controllers\site_controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Redirect;
use Image;
class MyController extends BaseController
{
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //---------------------------------------- FIRE CURL TO GET RESPONSE FROM REST API  ----------------------------------------------//
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   public function fireCurl($url='',$data=array())
   {
      if($url != '')
      {
      	//$agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
         $curl = curl_init();
			$curl_header_data = array(
					'Content-Type:application/json','Content-Type:multipart/form-data'
				);

         curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POST=> 1,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => $curl_header_data
         ));
         //curl_setopt($curl, CURLOPT_USERAGENT, $agent);
         $response = curl_exec($curl);
         $err = curl_error($curl);
         curl_close($curl);
         
         if ($err) {
            echo "cURL Error #:" . $err;
         } else {
            //print_r(json_decode($response));
            return $response;
         }
      }
   }
	
	public function fireCurlTest($url='',$data=array())
	{
		if($url != '')
      {
         $curl = curl_init();
			$curl_header_data = array(
					'Content-Type:multipart/form-data'
				);
         curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POST=> 1,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => $curl_header_data
         ));
         $response = curl_exec($curl);
         $err = curl_error($curl);
         curl_close($curl);
         
         if ($err) {
            echo "cURL Error #:" . $err;
         } else {
            //print_r(json_decode($response));
            return $response;
         }
      }
	}
	
	public function fireCurlTestNew($url='',$data=array())
	{
		
		if($url != '')
      {
         $curl = curl_init();
			$curl_header_data = array(
					'Content-Type:application/json','Content-Type:multipart/form-data'
				);
         curl_setopt_array($curl, array(
				
            CURLOPT_URL => $url,
				CURLOPT_HEADER =>0,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_SAFE_UPLOAD, false,
				CURLOPT_POST=> true,
				CURLOPT_UPLOAD => true,
            CURLOPT_POSTFIELDS => $data,
				CURLOPT_HTTPHEADER => $curl_header_data
          
         ));
			
			
         $response = curl_exec($curl);
         $err = curl_error($curl);
         curl_close($curl);
         
         if ($err) {
            echo "cURL Error #:" . $err;
         } else {
            //print_r(json_decode($response));
            return $response;
         }
      }
		
	}
	
	
	
	
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //--------------------------------------------- UPLOAD IMAGE TO STORAGE ------------------------------------------------------//
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function uploadImage($disk,$f_path,$image)
	{
		$storage = \Storage::disk($disk);//specify the storage like s3 or local
		$storage->put($f_path,$image,'public');//store image to storage
	}
    
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //---------------------------------------------  MAKE THUMBNAIL AND UPLOAD IMAGE TO STORAGE -----------------------------------------//
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function uploadThumbnail($disk,$fileThumb,$image,$Twidth,$Theight)
	{
		$storage = \Storage::disk($disk);//specify the storage like s3 or local
		$storage->put($fileThumb,$image,'public');//store image to storage
		
		$url = \Config::get('app.url');
		list($width, $height, $type, $attr) = getimagesize($url.'storage/'.$fileThumb);
		$per = 80;
		$thumb_w = $width;
		$thumb_h = $height;
		if($thumb_w > $Twidth || $thumb_h > $Theight)
		{
			while($per >= 20)
			{
				if($thumb_w > $thumb_h)
				{
					$chkVarThumb = $Twidth;
					$chkVarOriginal = $thumb_w;
				}
				else
				{
					$chkVarThumb = $Theight;
					$chkVarOriginal = $thumb_h;
				}
				
				//if($thumb_w > $Twidth || $thumb_h > $Theight)
				if($chkVarOriginal > $chkVarThumb)
				{
					$thumb_w = intval(($per * $width) / 100);
					$thumb_h = intval(($per * $height) / 100);
				}
				$per = $per - 20;
			}
		}
		$image_t = Image::make(\Storage::disk('public')->get($fileThumb))->resize($thumb_w,$thumb_h)->encode();
        $this->uploadImage('public',$fileThumb,$image_t);
	}
   
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //----------------------------------- ALL QUERIES FOR MYSQL TABLES WITH LARAVEL QUIRY BUILDER--------------------------------------//
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //================ SEND RESPONSE FOR GET ALL MYSQL ROW WITH CONDITION ============//
    public function getAllData($table,$whereCondition=array(),$order_by='',$order_type='DESC',$limit='10',$skip='',$group_by='',$select='*')
    {
        $query = DB::table($table)->select($select);
        if(!empty($whereCondition))
        {
            $query = $query->where($whereCondition);
        }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        if(!empty($skip))
        {
            $query = $query->offset($skip)->limit($limit);
        }
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        $result = $query->get();
        return json_decode($result,1);
    }
    //======================== UPDATE MYSQL TABLE ==================//
	public function update($table,$whereCondition,$updateDetails){
		if(!empty($whereCondition)){
			return DB::table($table)->where($whereCondition)->update($updateDetails);
		}else{
			return 0;
		}
	} 
	 //======================== Delete MYSQL TABLE ==================//
	public function delete($table,$whereCondition){
		if(!empty($whereCondition)){
			return DB::table($table)->where($whereCondition)->delete();
		}else{
			return 0;
		}
	} 
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //----------------------------- ALL QUERIES FOR MONGODB COLLECTIONS WITH LARAVEL QUIRY BUILDER--------------------------------------//
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////================ SEND RESPONSE FOR GET ALL DOCUMENT WITH CONDITION ============/////
    public function getAllDocument($collection,$whereCondition=array(),$order_by='',$order_type='DESC',$limit='10',$skip='',$group_by='')
    {
        $query = DB::connection('mongodb')->collection($collection);
        if(!empty($whereCondition))
        {
            $query = $query->where($whereCondition);
        }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        if(!empty($skip))
        {
            $query = $query->offset($skip)->limit($limit);
        }
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        $result = $query->get();
         return json_decode($result,1);
        //return $result;
    }
    
    //////================ INSERT A DOCUMENT TO MONGODB COLLECTION ============/////
    public function insertDocument($collection,$document)
    {
        return DB::connection('mongodb')->collection($collection)->insertGetId($document);
    }
    // =================== UPDATE DATA TO MONGODB COLLECTION ================= //
	public function updateDocument($collection,$document,$condition){
		return DB::connection('mongodb')->collection($collection)->where($condition)->update($document);
	}
	//=====================delete document from MONGODB collection ==================//
	public function deleteDocument($collection,$condition){
		return DB::connection('mongodb')->collection($collection)->where($condition)->delete();
	}
 //===================== INSERT A DOCUMENT TO MYSQL==========================//
	 public function insertDatafunc($insertArr,$table){

                        $id = DB::table($table)->insertGetId($insertArr);
                        // return 1;
                         return $id;
    }
     //=====================common pagination function start==========================//
    
   public function myPagination($total=0,$per_page=10,$page=1,$url='?',$section_old='')
	{
		$section='"'.$section_old.'"';
		
		 $total = $total;
		 $adjacents = "2"; 
		   
		// $prevlabel = "&lsaquo; Prev";
		 $prevlabel = "&lsaquo;";
		 //$nextlabel = "Next &rsaquo;";
		 $nextlabel = " &rsaquo;";
		// $lastlabel = "Last &rsaquo;&rsaquo;";
		 $lastlabel = " &rsaquo;&rsaquo;";
		   
		 $page = ($page == 0 ? 1 : $page);  
		 $start = ($page - 1) * $per_page;
		 
		 $prev = $page - 1;                          
		 $next = $page + 1;
		   
		 $lastpage = ceil($total/$per_page);
	 
		 if($lastpage < 2){
			 return '';
		 }
		 $lpm1 = $lastpage - 1; // //last page minus 1
		   
		 $pagination = "";
		 if($lastpage > 1){   
			 $pagination .= "<ul class='pagination'>";
			 //$pagination .= "<li class='page_info'><span>Page {$page} of {$lastpage}</span></li>";
				   
				 if ($page > 1) $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi' onclick='fetchDataPaginationWise($prev);' page='{$prev}'>{$prevlabel}</a></li>";
				   
			 if ($lastpage < 7 + ($adjacents * 2)){   
				 for ($counter = 1; $counter <= $lastpage; $counter++){
					 if ($counter == $page)
						 $pagination.= "<li class='paginate_button page-item active'><a class='current'>{$counter}</a></li>";
					 else
						 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi' onclick='fetchDataPaginationWise($counter);' page='{$counter}'>{$counter}</a></li>";                    
				 }
			   
			 } elseif($lastpage > 5 + ($adjacents * 2)){
				   
				 if($page < 1 + ($adjacents * 2)) {
					   
					 for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						 if ($counter == $page)
							 $pagination.= "<li class='paginate_button page-item active'><a class='current'>{$counter}</a></li>";
						 else
							 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi' onclick='fetchDataPaginationWise($counter);' page='{$counter}'>{$counter}</a></li>";                    
					 }
					 $pagination.= "<li class='paginate_button page-item dot'>...</li>";
					 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi' onclick='fetchDataPaginationWise($lpm1);' page='{$lpm1}'>{$lpm1}</a></li>";
					 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi' onclick='fetchDataPaginationWise($lastpage);' page='{$lastpage}'>{$lastpage}</a></li>";  
						   
				 } elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
					   
					 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi'  onclick='fetchDataPaginationWise(1);' page='1'>1</a></li>";
					 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi'  onclick='fetchDataPaginationWise(2);' page='2'>2</a></li>";
					 $pagination.= "<li class='paginate_button page-item dot'>...</li>";
					 for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
						 if ($counter == $page)
							 $pagination.= "<li class='paginate_button page-item active'><a class='current'>{$counter}</a></li>";
						 else
							 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi'  onclick='fetchDataPaginationWise($counter);' page='{$counter}'>{$counter}</a></li>";                    
					 }
					 $pagination.= "<li class='paginate_button page-item dot'>..</li>";
					 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi'  onclick='fetchDataPaginationWise($lpm1);' page='{$lpm1}'>{$lpm1}</a></li>";
					 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi'  onclick='fetchDataPaginationWise($lastpage);' page='{$lastpage}'>{$lastpage}</a></li>";      
					   
				 } else {
					   
					 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi'  onclick='fetchDataPaginationWise(1);' page='1'>1</a></li>";
					 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi'  onclick='fetchDataPaginationWise(2);' page='2'>2</a></li>";
					 $pagination.= "<li class='paginate_button page-item dot'>..</li>";
					 for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
						 if ($counter == $page)
							 $pagination.= "<li class='paginate_button page-item active'><a class='current'>{$counter}</a></li>";
						 else
							 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi'  onclick='fetchDataPaginationWise($counter);' page='{$counter}'>{$counter}</a></li>";                    
					 }
				 }
			 }
			   
				 if ($page < $counter - 1) {
					 $pagination.= "<li class='paginate_button page-item'><a href='javascript:void(0);' id='GoSearchPagi'  onclick='fetchDataPaginationWise($next);' page='{$next}'>{$nextlabel}</a></li>";
					// $pagination.= "<li><a href='javascript:void(0);' id='GoSearchPagi'  onclick='findPageVal($lastpage);' page='{$lastpage}'>{$lastlabel}</a></li>";
					 //$pagination.= "<li><a href='javascript:void(0);' id='GoSearchPagi'  onclick='findPageVal($lastpage);' page='{$lastpage}'>{$lastlabel}</a></li>";
				 }
			   
			 $pagination.= "</ul>";        
		 }
		 return $pagination;
	}
    //******************* mongo pagination starts here inarray ********************


	public function getAllDocumentwhereinpagination($collection,$whereCondition=array(),$order_by='',$order_type='DESC',$limit=1,$skip='',$group_by='',$orWhere=array(),$serachCondition=array())
    {

        // echo '= limit ='.$limit;
        // echo '=== skip'.$skip;


    	 //print_r($serachCondition);die;
    	$array2 = array();
    	$array2 = array_values($whereCondition);
    	// echo 'array 2 ';print_r($array2);die;
    	

        $query = DB::connection('mongodb')->collection($collection);
		  
        if(!empty($whereCondition))
        {
						$query = $query->where(function($query1) use ($whereCondition,$orWhere){
                                 $query1->whereIn('machineId', $whereCondition);
                                 $query1->orWhere($orWhere);
									  });
            
            //$query = $query->whereIn('machineId', $whereCondition);
        }
		  else if(!empty($orWhere))
		  {
				$query = $query->where($orWhere);
		  }
			if(!empty($serachCondition))
		  {

			 $query = $query->where($serachCondition);
		  }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        //if(!empty($skip))
        //{
            // echo 'here skip'.$skip.'limit'.$limit;
            // $query = $query->offset(0)->limit(10);

           $query = $query->offset($skip)->limit($limit);
        //}
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        
       return $result = $query->get();
    }

    

    //****************** mongo pagination ends here inarray ***********************

    public function getAllDocumentAll($collection,$whereCondition,$order_by='',$order_type='DESC',$limit=10,$skip='',$group_by='',$orWhere=array(),$serachCondition=array())
    {

    	// echo '<pre>';
    	// print_r($whereCondition);
     //    die;
        $query = DB::connection('mongodb')->collection($collection);
        if(!empty($whereCondition))
        {
				$query = $query->where(function($query1) use ($whereCondition,$orWhere){
									$query1->whereIn('machineId', $whereCondition);
									$query1->orWhere($orWhere);
							  });
            
            //$query = $query->whereIn('machineId', $whereCondition);
        }
		  else if(!empty($orWhere))
		  {
				$query = $query->where($orWhere);
		  }
			if(!empty($serachCondition))
		  {

			 $query = $query->where($serachCondition);
		  }
        // if(!empty($order_by))
        // {
        //     $query = $query->orderBy($order_by,$order_type);
        // }
       
        // if(!empty($group_by))
        // {
        //     $query = $query->groupBy($group_by);
        // }
        // return 
        return $result = $query->get()->toArray();

        // print_r($result);die;
        //return json_decode($result,1);
    }
	 
	 //*********************get all data common function*************************************//
	 public function getAllDocumentCommon($collection,$whereCondition=array(),$order_by='',$order_type='DESC',$limit=10,$skip='',$group_by='')
    {
        $query = DB::connection('mongodb')->collection($collection);
        if(!empty($whereCondition))
        {
            $query = $query->where($whereCondition);
        }
        if(!empty($order_by))
        {
            $query = $query->orderBy($order_by,$order_type);
        }
        //if(!empty($skip))
        //{
            //$query = $query->offset($skip)->limit($limit);
			
        //}
        if(!empty($group_by))
        {
            $query = $query->groupBy($group_by);
        }
        return $result = $query->get()->toArray();
        //return json_decode($result,1);
    }
	
	//***************************** get inventory details **************************************//
	public function getInventory($userId)
	{
		$total_inventory = 0;
		$low_inventory = 0;
		$data['low_inventory'] = '';
		$query = DB::connection('mongodb')->collection('cbd_products')->select('variants.inventory_quantity')->where('userId',(string)$userId)->where('variants.inventory_policy','1')->get()->toArray();
		
		foreach($query as $row)
		{
			foreach($row['variants'] as $v_row)
			{
				$total_inventory = $total_inventory + $v_row['inventory_quantity'];
			}
		}
		//================ search for low inventory =======================//
		$machine = DB::connection('mongodb')->collection('machines')->select('_id','name')->where('userId',(string)$userId)->get()->toArray();
		if(!empty($machine))
		{
			foreach($machine as $key => $row)
			{
				$total_inventory_kiosk = 0;
				$m_query = DB::connection('mongodb')->collection('cbd_products')->select('variants.inventory_quantity')->where('userId',(string)$userId)->where('variants.inventory_policy','1')->where('machine',(string)$row['_id'])->get()->toArray();
				foreach($m_query as $row1)
				{
					foreach($row1['variants'] as $m_row)
					{
						$total_inventory_kiosk = $total_inventory_kiosk + $m_row['inventory_quantity'];
					}
				}
				if($key == '1' && count($machine) > 0)
				{
					$low_inventory = $total_inventory_kiosk;
					$data['low_inventory'] = $row['name'].'('.$low_inventory.')';
				}
				else if($low_inventory > $total_inventory_kiosk)
				{
					$low_inventory = $total_inventory_kiosk;
					$data['low_inventory'] = $row['name'].'('.$low_inventory.')';
				}
			}
		}
		$data['total_inventory'] = $total_inventory;
		return $data;
	}
}
?>