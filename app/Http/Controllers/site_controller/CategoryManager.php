<?php
namespace App\Http\Controllers\site_controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Mail\addSubadminTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Illuminate\Support\Facades\Storage;  /*for file upload*/
use Redirect;
use Session;
use Excel;
use Image;

class CategoryManager extends MyController
{
	//================Category details==============//
    public function getDetails(Request $request)
    {
		//echo $id;die();
		$categoryid=base64_decode($request->segment(4));

		//echo $categoryid;die;
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		if($userId != '')
        {
			//======== GET CATEGORY DETAILS FROM API =========//
			$curlUrl = 'api/category';
			$lastRow='';
			
			$curlPostData = array(
								'mode' => 'get_category_list',
								'lastRow' => $lastRow,
								'userId'=>$userId,
								'categoryid'=>$categoryid,
								'storeId' =>$userStoretoreId,
								'flag'=>'0'
							);
			$url = $baseUrl.$curlUrl;
			$category = $this->fireCurl($url,$curlPostData);
			$category_array = json_decode($category,1);
			//echo "<pre>";print_r($category_array);die();
			//======== GET STORE DETAILS FROM API =========//
	            $curlstoreUrl = 'api/StoreManagement';
	            $curlstorePostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlstoreUrl;
				$store = $this->fireCurl($url,$curlstorePostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }

	        //======== GET PRODUCT TYPE FROM API =========//
	            $curlUrl = 'api/ProductType';
	            $curlPostData = array(
	                            'mode' => 'get_producttype_list',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeId'=>$userStoretoreId,
	                            'producttypeid'=>''                            
	                            );//if u sent storeid = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$producttype = $this->fireCurl($url,$curlPostData);
	            $producttype_array = json_decode($producttype,1);
				 // echo "<pre>";
				 // print_r($curlPostData);
	    		 //print_r($producttype_array);die;
	            if(!empty($producttype_array) && $producttype_array['status'] == 1)
	            {
	                $data['producttype'] = $producttype_array['data']['producttype'];
	            }else{
	            	$data['producttype'] =array();
	            }
	         //=========================    
			$categoryarr=array();
			if(!empty($category_array['data']) && $category_array['status'] == 1)
			{
				$categoryarr=$category_array['data']['category'][0];
				$storeaccess=$category_array['data']['category'][0]['Store'];
				$productTypeaccess=$category_array['data']['category'][0]['productTypeId'];
				if($storeaccess != '')
				{
					$strAccess=explode(',', $storeaccess);
                	$data['storeaccess'] = $strAccess;
				}
				else{
					$data['storeaccess'] = array();
				}

				if($productTypeaccess != '')
				{
					$proTypeaccess = explode(',',$productTypeaccess);
					$data['proTypeaccess'] = $proTypeaccess;

				}else{
					$data['proTypeaccess'] = array();
				}
			//==========================

               
				$data['title'] = 'Category Details';
				$data['viewPage'] = 'control/category/edit_category';
				return view('control/includes/master_view',compact('data','categoryarr','storearr'));
			}
			else
			{
				return redirect('/oms/category/category-list');
				
			}
		}
		else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
        
    }
    
	//=======================Category list===========================//
    public function getList()
    {
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
		$data=array();
		$banner_array=array();
        if($userId != '')
        {
				//======== GET CATEGORY LIST FROM API =========//
				$curlUrl = 'api/category';
				$lastRow='';
				
				$curlPostData = array(
									'mode' => 'get_category_list',
									'lastRow' => $lastRow,
									'userId'=>$userId,
									'categoryid'=>'',
									'storeId' =>$userStoretoreId,
									'flag'=>'1'
								);
					
				$url = $baseUrl.$curlUrl;
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);
			//echo "<pre>";
		//print_r($category_array);
	//die();
            if(!empty($category_array) && $category_array['status'] == 1)
            {
	            $data['categorylist'] = $category_array['data']['category'];
				$data['title'] = 'Category';
				$data['viewPage'] = 'control/category/category_list';
				
				return view('control/includes/master_view',compact('data'));
			}
			else
			{
				$data['categorylist'] = array();
				$data['title'] = 'Category';
				$data['viewPage'] = 'control/category/category_list';
				return view('control/includes/master_view',compact('data'));
			}
			
			
		}
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }
	
	
	//=================edit category details=========================//
	public function updateCategoryDtls(Request $request)
	{
		$bannerFor=$request->post('bannerfor');
		$categoryName=$request->post('categoryName');
		$urlName=$request->post('urlName');
		$pageTitle=$request->post('pageTitle');
		$pageKeyword=$request->post('pageKeyword');
		$pageDesc=$request->post('pageDesc');
		$categoryDesc=$request->post('categoryDesc');
		$bannerText=$request->post('bannerText');
		$discount=$request->post('discount');
		$displayOrder=$request->post('displayOrder');
		$IsActive=$request->post('IsActive');
		$InSale=$request->post('InSale');
		$categoryId=$request->post('categoryId');
		$store=$request->post('store');
		$producttype = $request->post('producttype');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}
		if(!empty($producttype))
		{
			$producttypeAccess=implode(',', $producttype);
		}
		else{
			$producttypeAccess='';
		}

		
		$file=$request->file('image');

		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		if($InSale =='')
		{
			$InSaleVal='0';
		}
		else{
			$InSaleVal=$IsActive;
		}
		//======== insert category =========//
		$curlUrl = 'api/category';
		//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActive;echo "</pre>";
		// echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $categoryId!='' && $categoryName!='' && $pageTitle!='' && $pageKeyword!='' && $pageDesc!='' && $categoryDesc!='' && $IsActiveVal!='' && $displayOrder!='' && $strAccess!='' )
		{
			if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
			{
				$imagesfile= $file->getClientSize();
				$Image1FileSize=filesize($file);//$imagesfile;
				$Image1FileName = $file->getClientOriginalName();//['filename'];//$request->file('input_pdf')->getClientOriginalName();
				//echo $Image1FileSize.'@@@@'.$Image1FileName;die();
				$fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
				list($width,$height) = getimagesize($file);
				//echo $width.'       '.$height;die();
				$image_content = file_get_contents($file);
				$path = 'uploads/category_images/';
				$thumb_path = 'uploads/category_images/thumbnail/';
				$fileNormal = $path.$fileName;
				$fileThumb = $thumb_path.$fileName;
			   
				$this->uploadImage('public',$fileNormal,$image_content);
				$this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
			   
			}
			else
			{
				$fileName='';
				$Image1FileSize='';
				$Image1FileName ='';
			}	
				$curlPostData = array(
							'mode' => 'insert_category_details',
							'userId'=>$userId,
							'categoryid'=>$categoryId,
							'login_device_type'=>3,
							'categoryName'=>$categoryName,
							'urlName'=>$urlName,
							'pageTitle'=>$pageTitle,
							'pageKeyword'=>$pageKeyword,
							'pageDesc'=>$pageDesc,
							'categoryDesc'=>$categoryDesc,
							'bannerImage'=>$fileName,
							'bannerText'=>$bannerText,
							'IsActive'=>$IsActiveVal,
							'DisplayOrder' => $displayOrder,
							'Store' =>$strAccess,
							'discount'=>$discount,
							'salecategory' =>$InSaleVal,
							'productTypeId'=>$producttypeAccess
							);
				$url = $baseUrl.$curlUrl;
				
			// echo "<pre>";
			// print_r($curlPostData);
			// die();
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);

				//echo "<pre>";print_r($category_array);die();
				if($category_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$category_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$category_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/category/category-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/category/category-list');
		}
	}
	//==============================load add category view================================//
	public function addCategory()
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        $Is_superadmin = Session::get('Is_superadmin');
        $storeId = Session::get('storeId');
        if($Is_superadmin == 1)
        {
        	$userStoretoreId ='';
        }
        else{
        	$userStoretoreId = $storeId;
        }
        //echo $userId.'+++'.$Is_superadmin.'+++'. $storeId;die();
		if($userId != '')
        {
			$lastRow='';
			//======== GET STORE DETAILS FROM API =========//
	            $curlUrl = 'api/StoreManagement';
	            $curlPostData = array(
	                            'mode' => 'fetch_store',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeid'=>$userStoretoreId
	                            );//if u sent storeid = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$store = $this->fireCurl($url,$curlPostData);
	            $store_array = json_decode($store,1);
				 //echo "<pre>";
	           //print_r($store_array);die;
	            if(!empty($store_array) && $store_array['status'] == 1)
	            {
	                $data['store'] = $store_array['data']['store'];
	            }else{
	            	$data['store'] =array();
	            }
	        //======== GET PRODUCT TYPE FROM API =========//
	            $curlUrl = 'api/ProductType';
	            $curlPostData = array(
	                            'mode' => 'get_producttype_list',
	                            'lastrow' => $lastRow,
	                            'flag' => 1,
	                            'userId' => $userId,
	                            'storeId'=>$userStoretoreId,
	                            'producttypeid'=>''                            
	                            );//if u sent storeid = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
	            $url = $baseUrl.$curlUrl;
				$producttype = $this->fireCurl($url,$curlPostData);
	            $producttype_array = json_decode($producttype,1);
				 // echo "<pre>";
				 // print_r($curlPostData);
	    //        print_r($producttype_array);die;
	            if(!empty($producttype_array) && $producttype_array['status'] == 1)
	            {
	                $data['producttype'] = $producttype_array['data']['producttype'];
	            }else{
	            	$data['producttype'] =array();
	            }
	                
			$data['title'] = 'Category';
	        $data['viewPage'] = 'control/category/add_category';
	        return view('control/includes/master_view',compact('data'));
	    }
	    else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

	}
	

	//================================insert Category details===========================//
	public function insertCategoryDtls(Request $request)
	{
		// echo '<pre>';
		// print_r($_REQUEST);
		// die('hello there');
		$bannerFor   =$request->post('bannerfor');
		$categoryName=$request->post('categoryName');
		$urlName     =$request->post('urlName');
		$pageTitle   =$request->post('pageTitle');
		$pageKeyword =$request->post('pageKeyword');
		$pageDesc    =$request->post('pageDesc');
		$categoryDesc=$request->post('categoryDesc');
		$bannerText  =$request->post('bannerText');
		$discount    =$request->post('discount');
		$displayOrder=$request->post('displayOrder');
		$IsActive    =$request->post('IsActive');
		$InSale      =$request->post('InSale');
		$categoryId  =$request->post('categoryId');
		$store       =$request->post('store');
		$producttype = $request->post('producttype');
		if(!empty($store))
		{
			$strAccess=implode(',', $store);
		}
		else{
			$strAccess='';
		}
		if(!empty($producttype))
		{
			$producttypeAccess=implode(',', $producttype);
		}
		else{
			$producttypeAccess='';
		}




		$file=$request->file('image');

		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		if($InSale =='')
		{
			$InSaleVal='0';
		}
		else{
			$InSaleVal=$IsActive;
		}
		//======== insert category =========//
		$curlUrl = 'api/category';
		//echo "<pre>";echo $userId.'=='.$bannerFor.'=='.$bannerName.'=='.$bannerText.'=='.$bannerURL.'=='.$displayOrder.'=='.$IsActive;echo "</pre>";
		// echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $categoryName!='' && $pageTitle!='' && $pageKeyword!='' && $pageDesc!='' && $categoryDesc!='' && $IsActiveVal!='' && $displayOrder!='' && $strAccess!='' )
		{
			if(!empty($file) && strpos($file->getClientMimeType(),'image/') >= 0)
			{
				$imagesfile= $file->getClientSize();
				$Image1FileSize=filesize($file);//$imagesfile;
				$Image1FileName = $file->getClientOriginalName();//['filename'];//$request->file('input_pdf')->getClientOriginalName();
				//echo $Image1FileSize.'@@@@'.$Image1FileName;die();
				$fileName = time().rand(0,999). '.' . $file->getClientOriginalExtension();
				list($width,$height) = getimagesize($file);
				//echo $width.'       '.$height;die();
				$image_content = file_get_contents($file);
				$path = 'uploads/category_images/';
				$thumb_path = 'uploads/category_images/thumbnail/';
				$fileNormal = $path.$fileName;
				$fileThumb = $thumb_path.$fileName;
			   
				$this->uploadImage('public',$fileNormal,$image_content);
				$this->uploadThumbnail('public',$fileThumb,$image_content,50,50);
			   
			}
			else
			{
				$fileName='';
				$Image1FileSize='';
				$Image1FileName ='';
			}	
				$curlPostData = array(
							'mode' => 'insert_category_details',
							'userId'=>$userId,
							'categoryid'=>'',
							'login_device_type'=>3,
							'categoryName'=>$categoryName,
							'urlName'=>$urlName,
							'pageTitle'=>$pageTitle,
							'pageKeyword'=>$pageKeyword,
							'pageDesc'=>$pageDesc,
							'categoryDesc'=>$categoryDesc,
							'bannerImage'=>$fileName,
							'bannerText'=>$bannerText,
							'IsActive'=>$IsActiveVal,
							'DisplayOrder' => $displayOrder,
							'Store' =>$strAccess,
							'discount'=>$discount,
							'salecategory' =>$InSaleVal,
							'productTypeId'=>$producttypeAccess
							);
				$url = $baseUrl.$curlUrl;
				
			//echo "<pre>";
			//print_r($curlPostData);
			//die();
				$category = $this->fireCurl($url,$curlPostData);
				$category_array = json_decode($category,1);

				//echo "<pre>";print_r($category_array);die();
				if($category_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$category_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$category_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/category/category-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/category/category-list');
		}
		
		
	}
	
	//================================change status===================================//
	public function changeStatus(Request $request)
	{
		$categoryid=$request->segment(4);
		$status=$request->segment(5);
		
		$whereArr=array('CategoryID'=>$categoryid);
		$updatearr=array('DisplayCategory'=>$status, 'Status'=>$status);
		
        $updatebannerDetails=$this->update('categories',$whereArr,$updatearr);
		
		$request->session()->put('success_msg','Status updated successfully!');
		
		return Redirect::to('/oms/category/category-list');
	}
	

	///=============== delete a category ====================//
	public function deleteCategory(Request $request)
	{
		$id = $request->segment(4);
		if($id !='')
		{
			$ConditionCategory=array('CategoryID'=>$id);
			$fetchcategory=$this->getAllData('categories',$ConditionCategory,'CategoryID','DESC');
			//echo "<pre>";print_r($fetchbanner);die();
			if(!empty($fetchcategory))
			{
				// $previmage=$fetchcategory[0]['BannerImage'];
				// $delete = $this->delete('categories',array('CategoryID'=>$id));
				// if($delete)
				// {
				// 	if($previmage !='')
				// 	{
				// 		$normalpath=storage_path().'/app/'.'public/uploads/category_images/'.$previmage;
				// 		$thumbnailpath=storage_path().'/app/'.'public/uploads/category_images/thumbnail/'.$previmage;
				// 		//echo $normalpath.'thumbnailPath='.$thumbnailpath;die();
				// 		if(file_exists($normalpath))
				// 		{
				// 			unlink(storage_path().'/app/'.'public/uploads/category_images/'.$previmage);
				// 		}
				// 		if(file_exists($thumbnailpath))
				// 		{
				// 			unlink(storage_path().'/app/'.'public/uploads/category_images/thumbnail/'.$previmage);
				// 		}
				// 	}
					
				// 	$request->session()->put('success_msg',"Category deleted.");
				// }
				// else
				// {
				// 	$request->session()->put('error_msg',"Failed to delete category.");
				// }
				$status = '2';
				$whereArr=array('CategoryID'=>$id);
				$updatearr=array('DisplayCategory'=>'0', 'Status'=>$status);
				
		        $updatebannerDetails=$this->update('categories',$whereArr,$updatearr);
				$request->session()->put('success_msg',"Category deleted.");
				
			}
			else
			{
				$request->session()->put('error_msg',"Please provide valid details.");
			}
		}
		else
		{
			$request->session()->put('error_msg',"Please provide valid details.");
		}
		return Redirect::to('/oms/category/category-list');
	}
}
   
   
?>