<!-- ############ LAYOUT END-->
</div>
<script>
   //======== hide the alert msg ============//
    setTimeout(function(){
        $(".alert").fadeOut('slow');
    },6000);
   </script>
<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  {!! HTML::script('scripts/jquery.min.js') !!}
<!-- Bootstrap -->
  {!! HTML::script('scripts/popper.min.js') !!}
  {!! HTML::script('scripts/bootstrap.min.js') !!}
<!-- core -->
 {!! HTML::script('scripts/pace.min.js') !!}
  {!! HTML::script('scripts/pjax.js') !!}

  {!! HTML::script('scripts/lazyload.config.js') !!}
  {!! HTML::script('scripts/lazyload.js') !!}
  {!! HTML::script('scripts/plugin.js') !!}
  {!! HTML::script('scripts/nav.js') !!}
  {!! HTML::script('scripts/scrollto.js') !!}
  {!! HTML::script('scripts/toggleclass.js') !!}
  {!! HTML::script('scripts/theme.js') !!}
  {!! HTML::script('scripts/ajax.js') !!}
  {!! HTML::script('scripts/app.js') !!}
<!-- endbuild -->
</body>
</html>