<!-- Main -->
    <div class="content-main staic_table_custom" id="content-main">
 
        <!-- ############ Main START-->
        <div class="padding">
            <div class="container">
            <div class="clearfix top_export_sec">
                <div class="clearfix breadcram">
                    <a href="{!! url('/') !!}/oms/producttype/type-list"><i class="fa fa-angle-left"></i>Product Type Summary</a>
                </div>
                <div class="left_head">
                    <h2>Edit Product Type</h2>
                   <a href="#" class="eml_anchor" data-toggle="modal" data-target="#usr_email_modal">
                        <!-- <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M4 14V6.943l5.42 3.87a.993.993 0 0 0 1.16.001L16 6.944V14H4zm9.88-8L10 8.77 6.12 6h7.76zM17 4H3a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z"></path>
                        </svg> -->
                        <span>In this section, you can edit a Product Type.</span>
                    </a>
                </div>
            </div>

            <div class="row">
                    <div class="col-sm-12">
                    {!! Form::open(array('aria-labelledby' => 'formlabel', 'id' =>'add_brand', 'class' => 'form-horizontal', 'url' => '/oms/producttype/edit-type-details','files' => true, 'enctype'=>'multipart/form-data')) !!} 
                    <input type="hidden" id="brandId" name="brandId" value="@if(isset($producttype['producttypeid'])) {!! $producttype['producttypeid'] !!} @endif">
                        <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Edit Product Type</h6>
                                </header>
                                <div class="form-row">
                                    <div class="form-group col-md-9">
                                        <label for="usr5" class="ad_prd_lbl">Product Type Name *</label>
                                         <input type="text" class="form-control need" id="brandName" name="brandName" value="@if(isset($producttype['producttypeName'])) {!! $producttype['producttypeName'] !!} @endif" label="product type Name" placeholder="product type Name" autocomplete="off">
                                         <span id="brandName_error" style="color:red"></span>
                                    </div>
                                </div>
                            </div>             
                        </div>
                        <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Display Properties</h6>
                                </header>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">Display level * (Note: should be a number e.g 1)</label>
                                        <input type="text" class="form-control isNumberKey need" id="displayOrder" name="displayOrder" value="@if(isset($producttype['displayLevel'])) {!! $producttype['displayLevel'] !!} @endif" label="Display Order" placeholder="" autocomplete="off">
                                         <span id="displayOrder_error" style="color:red"></span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl col-md-6">Store *:</label>
                                        <select id="store" name="store[]" class="custom-select w-100 " autocomplete="off" label="Store" multiple >
                                            @if(!empty($data['store']))
                                              @foreach($data['store'] as $store)
 <option value="{!! $store['StoreId'] !!}" @if(!empty($data['storeaccess']) && (in_array($store['StoreId'], $data['storeaccess']))){!! 'selected' !!}@endif>{!! $store['StoreName'].' ('.$store['CountryName'].')' !!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span id="store_error" style="color:red"></span>
                                    </div>
                                </div>
                                 <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl col-md-6">Colour *:</label>
                                        <select id="colour" name="colour[]" class="custom-select w-100 " autocomplete="off" label="Colour" multiple >
                                            @if(!empty($data['colourData']))
                                              @foreach($data['colourData'] as $colour)
<option value="{!! $colour['Id'] !!}" @if(!empty($data['colourAccess']) && (in_array($colour['Id'], $data['colourAccess']))){!! 'selected' !!}@endif>{!! ucfirst($colour['Name']) !!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span id="colour_error" style="color:red"></span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl col-md-6">Material *:</label>
                                        <select id="material" name="material[]" class="custom-select w-100 " autocomplete="off" label="Material" multiple >
                                            @if(!empty($data['MaterialData']))
                                              @foreach($data['MaterialData'] as $Material)
<option value="{!! $Material['Id'] !!}" @if(!empty($data['materialAccess']) && (in_array($Material['Id'], $data['materialAccess']))){!! 'selected' !!}@endif>{!! ucfirst($Material['Name']) !!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span id="material_error" style="color:red"></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl col-md-6">Measure Unit *:</label>
                                        <select id="unitName" name="unitName[]" class="custom-select w-100 " autocomplete="off" label="measureUnit" multiple >
                                            @if(!empty($data['measureUnit']))
                                              @foreach($data['measureUnit'] as $measureUnit)
                                                  <option value="{!! $measureUnit['id'] !!}" @if(!empty($data['measureUnitaccess']) && (in_array($measureUnit['id'], $data['measureUnitaccess']))){!! 'selected' !!}@endif>{!! $measureUnit['unitName'] !!}</option>

                                                    
                                                @endforeach
                                            @endif
                                        </select>
                                        <span id="unitName_error" style="color:red"></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">Display In Menu List </label>
                                        <input type="checkbox" class="col-md-1 " id="displayInMenulist" name="displayInMenulist" value="1" label="Display In Menu List"  autocomplete="off" @if($producttype['displayInMenulist'] == '1'){!! 'checked' !!}@endif>
                                         <span id="displayInMenulist_error" style="color:red"></span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">Is Active </label>
                                        <input type="checkbox" class="col-md-1" id="IsActive" name="IsActive" label="Is Active" autocomplete="off" value='1' @if($producttype['status'] == '1'){!! 'checked' !!}@endif>
                                         <span id="IsActive_error" style="color:red"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="img" name="img" value="">
                        <div class="form-row">
                                <div class="save_but">                            
                                <button class="mailbut_all float-right" type="button" id="user_create" onclick="update();">Submit</button>
                                <button type="button" class="but_all  float-right cancel_action" id="cancel_update">Cancel</button>
                            </div>
                        </div>
                             {!!Form::close()!!} 
                </div>
            </div>

         </div>
       </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

 <script type="text/javascript">
   
      $("#cancel_update").click(function() {
       // alert('ok');
       window.location.href="{!! url('/') !!}/oms/producttype/type-list";
      });
      function update()
    {
            var has_log_error = 0;
            var store=$('#store').val();
            // alert(store);
            var banner=$('#img').val();
            var colour    =$('#colour').val();
            var material  =$('#material').val();

        $('.need').each(function(){
            var elem_id = $(this).attr('id');
            var elem_val = $(this).val();
            var elem_label = $(this).attr('label');
            var error_div = $("#"+elem_id+"_error");
            if(elem_val.search(/\S/) === -1)
            {
                has_log_error++;
                $("#"+elem_id).css('border-color','red');
                error_div.html(elem_label+' is required.');
            }
            else
            {
                $("#"+elem_id).css('border-color','');
                error_div.html('');
            }
        });
            if(store == '')
            {
                $('#store_error').html('Please Enter Store.');
                $("#store_error").css('border-color','red');
                has_log_error++;
            }
            else
            {
                $('#store_error').html('');
                $("#store_error").css('border-color','');
            }
             if(colour == '')
            {
                $('#colour_error').html('Please Select Colour.');
                $("#colour_error").css('border-color','red');
                has_log_error++;
            }
            else
            {
                $('#colour_error').html('');
                $("#colour_error").css('border-color','');
            }
            if(material == '')
            {
                $('#material_error').html('Please Select Material.');
                $("#material_error").css('border-color','red');
                has_log_error++;
            }
            else
            {
                $('#material_error').html('');
                $("#material_error").css('border-color','');
            }

        if(has_log_error > 0 )
        {
            //$("#"+elem_id).focus();
            return false;
        } 
        else
        {
            $("#add_brand").submit();
        }
    }

    
      </script>
  