<style>

                .table.dataTable thead>tr>th:first-child
                 {
                    width: 125px !important;
                 }
                .table.dataTable thead>tr>th.sorting:nth-child(2)
                {
                width: 133px !important; 
                } 
                .table.dataTable thead>tr>th.sorting:nth-child(3)
                {
                width: 94px !important;
                }
                .table.dataTable thead>tr>th.sorting:nth-child(4)
                {
                width: 99px !important;
                }
                .table.dataTable thead>tr>th.sorting:nth-child(5)
                {
                width: 142px !important;
                }
                .table.dataTable thead>tr>th.sorting:nth-child(6)
                {
                width: 144px !important; 
                }
                .table.dataTable thead>tr>th.sorting:nth-child(7)
                {
                width: 69px !important;
                }
                .table.dataTable thead>tr>th.sorting:nth-child(8)
                {
                width: 82px !important;
                }
                .table.dataTable thead>tr>th.sorting:nth-child(9) {
                width: 21px !important;
                }
                #example_wrapper
                {
                margin-bottom: 2% !important;
                }
    </style>



<div class="content-main staic_table_custom" id="content-main">
        <!-- ############ Main START-->
        <div class="padding">
            <div class="clearfix top_export_sec">
            <div class="left_head">
            <h2>Product Type Summary</h2>
            <div class="export_but">
                
            </div>
                </div>
                <div class="create_order"><a href="{!! url('/') !!}/oms/producttype/add-type">Add Product Type</a></div>
                </div>
            <div class="box">
                <div class="top_filter">
                
        </div>
                
        <div class="padding">
                <div class="table-responsive table-responsive_custom">
                    <table id="example" class="table table-bordered example_cbd_table" cellspacing="0" width="100%" style="font-size: 12px; min-width: 1020px !important;">
            <thead>
                <tr>
                    <th class="no-sort" style="display:none;">
                    </th>
                    <th>Product Type</th>
                    <th>Position</th>
                    
                    <th>Hits</th>
                    <th>Menu</th>
                    <th>Status</th>
                    <th>Edit </th>
                    <th>Delete</th> 
                </tr>
            </thead>
            <tbody>
           
                
                @if(!empty($data['producttype']))
                        @foreach($data['producttype'] as $allproducttype)
                            <?php $totalProduct = 0; ?>      
                                <tr>
                                    <td style="display:none;"></td>
                                    <td class="tdleft">
                                        <span title="{!! $allproducttype['producttypeName'] !!}" class="textellips" style="width: 133px">
                                           {!! $allproducttype['producttypeName'] !!}
                                        </span>
                                    </td>
                                    <td >
                                        <span title="{!! $allproducttype['displayLevel'] !!}" class="textellips" style="width: 94px">
                                           {!! $allproducttype['displayLevel'] !!}
                                        </span>
                                    </td>
                                  
                                    <td>
                                        <span title="{!! $allproducttype['numberofHits'] !!}" class="textellips" style="width: 99px">
                                        @if(!empty($allproducttype['numberofHits'])) {!! $allproducttype['numberofHits'] !!} @else {!! '0' !!} @endif
                                        </span>
                                    </td>
                                   
                                            @if($allproducttype['displayInMenulist'] == 'null' || $allproducttype['displayInMenulist'] == '' )
                                            <td><input type="checkbox" id="displayInMenulist" name="displayInMenulist" value="0"></td>
                                            @else
                                            <td><input type="checkbox" id="displayInMenulist" name="displayInMenulist" value="1" checked=""></td>
                                            @endif 
                                   
                                    <td>
                                     @if($allproducttype['status'] == 1)
                                     <span style="cursor:pointer;" class="tdcenter" title="Active brand" onclick="changeStauts('{!! $allproducttype['producttypeid'] !!}', 0);">Active
                                        <i class="fa fa-check-circle"></i>
                                     </span>
                                      @elseif($allproducttype['status'] == 2)
                                      <span  title="Deleted Brand" >Deleted
                                        <i class="fa fa-trash-o"></i>
                                      </span>
                                     @else
                                      <span style="cursor:pointer;" class="tdcenter" title="Inactive Brand" onclick="changeStauts('{!! $allproducttype['producttypeid'] !!}', 1);">Inactive
                                        <i class="fa fa-ban"></i>
                                      </span>       
                                     @endif
                                     </td>
                                     <td>
                                      <a href="{!! url('/') !!}/oms/producttype/type-details/{!! base64_encode($allproducttype['producttypeid']) !!}">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                      </a>
                                     </td>
                                    <td class="tdcenter">
                                        <!--=============delete Category ==============-->
                                        <i class="fa fa-trash-o" aria-hidden="true" onclick="deleteBrand('{!! $allproducttype['producttypeid'] !!}');"></i>
                                    </td>
                                </tr>
                        @endforeach                
                @endif                       
            </tbody>
        </table>
               
                
                
                        <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                
                        </div>
               
            </div>
                </div>
                        
                    
        </div>
        </div>
        
        
        
        
      
        <!--Delete products-->
        <!--<div class="modal" id="productDeleteModal" tabindex="-1" role="dialog">-->
        <!--  <div class="modal-dialog" role="document">-->
        <!--    <div class="modal-content">-->
        <!--      <div class="modal-header">-->
        <!--        <h5 class="modal-title" id="exampleModalLabel">Delete <span class="selection_count">27</span> products ?</h5>-->
        <!--        <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
        <!--          <span aria-hidden="true">×</span>-->
        <!--        </button>-->
        <!--      </div>-->
        <!--      <div class="modal-body">-->
        <!--        <div class="clearfix row">-->
        <!--            <div class="col-sm-12">-->
        <!--                <label>Deleted products cannot be recovered. Do you still want to continue?</label>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--      </div>-->
        <!--      <div class="modal-footer pop_foot">-->
        <!--        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>-->
        <!--        <button type="button" class="btn btn-danger">Delete <span class="selection_count"></span></button>-->
        <!--      </div>-->
        <!--    </div>-->
        <!--  </div>-->
        <!--</div>-->
        
        
        <!-- ############ Main END-->
            <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        
        <!--add-tag from input--15.2.2018-->
            <script src="/scripts/plugins/bootstrap-tagsinput.min.js"></script>
        <!--end add-tag from input--15.2.2018-->
        

            <script>
                $(document).ready(function() {    
                   $('#example').dataTable( {
                        "columnDefs": [ {
                          "targets"  : 'no-sort',
                          "orderable": false,
                          "order": []
                        }]
                    });
                //$("#example_info,#example_paginate,#example_length").remove();
                });
               
               
                
                
                
        
        
                
    </script>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" type="text/css" />
   
   <script>
        //============================ delete category ===========================//
        function deleteBrand(id)
        {
                if(confirm("Do you want to delete this Product Type?"))
                {
                        window.location.href="{!! url('/') !!}/oms/producttype/delete-type/"+id;
                }
        }
        //=====================script for change status start======================//
        function changeStauts(storeid,status)
        {
        
                var confirmpromo=confirm('Are you sure you want to change status');
                if(confirmpromo)
                {
                        location.href="{!! url('/') !!}/oms/producttype/change-status/"+storeid+"/"+status;
                }
        }
        
   </script>
   