

<!-- Main -->
    <div class="content-main staic_table_custom" id="content-main">
 
        <!-- ############ Main START-->
        <div class="padding">
            <div class="container">
            <div class="clearfix top_export_sec">
                <div class="clearfix breadcram">
                    <a href="{!! url('/') !!}/oms/producttype/type-list"><i class="fa fa-angle-left"></i>Product Type Summary</a>
                </div>
                <div class="left_head">
                    <h2>Add Product Type</h2>
                   <a href="#" class="eml_anchor" data-toggle="modal" data-target="#usr_email_modal">
                        <!-- <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M4 14V6.943l5.42 3.87a.993.993 0 0 0 1.16.001L16 6.944V14H4zm9.88-8L10 8.77 6.12 6h7.76zM17 4H3a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z"></path>
                        </svg> -->
                        <span>In this section, you can create a new product type.</span>
                    </a>
                </div>
            </div>

            <div class="row">
                    <div class="col-sm-12">
                    {!! Form::open(array('aria-labelledby' => 'formlabel', 'id' =>'add_productttype', 'class' => 'form-horizontal', 'url' => '/oms/producttype/insert-type-details','files' => true, 'enctype'=>'multipart/form-data')) !!} 
                    <input type="hidden" id="productTypeId" name="productTypeId" value="">
                        <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Add a New Product Type</h6>
                                </header>
                                <div class="form-row">
                                    <div class="form-group col-md-9">
                                        <label for="usr5" class="ad_prd_lbl">Product Type Name *</label>
                                         <input type="text" class="form-control need" id="productTypeName" name="productTypeName" value="" label="Product Type Name" placeholder="Product Type Name" autocomplete="off">
                                         <span id="productTypeName_error" style="color:red"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Display Properties</h6>
                                </header>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">Display level * (Note: should be a number e.g 1)</label>
                                        <input type="text" class="form-control isNumberKey need" id="displayOrder" name="displayOrder" value="" label="Display Order" placeholder="" autocomplete="off">
                                         <span id="displayOrder_error" style="color:red"></span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl col-md-6">Store *:</label>
                                        <select id="store" name="store[]" class="custom-select w-100 " autocomplete="off" label="Store" multiple >
                                            @if(!empty($data['store']))
                                              @foreach($data['store'] as $store)
                                                    <option value="{!! $store['StoreId'] !!}">{!! $store['StoreName'].' ('.$store['CountryName'].')' !!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span id="store_error" style="color:red"></span>
                                    </div>
                                </div>
                            </div>            
                        </div>
                        <div class="box">
                            <div class="order_details">
                                                             
                                <div class="form-row">
                                  
                                    <!-- colour -->
                                    
                                    <div class="">
                                        <table id="form-result_table" class="table table-bordered cbd_table_product_table" cellspacing="0" width="100%">
                                          <thead>
                                              <tr>
                                                <!--<th class="no-sort">&nbsp;</th>-->
                                                <th>Colour</th>
                                                
                                                <th class="inventory_div hide">Colour</th>
                                                <th></th>
                                              </tr>
                                            </thead>
                                            <tbody id="allColour">
                                              <tr>
                                                <td class="tb_input">
                                                  <input type="text" class="form-control varneed need" id="colour_0" name="colour[]" placeholder="Colour" autocomplete='off'>
                                                </td>                                                  
                                                <!-- <td>
                                                    <div class="input-group-addon">
                                                    <i></i>
                                                    </div>
                                                </td> -->
                                                
                                                <td class="tb_input">
                                                    <button class="but_all btn_rmv removeColour" type="button" style="display:none">
                                                        <svg id="delete-minor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                            <path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z"></path>
                                                        </svg>
                                                    </button>
                                                </td>
                                              </tr>
                                          </tbody>
                                        </table>
                                        <div class="form-row">
                                            <div class="form-group col-sm-12">
                                                <button class="but_all" type="button" id="add_more_colour">Add more Colour</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="order_details">                                                             
                                <div class="form-row">
                                    <!-- materials -->
                                    <div class="">
                                        <table id="form-result_table" class="table table-bordered cbd_table_product_table" cellspacing="0" width="100%">
                                          <thead>
                                              <tr>
                                                <!--<th class="no-sort">&nbsp;</th>-->
                                                <th>Material</th>
                                                
                                                <th class="inventory_div hide">Material</th>
                                                <th></th>
                                              </tr>
                                            </thead>
                                            <tbody id="allMaterial">
                                              <tr>
                                                <td class="tb_input">
                                                  <input type="text" class="form-control varneed1 need" id="material_0" name="material[]" placeholder="Material" autocomplete='off'>
                                                </td>                                                  
                                              
                                                
                                                <td class="tb_input">
                                                    <button class="but_all btn_rmv removeMaterial" type="button" style="display:none">
                                                        <svg id="delete-minor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                            <path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z"></path>
                                                        </svg>
                                                    </button>
                                                </td>
                                              </tr>
                                          </tbody>
                                        </table>
                                        <div class="form-row">
                                            <div class="form-group col-sm-12">
                                                <button class="but_all" type="button" id="add_more_material">Add more Material</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="order_details">                                                             
                                <div class="form-row">
                                    <!-- size -->
                                    <div class="">
                                        <table id="form-result_table" class="table table-bordered cbd_table_product_table" cellspacing="0" width="100%">
                                          <thead>
                                              <tr>                                                
                                                <th>Size</th>                                                
                                                <th class="inventory_div hide">Size</th>                                                
                                              </tr>
                                            </thead>
                                            <tbody id="allSize">
                                              
                                              <tr>
                                                <td>
                                                    <div class="col-sm-12 form-group">
                                                        <label class="">EU Chart</label>
                                                        <div class="select_field">
                                                            <input class="field_box varneed2 need isNumberKey" id="size_0" placeholder="Size" type="text" name='size0[]' label="Size" maxlength="100" autocomplete="off">
                                                            <select id="size_unit0" class="custom-select sizeselect" name="size_unit0[]" autocomplete="off">
                                                                <option selected="selected" value="1">cm</option>
                                                                <option value="2">inches</option>
                                                                <option value="3">mm</option>
                                                            </select>
                                                        </div>
                                                        <span id="size_unit_error" style="color:red"></span>
                                                    </div>
                                                    </td>
                                                    <td>
                                                   <div class="col-sm-12 form-group">
                                                        <label class="">USA Chart</label>
                                                        <div class="select_field">
                                                            <input class="field_box varneed2 need isNumberKey" id="size_00" placeholder="Size" type="text" name='size1[]' label="Size" maxlength="100" autocomplete="off">
                                                            <select id="size_unit00" class="custom-select sizeselect" name="size_unit00[]" autocomplete="off">
                                                                <option selected="selected" value="1">cm</option>
                                                                <option value="2">inches</option>
                                                                <option value="3">mm</option>
                                                            </select>
                                                        </div>
                                                        <span id="size_unit_error" style="color:red"></span>
                                                    </div>
                                                    </td>
                                                    <td>
                                                    <div class="col-sm-12 form-group">
                                                        <label class="">ASIA Chart</label>
                                                         <div class="select_field">
                                                            <input class="field_box varneed2 need isNumberKey" id="size_000" placeholder="Size" type="text" name='size2[]' label="Size" maxlength="100" autocomplete="off">
                                                            <select id="size_unit000" class="custom-select sizeselect" name="size_unit000[]" autocomplete="off">
                                                                <option selected="selected" value="1">cm</option>
                                                                <option value="2">inches</option>
                                                                <option value="3">mm</option>
                                                            </select>
                                                        </div>
                                                        <span id="size_unit_error" style="color:red"></span>
                                                    </div>
                                                </td>
                                                <td class="tb_input" style="padding-top: 36px;">
                                                    
                                                        <button class="but_all btn_rmv removeSize" type="button" style="display:none">
                                                            <svg id="delete-minor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                                <path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z"></path>
                                                            </svg>
                                                        </button>
                                                    
                                                </td>
                                              </tr>
                                          </tbody>
                                        </table>
                                        <div class="form-row">
                                            <div class="form-group col-sm-12">
                                                <button class="but_all" type="button" id="add_more_size">Add more Size</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                       
                         <div class="box">
                            <div class="order_details">                                                             
                                <div class="form-row">

                                </div>
                                <!-- <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl col-md-6">Measure Unit *:</label>
                                        <select id="unitName" name="unitName[]" class="custom-select w-100 " autocomplete="off" label="measureUnit" multiple >
                                            @if(!empty($data['measureUnit']))
                                              @foreach($data['measureUnit'] as $measureUnit)
                                                    <option value="{!! $measureUnit['id'] !!}">{!! $measureUnit['unitName'] !!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span id="unitName_error" style="color:red"></span>
                                    </div>
                                </div> -->
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">Display In Menu List </label>
                                        <input type="checkbox" class="col-md-1 " id="displayInMenulist" name="displayInMenulist" value="1" label="Display In Menu List"  autocomplete="off">
                                         <span id="displayInMenulist_error" style="color:red"></span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">Is Active </label>
                                        <input type="checkbox" class="col-md-1" id="IsActive" name="IsActive" label="Is Active" autocomplete="off" value='1'>
                                         <span id="IsActive_error" style="color:red"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="img" name="img" value="">
                        <div class="form-row">
                                <div class="save_but">                            
                                <button class="mailbut_all float-right" type="button" id="user_create" onclick="update();">Submit</button>
                                <button type="button" class="but_all  float-right cancel_action" id="cancel_update">Cancel</button>
                            </div>
                        </div>
                             {!!Form::close()!!} 
                </div>
            </div>

         </div>
       </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

 <script type="text/javascript">
   
      $("#cancel_update").click(function() {
       // alert('ok');
       window.location.href="{!! url('/') !!}/oms/producttype/type-list";
      });
      function update()
    {
            var has_log_error = 0;
             var store    =$('#store').val();
             var unitName =$('#unitName').val();

             var colour    =$('#colour').val();
             var material  =$('#material').val();

            // alert(store);
            var banner=$('#img').val();
        $('.need').each(function(){
            var elem_id = $(this).attr('id');
            var elem_val = $(this).val();
            var elem_label = $(this).attr('label');
            var error_div = $("#"+elem_id+"_error");
            if(elem_val.search(/\S/) === -1)
            {
                has_log_error++;
                $("#"+elem_id).css('border-color','red');
                $("#"+elem_id).css('background','#ff00001f');
                error_div.html(elem_label+' is required.');
            }
            else
            {
                $("#"+elem_id).css('border-color','');
                $("#"+elem_id).css('background','');
                error_div.html('');
            }
        });
            if(store == '')
            {
                $('#store_error').html('Please Select Store.');
                $("#store_error").css('border-color','red');
                has_log_error++;
            }
            else
            {
                $('#store_error').html('');
                $("#store_error").css('border-color','');
            }

            if(colour == '')
            {
                $('#colour_error').html('Please Select Colour.');
                $("#colour_error").css('border-color','red');
                has_log_error++;
            }
            else
            {
                $('#colour_error').html('');
                $("#colour_error").css('border-color','');
            }
            if(material == '')
            {
                $('#material_error').html('Please Select Material.');
                $("#material_error").css('border-color','red');
                has_log_error++;
            }
            else
            {
                $('#material_error').html('');
                $("#material_error").css('border-color','');
            }

            // if(unitName == '')
            // {
            //     $('#unitName_error').html('Please Select Measure Unit .');
            //     $("#unitName_error").css('border-color','red');
            //     has_log_error++;
            // }
            // else
            // {
            //     $('#unitName_error').html('');
            //     $("#unitName_error").css('border-color','');
            // }
        if(has_log_error > 0 )
        {
            //$("#"+elem_id).focus();
            return false;
        } 
        else
        {
            //alert('ok');
            $("#add_productttype").submit();
        }
    }


$( document ).ready(function() {
    $('.my-colorpicker2').colorpicker();
    console.log( "ready!" );
   var_count = 1;
   var_count1 = 1;
   var_count2 = 1;
});
    //=================== add more Colour ==================//
$("#add_more_colour").click(function(){
     varError = 0;
     $('.varneed').each(function(){
        var_count++
          var variantVal = $(this).val();
          var variantid = $(this).attr('id');
          if(variantVal.search(/\S/) === -1)
          {
               varError++;
               $("#"+variantid).css('border-color','red');
               if(varError === 1)
               {
                    $("#"+variantid).focus();
               }
          }
          else
          {
               $("#"+variantid).css('border-color','');
          }
     });
     if(varError === 0)
     {
          var hideClass = 'hide';
          var inventory_status = parseInt($('#inputState').val());
          if(inventory_status === 1)
          {
               hideClass = '';
          }
          var newColourContent = "<tr>\
                              <td class='tb_input'>\
                                <input type='text' class='form-control varneed user_need need' id='colour_"+var_count+"' name='colour[]' placeholder='Colour'>\
                              </td>\
                              <td class='tb_input'>\
                                  <button class='but_all btn_rmv removeColour' type='button'>\
                                      <svg id='delete-minor' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>\
                                          <path d='M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z'></path>\
                                      </svg>\
                                  </button>\
                              </td>\
                          </tr>";
          $("#allColour").append(newColourContent);
          if($("#form-result_table tr").length > 1)
          {
               $(".removeColour").css('display','block');
          }
          else
          {
               $(".removeColour").css('display','none');
          }
     }
});
//================== remove Colour ===============//
$(document).on('click','.removeColour',function(){
     var confirmVar = confirm("Are you sure want to remove this Colour?");
     if(confirmVar)
     {
          if($(this).parent('td').parent('tr').remove())
          {
               if($("#form-result_table tr").length > 6)
               {
                    $(".removeColour").css('display','block');
               }
               else
               {
                    $(".removeColour").css('display','none');
               }
          }
     }
});


 //=================== add more Material ==================//
$("#add_more_material").click(function(){
     varError = 0;
     $('.varneed1').each(function(){
        var_count1++;
          var variantVal = $(this).val();
          var variantid = $(this).attr('id');
          if(variantVal.search(/\S/) === -1)
          {
               varError++;
               $("#"+variantid).css('border-color','red');
               if(varError === 1)
               {
                    $("#"+variantid).focus();
               }
          }
          else
          {
               $("#"+variantid).css('border-color','');
          }
     });
     if(varError === 0)
     {
          var hideClass = 'hide';
          var inventory_status = parseInt($('#inputState').val());
          if(inventory_status === 1)
          {
               hideClass = '';
          }
          var newMaterialContent = "<tr>\
                              <td class='tb_input'>\
                                <input type='text' class='form-control varneed1 need' id='material_"+var_count1+"' name='material[]' placeholder='Material'>\
                              </td>\
                              <td class='tb_input'>\
                                  <button class='but_all btn_rmv removeMaterial' type='button'>\
                                      <svg id='delete-minor' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>\
                                          <path d='M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z'></path>\
                                      </svg>\
                                  </button>\
                              </td>\
                          </tr>";
          $("#allMaterial").append(newMaterialContent);
          if($("#form-result_table tr").length > 1)
          {
               $(".removeMaterial").css('display','block');
          }
          else
          {
               $(".removeMaterial").css('display','none');
          }
     }
});
//================== remove Material ===============//
$(document).on('click','.removeMaterial',function(){
     var confirmVar = confirm("Are you sure want to remove this Material?");
     if(confirmVar)
     {
          if($(this).parent('td').parent('tr').remove())
          {
            // var tt = $("#form-result_table tr").length;
            // alert(tt);
               if($("#form-result_table tr").length > 6)
               {
                    $(".removeMaterial").css('display','block');
               }
               else
               {
                    $(".removeMaterial").css('display','none');
               }
          }
     }
});


 //=================== add more Size ==================//
$("#add_more_size").click(function(){
     varError = 0;
     $('.varneed2').each(function(){
        var_count2++;
          var variantVal = $(this).val();
          var variantid = $(this).attr('id');

          console.log(variantid);
          if(variantVal.search(/\S/) === -1)
          {
               varError++;
               //$("#"+variantid).css('border-color','red');
               $("#"+variantid).css('background','#ff00001f')
               if(varError === 1)
               {
                    $("#"+variantid).focus();
               }
          }
          else
          {
               $("#"+variantid).css('border-color','');
               $("#"+variantid).css('background','')
          }
     });
     if(varError === 0 )
     {
          var hideClass = 'hide';
          var inventory_status = parseInt($('#inputState').val());
          if(inventory_status === 1)
          {
               hideClass = '';
          }
          var newSizeContent = '<tr>\
                                <td>\
                                <div class="col-sm-12 form-group">\
                                    <label class="">EU Chart</label>\
                                    <div class="select_field">\
                                        <input class="field_box varneed2 need isNumberKey" id="size_'+var_count2+'" placeholder="Size" type="text" name="size0[]" label="Size" maxlength="" autocomplete="off">\
                                        <select id="size_'+var_count2+'" class="custom-select sizeselect" name="size_unit0[]" autocomplete="off">\
                                            <option selected="selected" value="1">cm</option>\
                                            <option value="2">inches</option>\
                                            <option value="3">mm</option>\
                                        </select>\
                                    </div>\
                                    <span id="srh5_error" style="color:red"></span>\
                                </div>\
                                </td>\
                                <td>\
                                <div class="col-sm-12 form-group">\
                                    <label class="">USA Chart</label>\
                                    <div class="select_field">\
                                        <input class="field_box varneed2 need isNumberKey" id="size_'+var_count2+var_count2+'" placeholder="Size" type="text" name="size1[]" label="Size" maxlength="" autocomplete="off">\
                                        <select id="size_'+var_count2+var_count2+'" class="custom-select sizeselect" name="size_unit00[]" autocomplete="off">\
                                            <option selected="selected" value="1">cm</option>\
                                            <option value="2">inches</option>\
                                            <option value="3">mm</option>\
                                        </select>\
                                    </div>\
                                    <span id="srh5_error" style="color:red"></span>\
                                </div>\
                                </td>\
                                <td>\
                                <div class="col-sm-12 form-group">\
                                    <label class="">ASIA Chart</label>\
                                    <div class="select_field">\
                                        <input class="field_box varneed2 need isNumberKey" id="size_'+var_count2+var_count2+var_count2+'" placeholder="Size" type="text" name="size2[]" label="Size" maxlength="" autocomplete="off">\
                                        <select id="size_'+var_count2+var_count2+var_count2+'" class="custom-select sizeselect" name="size_unit000[]" autocomplete="off">\
                                                <option selected="selected" value="1">cm</option>\
                                                <option value="2">inches</option>\
                                                <option value="3">mm</option>\
                                        </select>\
                                    </div>\
                                    <span id="srh5_error" style="color:red"></span>\
                                </div>\
                            </td>\
                              <td class="tb_input" style="padding-top: 36px;">\
                                  <button class="but_all btn_rmv removeSize" type="button">\
                                      <svg id="delete-minor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">\
                                          <path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z"></path>\
                                      </svg>\
                                  </button>\
                              </td>\
                          </tr>';
          $("#allSize").append(newSizeContent);
          if($("#form-result_table tr").length > 1)
          {
               $(".removeSize").css('display','block');
          }
          else
          {
               $(".removeSize").css('display','none');
          }
     }
});
//================== remove Size ===============//
$(document).on('click','.removeSize',function(){
     var confirmVar = confirm("Are you sure want to remove this Size?");
     if(confirmVar)
     {
          if($(this).parent('td').parent('tr').remove())
          {
            // var ttt = $("#form-result_table tr").length;
            // alert(ttt);
               if($("#form-result_table tr").length > 6)
               {
                    $(".removeSize").css('display','block');
               }
               else
               {
                    $(".removeSize").css('display','none');
               }
          }
     }
});


function filterDigitss(eventInstance) {
//alert(eventInstance);
eventInstance = eventInstance || window.event;
    key = eventInstance.keyCode || eventInstance.which;
    //alert(key);
if ((46 <= key) && (key < 58) || key == 8 ) {
    return true;
} else {
        if (eventInstance.preventDefault) 
             eventInstance.preventDefault();
        eventInstance.returnValue = false;
        return false;
    } //if
}
      </script>
  