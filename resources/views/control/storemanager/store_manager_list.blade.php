<style>
 .table.dataTable thead>tr>th.sorting:nth-child(6){
    width: 60px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(7){
    width: 50px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(5){
    width: 100px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(4){
    width: 107px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(3){
    width: 90px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(2){
    width: 216px !important;

   /* display: block;
    width: 156px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;*/
    }
    .textellips{
      display: block;    
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;   
    }
</style>
<div class="content-main staic_table_custom" id="content-main">
        <!-- ############ Main START-->
        <div class="padding">
            <div class="clearfix top_export_sec">
            <div class="left_head">
            <h2>Store Managers</h2>
            <div class="export_but">
                
            </div>
                </div>
                <div class="create_order"><a href="{!! url('/') !!}/oms/storemanager/store-manager-add">Add Store Manager</a></div>
                </div>
            <div class="box">
                <div class="top_filter">
                
        </div>
                
        <div class="padding">
                <div class="table-responsive table-responsive_custom">
                    <table id="example" class="table table-bordered example_cbd_table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="no-sort" style="display:none;">
                    </th>
                    <th>Name</th>
                    <!-- <th>last Name</th> -->
                    <th>Email</th>
                   <!--  <th>password</th> -->
                    <th>Access</th>
                    <th>Roles</th>
                    <th>Status</th>
                    <th>Edit </th>
                    <!-- <th>Delete</th> -->
                </tr>
            </thead>
            <tbody>
           
                
                @if(!empty($data['store-manager']))
                        @foreach($data['store-manager'] as $allstore) 

                                 @php 
                                 $storeaccessName2 = '';
                                 $storeaccessName = array();
                                  if($allstore['AccessStore'] != '')
                                    {
                                        $strAccess=explode(',', $allstore['AccessStore']);
                                        
                                            

                                            foreach($strAccess as $strVal)
                                            {
                                                $whereStoreSizeCondition = array('storeid' =>$strVal);
                                                $getstoresizedetails = DB::table('Store')->select('StoreName')->where($whereStoreSizeCondition)->get();
                                                
                                                if(isset($getstoresizedetails[0]->StoreName))
                                                {
                                                    array_push($storeaccessName,$getstoresizedetails[0]->StoreName);
                                                }
                                            }

                                        $storeaccessName1 = $storeaccessName;
                                    }
                                    else{
                                        $storeaccessName1 =array();
                                    }
                                  
                                   $storeaccessName2 = implode(', ',$storeaccessName1);

                                @endphp

                                <tr>
                                    <td style="display:none;"></td>
                                    <td class="tdleft">
                                       <!--  <span @if(strlen($allstore['firstname'].' '.$allstore['lastname']) > 35) title="{!! $allstore['firstname'].' '.$allstore['lastname']!!}" @endif>
                                        @if(strlen($allstore['firstname'].' '.$allstore['lastname']) > 35)
                                                     {!! substr($allstore['firstname'].' '.$allstore['lastname'],0,35) !!}...
                                                @else
                                                     {!! $allstore['firstname'].' '.$allstore['lastname'] !!}
                                                @endif
                                        </span>
 -->
                                <span class="textellips" style="width: 216px" title="{!! $allstore['firstname'].' '.$allstore['lastname'] !!}">
                                    {!! $allstore['firstname'].' '.$allstore['lastname'] !!}
                                </span>
                                       
                                    </td>
                                    <!-- <td>
                                        <span >
                                           {!! $allstore['lastname'] !!}
                                        </span>
                                    </td> -->
                                    <td class="tdleft">
                                        <span >
                                         {!! $allstore['email'] !!}
                                        </span>
                                    </td>
                                    <td class="tdcenter">
                                        <span>
                                            {!! $storeaccessName2 !!}
                                        </span>
                                    </td>
                                    <td class="tdcenter">
                                            <span class="textellips" style="width: 118px" title="{!! $allstore['roles'] !!}">
                                                {!! ucfirst($allstore['roles']) !!}
                                            </span>
                                    </td>
                                    <td>
                                     @if($allstore['status'] == 1)
                                     <span style="cursor:pointer;" class="tdcenter" title="Active Store Manager" onclick="changeStauts('{!! $allstore['StoreManagerId'] !!}', 0);">Active
                                        <i class="fa fa-check-circle"></i>
                                     </span>
                                     @else
                                      <span style="cursor:pointer;" class="tdcenter" title="Inactive Store Manager" onclick="changeStauts('{!! $allstore['StoreManagerId'] !!}', 1);">Blocked
                                        <i class="fa fa-ban"></i>
                                      </span>       
                                     @endif
                                     </td>
                                     <td>
                                      <a href="{!! url('/') !!}/oms/storemanager/store-manager-edit/{!! base64_encode($allstore['StoreManagerId']) !!}">
                                       <i class="fa fa-pencil" aria-hidden="true"></i>
                                      </a>
                                     </td>
                                   
                                </tr>
                        @endforeach                
                @endif                       
            </tbody>
        </table>
               
                
                
                        <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                
                        </div>
               
            </div>
                </div>
                        
                    
        </div>
        </div>
        
        
        
        
      
        <!--Delete products-->
        <!--<div class="modal" id="productDeleteModal" tabindex="-1" role="dialog">-->
        <!--  <div class="modal-dialog" role="document">-->
        <!--    <div class="modal-content">-->
        <!--      <div class="modal-header">-->
        <!--        <h5 class="modal-title" id="exampleModalLabel">Delete <span class="selection_count">27</span> products ?</h5>-->
        <!--        <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
        <!--          <span aria-hidden="true">×</span>-->
        <!--        </button>-->
        <!--      </div>-->
        <!--      <div class="modal-body">-->
        <!--        <div class="clearfix row">-->
        <!--            <div class="col-sm-12">-->
        <!--                <label>Deleted products cannot be recovered. Do you still want to continue?</label>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--      </div>-->
        <!--      <div class="modal-footer pop_foot">-->
        <!--        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>-->
        <!--        <button type="button" class="btn btn-danger">Delete <span class="selection_count"></span></button>-->
        <!--      </div>-->
        <!--    </div>-->
        <!--  </div>-->
        <!--</div>-->
        
        
        <!-- ############ Main END-->
            <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        
        <!--add-tag from input--15.2.2018-->
            <script src="/scripts/plugins/bootstrap-tagsinput.min.js"></script>
        <!--end add-tag from input--15.2.2018-->
        

            <script>
                $(document).ready(function() {    
                   $('#example').dataTable( {
                        "columnDefs": [ {
                          "targets"  : 'no-sort',
                          "orderable": false,
                          "order": []
                        }]
                    });
                //$("#example_info,#example_paginate,#example_length").remove();
                });
               
              
                
                
                
        
        
                
    </script>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" type="text/css" />
   
   <script>
        //============================ delete promo code ===========================//
        // function deleteBanner(id)
        // {
        //         if(confirm("Do you want to delete this promo code?"))
        //         {
        //                 window.location.href="{!! url('/') !!}/oms/banner/deletebanner/"+id;
        //         }
        // }
         //=====================script for change status start======================//
        function changeStauts(storeid,status)
        {
        
                var confirmpromo=confirm('Are you sure you want to change status');
                if(confirmpromo)
                {
                        location.href="{!! url('/') !!}/oms/storemanager/store-manager-change-status/"+storeid+"/"+status;
                }
        }
        
   </script>
   