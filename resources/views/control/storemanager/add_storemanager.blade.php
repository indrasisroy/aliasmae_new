<!-- Main -->
    <div class="content-main staic_table_custom" id="content-main">
 
        <!-- ############ Main START-->
        <div class="padding">
            <div class="container">
            <div class="clearfix top_export_sec">
                <div class="clearfix breadcram">
                    <a href="{!! url('/') !!}/oms/storemanager/store-list"><i class="fa fa-angle-left"></i>Store Managers </a>
                </div>
                <div class="left_head">
                    <h2>Add Store Manager</h2>
                   <a href="#" class="eml_anchor" data-toggle="modal" data-target="#usr_email_modal">
                        <!-- <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M4 14V6.943l5.42 3.87a.993.993 0 0 0 1.16.001L16 6.944V14H4zm9.88-8L10 8.77 6.12 6h7.76zM17 4H3a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z"></path>
                        </svg> -->
                        <span>In this section, you can create a new Store Manager.</span>
                    </a>
                </div>
            </div>

            <div class="row">
                    <div class="col-sm-12">
                    {!! Form::open(array('aria-labelledby' => 'formlabel', 'id' =>'add_storemanager', 'class' => 'form-horizontal', 'url' => '/oms/storemanager/store-manager-insert','files' => true, 'enctype'=>'multipart/form-data')) !!} 
                    <input type="hidden" id="storemanagerId" name="storemanagerId" value="">
                        <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Add a New Store Manager</h6>
                                </header>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">First Name *</label>
                                        <input type="text" class="form-control need" id="firstName" name="firstName" value="" label="First Name" placeholder="First Name" autocomplete="off">
                                         <span id="firstName_error" style="color:red"></span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">Last Name *</label>
                                        <input type="text" class="form-control need" id="lastName" name="lastName" value="" label="Last Name" placeholder="Last Name" autocomplete="off">
                                         <span id="lastName_error" style="color:red"></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">Email *</label>
                                        <input type="text" class="form-control need" id="email" name="email" value="" label="Email" placeholder="" autocomplete="off">
                                         <span id="email_error" style="color:red"></span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">Password * </label>
                                        <input type="text" class="form-control need" id="password" name="password" value="" label="Password" placeholder="" autocomplete="off">
                                         <span id="password_error" style="color:red"></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="usr5" class="ad_prd_lbl">Username *</label>
                                        <input type="text" class="form-control need" id="username" name="username" value="" label="Username" placeholder="" autocomplete="off">
                                         <span id="username_error" style="color:red"></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="Store Access" class="ad_prd_lbl">Store Access</label> 
                                    </div>
                                </div>
                                <div class="form-row">
                                    
                                            <!-- <div class="form-group col-md-12">
                                                <label class="custom-control custom-checkbox ad_prd_lbl">
                                                    <input type="checkbox" name="select-all" id="select-all" class="custom-control-input chek_1 chkRow"/>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Select All</span>
                                                </label>   
                                            </div> -->
                                            @if(isset($data['store']) && !empty($data['store']))
                                                @foreach($data['store'] as $store_details)
                                                <div class="form-group col-md-1">
                                                    <label class="custom-control custom-checkbox ad_prd_lbl">
                                                        <input type="radio" class="custom-control-input chek_1 " name="storeaccess[]" value="{!! $store_details['StoreId'] !!}">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{!! $store_details['StoreName'] !!}</span>
                                                    </label>   
                                                </div>
                                                @endforeach
                                            <span id="store_error" style="color:red"></span>
                                            @endif  
                                                                 
                                </div>
                                <div class="form-row">
                                    
                                        <div class="form-group col-md-9">
                                            <label for="usr5" class="ad_prd_lbl">Is Active </label>
                                            <input type="checkbox" class="col-md-1" id="IsActive" name="IsActive" label="Is Active" autocomplete="off" value='1'>
                                             <span id="IsActive_error" style="color:red"></span>
                                        </div>
                                    
                                </div>

        <!-- $storeId = Session::get('storeId'); -->
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                      <label for="usr5" class="ad_prd_lbl ">Roles *</label>
                                      <select id="roles" name="roles" class="custom-select w-100 " autocomplete="off" label="Roles"  >
                                            <!-- <option value="">Select one...</option> -->
                                            @if (Session::get('Is_superadmin') == '1')
                                                <option value="1">Super Admin</option>                                            
                                                 <option value="1">Admin</option>                                            
                                                  <option value="1">Manager</option>                                            
                                            @endif
                                                <option value="0">User</option>
                                            
                                      </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="Page Access" class="ad_prd_lbl">Page Access</label> 
                                    </div>
                                </div>
                                <div class="form-row">
                                    @if(isset($data['adminmanagement']) && !empty($data['adminmanagement']))
                                        <div class="form-group col-md-12">
                                            <label class="custom-control custom-checkbox ad_prd_lbl">
                                                <input type="checkbox" name="select-all-pageaccess" id="select-all-pageaccess" class="custom-control-input chek_1 chkRow2">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Select All</span>
                                            </label>   
                                        </div>

                                    
                                        @foreach($data['adminmanagement'] as $adminmanagement_details)
                                        <div class="form-group col-md-2">
                                            <label class="custom-control custom-checkbox ad_prd_lbl">
                                                <input type="checkbox" class="custom-control-input chek_1 chkRow2" name="pageaccess[]" value="{!! $adminmanagement_details['id'] !!}">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{!! ucfirst($adminmanagement_details['ManagementName']) !!}</span>
                                            </label>   
                                        </div>
                                        @endforeach

                                        <span id="adminmanagement_error" style="color:red"></span>
                                    @endif


                                   <!--  <div class="form-group col-md-2">
                                        <label class="custom-control custom-checkbox ad_prd_lbl">
                                            <input type="checkbox" class="custom-control-input chek_1 chkRow2" name="pageaccess[]" value="2">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Store Managers</span>
                                        </label>   
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label class="custom-control custom-checkbox ad_prd_lbl">
                                            <input type="checkbox" class="custom-control-input chek_1 chkRow2" name="pageaccess[]" value="9">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Customer Manager</span>
                                        </label>   
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label class="custom-control custom-checkbox ad_prd_lbl">
                                            <input type="checkbox" class="custom-control-input chek_1 chkRow2" name="pageaccess[]" value="15">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Product Manager</span>
                                        </label>   
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label class="custom-control custom-checkbox ad_prd_lbl">
                                            <input type="checkbox" class="custom-control-input chek_1 chkRow2" name="pageaccess[]" value="21">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Order Manager</span>
                                        </label>   
                                    </div>
                                    <span id="store_error" style="color:red"></span> -->
                                                                    
                                </div>


                            </div>
                        </div>
                        <input type="hidden" id="img" name="img" value="">
                        <div class="form-row">
                                <div class="save_but">                            
                                <button class="mailbut_all float-right" type="button" id="user_create" onclick="update();">Submit</button>
                                <button type="button" class="but_all  float-right cancel_action" id="cancel_update">Cancel</button>
                            </div>
                        </div>
                             {!!Form::close()!!} 
                </div>
            </div>

         </div>
       </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

 <script type="text/javascript">

      $("#cancel_update").click(function() {
       // alert('ok');
       window.location.href="{!! url('/') !!}/oms/storemanager/store-list";
      });
      function update()
    {
            var has_log_error = 0;
            var banner=$('#img').val();
        $('.need').each(function(){
            var elem_id = $(this).attr('id');
            var elem_val = $(this).val();
            var elem_label = $(this).attr('label');
            var error_div = $("#"+elem_id+"_error");
            if(elem_val.search(/\S/) === -1)
            {
                has_log_error++;
                $("#"+elem_id).css('border-color','red');
                error_div.html(elem_label+' is required.');
            }
            else
            {
                $("#"+elem_id).css('border-color','');
                error_div.html('');
            }
        });
        if(has_log_error > 0 )
        {
            //$("#"+elem_id).focus();
            return false;
        } 
        else
        {
            $("#add_storemanager").submit();
        }
    }

    $("#select-all").change(function(){  //"select all" change 
    $(".chkRow").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
    });

    //".checkbox" change 
    $('.chkRow').change(function(){ 
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(false == $(this).prop("checked")){ //if this item is unchecked
    $("#select-all").prop('checked', false); //change "select all" checked status to false
    }
    //check "select all" if all checkbox items are checked
    if ($('.chkRow:checked').length == $('.chkRow').length ){
    $("#select-all").prop('checked', true);
    }
    });


    $("#select-all-pageaccess").change(function(){  //"select all" change 
    $(".chkRow2").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
    });

    //".checkbox" change 
    $('.chkRow2').change(function(){ 
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(false == $(this).prop("checked")){ //if this item is unchecked
    $("#select-all-pageaccess").prop('checked', false); //change "select all" checked status to false
    }
    //check "select all" if all checkbox items are checked
    if ($('.chkRow2:checked').length == $('.chkRow2').length ){
    $("#select-all-pageaccess").prop('checked', true);
    }
    });
      </script>
  