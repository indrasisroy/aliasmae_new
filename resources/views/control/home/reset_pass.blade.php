@include('control/includes/index_header')
<!-- Main -->

<!-- ############ Main START-->
<div class="index_log">
    <div class="indx_login_form">
        <form class="w100" method="post" action="{!! url('/') !!}/oms/reset-password" id="pass_form">
            @csrf
            <input type="hidden" name="userId" value="{!! $data['userId'] !!}">
            <div class="indx_title text-center">
                <h2>Alias Mae OMS</h2>
                <span>Reset your password</span>
            </div>
            @if(Session::has('error_msg'))
                
                <div class="alert alert-danger">
                  <strong>Sorry! </strong>{{ Session::pull('error_msg')}} 
                </div>
            @elseif(Session::has('success_msg'))
                <div class="alert alert-success">
                  <strong>Success! </strong>{{ Session::pull('success_msg')}} 
                </div>
            @endif
            
            <div class="login_inner w100">
                <div class="form-group">
                    <div class="inner_log_inp  uid_inp">
                        <span class="inp_logo">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path fill="currentColor" d="M1 3l9 9 9-9z"></path>
                                <path d="M2 16V5.414l7.293 7.293c.195.195.45.293.707.293s.512-.098.707-.293L18 5.414V16H2zM16.586 4L10 10.586 3.414 4h13.172zM19 2H1c-.552 0-1 .447-1 1v14c0 .553.448 1 1 1h18c.552 0 1-.447 1-1V3c0-.553-.448-1-1-1z"></path>
                            </svg>
                        </span>
                        <span class="log_inp">
                            <input type="password" class="form-control logneed" placeholder="New Password" id="n_pass" name="n_pass" label="Password" value="">
                            
                        </span>
                    </div>
                    <div class="inner_log_inp upas_inp">
                        <span class="inp_logo">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path fill="currentColor" d="M3 9h14v10H3V9z"></path>
                                <path d="M17 8c.553 0 1 .447 1 1v10c0 .553-.447 1-1 1H3c-.553 0-1-.447-1-1V9c0-.553.447-1 1-1h1V6c0-3.31 2.69-6 6-6s6 2.69 6 6v2h1zM4 18h12v-8H4v8zM6 6v2h8V6c0-2.206-1.794-4-4-4S6 3.794 6 6zm4 10c.553 0 1-.447 1-1v-2c0-.553-.447-1-1-1s-1 .447-1 1v2c0 .553.447 1 1 1z"></path>
                            </svg>
                        </span>
                        <span class="log_inp">
                            <input type="password" placeholder="Confirm Password" class="form-control logneed" id="c_pass" name="c_pass" label="Confirm password" value="">
                            <span id="c_pass_error" style="color:red"></span>
                        </span>
                    </div>
                    <span id="reset_error" style="color:red"></span>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-primary w-100 submit_btn" onclick="doValidation()">Submit</button>
                </div>
                <div class="form-group">
                    <a href="{!! url('/oms') !!}" class="frgt_ps" >Go to home</a>
                </div>

            </div>
        </form>
    </div>
</div>
<!-- ############ Main END-->
@include('control/includes/index_footer')
 <script>
    function doValidation()
    {
        var has_log_error = 0;
        $('.logneed').each(function(){
            var elem_id = $(this).attr('id');
            var elem_val = $(this).val();
            var elem_label = $(this).attr('label');
            
            if(elem_val.search(/\S/) === -1)
            {
                has_log_error++;
                $("#"+elem_id).css('border-color','red');
                
            }
            else if(elem_id === 'c_pass' && elem_val !== $('#n_pass').val())
            {
                has_log_error++;
                $("#"+elem_id).css('border-color','red');
                $("#reset_error").html('Confirm password is not matching with your new password.');
            }
            else if(elem_val.length < 6)
            {
                has_log_error++;
                $("#"+elem_id).css('border-color','red');
                $("#reset_error").html('Your password must contain minimum 6 characters.');
            }
            else
            {
                $("#"+elem_id).css('border-color','');
                $("#reset_error").html('');
            }
            if(has_log_error === 1)
            {
                $("#"+elem_id).focus();
            }
        });
        if(has_log_error === 0)
        {
            $("#pass_form").submit();
        }
    }
    //======== hide the alert msg ============//
    setTimeout(function(){
        $(".alert").fadeOut('slow');
    },6000);
    
 </script>
