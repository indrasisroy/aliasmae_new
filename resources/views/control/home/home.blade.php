<!--  Main -->
<div class="content-main " id="content-main">

<!-- ############ Main START-->
<div class="dark">
	<div class="p-4 p-md-5">
		<div class="row">
			<div class="col-md-8">
			    <h1 class="display-4 l-s-n-1x"><span class="text-muted _300">Hello </span><span class="">{!! ucfirst($data['FirstName']) !!}</span></h1><!-- text-theme _700 -->
			    <div>
			    	<span>{!! $data['Username']!!}, <i class="fa fa-map-marker mx-2"></i>{!! $data['Location']!!}</span>
			    </div>
			    <!-- <div class="mt-5">
			    	<a href="javascript:void(0)" class="btn btn-outline b-a">Edit your account <i class="fa fa-long-arrow-right ml-2"></i></a>
			    </div> -->
		    </div>
		    <div class="col-md-4">
		    	<div class="d-flex my-3 text-center">
			    	<div class="lt p-3 px-4 r-l b-a">
			    		<span class="text-muted">New Orders</span>
			    		<div class="text-white text-lg">40</div>
			    	</div>
			    	<div class="lt p-3 px-4 r-r b-a no-b-l">
			    		<span class="text-muted">Unsent Orders</span>
			    		<div class="text-theme text-lg">20</div>
			    	</div>
		    	</div>
		    </div>
	    </div>
	</div>
</div>
<div class="row no-gutters white">
    <div class="col-12 col-sm-12 col-md-9 col-xl-9">
        <div class="padding">
           	<div class="row">
                <div class="col-12 col-sm-4 col-md-4 col-xl-4">

                    <div class="b-a b-2x r p-3 mb-4">
                       	<div class="py-3 text-centere svg_color">
                            <svg xmlns="http://www.w3.org/2000/svg" width="50px" height="50px" viewBox="0 0 20 20"><path fill="currentColor" d="M19 10c0 4.97-4.03 9-9 9s-9-4.03-9-9 4.03-9 9-9 9 4.03 9 9z"></path><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zM9.977 7c.026 0 .65.04 1.316.707.39.39 1.024.39 1.414 0s.39-1.024 0-1.415C12.104 5.69 11.493 5.372 11 5.2V5c0-.553-.447-1-1-1-.552 0-1 .447-1 1v.184C7.84 5.598 7 6.698 7 8c0 2.28 1.727 2.712 2.758 2.97C10.873 11.25 11 11.354 11 12c0 .55-.448 1-.976 1-.026 0-.65-.04-1.317-.708-.39-.39-1.023-.39-1.414 0s-.39 1.024 0 1.415c.604.603 1.215.92 1.707 1.092v.2c0 .55.448 1 1 1 .553 0 1-.45 1-1v-.186C12.162 14.4 13 13.3 13 12c0-2.28-1.726-2.713-2.757-2.97C9.128 8.75 9 8.644 9 8c0-.552.45-1 .977-1z"></path></svg>
                        </div>
                        <div class="seltext_outer">
                                <span class="text-center seal_text">No sales yet</span>
                        </div>
                    </div>
              	</div>
                <div class="col-12 col-sm-4 col-md-4 col-xl-4">
                    <div class="b-a b-2x r p-3 mb-4">
                        <div class="py-3 text-centere svg_color">
                            <svg xmlns="http://www.w3.org/2000/svg"  width="50px" height="50px" viewBox="0 0 20 20"><path fill="currentColor" d="M1 13h5l1 2h6l1-2h5v6H1z"></path><path d="M2 18h16v-4h-3.382l-.723 1.447c-.17.34-.516.553-.895.553H7c-.38 0-.725-.214-.895-.553L5.382 14H2v4zM19 1c.552 0 1 .448 1 1v17c0 .552-.448 1-1 1H1c-.552 0-1-.448-1-1V2c0-.552.448-1 1-1h4c.552 0 1 .448 1 1s-.448 1-1 1H2v9h4c.38 0 .725.214.895.553L7.618 14h4.764l.723-1.447c.17-.34.516-.553.895-.553h4V3h-3c-.552 0-1-.448-1-1s.448-1 1-1h4zM6.293 6.707c-.39-.39-.39-1.023 0-1.414s1.023-.39 1.414 0L9 6.586V1c0-.552.448-1 1-1s1 .448 1 1v5.586l1.293-1.293c.39-.39 1.023-.39 1.414 0s.39 1.023 0 1.414l-3 3c-.195.195-.45.293-.707.293s-.512-.098-.707-.293l-3-3z"></path></svg>
                        </div>
						<div class="seltext_outer">
						    <span class="text-center seal_text">No orders yet</span>
						</div>    
                    </div>
              	</div>
              	<div class="col-12 col-sm-4 col-md-4 col-xl-4">
                    <div class="b-a b-2x r p-3 mb-4">
                        <div class="py-3 text-centere svg_color">
                            
                            <svg xmlns="http://www.w3.org/2000/svg" width="50px" height="50px" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2"></path></svg>
						</div>
						<div class="seltext_outer">
							<span class="text-center seal_text">Total Hits:{!! $data['totalHits'] !!} </span>
						</div>
                    </div>
              	</div>
        	</div>
  		</div>
    </div>
	<div class="col-sm-4 col-xl-3 b-l white lt">
		<div class="p-3">
		    <span class="badge success float-right">15</span>
		  	<h6 class="mb-3">Activity</h6>
		  	<div class="streamline streamline-xs streamline-dotted">
		    	<div class="sl-item  b- ">
		      		<div class="sl-content">
		        		<span class="sl-date text-muted">16:00</span>
		        	<div>
		          	<a href="#" class="text-primary">Tiger Nixon</a>
		          		Implemented new API
		        </div>
		    </div>
		</div>
	    <div class="sl-item  b- ">
		    <div class="sl-content">
		        <span class="sl-date text-muted">15:00</span>
		        <div>
		          	<a href="#" class="text-primary">Pablo Nouvelle</a>
		          		Added API call to update track element
		        </div>
		    </div>
	    </div>
	    <div class="sl-item  b- ">
	      	<div class="sl-content">
	        	<span class="sl-date text-muted">11:30</span>
	        <div>
	        <a href="#" class="text-primary">Ashton Cox</a>
	        New feedback from John
	    </div>
	      </div>
	    </div>
	    <div class="sl-item  b- ">
	      <div class="sl-content">
	        <span class="sl-date text-muted">13:23</span>
	        <div>
	          <a href="#" class="text-primary">Rita Ora</a>
	          Sent you an email
	        </div>
	      </div>
	    </div>
	    <div class="sl-item  b-primary ">
	      <div class="sl-content">
	        <span class="sl-date text-muted">July 21</span>
	        <div>
	          <a href="#" class="text-primary">Summerella</a>
	          Submit a support ticket
	        </div>
	      </div>
	    </div>
	    <div class="sl-item  b- ">
	      <div class="sl-content">
	        <span class="sl-date text-muted">05:35</span>
	        <div>
	          <a href="#" class="text-primary">Fifth Harmony</a>
	          Bug fixes
	        </div>
	      </div>
	    </div>
	    <div class="sl-item  b- ">
	      <div class="sl-content">
	        <span class="sl-date text-muted">08:00</span>
	        <div>
	          <a href="#" class="text-primary">Brielle Williamson</a>
	          User experience improvements
	        </div>
	      </div>
	    </div>
	    <div class="sl-item  b- ">
	      <div class="sl-content">
	        <span class="sl-date text-muted">09:05</span>
	        <div>
	          <a href="#" class="text-primary">Jeremy Scott</a>
	          Assign you a task
	        </div>
	      </div>
	    </div>
	    <div class="sl-item  b- ">
	      <div class="sl-content">
	        <span class="sl-date text-muted">12:30</span>
	        <div>
	          <a href="#" class="text-primary">Airi Satou</a>
	          New features added
	        </div>
	      </div>
	    </div>
		  </div>
		</div>
	</div>
</div>

<!-- ############ Main END