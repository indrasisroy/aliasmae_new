@include('control/includes/index_header')
<!-- Main -->


<!-- ############ Main START-->
<div class="index_log">
    <div class="indx_login_form">
        <!-- <form class="w100" method="post" action="{!! url('/') !!}/doLogin" id="login_form"> -->
        {!! Form::open(array('id' =>'login_form', 'class' => 'w100', 'url' => '/oms/login')) !!}  
            <div class="indx_title text-center">
                <h2>Alias Mae OMS</h2>
               <!--  <span>Please enter your credentials</span> -->
                <!-- <span>Please enter your credentials</span> -->
            </div>
            <div class="alert alert-danger" style="display:none;">
                <strong>Sorry! </strong>
                <span id="error_msg"></span>
            </div>
                
            @if(Session::has('error_msg'))
                
                <div class="alert alert-danger">
                  <strong>Sorry! </strong>{{ Session::pull('error_msg')}} <span id="error_msg"></span>
                </div>
            @elseif(Session::has('success_msg'))
                <div class="alert alert-success">
                  <strong>Success! </strong>{{ Session::pull('success_msg')}} 
                </div>
            @endif
            <div class="login_inner w100">
                <div class="form-group">
                    <div class="inner_log_inp  uid_inp">
                        <span class="inp_logo">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path fill="currentColor" d="M1 3l9 9 9-9z"></path>
                                <path d="M2 16V5.414l7.293 7.293c.195.195.45.293.707.293s.512-.098.707-.293L18 5.414V16H2zM16.586 4L10 10.586 3.414 4h13.172zM19 2H1c-.552 0-1 .447-1 1v14c0 .553.448 1 1 1h18c.552 0 1-.447 1-1V3c0-.553-.448-1-1-1z"></path>
                            </svg>
                        </span>
                        <span class="log_inp">
                            <input type="text" placeholder="Username" class="form-control logneed" id="email" name="email" label="Email" value="{{ Cookie::get('email') }}">
                        </span>
                    </div>
                    <div class="inner_log_inp upas_inp">
                        <span class="inp_logo">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path fill="currentColor" d="M3 9h14v10H3V9z"></path>
                                <path d="M17 8c.553 0 1 .447 1 1v10c0 .553-.447 1-1 1H3c-.553 0-1-.447-1-1V9c0-.553.447-1 1-1h1V6c0-3.31 2.69-6 6-6s6 2.69 6 6v2h1zM4 18h12v-8H4v8zM6 6v2h8V6c0-2.206-1.794-4-4-4S6 3.794 6 6zm4 10c.553 0 1-.447 1-1v-2c0-.553-.447-1-1-1s-1 .447-1 1v2c0 .553.447 1 1 1z"></path>
                            </svg>
                        </span>
                        <span class="log_inp">
                            <input type="password" placeholder="Password" class="form-control logneed" id="password" name="password" label="Password" value="{{ Cookie::get('password') }}">
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-primary w-100 submit_btn" onclick="doValidation()">Log in</button>
                </div>
                <div class="form-group">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="remember" class="custom-control-input chek_1 chkRow" @if(Cookie::get('remember') == '1') checked='checked' @endif>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Remember me</span>
                    </label>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#forgotPass" class="frgt_ps">Forgot password?</a>
                </div>

            </div>
        <!-- </form> -->
        {!!Form::close()!!}
    </div>
</div>
<!-- ############ Main END-->
<!--================= forgot password modal ============================-->
  <div class="modal fade" id="forgotPass" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header indx_title text-center">
          <h2><span>Forgot Password</span></h2>
        </div>
        <div class="modal-body login_inner">
         <div class="form-group">
            <div class="inner_log_inp">
                <span class="inp_logo">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path fill="currentColor" d="M1 3l9 9 9-9z"></path>
                        <path d="M2 16V5.414l7.293 7.293c.195.195.45.293.707.293s.512-.098.707-.293L18 5.414V16H2zM16.586 4L10 10.586 3.414 4h13.172zM19 2H1c-.552 0-1 .447-1 1v14c0 .553.448 1 1 1h18c.552 0 1-.447 1-1V3c0-.553-.448-1-1-1z"></path>
                    </svg>
                </span>
                <span class="log_inp">
                    <input type="text" class="form-control forgotneed" placeholder="Enter email or username" id="for_username" name="for_username" label="Email or username" value="">
                </span>
            </div>
            <span id="forgot_error" style="color:red"></span>
        </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary" onclick="forgotValidate()">Submit</button>
        </div>
      </div>
      
    </div>
  </div>
<!--=================== end of forgot password modal ==========================-->
@include('control/includes/index_footer')

<script>
 
 //==================enter key press login====================//
 
 $(document).ready(function(){
 
    $('.logneed').keypress(function (e) {
     if (e.which == 13) {
     
       doValidation();
       return false;    
     }
   });

 });
 

    function doValidation()
    {
        var has_log_error = 0;
        $('.logneed').each(function(){
            var elem_id = $(this).attr('id');
            var elem_val = $(this).val();
            var elem_label = $(this).attr('label');
            
            if(elem_val.search(/\S/) === -1)
            {
                has_log_error++;
                $("#"+elem_id).css('border-color','red');
                if(has_log_error === 1)
                {
                    $("#"+elem_id).focus();
                }
            }
            else
            {
                $("#"+elem_id).css('border-color','');
                /*if (elem_id == 'email')
                {
                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if (!emailReg.test(elem_val))
                    {
                        has_log_error++;
                        $("#"+elem_id).css('border-color','red');
                        $("#"+elem_id).focus();
                        $('.alert-danger').css('display', 'block');
                        $('#error_msg').html('Invalid email format');
                    }
                    else
                    {
                        $("#"+elem_id).css('border-color','');
                        $('#error_msg').html('');
                        $('.alert-danger').css('display', 'none');
                    }
                }*/
            }
        });
        if(has_log_error === 0)
        {
            $("#login_form").submit();
        }
    }
    //======== hide the alert msg ============//
    setTimeout(function(){
        $(".alert").fadeOut('slow');
    },6000);
    
    //============== validate forgot password form ============//
    function forgotValidate()
    {
        var for_user = $("#for_username").val();
        $('#forgot_error').html('');
        if(for_user.search(/\S/) === -1)
        {
            $("#for_username").css('border-color','red');
            $("#for_username").focus();
        }
        else
        {
            $.ajax({
                type: "POST",
                url: "{!! url('/') !!}/oms/get-email",
                data: {'username':for_user},
                success: function(data)
                {
                    //alert(data);
                    //console.log(data);
                    if(data === '1')
                    {
                        location.reload();
                    }
                    else
                    {
                        $('#forgot_error').html('Invalid username or email.');
                    }
                }
            });
        }
    }
 </script>