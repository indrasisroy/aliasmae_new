<?php
$userId = Session::get('loginUserId');
$adminId = Session::get('userId');
//$u_details = DB::table('admin')->select('image','storeName')->where(array('id'=>$userId))->get()->toArray();
//$admin_details = DB::table('admin')->select('storeName','its_superadmin','logo')->where(array('id'=>$adminId))->get()->toArray();
///$site_details = DB::table('site_settings')->select('site_name')->where(array('id'=>'1'))->get()->toArray();
/*if(!empty($u_details) && $u_details[0]->image != '')
{
    $logImage_src =url('/')."/storage/userImage/thumbnail/".$u_details[0]->image;
    $logImage_name=$u_details[0]->image;
}
else
{
    $logImage_src =url('/')."/storage/userImage/profile_default.png";
    $logImage_name='profile_default.png';
}
$logo_global = '';
if($admin_details[0]->its_superadmin != '1' && $admin_details[0]->storeName != '' )
{
	$storeName_global = $admin_details[0]->storeName;
}
else
{
	$storeName_global = $site_details[0]->site_name;
}
if($admin_details[0]->its_superadmin != '1' && $admin_details[0]->logo != '' && $admin_details[0]->logo != NULL)
{
	$logo_global = url('/')."/storage/uploadFiles/thumbnail/".$admin_details[0]->logo;
	
}
else if($admin_details[0]->its_superadmin == 1)
{
	$logo_global = url('/')."/images/Seed_ERP.png";
}*/
?>
{{-- this is the header page --}}
@include('control/includes/header')
{{-- this is main content --}}
@include($data['viewPage'])

{{-- this is the external js used only for this page --}}
@if(isset($data['js']) && $data['js'] != '')
		{!! HTML::script('developerJS/'.$data['js']) !!}
@endif
<script>
     //===================Number check=====================//
   $(document).on('keypress','.isNumberKey',function(evt){
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        {
            return false;
        }
        else
        {
            return true;
        }
	});
      //===================Alphanumeric check=====================//
   $(document).on('keypress','.isAlphaNum',function(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if((charCode >= 33 && charCode <= 37) || charCode === 42 || (charCode >= 91 && charCode <= 95) || charCode===123 || charCode=== 125 || charCode === 126 || (charCode >= 60 && charCode <= 64))
        {
            return false;
        }
        else
        {
            return true;
        }
	});
//================ reload current page =================//
/*$(document).on('click','.cancel_action',function(){
	window.location.href = window.location.href;
});*/
</script>
{{-- this is the footer page --}}
@include('control/includes/footer')