<style>
.table.dataTable thead>tr>th.sorting:nth-child(2){
    width: 99px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(3){
    width: 79px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(4){
    width: 99px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(5){
    width: 99px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(6){
    width: 70px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(7){
    width: 65px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(8){
    width: 65px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(9){
    width: 65px !important;
}
.save_text_box_outer{
    margin-top: 30px;
}
</style>
    <!-- Main -->
    <div class="content-main staic_table_custom" id="content-main">
         <?php $userId = Session::get('userId'); ?>
        <!-- ############ Main START-->
        <div class="padding">
            <div class="clearfix top_export_sec">
            <div class="left_head">
                <?php 
                    $dateval = date('Y');
                if($userId == 1)
                {
                ?>
                    <h2>Product Profiles</h2>
                <?php
                }
                else
                { 
                ?>
                    <h2>Product Variations</h2>
                    <?php

                }
                ?>
            <div class="export_but">
                
            </div>
                </div>
                <?php 
                // $userId = Session::get('userId'); 

                if($userId == 1)
                {
                ?>

                <div class="create_order"><a href="{!! url('/') !!}/oms/product/add-product">Add Product</a></div>
              
                <button class="exportbutt" type="button" style="float:right;">
                    <i class="fa fa-download"></i>
                    Export
                </button>
                <button class="exportbutt importbtcls" type="button" style="float:right;">
                    <i class="fa fa-upload"></i>
                    Import
                </button>
                  <?php 

                }

                ?>
                <form name="fileimportform" id="fileimportform" method="post" enctype="multipart/form-data" action="{!! url('/') !!}/oms/product/product-list/importproduct">
                    <input type="file" name="fileimport" id="fileimport" style="display: none;">
                </form>
                </div>
            <div class="box">
                <!-- <div class="top_filter">
                    <ul>
                        <li class="active"><a href="#">All</a></li>
                    </ul>
                </div> -->

                    


                <div class="product_flter row">

                    <div class="product_flter_blk col-sm-2" >
                        <div class="flter_cnt">
                            <label class="marg6"> Product Type:</label>
                        </div>
                         <div class="filter_product">
                            <select id='producttype' name='producttype' class='cust_select form-control' autocomplete='off' label='Product Type' >
                                     @if(!empty($data['ProductType']))
                                          @foreach($data['ProductType'] as $Producttype)
                                              <option value="{!! $Producttype['producttypeid'] !!}">{!! ucfirst($Producttype['producttypeName']) !!}</option>
                                          @endforeach
                                        @endif
                                    
                            </select>
                        </div>
                        
                    </div>

                    <div class="product_flter_blk col-sm-2">
                        <div class="flter_cnt">
                            <label class="marg6">Category 1:</label>
                        </div>
                         <div class="filter_product">
                            <select name="category" class="cust_select form-control" id="categoryid">
                                <option value=>Select one</option>
                                @if(!empty($data['categorylist']))
                                    @foreach($data['categorylist'] as $category)
                                        <option value="{!! $category['CategoryID'] !!}">{!! $category['CategoryName'] !!}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                    </div>

                   

                    <div class="product_flter_blk col-sm-2">
                        <div class="flter_cnt">
                            <label class="marg6">Category 2:</label>
                        </div>
                         <div class="filter_product">
                            <select name="category" class="cust_select form-control" id="subcategoryid">
                                <option value="">Select one</option>
                                @if(!empty($data['categorylist']))
                                    @foreach($data['categorylist'] as $category)
                                        <option value="{!! $category['CategoryID'] !!}">{!! $category['CategoryName'] !!}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        
                    </div>

                      <div class="product_flter_blk col-sm-2">
                        <div class="flter_cnt">
                            <label class="marg6"> Season:</label>
                        </div>
                         <div class="filter_product">
                            <select name="category" class="cust_select form-control" id="sessionid">
                                <option value="">Select one...</option>
                                @if(!empty($data['season']))
                                @foreach($data['season'] as $seasonData)
                                <option value="{!! $seasonData['Id'] !!}">{!! ucfirst($seasonData['Name']) !!}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        
                    </div>

                    <div class="product_flter_blk col-sm-2">
                        <div class="flter_cnt">
                            <label class="marg6"> Brand:</label>
                        </div>
                         <div class="filter_product">
                            <select id="subBrand" name="subBrand" class="cust_select form-control" autocomplete="off" label="Brand"  >
                                        <option value="">Select one...</option>
                                         @if(!empty($data['brandlist']))
                                          @foreach($data['brandlist'] as $brand)
                                              <option value="{!! $brand['brandId'] !!}">{!! strtoupper($brand['brandName']) !!}</option>
                                          @endforeach
                                        @endif
                            </select>
                        </div>
                        
                    </div>

                    <!-- year of manufacture starts here  -->
                      <div class="product_flter_blk col-sm-2">
                        <div class="flter_cnt">
                            <label class="marg6"> Year:</label>
                        </div>

                         <div class="filter_product">
                            <select id="manufacturedyear" name="manufacturedyear" class="cust_select form-control" autocomplete="off" label="Brand"  >
                                        <option value="">Select one...</option>
                                            
                                            @for($i=$dateval;$i>1970;$i--)
                                         
                                              <option value="{!! $i !!}">{!! $i !!}</option>
                                            @endfor
                            </select>
                        </div>
                        
                    </div>

                       

                    <!-- year of manufacture ends here  -->
                       

                     <div class="product_flter_blk col-sm-2 save_text_box_outer" >
                        <div class="flter_cnt">
                            <label class="marg6"> </label>
                        </div>
                         <div class="filter_product ">
                            <input type="button" class="btn btn-default" value="Clear Search" onclick="reloadpagefunc()" style="color: white;background:#000">
                        </div>
                        
                    </div>

                    

                   <!--  <div class="product_flter_blk col-sm-3 save_text_box_outer" style="display: none;">
                        <div class="flter_cnt">
                            <label class="marg6"> Alphabatically:</label>
                        </div>
                         <div class="filter_product">
                            <select name="country" class="cust_select form-control" id="dropdown2">
                                <option value="all">All</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                                <option value="D">D</option>
                                <option value="E">E</option>
                                <option value="F">F</option>
                                <option value="G">H</option>
                                <option value="I">I</option>
                                <option value="J">J</option>
                                <option value="K">K</option>
                                <option value="L">L</option>
                                <option value="M">M</option>
                                <option value="N">N</option>
                                <option value="O">O</option>
                                <option value="P">P</option>
                                <option value="Q">Q</option>
                                <option value="R">R</option>
                                <option value="S">S</option>
                                <option value="T">T</option>
                                <option value="U">U</option>
                                <option value="V">V</option>
                                <option value="W">W</option>
                                <option value="X">X</option>
                                <option value="Y">Y</option>
                                <option value="Z">Z</option>
                            </select>
                        </div>
                    </div>

                     -->

                </div>
               
                <div class="padding mycustmpadngnew">
                       <?php 

                    // echo '<pre>';
                    // print_r($data['productdata']);
                    // die('hello there');
                ?>
                <div class="table-responsive table-responsive_custom" id="datatablerespajax">
                    <table id="example" class="table display table-bordered example_cbd_table btmscrool" cellspacing="0" width="100%" style="min-width:1020px !important;font-size: 12px">
            <thead >
                <tr>
                    <th class="no-sort" style="display:none;">
                    </th>
                    
                    <th>Name</th>
                    <th>Factory ID</th>
                    <th>Product Type</th>
                    <th>Category 1</th>
                    <!-- <th>Edit</th> -->
                    <th>Category 2</th>
                    <th>Season</th>
                    <th>Brand</th>
                    <th>Year</th>
                    <th>Edit</th>
                    <!-- <th>Delete</th> -->
                </tr>
            </thead>
            <tbody >

                @if(!empty($data['productdata']))
                                    @foreach($data['productdata'] as $k=>$product)
                                        
                                    <!-- product name length starts here -->

                                    <!-- product name length ends here -->

                                        <tr class="oddRow">
                                            <td style="display:none;">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input chek_1 chkRow">
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                            </td>
                                            
                                            <td class="tdleft">
                                                <span title="{!! $product->styleName !!} " class="textellips" style="width: 99px">

                                                        {!! $product->styleName !!}


                                                </span>
                                            </td>
                                            <td class="tdleft">
                                            <span title="{!! $product->factoryId !!}" class="textellips" style="width: 79px">
                                                {!! $product->factoryId !!}
                                            </span>
                                            </td>
                                            <!-- <td></td> -->
                                            <!-- product type data starts here -->

                                    <?php 

                                        $fetchproducttypeqry = DB::table('ProductType')->where('producttypeid',$product->productType)->first();
                                        $fetchproducttypename="";
                                        if(isset($fetchproducttypeqry) && !empty($fetchproducttypeqry)){
                                        $fetchproducttypename= $fetchproducttypeqry->producttypeName;

                                        }
                                    ?>
        
                                            <td class="tdcenter">
                                                <span title="$50{!! $k !!}" class="textellips" style="width: 99px">
                                                   product type
                                                </span>
                                            </td>
                                            <!-- product type data ends here  -->
                                            <td class="tdcenter">
                                             
                                                <span title="$100{!! $k !!}" class="textellips" style="width: 99px">
                                                    0
                                                </span>
                                            </td>
                                            
                                            
                                            <!-- <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td> -->
                                            
                                                 <td>
                                                   <span title="$100{!! $k !!}" class="textellips" style="width: 99px">
                                                    0
                                                </span>
                                                </td>
                                          
                                            
                                          
                                            <td>
                                              <span title="$100{!! $k !!}" class="textellips" style="width: 99px">
                                                    0
                                                </span>
                                            </td>
                                        
                                           

                                            <td class="tdcenter"><span class="">{!! $k !!}</span></td>
                                            <td class="tdcenter">{!! $product->manufacturedYear !!}</td>

                                            <td>
                                                 <a href="{!! url('/') !!}/oms/product/edit-product/MQ=="><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            </td>
                                            <!-- <td><i class="fa fa-trash" aria-hidden="true"></i></td> -->
                                        </tr>
                                    @endforeach
                                @endif
                
               
                
            </tbody>
        </table>
            </div>
                </div>
        </div>
        </div>
        
        
        

        <!-- ############ Main END-->

            <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        
        <!--add-tag from input--15.2.2018-->
            <script src="scripts/plugins/bootstrap-tagsinput.min.js"></script>
        <!--end add-tag from input--15.2.2018-->
        

            <script>
                $(document).ready(function() {    
                   // $('#example').dataTable( {
                       
                   //      "pageLength": 25,
                   //      "columnDefs": [ {
                   //        "targets"  : 'no-sort',
                   //        "orderable": false,
                   //        "order": [],
                   //         columns: [
                   //                      { width: '99px' },
                   //                      { width: '79px' },
                   //                      { width: '49px' },
                   //                      { width: '49px' },
                   //                      { width: '49px' },
                   //                      { width: '49px' }
                   //                   ]
                   //      }]
                   //  });
                     
                });
                
                
                
                
        
                
                
    </script>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" type="text/css" />  <script>
        //============================ delete product ===========================//
        function deleteProduct(id)
        {
                if(confirm("Do you want to delete this product?"))
                {
                        window.location.href="{!! url('/') !!}/oms/category/delete-category/"+id;
                }
        }
        //=====================script for change status start======================//
        function changeStauts(storeid,status)
        {
        
                var confirmpromo=confirm('Are you sure you want to change status');
                if(confirmpromo)
                {
                        location.href="{!! url('/') !!}/oms/category/change-status/"+storeid+"/"+status;
                }
        }
        


        // datatable load on ajax starts here 

        $(document).ready(function(){
        // load datatable starts here 
        $("#producttype").trigger('change');
        searchfunction();

        // alert('hello there');
        // $("#example").html();
      

        //  load dataatable ends here 

        })
        // datatable load on ajax ends here 


        // search starts here 

        $("#categoryid").change(function(){
            // alert('hello there');
            searchfunction();
            // var searval = 
        })
        $("#subcategoryid").change(function(){
            // alert('hello there');
            searchfunction();
            // var searval = 
        })
        $("#sessionid").change(function(){
            // alert('hello there');
            searchfunction();
            // var searval = 
        })
        $("#subBrand").change(function(){
            // alert('hello there');
            searchfunction();
            // var searval = 
        })
         $("#producttype").change(function(){
            // alert('hello there');
            var producttype = $("#producttype").val();//:selected
            fetchcategory(producttype);
            searchfunction();
            // var searval = 
        })

        function searchfunction(){

                var categoryidval = $("#categoryid").val();
                var subcategoryidval = $("#subcategoryid").val();
                var sessionidval = $("#sessionid").val();
                var Brandidval = $("#subBrand").val();
                var producttypeidval = $("#producttype").val();
                var manufacturedyearval = $("#manufacturedyear").val();
                // alert(" manufacturedyearval "+manufacturedyearval);
                var searchdata = {categoryidval:categoryidval,subcategoryid:subcategoryidval,sessionid:sessionidval,brand:Brandidval,producttype:producttypeidval,manufacturedyear:manufacturedyearval}


                $.ajax({
                url: '/oms/product/product-list/searchproduct',
                data:searchdata,
                type: 'POST',
                dataType: 'html',
                success: function (data) {
                    $("#datatablerespajax").html('');
                     $("#datatablerespajax").html(data);

                    // alert(data);
                    $('#example').dataTable( {
                            /*"stripeClasses": [ 'odd-row', 'even-row' ],*/
                            "pageLength": 25,
                            "columnDefs": [ {
                              "targets"  : 'no-sort',
                              "orderable": false,
                              "order": [],
                               columns: [
                                            { width: '99px' },
                                            { width: '79px' },
                                            { width: '49px' },
                                            { width: '49px' },
                                            { width: '49px' },
                                            { width: '49px' }
                                         ]
                            }]
                        });
                }
            });
        }
        // search ends here 

        // reload function starts here
        function reloadpagefunc(){
            location.reload();
        }
        // reload function ends here


        // product type wise category change starts here 
        function fetchcategory(producttype)
        {
            //alert(producttype);
            var urldata_catDta = "{!! url('/') !!}/oms/fetchCategoryDta";
            $.ajax({
            url : urldata_catDta,
            type: 'POST',
            data : {'producttype':producttype},
            async: false,
            }).done(function(response){
               $("#categoryid").html(response);
               $("#subcategoryid").html(response);
            //}
            }) 
        }
        // product type wise category change ends here 


        // import btn click function starts here
        $('.importbtcls').click(function(){
            $("#fileimport").trigger('click');
        })

        $("#fileimport").change(function(){
            // alert('I am changed');
            $("#fileimportform").submit();
        })
        // import btn click function ends here

        $("#manufacturedyear").change(function(){
            // alert('hello there');
            searchfunction();
        })

   </script>  
 