  <style>
.table.dataTable thead>tr>th.sorting:nth-child(2){
    width: 99px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(3){
    width: 79px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(4){
    width: 99px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(5){
    width: 99px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(6){
    width: 70px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(7){
    width: 65px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(8){
    width: 65px !important;
}
.table.dataTable thead>tr>th.sorting:nth-child(9){
    width: 65px !important;
}
.save_text_box_outer{
    margin-top: 30px;
}
</style>
  <div class="table-responsive table-responsive_custom" id="datatablerespajax">
                    <table id="example" class="table display table-bordered example_cbd_table btmscrool" cellspacing="0" width="100%" style="min-width:1020px !important;font-size: 12px">
            <thead >
                <tr>
                    <th class="no-sort" style="display:none;">
                    </th>
                    
                   <!--  <th>Name</th>
                    <th>Factory ID</th>
                    <th>Retail Price</th>
                    <th>Sale Price</th>
                   
                    <th>Feature</th>
                    <th>Active</th>
                    <th>Position</th>
                    <th>Hits</th>
                    <th>Edit</th> -->
                    <!-- <th>Delete</th> -->





                    <th>Name</th>
                    <th>Factory ID</th>
                    <th>Product Type</th>
                    <th>Category 1</th>
                    <th>Category 2</th>
                    <th>Season</th>
                    <th>Brand</th>
                    <th>Year</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody >

                @if(!empty($productdata))
                                    @foreach($productdata as $k=>$product)
                                        
                                    <!-- product name length starts here -->

                                    <!-- product name length ends here -->

                                        <tr class="oddRow">
                                            <td style="display:none;">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input chek_1 chkRow">
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                            </td>
                                            
                                            <td class="tdleft">
                                                <span title="{!! $product->styleName !!} " class="textellips" style="width: 99px">

                                                        {!! $product->styleName !!}

                                               

                                                </span>
                                            </td>
                                            <td class="tdleft">
                                            <span title="{!! $product->factoryId !!}" class="textellips" style="width: 79px">
                                                {!! $product->factoryId !!}
                                            </span>
                                            </td>
                                            <!-- <td></td> -->
                                            <!-- product type starts here -->
                                            <?php 

                                                    $fetchproducttypeqry = DB::table('ProductType')->where('producttypeid',$product->productType)->first();
                                                    $fetchproducttypename="";
                                                    if(isset($fetchproducttypeqry) && !empty($fetchproducttypeqry))
                                                    {
                                                        $fetchproducttypename= $fetchproducttypeqry->producttypeName;

                                                    }
                                            ?>
                                            <td class="tdcenter">
                                                <span title="$50{!! $k !!}" class="textellips" style="width: 99px">
                                                    {!! $fetchproducttypename !!}
                                                </span>
                                            </td>
                                            <!-- product type ends here  -->
                                            <!-- category1 fetch starts here -->
                                            <?php 

                                                    $fetchcategoryqry = DB::table('categories')->where('CategoryID',$product->catagoryId1)->first();
                                                    $fetchcategoryname="";
                                                    if(isset($fetchcategoryqry) && !empty($fetchcategoryqry))
                                                    {
                                                        $fetchcategoryname= $fetchcategoryqry->CategoryName;

                                                    }
                                            ?>
                                            <td class="tdcenter">
                                             
                                                <span title="$100{!! $k !!}" class="textellips" style="width: 99px">
                                                    {!! $fetchcategoryname !!}
                                                </span>
                                            </td>
                                            <!-- category1 fetch ends here  -->
                                            
                                            
                                            <!-- <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td> -->
                                            <!-- category2 fetch starts here -->
                                            <?php 

                                                    $fetchcategoryqry = DB::table('categories')->where('CategoryID',$product->catagoryId2)->first();
                                                    $fetchcategory2name="-";
                                                    if(isset($fetchcategoryqry) && !empty($fetchcategoryqry))
                                                    {
                                                        $fetchcategory2name= $fetchcategoryqry->CategoryName;

                                                    }
                                            ?>
                                                 <td>
                                                    <span title="$100{!! $k !!}" class="textellips" style="width: 99px">
                                                  {!! $fetchcategory2name !!}
                                                </span>
                                                </td>
                                            <!-- category2 fetch ends here -->
                                          
                                            
                                            <!-- season starts here  -->
                                            <?php 

                                                    $fetchseasosnqry = DB::table('Season')->where('Id',$product->session)->first();
                                                    $fetchseasionname="-";
                                                    if(isset($fetchseasosnqry) && !empty($fetchseasosnqry))
                                                    {
                                                        $fetchseasionname= $fetchseasosnqry->Name;

                                                    }
                                            ?>
                                            <td>
                                                <span title="$100{!! $k !!}" class="textellips" style="width: 99px">
                                                  {!! $fetchseasionname !!}
                                                </span>
                                            </td>
                                            <!-- seasosn ends here  -->
                                        
                                           
                                            <!-- brand starts here  -->
                                             <?php 

                                                    $fetchbrandqry = DB::table('brand')->where('brandId',$product->brand)->first();
                                                    $fetchbrandname="-";
                                                    if(isset($fetchbrandqry) && !empty($fetchbrandqry))
                                                    {
                                                        $fetchbrandname= $fetchbrandqry->brandName;

                                                    }
                                            ?>
                                            <td class="tdcenter">
                                                <span class="">{!! $fetchbrandname !!}</span>
                                            </td>
                                            <!-- brands ends here  -->

                                            <td class="tdcenter">{!! $product->manufacturedYear !!}</td>

                                            <td>
                                                 <a href="{!! url('/') !!}/oms/product/edit-product/MQ=="><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            </td>
                                            <!-- <td><i class="fa fa-trash" aria-hidden="true"></i></td> -->
                                        </tr>
                                    @endforeach
                                @endif
                
               
                
            </tbody>
        </table>