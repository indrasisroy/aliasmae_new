
    <!-- Main -->
    <div class="content-main staic_table_custom" id="content-main">
        <!-- ############ Main START-->
        <div class="padding">
            <div class="clearfix top_export_sec">
            <div class="left_head">
            <h2>Shoes</h2>
            <div class="export_but">
                
            </div>
                </div>
                <div class="create_order"><a href="{!! url('/') !!}/oms/product/add-shoe-style">Add Style</a></div>
                <button class="exportbutt" type="button" style="float:right;">
                    <i class="fa fa-download"></i>
                    Export
                </button>
                <button class="exportbutt" type="button" style="float:right;">
                    <i class="fa fa-upload"></i>
                    Import
                </button>
                </div>
            <div class="box">
                <!-- <div class="top_filter">
                <ul>
                    <li class="active"><a href="#">All</a></li>
                </ul>
        </div> -->
                <div class="product_flter">
                    <div class="product_flter_blk">
                        <div class="flter_cnt">
                            <label>Select Category:</label>
                        </div>
                         <div class="filter_product">
                            <select name="category" class="cust_select" id="dropdown1">
                                <option >Select Category</option>
                                @if(!empty($data['categorylist']))
                                    @foreach($data['categorylist'] as $category)
                                        <option value="{!! $category['CategoryID'] !!}">{!! $category['CategoryName'] !!}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="product_flter_blk">
                        <div class="flter_cnt">
                            <label>Sort Alphabatically:</label>
                        </div>
                         <div class="filter_product">
                            <select name="country" class="cust_select" id="dropdown2">
                                <option value="all">All</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                                <option value="D">D</option>
                                <option value="E">E</option>
                                <option value="F">F</option>
                                <option value="G">H</option>
                                <option value="I">I</option>
                                <option value="J">J</option>
                                <option value="K">K</option>
                                <option value="L">L</option>
                                <option value="M">M</option>
                                <option value="N">N</option>
                                <option value="O">O</option>
                                <option value="P">P</option>
                                <option value="Q">Q</option>
                                <option value="R">R</option>
                                <option value="S">S</option>
                                <option value="T">T</option>
                                <option value="U">U</option>
                                <option value="V">V</option>
                                <option value="W">W</option>
                                <option value="X">X</option>
                                <option value="Y">Y</option>
                                <option value="Z">Z</option>
                            </select>
                        </div>
                    </div>
                </div>
<!--                 <div class="advance_fillter">
                    <div class="advance_out clearfix">
                        <div class="dropdown">
                              <button class="btn white dropdown-toggle" data-toggle="dropdown">Dropdown </button>
                              <div class="dropdown-menu">
                                <span>Show all products where:</span>
                                  <select class="form-control form-control-sm getFname">
                                      <option>Select filter..</option>
                                      <option value="1">Availability</option>
                                      <option value="2">Product type</option>
                                      <option value="3">Product vendor</option>
                                  </select>
                                  
                                  <span class="availability">
                                    is
                                      <select class="form-control form-control-sm">
                                      <option>Availability</option>
                                      <option>Product type</option>
                                      <option>Product vendor</option>
                                  </select>
                                      <input type="button" value="Add filter" />
                                  </span>
                              </div>
                            </div>
                        <div class="input_outer">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z"></path></svg>
                        <input type="text" />
                        </div>
                    </div> 
                </div> -->
                <div class="padding">
                <div class="table-responsive table-responsive_custom">
                    <table id="example" class="table display table-bordered example_cbd_table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="no-sort" style="display:none;">
                    </th>
                    <th>Display Level</th>
                    <th>Name</th>
                    <th>FactoryRef</th>
                    <th>Level</th>
                    <th>Sale Price</th>
                    <th>Retail Price</th>
                    <th>Number Hits</th>
                    <th>Edit</th>
                    <th>Featured?</th>
                    <th>Display</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                <tr class="oddRow">
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><!-- <em class="img_bored"><img src="images/p1.jpg" alt="" /></em> --><span class="product_tetx">410</span></td>
                    <td><span>Abaala</span></td>
                    <td>MD01-10</td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">357</span></td>
                    <td><span class="">Abacus</span></td>
                    <td>MD01-10</td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">359</span></td>
                    <td><span class="">Abagail</span></td>
                    <td>MD01-3</td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">360</span></td>
                    <td><span class="">Abeba</span></td>
                    <td>MD01-7</td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                 <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label> 
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">360</span></td>
                    <td><span class="">Abeba</span></td>
                    <td>MD01-7</td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">359</span></td>
                    <td><span class="">Abagail</span></td>
                    <td>MD01-3</td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                 <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label> 
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                 <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label> 
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                 <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label> 
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td style="display:none;">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td><span class="product_tetx">513</span></td>
                    <td><span class="">Abel</span></td>
                    <td>D02-79  </td>
                    <td>906</td>
                    <td>1.00</td>
                    <td>219.95</td>
                    <td>11211</td>
                    <td><a><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><input type="checkbox" id="featuredProduct" name="featured410" value="1" checked=""></td>
                    <td><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr>
                
            </tbody>
        </table>
            </div>
                </div>
        </div>
        </div>
        
        
        

        <!-- ############ Main END-->

            <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        
        <!--add-tag from input--15.2.2018-->
            <script src="scripts/plugins/bootstrap-tagsinput.min.js"></script>
        <!--end add-tag from input--15.2.2018-->
        

            <script>
                $(document).ready(function() {    
                   $('#example').dataTable( {
                        /*"stripeClasses": [ 'odd-row', 'even-row' ],*/
                        "pageLength": 25,
                        "columnDefs": [ {
                          "targets"  : 'no-sort',
                          "orderable": false,
                          "order": []
                        }]
                    });
                     
                });
                
                
                
                
        
                
                
    </script>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" type="text/css" />  <script>
        //============================ delete product ===========================//
        function deleteProduct(id)
        {
                if(confirm("Do you want to delete this product?"))
                {
                        window.location.href="{!! url('/') !!}/oms/category/delete-category/"+id;
                }
        }
        //=====================script for change status start======================//
        function changeStauts(storeid,status)
        {
        
                var confirmpromo=confirm('Are you sure you want to change status');
                if(confirmpromo)
                {
                        location.href="{!! url('/') !!}/oms/category/change-status/"+storeid+"/"+status;
                }
        }
        
   </script>  
 