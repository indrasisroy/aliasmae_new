<!-- include summernote css -->
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css') !!}
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css') !!}
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css') !!}
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') !!}
<!-- include dropzone js -->
{!! HTML::style('assets/css/dropzone.min.css') !!}
{!! HTML::style('assets/css/tagsinput.css') !!}

    <!-- Main -->
    <div class="content-main staic_table_custom" id="content-main">
        <!-- ############ Main START-->
        <div class="padding">
            <div class="container">
            <div class="clearfix top_export_sec">
                <div class="clearfix breadcram">
                    <a href="{!! url('/') !!}/oms/product/shoe-list"><i class="fa fa-angle-left"></i>Product Summary</a>
                </div>
                <div class="left_head">
                    <h2>Add Style</h2>
                </div>
            </div>
              <div id='all_img_div' style="display:none">
                <textarea name="body_html" id="body_html"></textarea>
                <input type="file" class='all_product_file' name="all_file[]" id="all_file_0" accept="image/*" style="display:none">
              </div>
                <div class="row">
                    <div class="col-sm-12 "><!-- cbd_main_content -->
                        <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Add a New Style</h6>
                                </header>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl">Style Name *</label>  
                                    <input class="form-control need" id="styleName" placeholder="Short Sleeve T-Shirt" type="text" name="styleName" />
                                    <span id="productName_error" style="color:red"></span>
                                  </div>
                                  <div class="form-group col-md-6">
                                      <label for="usr5" class="ad_prd_lbl">Factory Id *</label>
                                     <input class="form-control need" id="factoryId" placeholder="" type="text" name="factoryId" />
                                       <span id="factoryId_error" style="color:red"></span>
                                  </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl">Season *</label>
                                    <select id="season" name="season" class="custom-select w-100 need" autocomplete="off" label="Season" >
                                      <option value="Summer">Summer</option>
                                      <option value="Winter">Winter</option>
                                    </select>
                                    <span id="IsActive_error" style="color:red"></span>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="prz5" class="ad_prd_lbl col-md-6">Category 1*</label>
                                    <select id="category" name="category" class="custom-select w-100 need" autocomplete="off" label="Category"  >
                                      <option value="">Select one...</option>
                                      @if(!empty($data['categorylist']))
                                        @foreach($data['categorylist'] as $category)
                                            <option value="{!! $category['CategoryID'] !!}">{!! $category['CategoryName'] !!}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                  </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="prz5" class="ad_prd_lbl col-md-6">Category 2 *</label>
                                    <select id="subCategory" name="subCategory" class="custom-select w-100 " autocomplete="off" label="Sub Category"  >
                                      <option value="">Select one...</option>
                                      @if(!empty($data['categorylist']))
                                        @foreach($data['categorylist'] as $category)
                                        <option value="{!! $category['CategoryID'] !!}">{!! $category['CategoryName'] !!}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="prz5" class="ad_prd_lbl col-md-6">Sub Brand *</label>
                                    <select id="subBrand" name="subBrand" class="custom-select w-100 " autocomplete="off" label="Sub Brand"  >
                                      <option value="">Select one...</option>
                                       @if(!empty($data['brandlist']))
                                        @foreach($data['brandlist'] as $brand)
                                            <option value="{!! $brand['brandId'] !!}">{!! $brand['brandName'] !!}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                  </div>
                                </div> 
                                <div class="form-row">
                                  <div class="form-group col-md-12">
                                      <label for="prz5" class="ad_prd_lbl">Description *</label>
                                      <div class="browse_field" id="summernote1_form">
                                          <div id="summernote1" class="summernote " name="productDesc"></div>
                                      </div>
                                      <span id="summernote1_error" style="color:red"></span>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                          <div class="order_details">
                            <header class="prd_hdr clearfix">
                              <h6>Display Properties</h6>
                            </header>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="usr5" class="ad_prd_lbl">Display Level * (Note: should be a number e.g 1)</label>
                                <input type="text" class="form-control isNumberKey need" id="displayOrder" name="displayOrder" value="" label="Display Order" placeholder="" autocomplete="off">
                                <span id="displayOrder_error" style="color:red"></span>
                              </div>
                              <div class="form-group col-md-6">
                                <label for="prz5" class="ad_prd_lbl col-md-6">Store *</label>
                                <select id="store" name="store[]" class="custom-select w-100 need" autocomplete="off" label="Store" multiple >
                                    @if(!empty($data['store']))
                                      @foreach($data['store'] as $store)
                                        <option value="{!! $store['StoreId'] !!}" @if(!empty($selectStoreId) && ($store['StoreId'] == $selectStoreId)){!! 'selected' !!}@endif>{!! $store['StoreName'].' ('.$store['CountryName'].')' !!}</option>
                                      @endforeach
                                    @endif
                                </select>
                                <span id="store_error" style="color:red"></span>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="usr5" class="ad_prd_lbl ">Start Date *(Product Display Start date)</label>
                                <input type='text' id="startDate" name="startDate" class="form-control date need" label="Start Date" autocomplete="off" readonly/>
                                <span id="startDate_error" style="color:red"></span>
                              </div>
                              <div class="form-group col-md-6">
                                <label for="usr5" class="ad_prd_lbl ">End Date *(Product Display End date)</label>
                                <input type='text' id="endDate" name="endDate" class="form-control date need" label="End Date" autocomplete="off" readonly/>
                                <span id="endDate_error" style="color:red"></span>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-4">
                                  <label for="usr5" class="ad_prd_lbl">Is Active </label>
                                  <input type="checkbox" class="col-md-1" id="IsActive" name="IsActive" label="Is Active" autocomplete="off" value='1'>
                                   <span id="IsActive_error" style="color:red"></span>
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="usr5" class="ad_prd_lbl">Is New Arrival </label>
                                  <input type="checkbox" class="col-md-1" id="IsNew" name="IsNew" label="Is New Arrival" autocomplete="off" value='1'>
                                   <span id="IsActive_error" style="color:red"></span>
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="usr5" class="ad_prd_lbl">Is On Sale </label>
                                  <input type="checkbox" class="col-md-1" id="IsSale" name="IsSale" label="Is On Sale" autocomplete="off" value='1'>
                                   <span id="IsActive_error" style="color:red"></span>
                              </div>
                            </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Pricing</h6>
                                </header>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="comp_prz5" class="ad_prd_lbl">Cost price</label>
                                        <input type="text" class="form-control" id="comp_prz5" placeholder="$ ">
                                    </div>
                                    @if(!empty($data['store']))
                                      @foreach($data['store'] as $store)
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl"><?php echo $store['CurrencyCode'].' Retail Price'?></label>
                                        <input type="text" class="form-control" id="{!! $store['CurrencyCode'].'Retail Price' !!}" placeholder="$ 0.00">
                                    </div>
                                      @endforeach
                                    @endif
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl">Discounted Retail Price</label>
                                        <input type="text" class="form-control" id="prz5" placeholder="$ 0.00">
                                    </div>
                                </div>
                               <!--  <div class="form-row">
                                    
                                </div> -->
                            </div>
                        </div>

                        <!-- <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Inventory</h6>
                                </header>
                                <form>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="sku5" class="ad_prd_lbl">SKU (Stock Keeping Unit)</label>
                                            <input type="text" class="form-control" id="sku5" placeholder="SKU (Stock Keeping Unit)">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="Barcode5" class="ad_prd_lbl">Barcode (ISBN, UPC, GTIN, etc.)</label>
                                            <input type="text" class="form-control" id="Barcode5" placeholder="Barcode">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputState" class="ad_prd_lbl">Inventory policy</label>
                                            <select id="inputState" class="custom-select w-100">
                                                <option selected="selected" value="">Don't track inventory</option>
                                                <option value="">Shopify tracks this product's inventory</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> -->

                        <div class="box">
                            <div class="order_details btm_margin0">
                                <header class="prd_hdr clearfix ">
                                    <h6>Shipping</h6>
                                </header>
                                <!-- <div class="row">
                                    <div class="order_details col-sm-12">
                                        <label class="custom-control custom-checkbox ad_prd_lbl">
                                            <input type="checkbox" class="custom-control-input chek_1 chkRow">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">This product requires shipping</span>
                                        </label>
                                    </div>
                                </div> -->
                                <div class="row btm_border_cst ">
                                    <div class="order_details col-sm-12">
                                        <label class="ad_prd_lbl text-uppercase">Weight</label>
                                        <p>Used to calculate shipping rates at checkout and label prices during fulfillment.</p>
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-4 form-group">
                                    <label class="">Length</label>
                                    <div class="select_field">
                                      <input class="field_box" id="productLength" name="productLength" placeholder="Product Length" type="text">
                                      <select id="srh_sl5" name="lengthUnit" class="custom-select">
                                        <option value="inch">inches</option>
                                        <option value="cm">cm</option>
                                        <option value="mm">mm</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-4 form-group">
                                    <label class="">Width</label>
                                    <div class="select_field">
                                      <input class="field_box" id="productWidth" name="productWidth" placeholder="Product Width" type="text">
                                      <select id="srh_sl5" name="widthUnit" class="custom-select">
                                        <option value="inch">inches</option>
                                        <option value="cm">cm</option>
                                        <option value="mm">mm</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-4 form-group">
                                    <label class="">Height</label>
                                    <div class="select_field">
                                      <input class="field_box" id="productHeight" name="productHeight" placeholder="Product Height" type="text">
                                      <select id="srh_sl5" name="HeightUnit" class="custom-select">
                                        <option value="inch">inches</option>
                                        <option value="cm">cm</option>
                                        <option value="mm">mm</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-4 form-group">
                                    <label class="">Weight</label>
                                    <div class="select_field">
                                      <input class="field_box" id="productWeight" name="productWeight" placeholder="Product Weight" type="text">
                                      <select id="srh_sl5" name="weightUnit" class="custom-select">
                                        <option value="">lb</option>
                                        <option value="">oz</option>
                                        <option value="">kg</option>
                                        <option value="">g</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-4 form-group">
                                    <label class="">Cubic Weight</label>
                                    <div class="select_field">
                                      <input class="field_box" id="cubicWeight" name="cubicWeight" placeholder="Product Cubic Weight" type="text">
                                      <select id="srh_sl5" name="cubicweightUnit" class="custom-select">
                                        <option value="">lb</option>
                                        <option value="">oz</option>
                                        <option value="">kg</option>
                                        <option value="">g</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>

                        <div class="box">
                            <div class="order_details">
                              <header class="prd_hdr clearfix">
                                <h6>Variants</h6>
                                <button class="txt_btn fl_left" id="tgl_div1">Add variant</button>
                                <button class="txt_btn fl_left txt_btn_hide" id="tgl_div2">close</button>
                              </header>
                              <div class="form-group">
                                <p>Add variants if this product comes in multiple versions, like different colors.</p>
                              </div>
                            </div>
                            <div class="clearfix dropdown_div">
                              <div class="order_details gry_of_bg">
                                <div class="clearfix form-result_outer" id="form-result_cst">
                                  <div class="prd_hdr clearfix">
                                    <h4 class="ad_prd_lbl">Modify the variants to be created:</h4>
                                  </div>
                                  <div class="cbd_table_outer" >
                                    <div id="allVariant">
                                      <div class="form-row">
                                        <div class="form-group col-sm-6">
                                          <label for="sku5" class="ad_prd_lbl">Colour *</label>
                                          <select id="colour_0" name="colour[]" class="custom-select w-100 varneed" autocomplete="off" label="Colour" >
                                            <option value="blue">Blue</option>
                                            <option value="black">Black</option>
                                            <option value="red">Red</option>
                                          </select>
                                        </div>
                                        <!-- <div class="form-group col-sm-6">
                                          <label for="sku5" class="ad_prd_lbl">Colour 2</label>
                                          <select id="colour2_0" name="colour2[]" class="custom-select w-100 " autocomplete="off" label="Colour2" >
                                            <option value="blue">Blue</option>
                                            <option value="black">Black</option>
                                            <option value="red">Red</option>
                                          </select>
                                        </div> -->
                                        <div class="form-group col-sm-6">
                                          <label for="sku5" class="ad_prd_lbl">Material *</label>
                                          <select id="material0" name="material[]" class="custom-select w-100 varneed" autocomplete="off" label="Material" >
                                            <option value="Leather">Leather</option>
                                            <option value="Nubuck">Nubuck</option>
                                            <option value="Box">Box</option>
                                            <option value="Patent">Patent</option>
                                            <option value="Mirror">Mirror</option>
                                            <option value="Nappa">Nappa</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="form-row" id="StoreSizeHtml0">
                                      </div>
                                      <div class="form-row">
                                        <div class="form-group col-sm-8">
                                          <label for="sku5" class="ad_prd_lbl">Image :</label>
                                         <!--  <input type="file" class="form-control varneed" id="image0" name="image0" placeholder=""> -->
                                          <a href="javascript:void(0)" class="dropzone_btn" data-toggle="modal" data-target="#manage_upload_img">Add image from URL</a>
                                          <a href="javascript:void(0)" class="dropzone_btn" id="by_file">Upload image
                                         </a>
                                        </div>
                                        <div class="form-group col-sm-4">
                                          <label for="usr5" class="ad_prd_lbl">Is Active </label>
                                          <input type="checkbox" class="col-md-1 " id="IsActive0" name="IsActive[]" label="Is Active" autocomplete="off" value='1'>
                                        </div>
                                      </div>
                                        <!-- <div class="dropzone_zone clearfix" id="my-awesome-dropzone2"> -->
                                        <ul class=" clearfix" id="dropzone_imgs_outer_ul0" style="padding-left: 0px;"><!-- dropzone_imgs_outer -->
                                        </ul>

                                        <!-- </div> -->
                                      <div class="form-row">  
                                        <div class='form-group col-sm-6'>
                                          <button class='but_all btn_rmv removeVariant' type='button' style="display:none">
                                              <svg id='delete-minor' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>
                                                  <path d='M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z'></path>
                                              </svg>
                                          </button>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-row">
                                      <div class="form-group col-sm-12">
                                        <button class="but_all" type="button" id="add_more_varient">Add more variants</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Search engine listing preview</h6>
                                    <button class="txt_btn fl_left" id="tgl2_div1">Edit website SEO</button>
                                    <button class="txt_btn fl_left txt_btn_hide" id="tgl2_div2">close</button>
                                </header>
                                <div class="form-group">
                                    <p>Add a title and description to see how this product might appear in a search engine listing.</p>
                                </div>
                            </div>
                            <div class="dropdown_div btm_border_cst order_details clearfix" id="">
                                <div class="row  ">
                                    <div class="form-group col-sm-12">
                                        <label for="inpState5" class="ad_prd_lbl">Page title</label>
                                        <span class="fl_left">0 of 70 characters used</span>
                                        <input type="text" class="form-control" id="inpState5" placeholder="">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inpStatetxt5" class="ad_prd_lbl">Meta description</label>
                                        <span class="fl_left">0 of 320 characters used</span>
                                        <textarea class="form-control" id="inpStatetxt5" placeholder=""></textarea>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inpStateurl5" class="ad_prd_lbl">URL and handle</label>
                                        <input type="text" class="form-control" id="inpStateurl5" placeholder="https://anytimecbd.com/products/">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                   <!--  <div class="col-sm-4 cbd_side_content">

                         <div class="box gry_bx"> -->
                             <!-- <div class="order_details order_details_right">
                                <header>
                                    <h5>Collections</h5>
                                </header>
                                <p>Add this product to a collection so it’s easy to find in your store.</p>
                                <label class="ad_prd_lbl">Category type</label>
                                <div class="advance_out_cst clearfix">
                                <div class="ui-widget">
                                      <select class="combobox">
                                        <option value="">Select one...</option>
                                        @if(!empty($data['categorylist']))
                                          @foreach($data['categorylist'] as $category)
                                              <option value="{!! $category['CategoryID'] !!}">{!! $category['CategoryName'] !!}</option>
                                          @endforeach
                                        @endif
                                      </select>
                                    </div>

                                </div>
                                <label class="ad_prd_lbl">Brand</label>
                                <div class="advance_out_cst clearfix">
                                <div class="ui-widget">
                                      <select class="combobox">
                                        <option value="">Select one...</option>
                                         @if(!empty($data['brandlist']))
                                          @foreach($data['brandlist'] as $brand)
                                              <option value="{!! $brand['brandId'] !!}">{!! $brand['brandName'] !!}</option>
                                          @endforeach
                                        @endif
                                      </select>
                                    </div>

                                </div>
                            </div> -->
                            <!-- <div class="order_details order_details_right btm_border_cst">
                                <header>
                                  <h5>Size ManageMent</h5>
                                </header>
                                <div class="form-row">
                                  <div class="form-group col-md-12">
                                    <label for="prz5" class="ad_prd_lbl col-md-12">Store :</label>
                                    <div class="col-md-12">
                                      @if(!empty($data['store']))
                                        @foreach($data['store'] as $store)
                                          <input type="radio" class="col-md-1 storeRadio" name="store" value="{!! $store['StoreId'] !!}" autocomplete="off">{!! $store['StoreName'] !!}
                                        @endforeach
                                      @endif
                                    </div>
                                    <span id="store_error" style="color:red"></span>
                                  </div> 
                                </div>
                                <div id="selectstoreSize" style="display:none;">
                                  <label class="ad_prd_lbl">Store Size</label>
                                  <div class="advance_out_cst clearfix">
                                    <div class="ui-widget" id="ShoeSizeHtml">
                                    </div>
                                  </div>
                                  <input type="button" id="addSize" value="Add Size">
                                </div> 
                            </div>
                        </div>
                    </div> -->
                </div>
               
                <div class="row">
                    <div class="save_but">
                        <button class="mailbut_all float-right disabled" type="button" disabled="disabled">Save product</button>
                        <button type="button" id="cancel_update" class=" but_all  float-right">Cancel</button>
                    </div>
                </div>
        </div>

            </div>

        <!-- ############ Main END-->
          <!----------------- modal to add image from url ---------------->
        
        <div class="modal fade" id="manage_upload_img" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Add image from URL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-row">
                  <div class="form-group col-sm-12">
                        <label class="ad_prd_lbl">Paste image URL</label>
                        <input type="text" class="form-control" id="frm_eip1" placeholder="http://">
                        <span id="frm_eip1_error" style="color:red"></span>
                    </div>
                </div>
              </div>
              <div class="modal-footer pop_foot_but">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="addImageUrl">Add image</button>
              </div>
            </div>
          </div>
        </div>
        <!--end-->   
        <!-- include summernote js -->
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') !!}

            <!-- <script src="http://www.anytimecbd.seederp.com/scripts/datepicker.min.js"></script> -->
        <!-- include dropzone js -->
            <!--{!! HTML::script('scripts/plugins/dropzone.min.js') !!}-->

            {!! HTML::script('scripts/plugins/bootstrap-tagsinput.min.js') !!}
        <!-- include dropzone js -->
            <!-- <script type="text/javascript" src="scripts/plugins/dropzone.min.js"></script>
            <script src="scripts/plugins/bootstrap-tagsinput.min.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>

$(document).ready(function(){
 image_count = 0;
  image_url = 0;
  currect_imgurl = 0;
  var_count = 1;
  var storevalue = $("#store").val();//:selected
  //alert('storeId='+storevalue);
  setFootSize(storevalue);
});

$('#store').change(function(){
  var storevalue = $("#store").val();
  setFootSize(storevalue);
});

function setFootSize(storevalue)
{
        var urldata_shoesize = "{!! url('/') !!}/oms/fetchStoreShoeSize";
//alert(storevalue+'===='+urldata_shoesize);
            $.ajax({
            url : urldata_shoesize,
            type: 'POST',
            data : {'shoevalue':storevalue},
            async: false,
            }).done(function(response){
            // console.log('here'+response);
            //location.reload();
            //alert(response);
            //$("#selectstoreSize").css("display","block");
            //$("#ShoeSizeHtml").html(response);
            if(var_count == 1)
            {
              $("#StoreSizeHtml0").html(response);
            
            }
            else{
              $("#StoreSizeHtml0"+var_count).html(response);
            }
          }) 
}
</script>
