 //************************************* Date function  ******************************
$(document).ready(function() {

    $("#startDate").datepicker({
      dateFormat: "mm/dd/yy",
      minDate: 0 ,
      onSelect: function (dateText, inst) {
        //alert(dateText+'++++'+inst);
        var dateVarchkin = $("#startDate").datepicker("getDate");
        var selectedDate = new Date(dateText);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);

       //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
        $("#endDate").datepicker( "option", "minDate", endDate );
        //$("#endDate").datepicker( "option", "maxDate", '+2y' );
        //alert(dateVarchkin);
        //$("#calChkIndate_hide").val($.datepicker.formatDate("yy-mm-dd", dateVarchkin));
      },
      onClose: function() {
                var date2 = $('#startDate').datepicker('getDate');
                date2.setDate(date2.getDate()+1)
                $( "#endDate" ).datepicker("setDate", date2);
                /*$( "#endDate" ).datepicker("minDate", date2);*/
                //$("#calChkOutdate_hide").val($.datepicker.formatDate("yy-mm-dd", date2));
            }
    }).datepicker("setDate", new Date());
    $("#endDate").datepicker({dateFormat: "mm/dd/yy",minDate: 0,
      onSelect: function (dateText, inst) {
        var dateVarchkout = $("#endDate").datepicker("getDate");
        var selectedDate = new Date(dateText);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() - msecsInADay);

       //Set Minimum Date of StartDatePicker After Selected Date of EndDatePicker
        $("#startDate").datepicker( "option", "minDate", endDate );
        //alert(dateVarchkout);
        //$("#calChkOutdate_hide").val($.datepicker.formatDate("yy-mm-dd", dateVarchkout));
      }
     }).datepicker();

    $("#endDate").datepicker({
      dateFormat: "mm/dd/yy",
      minDate: 1,
      onSelect: function (dateText, inst) {
            var dateVarchkout = $("#endDate").datepicker("getDate");
            var selectedDate = new Date(dateText);
            var msecsInADay = 86400000;
            var endDate = new Date(selectedDate.getTime() - msecsInADay);

           //Set Minimum Date of StartDatePicker After Selected Date of EndDatePicker
            $("#startDate").datepicker( "option", "minDate", endDate );
            //lert(dateVarchkout);
            //$("#calChkOutdate_hide").val($.datepicker.formatDate("yy-mm-dd", dateVarchkout));
          },
      onClose: function() {
                    var datechkout = $('#endDate').datepicker('getDate');
                    datechkout.setDate(datechkout.getDate()-1)
                    $( "#startDate" ).datepicker("setDate", datechkout);
                    //$("#calChkIndate_hide").val($.datepicker.formatDate("yy-mm-dd", datechkout));
                }}).datepicker("setDate", new Date());
        $("#startDate").datepicker({dateFormat: "mm/dd/yy",minDate: 0 }).datepicker();


    $('.time').timepicker({
        timeFormat: 'h:mm p',
        interval: 1
        });

});

//************************************* Date function  ******************************


                $(document).ready(function() {
//                    /*--file upload--*/
//                    $("#my-awesome-dropzone2>div").dropzone({ url: "/file/post" });

                    /*--text editor--*/
                  $("#summernote1").summernote({
                     toolbar: [
                         // [groupName, [list of button]]
                         ['codeview', ['codeview']],
                         ['help', ['help']],
                         ['style',['style']],
                         ['font', ['fontname', 'color']],
                         ['style', ['bold', 'italic', 'underline']],
                         ['para', ['ul', 'ol', 'height', 'paragraph']],
                         ['insert', ['picture', 'link', 'video', 'table','clear']]
                      ],
                      popover: {
                        image: [],
                        link: [],
                        air: []
                      },
                      height: 200,
                      maxHeight: 350,             // set maximum height of editor
                      placeholder: 'Type your description here...'
                  });
                });

                /*--remove div--*/
                $(document).on("click","#form-row_cst .btn_rmv", function(){
                    //check if this div is last child of "#form-row_cst"
                    if( $("#form-row_cst").find(".form-row").length-1 < 1) {
                        alert("sorry, you can't delete this items");
                    }
                    else {
                        //remove div
                        $(this).parents(".form-row").remove();
                    }
                    if( ($("#form-row_cst").find(".form-row").length) < (dataArray.length)+1 ) {
                      $("#clone_btn").show();
                    }
                });

                $("#tgl_div1, #tgl2_div1").click(function(){
                    $(this).parents(".order_details").next(".dropdown_div").stop().slideDown();
                    $(this).hide();
                    $(this).next(".txt_btn").show();
                });
                $("#tgl_div2, #tgl2_div2").click(function(){
                    $(this).parents(".order_details").next(".dropdown_div").stop().slideUp();
                    $(this).hide();
                    $(this).prev(".txt_btn").show();
                });

                $("#clndr_btn").click(function(){
                    $(this).parents(".bx_panel").next(".dropdown_div").stop().slideToggle();
                });



                $(document).ready(function($){
//
//                    $('.suggestion li').each(function(){
//                    $(this).attr('data-search-term', $(this).text().toLowerCase());
//                    });
//
//                    $('.field_box_cst').on('keyup', function(){
//                            $('.suggestion').toggleClass('d-block');
//
//
//                    var searchTerm = $(this).val().toLowerCase();
//
//                        $('.suggestion li').each(function(){
//
//                            if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
//                                $(this).show();
//                            }
//                            else {
//                                $(this).hide();
//                            }
//
//                        });
//
//                    });
//

                    $('.dropdown-menuA li a').click(function(){
                        var value=$(this).attr('data-title');
                        $(this).parents('.select_field').find('input#dlAutoCom').val(value);
                        $(this).parents('.select_field_cst, .dropdown-menu').toggleClass('show');
                    });
//                     $('.suggestion li a').click(function(){
//                        var value=$(this).attr('data-title');
//                        $(this).parents('.select_field_cst').find('input#dlAutoCom').val(value);
//                         $(this).parents('.suggestion').removeClass('d-block');
//                    });
});

//=================== add more varient ==================//
$("#add_more_varient").click(function(){
     varError = 0;

      $('.varneed').each(function(){
          var variantVal = $(this).val();
          var variantid = $(this).attr('id');
          var field_label = $(this).attr('label');
          if(variantVal.search(/\S/) === -1)
          {
               varError++;
               $("#"+variantid).css('border-color','red');
               if(varError === 1)
               {
                    $("#"+variantid).focus();
               }

          }
          else
          {
               $("#"+variantid).css('border-color','');
               $("#"+variantid+"_error").html('');
               // ============================= image display block hide strat=======================
                //alert(var_count);
                // by_file0
                //$("#by_file"+var_count).addClass('hide');
               // ============================= image display block hide hide========================
          }
     });
     $('.minthirteen').each(function(){
          var variantVal = $(this).val();
          var variantid = $(this).attr('id');
         // alert(variantVal);
         // alert(variantVal.length);
          if(variantVal.length < 13)
          {
                varError++;
                $("#"+variantid).css('border-color','red');
                if(varError === 1)
               {
                    $("#"+variantid).focus();
               }
          }
          else
          {
               $("#"+variantid).css('border-color','');
          }
     });
     if(varError === 0)
     {
      var_count++; 

      //alert(var_count);
     
          var hideClass = 'hide';
          var inventory_status = parseInt($('#inputState').val());
          if(inventory_status === 1)
          {
               hideClass = '';
          }
          /*<input type='file' class='form-control varneed' id='image"+var_count+"' name='image"+var_count+"' placeholder=''>\*/
          var newTrContent = "<div class='form-row '>\
                                  <div class='form-group col-sm-2'>\
                                          <label for='sku5' class='ad_prd_lbl'>Barcode *</label>\
                                          <input type='text' id='barcode"+var_count+"' class='form-control w-100 varneed isNumberKey minthirteen product_need' name='barcode[]' value='' label='Barcode'>\
                                          <span id='barcode"+var_count+"_error' style='color:red'></span>\
                                        </div>\
                                <div class='form-group col-sm-2'>\
                                  <label for='sku5' class='ad_prd_lbl'>Color 1*</label>\
                                  <select id='colour_"+var_count+"' name='colour[]' class='custom-select w-100 varneed product_need' autocomplete='off' label='Colour 1' >\
                                    <option value='1'>Colour1</option>\
                                    <option value='2'>Colour2</option>\
                                  </select>\
                                  <span id='colour_"+var_count+"_error' style='color:red'></span>\
                                </div>\
                                <div class='form-group col-sm-2'>\
                                  <label for='sku5' class='ad_prd_lbl'>Color 2</label>\
                                  <select id='colour2_"+var_count+"' name='colour2[]' class='custom-select w-100 ' autocomplete='off' label='Colour 2' >\
                                    <option value='1'>Colour1</option>\
                                    <option value='2'>Colour2</option>\
                                  </select>\
                                </div>\
                                <div class='form-group col-sm-2'>\
                                  <label for='sku5' class='ad_prd_lbl'>Material 1*</label>\
                                  <select id='material"+var_count+"' name='material[]' class='custom-select w-100 varneed product_need' autocomplete='off' label='Material 1' >\
                                    <option value='1'>Material1</option>\
                                    <option value='2'>Material2</option>\
                                  </select>\
                                  <span id='material"+var_count+"_error' style='color:red'></span>\
                                </div>\
                                <div class='form-group col-sm-2'>\
                                  <label for='sku5' class='ad_prd_lbl'>Material 2</label>\
                                  <select id='material2"+var_count+"' name='material2[]' class='custom-select w-100 ' autocomplete='off' label='Material 2' >\
                                    <option value='1'>Material1</option>\
                                    <option value='2'>Material2</option>\
                                  </select>\
                                </div>\
                                <div class='form-group col-sm-2'>\
                                  <label for='sku5' class='ad_prd_lbl'>Size*</label>\
                                  <select id='size"+var_count+"' name='size[]' class='custom-select w-100 varneed product_need' autocomplete='off' label='Size' >\
                                    <option value='1'>32.5(cm)/3(cm)/19(cm)</option>\
                                    <option value='2'>33(cm)/3.5(cm)/19.5(cm)</option>\
                                  </select>\
                                  <span id='size"+var_count+"_error' style='color:red'></span>\
                                </div>\
                                <div class='form-group col-sm-1' style='display:none'>\
                                  <label for='sku5' class='ad_prd_lbl'>Size(USA)*</label>\
                                  <select id='size2"+var_count+"' name='size2[]' class='custom-select w-100 ' autocomplete='off' label='Size' >\
                                    <option value='1'>32.5(cm)/3(cm)/19(cm)</option>\
                                    <option value='2'>33(cm)/3.5(cm)/19.5(cm)</option>\
                                  </select>\
                                </div>\
                              </div>\
                              <div class='form-row' id='StoreSizeHtml"+var_count+"'>\
                              </div>\
                              <div class='form-row'>\
                                <div class='form-group col-sm-8'>\
                                  <label for='sku5' class='ad_prd_lbl'>Image </label>\
                                  <a href='javascript:void(0)' class='dropzone_btn by_file' id='by_file"+var_count+"' onclick='UploadImage("+var_count+")'>Upload image</a>\
                                </div>\
                              </div>\
                                <ul class='clearfix' id='dropzone_imgs_outer_ul"+var_count+"' style='padding-left: 0px;'>\
                                </ul>\
                                <div class='form-group col-sm-6'>\
                                  <a href='javascript:void(0)' class='but_all btn_rmv removeVariant' id='removeVariant"+var_count+"' style='display:none'>\
                                    <svg id='delete-minor' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>\
                                      <path d='M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z'></path>\
                                              </svg>\
                                          </a>\
                                </div>\
                              </div>";
          $("#allVariant").append(newTrContent);

           //=================================== call to fetch colour and material value 19/09/2018 =====================
            var producttype = $("#producttype").val();
            fetchcolour(producttype);
            fetchmaterial(producttype);
            fetchSize(producttype);
            //=================================== call to fetch colour and material value 19/09/2018 =====================


          var storeIdvalue = $("#store").val();
          setFootSize(storeIdvalue);
          
          if($("#form-result_table tr").length > 1)
          {
            //alert($("#form-result_table tr").length);
               $(".removeVariant").css('display','block');
          }
          else
          {
            //alert($("#form-result_table tr").length);
               $(".removeVariant").css('display','none');
          }

     }
});
//================== remove variant ===============//
$(document).on('click','.removeVariant',function(){
     var confirmVar = confirm("Are you sure want to remove this variant?");
     if(confirmVar)
     {
          if($(this).parent('td').parent('tr').remove())
          {
               if($("#form-result_table tr").length > 2)
               {
                    $(".removeVariant").css('display','block');
               }
               else
               {
                    $(".removeVariant").css('display','none');
               }
          }
     }
});

  //===================Number check=====================//
   $(document).on('keypress','.isNumberKey',function(evt){
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        {
            return false;
        }
        else
        {
            return true;
        }
  });

//////================ file check from html input type file =================/////
   //$(".by_file").click(function()
   function UploadImage(countnumber)//onclick="UploadImage()"
   {
    countnumberCount = countnumber;
    alert(countnumberCount);
    alert(image_count);
      if(image_count !== 0)
      {
         img_html_content = "<input type='file' class='all_product_file' name='all_file"+countnumberCount+"[]' id='all_file"+var_count+"_"+image_count+"' accept='image/*' style='display:none' >";
         $("#all_img_div").append(img_html_content);
         click_img = 'all_file'+var_count+'_'+image_count;
      }
      else
      {
         click_img = 'all_file'+var_count+'_0';
      }
      currect_img = image_count;
      add_edit = 'add';
      $("#"+click_img).click();
   }//);
   
   //=============== check uploaded file =================//
   $(document).on('change','.all_product_file',function(){
      input = this;
      var imgType = input.files[0].type.split('/');
      var filename = input.files[0].name;
      var extArr = filename.split('.');
      var ext = extArr[extArr.length-1];
      
      if(input.files && input.files[0] && imgType[0] == 'image' && (ext.toLowerCase() == 'jpeg' || ext.toLowerCase() == 'jpg' || ext.toLowerCase() == 'png')) {
         var reader = new FileReader();
         reader.onload = function(e) {
            if(add_edit === 'add')
            {
               setImageContent('img',e.target.result,image_count);
               image_count++;
            }
            else
            {
               $("#pre_div_"+currect_img).css('display','block');
               $("#pre_div_"+currect_img).find('.dropzone_img').css('background-image', 'url(' + e.target.result + ')');
            }
         };
         reader.readAsDataURL(input.files[0]);
      }
      else
      {
        alert('Please upload only image file.');
        $(input).val('');
      }
   });
   //================ edit image ==============//
   $(document).on('click','.edit_img',function(){
      var dataTypeImg = $(this).attr('data-type');
      var dataTypeImgaArray = dataTypeImg.split('-');
      currect_img = dataTypeImgaArray[1];
      add_edit = 'edit'; 
      if(dataTypeImgaArray[0] === 'img')
      {
          if($("#all_file_"+currect_img).length === 0)
          {
               img_html_content = "<input type='file' class='all_product_file' name='all_file[]' id='all_file_"+currect_img+"' accept='image/*' style='display:none' >";
               $("#all_img_div").append(img_html_content);
          }
          $("#all_file_"+currect_img).click();
          $("#pre_div_"+currect_img).css('display','none');
      }
      else
      {
          $("#frm_eip1").val('');
          $("#frm_eip1_error").html('');
          $("#manage_upload_img").modal('show');
      }
   });
   
   //=============== delete image =============//
   $(document).on('click','.delete_img',function(){
      var dataTypeImg = $(this).attr('data-type');
      var dataTypeImgaArray = dataTypeImg.split('-');
      var del_con = confirm("Are you sure want to remove this image?");
      if(del_con)
      {
         $("#pre_div_"+dataTypeImgaArray[1]).remove();
         if(dataTypeImgaArray[0] === 'img')
         {
               if($("#all_file_"+dataTypeImgaArray[1]).length)
               {
                    $("#all_file_"+dataTypeImgaArray[1]).remove();
               }
               if($("#allPreImage_"+dataTypeImgaArray[1]).length)
               {
                    $("#allPreImage_"+dataTypeImgaArray[1]).remove();
               }
         }
         else
         {
            $("#all_file_url"+dataTypeImgaArray[1]).remove();
         }
      }
   });
//=============== store image url =====================//
     $("#addImageUrl").click(function(){
      //alert('ok');
      var imgURL = $("#frm_eip1").val();

      //alert(imgURL);



     //      $.ajax ({
     //           url: imgURL,
     //           type: 'HEAD',
     //           success: function ( imgdata, textStatus, xhr ) {
     //                var contentType = xhr.getResponseHeader("Content-Type");
     //                if(contentType.indexOf('image/') >=0)
     //                {
     //                     $("#frm_eip1_error").html('');
     //                     if(add_edit === 'add')
     //                     {
     //                          var imageUrlInput = "<input type='hidden' class='all_file_url' name='all_file_url[]' id='all_file_url_"+currect_imgurl+"' value='"+imgURL+"' style='display:none'>";
     //                          $("#all_img_div").append(imageUrlInput);
     //                          setImageContent('url',imgURL,currect_imgurl);
     //                          currect_imgurl++;
     //                     }
     //                     else
     //                     {
     //                          $("#pre_div_"+currect_img).find('.dropzone_img').css('background-image', 'url(' + imgURL + ')');
     //                     }
     //                     $("#manage_upload_img").modal('hide');
     //                }
     //                else
     //                {
     //                     $("#frm_eip1_error").html('Invalid image URL.');
     //                }
     //           },
     //           error: function() {
     //                imgURL_array = imgURL.split('?');
     //                if(imgURL_array[0].match(/\.(jpeg|jpg|gif|png)$/))
     //                {
     //                     $("#frm_eip1_error").html('');
     //                     if(add_edit === 'add')
     //                     {
     //                          var imageUrlInput = "<input type='hidden' class='all_file_url' name='all_file_url[]' id='all_file_url_"+currect_imgurl+"' value='"+imgURL+"' style='display:none'>";
     //                          $("#all_img_div").append(imageUrlInput);
     //                          setImageContent('url',imgURL,currect_imgurl);
     //                          currect_imgurl++;
     //                     }
     //                     else
     //                     {
     //                          $("#pre_div_"+currect_img).find('.dropzone_img').css('background-image', 'url(' + imgURL + ')');
     //                     }
     //                     $("#manage_upload_img").modal('hide');
     //                }
     //                else
     //                {
     //                     $("#frm_eip1_error").html('Invalid image URL.');
     //                }
     //           }
     //      });
     // });
});
//============== set image content =============//
//============== set image content =============//
   function setImageContent(imgType,target_data,set_count)
   {

     var img_drop_html = "<li class='dropzone_single_img' id='pre_div_"+set_count+"'>\
                              <div class='dropzone_img' style='background-image: url("+target_data+")'>\
                              </div>\
                              <div class='dropzone_img_action'>\
                                <ul class='img_overlay_outer'>\
                                  <li class='edit_img' data-type='"+imgType+"-"+set_count+"'>\
                                    <a class='sm_but' href='javascript:void(0)' data-toggle='tooltip' data-placement='top' title='' data-original-title='Edit image'>\
                                      <svg id='next-edit' width='100%' height='100%'>\
                                        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32'>\
                                          <path fill='transparent' d='M11.2 27.2l16-16-6.4-6.4-16 16z'></path>\
                                          <path d='M30.205 1.736c-2.312-2.314-6.347-2.314-8.662 0L3.67 19.61c-.172.17-.287.373-.364.587-.005.014-.02.024-.024.038l-3.2 9.6c-.192.574-.042 1.21.387 1.637.3.306.71.47 1.13.47.17 0 .34-.027.5-.083l9.6-3.2.04-.03c.214-.077.416-.19.587-.363L30.2 10.393C31.363 9.24 32 7.703 32 6.066c0-1.636-.638-3.173-1.795-4.33zm-2.262 6.4l-.742.742L23.07 4.74l.742-.742c1.105-1.106 3.03-1.106 4.137 0 .554.554.858 1.288.858 2.07s-.303 1.514-.857 2.068zM5.5 23.702l2.74 2.74-4.11 1.37L5.5 23.7zm5.7 1.176L7.064 20.74 20.8 7.002l4.14 4.138L11.2 24.878z'></path>\
                                        </svg>\
                                      </svg>\
                                    </a>\
                                  </li>\
                                  <li class='delete_img' data-type='"+imgType+"-"+set_count+"'>\
                                    <a class='sm_but' href='javascript:void(0)' data-toggle='tooltip' data-placement='top' title='' data-original-title='Delete'>\
                                      <svg id='delete-minor' width='100%' height='100%'>\
                                        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>\
                                          <path d='M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z'></path>\
                                        </svg>\
                                      </svg>\
                                    </a>\
                                  </li>\
                                </ul>\
                              </div>\
                            </li>";
            /*if(var_count == 1)
            {
              $("#dropzone_imgs_outer_ul0").append(img_drop_html);
            }
            else
            {*/
              // $("#dropzone_imgs_outer_ul"+var_count).append(img_drop_html); // 01.10.2018 closed debapriyo. for image show in particular section
                $("#dropzone_imgs_outer_ul"+countnumberCount).append(img_drop_html);

            //}            
              
   }
   
$( function() {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );

        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },

      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";

        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            classes: {
              "ui-tooltip": "ui-state-highlight"
            }
          });

        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },

          autocompletechange: "_removeIfInvalid"
        });
      },

      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;

        $( "<a>" )
          .attr( "tabIndex", -1 )
//          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .on( "mousedown", function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .on( "click", function() {
            input.trigger( "focus" );

            // Close if already visible
            if ( wasOpen ) {
              return;
            }

            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },

      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },

      _removeIfInvalid: function( event, ui ) {

        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }

        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });

        // Found a match, nothing to do
        if ( valid ) {
          return;
        }

        // Remove invalid value
        this.input
          .val( "" )
//          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },

      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });

    $( ".combobox" ).combobox();
    $( "#toggle" ).on( "click", function() {
      $( ".combobox" ).toggle();
    });
  } );
//input:radio
  $('.storeRadio').change(function(){
        var storevalue = $("input[name='store']:checked").val();
        var urldata_shoesize = "{!! url('/') !!}/oms/fetchStoreShoeSize";
//alert(storevalue);
            $.ajax({
            url : urldata_shoesize,
            type: 'POST',
            data : {'shoevalue':storevalue},
            async: false,
            }).done(function(response){
             //console.log('here'+response);
            //location.reload();
            //alert(response);
            $("#selectstoreSize").css("display","block");
            $("#ShoeSizeHtml").html(response);
            })
        //alert("Value of Changed Radio is : " +shoevalue);

        });


        
        //alert("Value of Changed Radio is : " +shoevalue);

         //=============== add product ===============//
   $("#saveproduct").click(function()
   {
    // alert('save product');
      has_product_error = 0;

      var descProduct     = $('#summernote1').summernote('code');
       var descProductHtml = $('#summernote1').summernote('code');

      $('.product_need1').each(function(){
         var field_id = $(this).attr('id'); 
         var field_val = $(this).val(); 
         var field_label = $(this).attr('label');
         if(field_val.search(/\S/) ===-1)
         {
            has_product_error++;
            $("#"+field_id+"_error").html(field_label+' is required.');
            $("#"+field_id).css('border-color','red');
            if(has_product_error === 1)
            {
               $("#"+field_id).focus();
            }
         }
         else
         {
            $("#"+field_id+"_error").html('');
            $("#"+field_id).css('border-color','');
         }
      });

      if(descProduct.search(/\S/) === -1)
      {
         has_product_error++;
         $("#summernote1_error").html("Product details is required.");
         if(has_product_error === 1)
          {
             $("#summernote1").focus();
          }
      }
      else
      {
         $("#summernote1_error").html("");
         $("#body_html").val(descProductHtml);
      }
      // alert(has_product_error);
       if(has_product_error === 0)
      {
         //$("#add_product_data").submit();
         // alert('form submit starts here ');
         // ajax starts here

          var frm = $('#add_product_data');
          $.ajax({
              url : urldata_productad,
              type: 'POST',
              data : frm.serialize(),
              async: false,
            }).done(function(response){
              // alert('response');
              window.location.href='/oms/product/product-list';
              // $("#selectstoreSize").css("display","block");
              // $("#ShoeSizeHtml").html(response);
            })

         // ajax ends here
      }

   });
