<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//===================================customer section start======================================//
Route::post('Product','rest_controller\Product@index');
//===================================customer section end=======================================//


//==== test page //////////
Route::get('uploadFile', 'rest_controller\Home@upload');
//================================ store Management =======================================//
Route::post('StoreManagement', 'rest_controller\StoreManagement@index');
//================================ store Management =======================================//
//================================ store Management Manager ===============================//
Route::post('StoreManager', 'rest_controller\StoreManagerManagement@index');
//================================ store Management Manager ===============================//
//================================ user login admin =======================================//
Route::post('connectSite', 'rest_controller\Home@index');
//================================ Banner Management =======================================//
Route::post('banner', 'rest_controller\Banner@index');
//================================ Banner Management =======================================//
Route::post('trend', 'rest_controller\Trend@index');
//================================ Article Management =======================================//
Route::post('article', 'rest_controller\Article@index');
//================================ Category Management =======================================//
Route::post('category', 'rest_controller\Category@index');
//================================ Category Management =======================================//
Route::post('brand', 'rest_controller\Brand@index');
//================================ Product Management =======================================//
Route::post('allproduct', 'rest_controller\Allproduct@index');
//================================ Product Type Management =======================================//
Route::post('ProductType', 'rest_controller\ProductType@index');
//================================ Customer Management =======================================//
Route::post('allcustomer', 'rest_controller\Allcustomer@index');


