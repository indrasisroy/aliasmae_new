<?php
namespace App\Http\Controllers\site_controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;  //adedd for test auth
//use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;  /*for auth user*/
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use App\Mail\sendMyMail;  /*send mail class*/
use Redirect;
use Cookie;
use Session;
use Illuminate\Support\Facades\URL;
use DateTime;



class Home extends MyController
{
    public function __construct(Request $request)
    {
        //print_r(Session::all());die();
        /*if(Session::get('userId') != null && Session::has('is_loggedin') != null)
        {
            Redirect::to('admin/dashboard')->send();
        }*/
    }
    
    public function index()
    {
        

        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $data['title'] = 'Home';
        // echo 'here';
        // die('hello there');
        //======== GET CATEGORY LIST FROM API =========//
                $curlUrl = 'api/connectSite';
                $lastRow='';
                
                $curlPostData = array(
                                    'mode' => 'get_forntend_category_list',
                                    'flag'=>'1'
                                );
                    
                $url = $baseUrl.$curlUrl;
                $category = $this->fireCurl($url,$curlPostData);
                $category_array = json_decode($category,1);
                if(!empty($category_array) && $category_array['status'] == 1)
                {
                    $data['categorylist'] = $category_array['data']['category'];
                }
                else
                {
                    $data['categorylist'] = array();
                }

                // echo '<pre>';
                // print_r($data);
        //=========================================== Load view Page ==============================
        //$pageName='home';
        return view('front/home/home',compact('data'));
    }
    //============= load admin ============//
    public function admin(Request $request)
    {
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        //$siteDetails = $this->getAllData('site_settings',array('id'=>1),'id','DESC');
        $data['pageName']='Admin';
        return view('control/home/index',compact('data'));
        
    }
    //======= login functionality starts here ========//
    public function doLogin(Request $request)
    {

       

        $username = $request->get('email');
        $password = $request->post('password');
        //$password = hash('sha256',$pass);
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $curlUrl = 'api/connectSite';

        $curlPostData = array(
                        'mode'=>'userLogin',
                        'username'=>$username,
                        'password'=>$password,
                        );
        $url = $baseUrl.$curlUrl;
      //echo $url;echo "<pre>";print_r($curlPostData);die();
        $userlogin = $this->fireCurl($url,$curlPostData);//die();
        $userlogin_array = json_decode($userlogin,1);
        // echo $url;echo "<pre>";print_r($userlogin_array);die();
        if($userlogin_array['status'] == '1')
        {
            $userId=$userlogin_array['data']['userdetails']['StoreManagerId'];
            $IsSuperAdmin=$userlogin_array['data']['userdetails']['Is_superadmin'];
            $storeId=$userlogin_array['data']['userdetails']['AccessStore'];
            $pageaccess = $userlogin_array['data']['userdetails']['page_access']; 
            if($storeId != '')
            {
                $storearr=explode(',', $storeId);
                $storeId=$storearr[0];
            }
        }
        else{
            $userId='';
            $IsSuperAdmin='';
            $storeId='';
            $pageaccess='';
        }
        // echo $userId."++++".$IsSuperAdmin.'++++'.$storeId;//print_r($userlogin_array);
        // die();
            if($userId !== '' &&  $IsSuperAdmin !== '' &&  $storeId !== '')
            {
                //echo 'in userId'.$userId."++++".$IsSuperAdmin;//print_r($userlogin_array);
                //die();
                //================ set cookie ===========//
                if($request->get('remember') != '')
                {
                    Cookie::queue('userName', $username, 45000);
                    Cookie::queue('password', $password, 45000);
                    Cookie::queue('remember', true, 45000);
                   // Cookie::queue(Cookie::make('username1', $name, 45000),Cookie::make('password1', $pass, 45000),Cookie::make('remember', true, 45000));
                }
                else
                {
                    Cookie::queue('userName', "", 45000);
                    Cookie::queue('password', "", 45000);
                    Cookie::queue('remember', "", 45000);
                }
                //============== set cookie ==============//
                
                $sess_array = array('userId' => $userId,
                                    'loginUserId' => $userId,
                                    'is_loggedin' => 1,
                                    'Is_superadmin' =>$IsSuperAdmin,
                                    'storeId' => $storeId,
                                    'pageaccess'=>$pageaccess,
                                    'success_msg' => "Logged in successfully!");
                $request->session()->put($sess_array);
                setcookie('loggedincookie', $storeId, time() + (86400 * 30), "/"); // 86400 = 1 day
                // echo  $value = $request->session()->get('pageaccess'); die('hello there');
                 // Cookie::queue('loggedincookie',$userId, 45000);
                // echo "<pre>";Print_r( Session::get());die();
                //============ update last login time ============//
                //$this->update('admin',array('id'=>$data['admin'][0]['id']),array('lastLogin'=>date('Y-m-d H:i:s'),'islogin'=>'1'));
                return Redirect::to('/oms/dashboard');   
            }
            else
            {
                $request->session()->put('error_msg',"Wrong username and password!");
                return Redirect::to('/oms');
            }
        
    }
    //=============== reset password ====================//
    function resetPassword(Request $request)
    {
        $token = $request->segment('4');
        $userId= base64_decode($request->segment('3'));
        $data['userId'] =$request->segment('3');
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $curlUserUrl = 'api/connectSite';
        $curlUserPostData = array(
                            'mode' => 'getuserdetails',
                            'userId' => $userId
                            );
            $Userurl = $baseUrl.$curlUserUrl;
            $user = $this->fireCurl($Userurl,$curlUserPostData);
            $user_array = json_decode($user,1);
       // echo "<pre>".$data['userId'];
   // print_r($user_array);die;
        $currentTime = date('Y-m-d H:i:s');
        if($user_array['status'] == 1)
        {
            if($token == $user_array['data']['userDetails'][0]['reset_token'])
            {
                $hourdiff = round((strtotime($currentTime) - ($user_array['data']['userDetails'][0]['request_time']))/3600, 1);
                if($hourdiff < 24)
                {
                    return view('control/home/reset_pass',compact('data'));
                }
                else
                {
                    $request->session()->put('error_msg',"You have reached the maximum time limit!");
                    Redirect::to('/oms')->send();
                }
            }
            else{
                $request->session()->put('error_msg',"Sorry!Wrong Token.");
            Redirect::to('/oms')->send();
            }
        }
        else
        {
            $request->session()->put('error_msg',"Something went wrong.");
            Redirect::to('/oms')->send();
        }
    }
    //============== update password =========================//
    function updatePassword(Request $request)
    {
        $userId = $request->post('userId');
        $password = $request->post('n_pass');
        //$password = hash('sha256',$pass);
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $curlUrl = 'api/connectSite';
        $curlPostData = array(
                            'mode' => 'ChangePasswordAdmin',
                            'userId' => $userId,
                            'password' =>$password
                            );
            $url = $baseUrl.$curlUrl;
            $chngPassword = $this->fireCurl($url,$curlPostData);
            $chngPassword_array = json_decode($chngPassword,1);
            //echo "<pre>";
           // print_r($chngPassword_array);die();
            if($chngPassword_array['status'] == 1)
            {
                $request->session()->put('success_msg',"Password changed. Please login to continue!");
                Redirect::to('/oms')->send();
            }
            else{
                $request->session()->put('error_msg',"Password has not been changed. Please try again!");
                Redirect::to('/oms')->send();
            }
        
    }
    //============== check user existance from uername or email id ==========//
    function getEmail(Request $request)
    {
        $username = $request->post('username');
        //echo $username;die();
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $curlUrl = 'api/connectSite';
        $curlPostData = array(
                            'mode' => 'retreivePassword',
                            'admin_email_or_username' => $username
                            );
            $url = $baseUrl.$curlUrl;
            $mail = $this->fireCurl($url,$curlPostData);
            $mail_array = json_decode($mail,1);
            if($mail_array['status'] == 1)
            {
                $request->session()->put('success_msg',"Please check your mail to reset password!");
                echo 1;
            }
            else{
                 echo '0';
            }
        //echo "<pre>";
        //print_r($mail_array);die;
    }
    function errorPage()
    {
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $pageName='Error Page';
        return view('front/home/error',compact('pageName'));
    }
}
?>