<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
//Route::get('/', ['middleware' => ['ipcheck'], function () {
//============ load error page ==============//
Route::get('page-error','site_controller\Home@errorPage');//Error page

Route::middleware('ipcheck')->group(function () {
    // your routes here
//============ load home page ==============//
    Route::get('/','site_controller\Home@index');//Home page



    
//============ load Admin Login page ==========//	
  	Route::get('/oms','site_controller\Home@admin');//Admin login page
  	Route::post('oms/login','site_controller\Home@doLogin');//Login functionality of ADMIN
  	Route::get('oms/logout','site_controller\Dashboard@logout');//LOGOUT PAGE of ADMIN
  	Route::get('oms/reset-password/{id}/{token}','site_controller\Home@resetPassword');//check user existance from username or email
    Route::post('oms/reset-password','site_controller\Home@updatePassword');//check user existance from username or email
    Route::post('oms/get-email','site_controller\Home@getEmail');//check user existance from username or email
//============ load category page =========//
	Route::get('category','site_controller\Category@index');//Category page
//============ load product page ==========//	
	Route::get('product','site_controller\Product@index');//Category page
//============ load Cart page ==============//
   Route::get('cart','site_controller\Cart@index');//Cart page
//============ load Checkout Page ===========//   
   Route::get('checkOut','site_controller\Cart@Checkout');//checkOut page
//============ load Account page ==========//	
	Route::get('account','site_controller\Account@index');//Account page
//============ load wishlist Page =========//    
    Route::get('wishlist','site_controller\Account@wishList');//Wishlist page
//============ load Return page ==========//	
	Route::get('return_product','site_controller\Return_product@index');//Return page

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//---------------------- BEFORE OPEN ANY URL OF THIS ROUTE GROUP IT WILL CHECK FOR USER LOGIN ---------------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
Route::middleware('CheckLogin')->group(function () {
        //=========== DASHBOARD AND LOGOUT PAGE ==============//
    Route::get('oms/dashboard','site_controller\Dashboard@index');//DASHBOARD PAGE
    Route::post('oms/changeloginusertype','site_controller\Dashboard@changestoretype');//Dashboard change store type
    //Route::get('oms/adminmanager/passwordmanager','site_controller\AdminManager@index');//ADMIN MANAGER CHANGE USER NAME AND PASSWORD PAGE 
    Route::get('oms/adminmanager/settingsmanager','site_controller\AdminManager@index');//ADMIN MANAGER CHANGE USER NAME AND PASSWORD PAGE 
    Route::post('oms/adminmanager/updatepasswordmanager','site_controller\AdminManager@updatepassword');       
    Route::get('oms/adminmanager/manageshipping','site_controller\AdminManager@manageShipping');//Shipping Manager Listing
    Route::post('oms/adminmanager/updateshipping','site_controller\AdminManager@updateShipping');//Update shipping Management
    Route::get('oms/adminmanager/managepairdiscount','site_controller\AdminManager@managepairdiscount');//Pair Discount Management
    Route::post('oms/adminmanager/updatepairdiscount','site_controller\AdminManager@updatemanagepairdiscount');//Update Pair discount Management
    Route::get('oms/adminmanager/reportmail','site_controller\AdminManager@manageReportMail');//sale order report email Management
    Route::post('oms/adminmanager/updatereportmail','site_controller\AdminManager@updateReportMail');//Update sale order report email  Management
    // =============== STORE SECTION STARTS HERE ============== //
        Route::get('oms/store/store-list','site_controller\Store@index');//store listing page
        Route::get('oms/store/add-store','site_controller\Store@addStore');//add Store
        Route::post('oms/store/store-insert','site_controller\Store@storeInsert');//Insert Store
        Route::get('oms/store/edit-store/{id}','site_controller\Store@editStore');//edit Store
        Route::post('oms/store/store-update','site_controller\Store@storeUpdate');//Update Store
        Route::get('oms/store/change-status/{id}/{status}','site_controller\Store@changeStatus');//change status
        Route::post('oms/fetchShoeSize','site_controller\Store@fetchShoeSize');//Store Shoe Size fore particular store
    //=================================Banner Manager Start=======================================//
    Route::get('oms/banner/bannerlist','site_controller\BannerManager@getList');//Banner listing
    Route::get('oms/banner/addbanner','site_controller\BannerManager@addBanner');//Add banner
    Route::post('oms/banner/insert-banner-details','site_controller\BannerManager@insertBannerDtls');//Insert Banner
    Route::get('oms/banner/banner-details/{id}','site_controller\BannerManager@getDetails');//Edit banner
    Route::post('oms/banner/edit-banner-details','site_controller\BannerManager@updateBannerDtls');//Update Banner
    Route::get('oms/banner/change-status/{id}/{status}','site_controller\BannerManager@changeStatus');//change status
    Route::get('oms/banner/deletebanner/{id}','site_controller\BannerManager@deleteBanner');//delete banner
    //=================================Banner Manager End=======================================//
    //=================================Trend Manager Start=======================================//
    Route::get('oms/trend/trendlist','site_controller\TrendManager@getList');//Trend listing
    Route::get('oms/trend/addtrend','site_controller\TrendManager@addTrend');//Add Trend
    Route::post('oms/trend/insert-trend-details','site_controller\TrendManager@insertTrendDtls');//Insert Trend
    Route::get('oms/trend/trend-details/{id}','site_controller\TrendManager@getDetails');//Edit Trend
    Route::post('oms/trend/edit-trend-details','site_controller\TrendManager@updateTrendDtls');//Update Trend
    Route::get('oms/trend/change-status/{id}/{status}','site_controller\TrendManager@changeStatus');//change status
    Route::get('oms/trend/deletetrend/{id}','site_controller\TrendManager@deletetrend');//delete trend
    //=================================Trend Manager End=======================================//
    //================================= Store Manager Start ===================================//
    Route::get('oms/storemanager/store-list','site_controller\StoreManager@index'); //Store Manager listing
    Route::get('oms/storemanager/store-manager-add','site_controller\StoreManager@addStoreManager'); //Store Manager Add page
    Route::post('oms/storemanager/store-manager-insert','site_controller\StoreManager@storemanagerInsert'); //Store Manager insert
    Route::get('oms/storemanager/store-manager-edit/{id}','site_controller\StoreManager@editStoreManager'); //Store Manager edit view
    Route::post('oms/storemanager/store-manager-update','site_controller\StoreManager@storemanagerUpdate'); //Store Manager insert
    Route::get('oms/storemanager/store-manager-change-status/{id}/{status}','site_controller\StoreManager@changeStatus'); //Store Manager insert
    //================================= Store Manager End ===================================//
     //=================================Page Manager Start=======================================//
    // Route::get('oms/article/article-list','site_controller\ArticleManager@getList');//Page listing
    // Route::get('oms/article/add-article','site_controller\ArticleManager@addArticle');//Add Page
    // Route::post('oms/article/insert-article-details','site_controller\ArticleManager@insertArticleDtls');//Insert Page
    // Route::get('oms/article/article-details/{id}','site_controller\ArticleManager@getDetails');//Edit Page
    // Route::post('oms/article/edit-article-details','site_controller\ArticleManager@updateArticleDtls');//Update Page
    // Route::get('oms/article/change-status/{id}/{status}','site_controller\ArticleManager@changeStatus');//change status
    // Route::get('oms/article/delete-article/{id}','site_controller\ArticleManager@deleteArticle');//delete Page
    Route::get('oms/page/page-list','site_controller\ArticleManager@getList');//Page listing
    Route::get('oms/page/add-page','site_controller\ArticleManager@addArticle');//Add Page
    Route::post('oms/page/insert-page-details','site_controller\ArticleManager@insertArticleDtls');//Insert Page
    Route::get('oms/page/page-details/{id}','site_controller\ArticleManager@getDetails');//Edit Page
    Route::post('oms/page/edit-page-details','site_controller\ArticleManager@updateArticleDtls');//Update Page
    Route::get('oms/page/change-status/{id}/{status}','site_controller\ArticleManager@changeStatus');//change status
    Route::get('oms/page/delete-page/{id}','site_controller\ArticleManager@deleteArticle');//delete Page
    //=================================Article Manager End=======================================//
    //=================================Category Manager Start=======================================//
    Route::get('oms/category/category-list','site_controller\CategoryManager@getList');//Category listing
    Route::get('oms/category/add-category','site_controller\CategoryManager@addCategory');//Add Category
    Route::post('oms/category/insert-category-details','site_controller\CategoryManager@insertCategoryDtls');//Insert Category
    Route::get('oms/category/category-details/{id}','site_controller\CategoryManager@getDetails');//Edit Category
    Route::post('oms/category/edit-category-details','site_controller\CategoryManager@updateCategoryDtls');//Update Category
    Route::get('oms/category/change-status/{id}/{status}','site_controller\CategoryManager@changeStatus');//change status
    Route::get('oms/category/delete-category/{id}','site_controller\CategoryManager@deleteCategory');//delete Category
    //=================================Category Manager End=======================================//
    //=================================Product Type Manager Start=======================================//
    Route::get('oms/producttype/type-list','site_controller\TypeManager@getList');//Category listing
    Route::get('oms/producttype/add-type','site_controller\TypeManager@addType');//Add Category
    Route::post('oms/producttype/insert-type-details','site_controller\TypeManager@insertTypeDtls');//Insert Category
    Route::get('oms/producttype/type-details/{id}','site_controller\TypeManager@getDetails');//Edit Category
    Route::post('oms/producttype/edit-type-details','site_controller\TypeManager@updateTypeDtls');//Update Category
    Route::get('oms/producttype/change-status/{id}/{status}','site_controller\TypeManager@changeStatus');//change status
    Route::get('oms/producttype/delete-type/{id}','site_controller\TypeManager@deleteType');//delete Category
    //================================= Product Type Manager End=======================================//    
    //=================================Brand Manager Start=======================================//
    Route::get('oms/brand/brand-list','site_controller\BrandManager@getList');//Brand listing
    Route::get('oms/brand/add-brand','site_controller\BrandManager@addBrand');//Add Brand
    Route::post('oms/brand/insert-brand-details','site_controller\BrandManager@insertBrandDtls');//Insert Brand
    Route::get('oms/brand/brand-details/{id}','site_controller\BrandManager@getDetails');//Edit Brand
    Route::post('oms/brand/edit-brand-details','site_controller\BrandManager@updateBrandDtls');//Update Brand
    Route::get('oms/brand/change-status/{id}/{status}','site_controller\BrandManager@changeStatus');//change status
    Route::get('oms/brand/delete-brand/{id}','site_controller\BrandManager@deleteBrand');//delete Brand
    //=================================Brand Manager End=======================================//
    //=================================Product Manager Start=======================================//
    Route::get('oms/product/product-list','site_controller\ProductManager@getList2');//Product listing
    Route::get('oms/product/product-list2','site_controller\ProductManager@getList1');//Product listing
    Route::get('oms/product/product-list3','site_controller\ProductManager@getList3');//Product listing

    Route::get('oms/product/add-product','site_controller\ProductManager@addProduct');//Add Product page
    Route::post('oms/product/add_product_data','site_controller\ProductManager@insertProductDtls');//Insert Product Data

     Route::get('oms/product/edit-product/{id}','site_controller\ProductManager@editProduct');//edit Product

    Route::get('oms/product/shoe-list','site_controller\ProductManager@getShoeList');//Shoes listing
    Route::get('oms/product/add-shoe-style','site_controller\ProductManager@addShoeProduct');//Add Shoe
    Route::get('oms/product/apparel-list','site_controller\ProductManager@getApparelList');//Apparel listing
    Route::get('oms/product/add-apparel-style','site_controller\ProductManager@addApparelProduct');//Add Apparel
    Route::get('oms/product/bag-list','site_controller\ProductManager@geBagtList');//Bags listing
    Route::get('oms/product/add-bag-style','site_controller\ProductManager@addBagProduct');//Add Bags
    Route::post('oms/product/insert-brand-details','site_controller\ProductManager@insertBrandDtls');//Insert Brand
    Route::get('oms/product/brand-details/{id}','site_controller\ProductManager@getDetails');//Edit Brand
    Route::post('oms/product/edit-brand-details','site_controller\ProductManager@updateBrandDtls');//Update Brand
    Route::get('oms/product/change-status/{id}/{status}','site_controller\ProductManager@changeStatus');//change status
    Route::get('oms/product/delete-brand/{id}','site_controller\ProductManager@deleteBrand');//delete Brand
    Route::post('oms/fetchStoreShoeSize','site_controller\ProductManager@fetchStoreShoeSize');//Store Shoe Size fore particular store

    Route::post('oms/fetchCategoryDta','site_controller\ProductManager@fetchCategoryDta');//Store Category for particular product type
    Route::post('oms/fetchColourDta','site_controller\ProductManager@fetchColourDta');//Store Colour for particular product type
    Route::post('oms/fetchMaterialDta','site_controller\ProductManager@fetchMaterialDta');//Store Colour for particular product type
    Route::post('oms/fetchSizeDtaEU','site_controller\ProductManager@fetchSizeDtaEU');//Store Size for particular product type for EU
    Route::post('oms/fetchSizeDtaUSA','site_controller\ProductManager@fetchSizeDtaUSA');//Store Size for particular product type for USA

    
    //=================================Product Manager End=======================================//
    //================================= customer category =======================================//
    Route::get('oms/customer/customer-list','site_controller\Allcustomer@getList');//Customer Details list from member table
    Route::get('oms/customer/add-customer','site_controller\Allcustomer@addCustomer');//Add Customer
    Route::get('oms/customer/edit-customer','site_controller\Allcustomer@editCustomer');//edit Customer
    Route::post('oms/customer/insert-customer-details','site_controller\Allcustomer@insertCustomerDtls');//Insert Customer details

    
    
    //================================= customer category =======================================//
    //================================= Order Manager Section ===================================//
    Route::get('oms/order/order-list','site_controller\Allorder@getList');//Order Details list 

    //================================= Order Manager Section ===================================//
    
    //================================= Test For developer ======================================//
    Route::get('oms/test/weather','site_controller\Test@getweather');//Weather data     
    //================================= Test For developer ======================================//
});
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
});
