<!-- include summernote css -->
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css') !!}
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css') !!}
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css') !!}
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') !!}
<!-- include dropzone js -->
{!! HTML::style('assets/css/dropzone.min.css') !!}
{!! HTML::style('assets/css/tagsinput.css') !!}

  
    <!-- Main -->

    <div class="content-main staic_table_custom" id="content-main">
        <!-- ############ Main START-->
        <div class="padding">
            <div class="container">
            <div class="clearfix top_export_sec">
                <div class="clearfix breadcram">
                    <a href="{!! url('/') !!}/oms/product/product-list"><i class="fa fa-angle-left"></i>Product Summary</a>
                </div>
                <div class="left_head">
                    <h2>Products</h2>
                </div>
            </div>
              <div id='all_img_div' style="display:none">
                <textarea name="body_html" id="body_html"></textarea>
                <input type="file" class='all_product_file' name="all_file0[]" id="all_file0_0" accept="image/*" style="display:none">
              </div>
                <div class="row">
                    <div class="col-sm-12 "><!-- cbd_main_content -->
                        <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                   @if($data['storeadminorsiteadmin'] == 0) <h6>Edit Your Product</h6> @else <h6>Edit Product</h6> @endif
                                </header>
                                <div class="form-row">
                                 <div class='form-group col-sm-6'>
                                  <label for='sku5' class='ad_prd_lbl'>Product Type </label>
                                <!--   <select id='producttype' name='producttype[]' class='custom-select w-100 ' autocomplete='off' label='product type' >
                                   @if(!empty($data['ProductType']))
                                        @foreach($data['ProductType'] as $Producttype)
                                            <option value="{!! $Producttype['producttypeid'] !!}">{!! ucfirst($Producttype['producttypeName']) !!}</option>
                                        @endforeach
                                      @endif
                                  
                                  </select> -->


                                    <input class="form-control need" id="producttype" placeholder="Product Type" type="text" name="producttype" value="Shoe" readonly="true" />

                                </div>
                              </div>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl">Style Name *</label>  
                                    <input class="form-control need" id="styleName" placeholder="Style Name" type="text" name="styleName" value="Sadie" />
                                    <span id="styleName_error" style="color:red"></span>
                                  </div>
                                  <div class="form-group col-md-6">
                                      <label for="usr5" class="ad_prd_lbl">Factory ID *</label>
                                     <input class="form-control need" id="factoryId" placeholder="Factory Id" type="text" name="factoryId" value="A10250A" />
                                       <span id="factoryId_error" style="color:red"></span>
                                  </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl">Season *</label>
                                    <select id="season" name="season" class="custom-select w-100 need" autocomplete="off" label="Season" >
                                      @if(!empty($data['season']))
                                        @foreach($data['season'] as $seasonData)
                                            <option value="{!! $seasonData['Id'] !!}" @if($seasonData['Id']) selected @endif>{!! ucfirst($seasonData['Name']) !!}</option>
                                        @endforeach
                                      @endif
                                      <!-- <option value="Summer">Summer</option>
                                      <option value="Winter">Winter</option> -->
                                    </select>
                                    <span id="season_error" style="color:red"></span>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl ">Category 1*</label>
                                   <!--  <select id="category" name="category" class="custom-select w-100 need" autocomplete="off" label="Category"  >
                                      <option value="">Select one...</option>
                                      @if(!empty($data['categorylist']))
                                        @foreach($data['categorylist'] as $category)
                                            <option value="{!! $category['CategoryID'] !!}">{!! ucfirst($category['CategoryName']) !!}</option>
                                        @endforeach
                                      @endif
                                    </select> -->


                                    <select id="category10" name="category10" class="custom-select w-100 need" autocomplete="off" label="Category"><option value="">Select one...</option><option value="7">Sandals</option><option value="8">Heels</option><option value="9">Sale</option><option value="11">Sneakers</option><option selected value="12">Pre Order</option><option value="15">AS FEATURED</option><option value="17">Bridal</option><option value="18">Flatforms</option></select>

                                  </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl ">Category 2 *</label>
                                   <!--  <select id="subCategory" name="subCategory" class="custom-select w-100 " autocomplete="off" label="Sub Category"  >
                                      <option value="">Select one...</option>
                                      @if(!empty($data['categorylist']))
                                        @foreach($data['categorylist'] as $category)
                                        <option value="{!! $category['CategoryID'] !!}">{!! ucfirst($category['CategoryName']) !!}</option>
                                        @endforeach
                                      @endif
                                    </select> -->

                                    <select id="subCategory3" name="subCategory3" class="custom-select w-100 " autocomplete="off" label="Sub Category"><option value="">Select one...</option><option value="7">Sandals</option><option value="8">Heels</option><option value="9">Sale</option><option value="11">Sneakers</option><option value="12">Pre Order</option><option value="15" selected>AS FEATURED</option><option value="17">Bridal</option><option value="18">Flatforms</option></select>

                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl ">Sub Brand *</label>
                                    <select id="subBrand" name="subBrand" class="custom-select w-100 " autocomplete="off" label="Sub Brand"  >
                                      <option value="">Select one...</option>
                                       @if(!empty($data['brandlist']))
                                        @foreach($data['brandlist'] as $brand)
                                            <option value="{!! $brand['brandId'] !!}" selected>{!! ucfirst($brand['brandName']) !!}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                  </div>
                                </div> 
                                <div class="form-row">
                                  <div class="form-group col-md-12">
                                      <label for="prz5" class="ad_prd_lbl">Description *</label>
                                      <div class="browse_field" id="summernote1_form">
                                          <div id="summernote1" class="summernote need" name="productDesc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

</div>
                                      </div>
                                      <span id="summernote1_error" style="color:red"></span>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                          <div class="order_details">
                            <header class="prd_hdr clearfix">
                              <h6>Display Properties</h6>
                            </header>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="usr5" class="ad_prd_lbl">Display Priority * (Note: should be a number e.g 1)</label>
                                <input type="text" class="form-control isNumberKey need" id="displayOrder" name="displayOrder" value="" label="Display Order" placeholder="" autocomplete="off" value="1">
                                <span id="displayOrder_error" style="color:red"></span>
                              </div>
                            
                            </div>
                            @if($data['storeadminorsiteadmin'] == 0)
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="usr5" class="ad_prd_lbl ">Start Date *(Product Display Start date)</label>
                                <input type='text' id="startDate" name="startDate" class="form-control date need" label="Start Date" autocomplete="off" readonly/>
                                <span id="startDate_error" style="color:red"></span>
                              </div>
                              <div class="form-group col-md-6">
                                <label for="usr5" class="ad_prd_lbl ">End Date *(Product Display End date)</label>
                                <input type='text' id="endDate" name="endDate" class="form-control date need" label="End Date" autocomplete="off" readonly/>
                                <span id="endDate_error" style="color:red"></span>
                              </div>
                            </div>
                            @endif
                            <div class="form-row">
                              <div class="form-group col-md-4">
                                  <label for="usr5" class="ad_prd_lbl">Is Active </label>
                                  <input type="checkbox" class="col-md-1" id="IsActive" name="IsActive" label="Is Active" autocomplete="off" value='1'>
                                   <span id="IsActive_error" style="color:red"></span>
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="usr5" class="ad_prd_lbl">Is New Arrival </label>
                                  <input type="checkbox" class="col-md-1" id="IsNew" name="IsNew" label="Is New Arrival" autocomplete="off" value='1'>
                                   <span id="IsActive_error" style="color:red"></span>
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="usr5" class="ad_prd_lbl">Is On Sale </label>
                                  <input type="checkbox" class="col-md-1" id="IsSale" name="IsSale" label="Is On Sale" autocomplete="off" value='1'>
                                   <span id="IsActive_error" style="color:red"></span>
                              </div>
                            </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Pricing</h6>
                                </header>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="comp_prz5" class="ad_prd_lbl">Cost price (AUD)</label>
                                        <input type="text" class="form-control isNumberKey" id="comp_prz5" placeholder="AUD 0.00" value="60.00">
                                    </div>
                                    <!-- @if(!empty($data['store']))
                                      @foreach($data['store'] as $store)
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl"><?php echo $store['CurrencyCode'].' Retail Price'?></label>
                                        <input type="text" class="form-control isNumberKey" id="{!! $store['CurrencyCode'].'Retail Price' !!}" placeholder="AUD 0.00">
                                    </div>
                                      @endforeach
                                    @endif -->
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl"> Retail Price (AUD)</label>
                                        <input type="text" class="form-control isNumberKey" id="Retail Price" placeholder="AUD 0.00" value="129.75">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl">Discounted Retail Price (AUD)</label>
                                        <input type="text" class="form-control isNumberKey" id="prz5" placeholder="AUD 0.00" value=" 60.00">
                                    </div>
                                </div>
                               <!--  <div class="form-row">
                                    
                                </div> -->
                            </div>
                        </div>

                       

                       
<!-- //eta open  **********************************-->
                        <div class="box">
                            <div class="order_details">
                              <header class="prd_hdr clearfix">
                                <h6>Variants</h6>
                                <button class="txt_btn fl_left" id="tgl_div1">Add variant</button>
                                <button class="txt_btn fl_left txt_btn_hide" id="tgl_div2">close</button>
                              </header>
                              <div class="form-group">
                                <p>Add variants if this product comes in multiple versions, like different colors.</p>
                              </div>
                            </div>
                            <div class="clearfix dropdown_div">
                              <div class="order_details gry_of_bg">
                                <div class="clearfix form-result_outer" id="form-result_cst">
                                  <div class="prd_hdr clearfix">
                                    <h4 class="ad_prd_lbl">Modify the variants to be created:</h4>
                                  </div>
                                  <div class="cbd_table_outer custmclstableouter">
                                    <div id="allVariant">
                                      <div class="form-row">

                                        <div class="form-group col-sm-3">
                                          <label for="sku5" class="ad_prd_lbl">Colour *</label>
                                         <select id="colour88" name="colour[]" class="custom-select w-100 varneed" autocomplete="off" label="Colour"><option value="">Select one...</option><option value="1">Black</option><option value="2">Black Leopard</option><option value="3">Black Red</option><option value="4">Black Tan</option><option value="5" selected="true">Black White</option><option value="6">Blue</option><option value="7">Blue Leopard</option><option value="8">Blush</option><option value="9">Bone</option><option value="10">Bronze</option><option value="11">Bronze Black</option><option value="12">Burgundy</option><option value="13">Burnt Orange</option><option value="14">Camel</option><option value="15">Chai White</option><option value="16">Charcoal</option><option value="17">Cyan</option><option value="18">Dark Natural</option><option value="19">Denim</option><option value="20">Earth</option><option value="21">Fudge</option><option value="22">Gold</option><option value="23">Green</option><option value="24">Grey</option><option value="25">Gun Metal</option><option value="26">Khaki</option><option value="27">Leopard</option><option value="28">Light Grey</option><option value="29">Light Tan</option><option value="30">Lizard</option><option value="31">Marine</option><option value="32">Midnight</option><option value="33">Moss</option><option value="34">Moss Black</option><option value="35">Multi</option><option value="36">Musk</option><option value="37">Natural</option><option value="38">Navy</option><option value="39">Navy Black</option><option value="40">Navy Red</option><option value="41">Nude</option><option value="42">Nude Black</option><option value="43">Orange</option><option value="44">Orange Tan</option><option value="45">Pale Blue</option><option value="46">Purple</option><option value="47">Red</option><option value="48">Red Black</option><option value="49">Red Navy</option><option value="50">Rust</option><option value="51">Sand</option><option value="52">Silver</option><option value="53">Smoke</option><option value="54">Stone</option><option value="55">Tan</option><option value="56">Tan Black</option><option value="57">Tan White</option><option value="58">Taupe</option><option value="59">Toffee</option><option value="60">White</option><option value="61">White Natural</option><option value="62">Yellow</option></select>
                                        </div>
                                       
                                        <div class="form-group col-sm-3">
                                          <label for="sku5" class="ad_prd_lbl">Material *</label>
                                       <select id="material1111" name="material[]" class="custom-select w-100 varneed" autocomplete="off" label="Material"><option value="">Select one...</option><option value="1">Burnished</option><option value="2">Burnished Box</option><option value="3">Calf</option><option value="4">Canvas</option><option value="5">Combo</option><option value="6">Crocodile</option><option value="7">Elephant</option><option value="8">Fabric</option><option value="9">Kid Suede</option><option value="10">Leather</option><option value="11">Leopard</option><option value="12">Linen</option><option value="13">Lizard</option><option value="14">Mirror</option><option value="15">Nappa</option><option value="16" selected="true">Nubuck</option><option value="17">Patent</option><option value="18">Pony</option><option value="19">Snail</option><option value="20">Snake</option><option value="21">Stingray</option><option value="22">Stretch Suede</option><option value="23">Stretch Velvet</option><option value="24">Suede</option><option value="25">Tumble</option><option value="26">Velvet</option><option value="27">Weave</option></select>
                                        </div>

                                        <div class="form-group col-sm-3">
                                          <label for="sku5" class="ad_prd_lbl">Size *</label>
                                          <select id="size01234" name="size[]" class="custom-select w-100 varneed" autocomplete="off" label="Size"><option value="">Select one...</option><option value="1">32.5(cm)/3(cm)/19(cm)</option><option value="2">33(cm)/3.5(cm)/19.5(cm)</option><option value="3">33.5(cm)/4(cm)/20(cm)</option><option value="4" selected="true">34(cm)/4.5(cm)/20.5(cm)</option><option value="5">34.5(cm)/5(cm)/21(cm)</option><option value="6">35.5(cm)/5.5(cm)/21.5(cm)</option><option value="7">36.5(cm)/6(cm)/22(cm)</option></select>
                                        </div>

                                          <div class="form-group col-sm-3">
                                          <label for="sku5" class="ad_prd_lbl">Quantity *</label>
                                           <input type="text" class="form-control" id="inpState5" placeholder="" value="10">
                                        </div>
                                    
                                      </div>

                                     <!--  <img src="https://www.aliasmae.com.au/product_images/agnes-gldemb-u.jpg" class="img-rounded" alt="Cinque Terre"> -->

                                       <div class="form-row">

                                        <div class="form-group col-sm-3">
                                          <label for="sku5" class="ad_prd_lbl">Colour *</label>
                                         <select id="colour888" name="colour[]" class="custom-select w-100 varneed" autocomplete="off" label="Colour"><option value="">Select one...</option><option value="1">Black</option><option value="2">Black Leopard</option><option value="3">Black Red</option><option value="4">Black Tan</option><option value="5" selected="true">Black White</option><option value="6">Blue</option><option value="7">Blue Leopard</option><option value="8">Blush</option><option value="9">Bone</option><option value="10">Bronze</option><option value="11">Bronze Black</option><option value="12">Burgundy</option><option value="13">Burnt Orange</option><option value="14">Camel</option><option value="15">Chai White</option><option value="16">Charcoal</option><option value="17">Cyan</option><option value="18">Dark Natural</option><option value="19">Denim</option><option value="20">Earth</option><option value="21">Fudge</option><option value="22">Gold</option><option value="23">Green</option><option value="24">Grey</option><option value="25">Gun Metal</option><option value="26">Khaki</option><option value="27">Leopard</option><option value="28">Light Grey</option><option value="29" selected="true">Light Tan</option><option value="30">Lizard</option><option value="31">Marine</option><option value="32">Midnight</option><option value="33">Moss</option><option value="34">Moss Black</option><option value="35">Multi</option><option value="36">Musk</option><option value="37">Natural</option><option value="38">Navy</option><option value="39">Navy Black</option><option value="40">Navy Red</option><option value="41">Nude</option><option value="42">Nude Black</option><option value="43">Orange</option><option value="44">Orange Tan</option><option value="45">Pale Blue</option><option value="46">Purple</option><option value="47">Red</option><option value="48">Red Black</option><option value="49">Red Navy</option><option value="50">Rust</option><option value="51">Sand</option><option value="52">Silver</option><option value="53">Smoke</option><option value="54">Stone</option><option value="55">Tan</option><option value="56">Tan Black</option><option value="57">Tan White</option><option value="58">Taupe</option><option value="59">Toffee</option><option value="60">White</option><option value="61">White Natural</option><option value="62">Yellow</option></select>
                                        </div>
                                       
                                        <div class="form-group col-sm-3">
                                          <label for="sku5" class="ad_prd_lbl">Material *</label>
                                          <select id="material111" name="material[]" class="custom-select w-100 varneed" autocomplete="off" label="Material"><option value="">Select one...</option><option value="1">Burnished</option><option value="2">Burnished Box</option><option value="3">Calf</option><option value="4">Canvas</option><option value="5">Combo</option><option value="6">Crocodile</option><option value="7">Elephant</option><option value="8">Fabric</option><option value="9">Kid Suede</option><option value="10">Leather</option><option value="11">Leopard</option><option value="12">Linen</option><option value="13">Lizard</option><option value="14">Mirror</option><option value="15">Nappa</option><option value="16">Nubuck</option><option value="17">Patent</option><option value="18" selected="true">Pony</option><option value="19">Snail</option><option value="20">Snake</option><option value="21">Stingray</option><option value="22">Stretch Suede</option><option value="23">Stretch Velvet</option><option value="24">Suede</option><option value="25">Tumble</option><option value="26">Velvet</option><option value="27">Weave</option></select>
                                        </div>

                                        <div class="form-group col-sm-3">
                                          <label for="sku5" class="ad_prd_lbl">Size *</label>
                                           <select id="size0123" name="size[]" class="custom-select w-100 varneed" autocomplete="off" label="Size"><option value="">Select one...</option><option value="1">32.5(cm)/3(cm)/19(cm)</option><option value="2">33(cm)/3.5(cm)/19.5(cm)</option><option value="3">33.5(cm)/4(cm)/20(cm)</option><option value="4" selected="true">34(cm)/4.5(cm)/20.5(cm)</option><option value="5">34.5(cm)/5(cm)/21(cm)</option><option value="6">35.5(cm)/5.5(cm)/21.5(cm)</option><option value="7">36.5(cm)/6(cm)/22(cm)</option></select>
                                        </div>

                                          <div class="form-group col-sm-3">
                                          <label for="sku5" class="ad_prd_lbl">Quantity *</label>
                                          <input type="text" class="form-control" id="inpState5" placeholder="" value="10">
                                        </div>
                                    
                                      </div>
                                      
                                      <div class="form-row" id="StoreSizeHtml0">



                                      </div>
                                      <div class="form-row">
                                        <div class="form-group col-sm-8">
                                          <label for="sku5" class="ad_prd_lbl">Image :</label>
                                        
                                          <a href="javascript:void(0)" class="dropzone_btn" data-toggle="modal" data-target="#manage_upload_img">Add image from URL</a>
                                          <a href="javascript:void(0)" class="dropzone_btn by_file" id="by_file0" onclick="UploadImage()">Upload image
                                         </a>
                                        </div>
                                        <div class="form-group col-sm-4">
                                          <label for="usr5" class="ad_prd_lbl">Is Active </label>
                                          <input type="checkbox" class="col-md-1 " id="IsActive0" name="IsActive[]" label="Is Active" autocomplete="off" value='1'>
                                        </div>
                                      </div>
                                        
                                        <ul class=" clearfix" id="dropzone_imgs_outer_ul0" style="padding-left: 0px;"></ul>

                                        
                                      <div class="form-row">  
                                        <div class='form-group col-sm-6'>
                                          <button class='but_all btn_rmv removeVariant' type='button' style="display:none">
                                              <svg id='delete-minor' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>
                                                  <path d='M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z'></path>
                                              </svg>
                                          </button>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-row" style="display: none;">
                                      <div class="form-group col-sm-12">
                                        <button class="but_all" type="button" id="add_more_varient">Add more variants</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          
                          
                          <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Search engine listing preview</h6>
                                    <button class="txt_btn fl_left" id="tgl2_div1">Edit website SEO</button>
                                    <button class="txt_btn fl_left txt_btn_hide" id="tgl2_div2">close</button>
                                </header>
                                <div class="form-group">
                                    <p>Add a title and description to see how this product might appear in a search engine listing.</p>
                                </div>
                            </div>
                            <div class="dropdown_div btm_border_cst order_details clearfix" id="">
                                <div class="row  ">
                                    <div class="form-group col-sm-12">
                                        <label for="inpState5" class="ad_prd_lbl">Page title</label>
                                        <span class="fl_left">0 of 70 characters used</span>
                                        <input type="text" class="form-control" id="inpState5" placeholder="" value="Sadie">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inpStatetxt5" class="ad_prd_lbl">Meta description</label>
                                        <span class="fl_left">0 of 320 characters used</span>
                                        <textarea class="form-control" id="inpStatetxt5" placeholder="" value="Sadie"></textarea>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inpStateurl5" class="ad_prd_lbl">URL and handle</label>
                                        <input type="text" class="form-control" id="inpStateurl5" placeholder="http://new.aliasmae.com/" value="http://new.aliasmae.com/Sadie">
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                  
                </div>
               
                <div class="row">
                    <div class="save_but">
                        <!-- <button class="mailbut_all float-right disabled" type="button" disabled="disabled">Save product</button> -->
                        <button class="mailbut_all float-right" type="button" id="saveeditproduct">Save product</button>

                        <button type="button" id="cancel_update" class=" but_all  float-right">Cancel</button>
                    </div>
                </div>
        </div>

            </div>

        <!-- ############ Main END-->
          <!----------------- modal to add image from url ---------------->
        
        <div class="modal fade" id="manage_upload_img" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Add image from URL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-row">
                  <div class="form-group col-sm-12">
                        <label class="ad_prd_lbl">Paste image URL</label>
                        <input type="text" class="form-control" id="frm_eip1" placeholder="http://">
                        <span id="frm_eip1_error" style="color:red"></span>
                    </div>
                </div>
              </div>
              <div class="modal-footer pop_foot_but">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="addImageUrl">Add image</button>
              </div>
            </div>
          </div>
        </div>
        <!--end-->   
        <!-- include summernote js -->
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') !!}

            <!-- <script src="http://new.aliasmae.com/scripts/datepicker.min.js"></script> -->
        <!-- include dropzone js -->
            {!! HTML::script('developerJS/editproduct.js') !!}

            {!! HTML::script('scripts/plugins/bootstrap-tagsinput.min.js') !!}
        <!-- include dropzone js -->
            <!-- <script type="text/javascript" src="scripts/plugins/dropzone.min.js"></script>
            <script src="scripts/plugins/bootstrap-tagsinput.min.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<!-- Fetch category details by product type -->
<script type="text/javascript">
$(document).ready(function(){
 
  var producttype = $("#producttype").val();//:selected

  fetchcategory(producttype);
   fetchcolour(producttype);
  fetchmaterial(producttype);
  fetchSize(producttype);
  //alert(var_count);
});

  $("#producttype").change(function(){
  var producttype = $("#producttype").val();//:selected
  fetchcategory(producttype);
  fetchcolour(producttype);
  fetchmaterial(producttype);
  fetchSize(producttype);
  //$("#StoreSizeHtml0").html();
  //alert(var_count);
});
// ==== Fetch category details by product type 
function fetchcategory(producttype)
{
  //alert(producttype);
  var urldata_catDta = "{!! url('/') !!}/oms/fetchCategoryDta";
  $.ajax({
            url : urldata_catDta,
            type: 'POST',
            data : {'producttype':producttype},
            async: false,
            }).done(function(response){
               $("#category").html(response);
               $("#subCategory").html(response);
            //}
          }) 
}

// ==== Fetch category details by product type 
// ==== Fetch colour details by product type 
function fetchcolour(producttype)
{
  //alert(producttype);
  var urldata_catDta = "{!! url('/') !!}/oms/fetchColourDta";
  $.ajax({
            url : urldata_catDta,
            type: 'POST',
            data : {'producttype':producttype},
            async: false,
            }).done(function(response){
               console.log(response);

               var var_countVal = var_count;
               console.log(var_countVal);
                $("#colour_"+var_countVal).html(response);
               
            //}
          }) 
}

// ==== Fetch colour details by product type 
// ==== Fetch material details by product type 
function fetchmaterial(producttype)
{
  //alert(producttype);
  var urldata_catDta = "{!! url('/') !!}/oms/fetchMaterialDta";
  $.ajax({
            url : urldata_catDta,
            type: 'POST',
            data : {'producttype':producttype},
            async: false,
            }).done(function(response){
               
              var var_countVal = var_count;
                $("#material"+var_countVal).html(response);
               
            //}
          }) 
}

// ==== Fetch material details by product type 
//======== fetch size details by product type ====
function fetchSize(producttype){

  var urldata_catDta = "{!! url('/') !!}/oms/fetchSizeDta";
  $.ajax({
            url : urldata_catDta,
            type: 'POST',
            data : {'producttype':producttype},
            async: false,
            }).done(function(response){
               
              var var_countVal = var_count;
                $("#size"+var_countVal).html(response);
               
            //}
          }) 

}

//======== fetch size details by product type ====
</script>

<script>
  var_count = 0;
  image_count = 0;
  image_url = 0;
  currect_imgurl = 0;
$(document).ready(function(){
 
  var storevalue = $("#store").val();//:selected
  //alert('storeId='+storevalue);
  setFootSize(storevalue);

  setTimeout(function(){

    $("#startDate").val('');

  },2000)

  setTimeout(function(){

    $("#endDate").val('');
    
  },2000)
  
});

$('#store').change(function(){
  var storevalue = $("#store").val();
  setFootSize(storevalue);
});

function setFootSize(storevalue)
{
        var urldata_shoesize = "{!! url('/') !!}/oms/fetchStoreShoeSize";
//alert(storevalue+'===='+urldata_shoesize);
            $.ajax({
            url : urldata_shoesize,
            type: 'POST',
            data : {'shoevalue':storevalue,'varcount':var_count},
            async: false,
            }).done(function(response){
            // console.log('here'+response);
            //location.reload();
            //alert(response);
            //$("#selectstoreSize").css("display","block");
            //$("#ShoeSizeHtml").html(response);
            /*if(var_count == 1)
            {
              $("#StoreSizeHtml0").html(response);
            
            }
            else{*/
              $("#StoreSizeHtml"+var_count).html(response);
            //}
          }) 
}



</script>
