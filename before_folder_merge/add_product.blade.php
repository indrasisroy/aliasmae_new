<!-- include summernote css -->
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css') !!}
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css') !!}
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css') !!}
{!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') !!}
<!-- include dropzone js -->
{!! HTML::style('assets/css/dropzone.min.css') !!}
{!! HTML::style('assets/css/tagsinput.css') !!}

    <!-- Main -->
    <div class="content-main staic_table_custom" id="content-main">
        <!-- ############ Main START-->
        <div class="padding">
            <div class="container">
            <div class="clearfix top_export_sec">
                <div class="clearfix breadcram">
                    <a href="{!! url('/') !!}/oms/product/product-list"><i class="fa fa-angle-left"></i>Product Summary</a>
                </div>
                <div class="left_head">
                    <h2>Add Product</h2>
                </div>
            </div>
              
              {!! Form::open(array('aria-labelledby' => 'formlabel', 'id' =>'add_product_data', 'class' => 'form-horizontal', 'url' => '/oms/product/add_product_data','files' => true, 'enctype'=>'multipart/form-data')) !!} 
              <div id='all_img_div' style="display:none">
                <textarea name="body_html" id="body_html"></textarea>
                <input type="file" class='all_product_file' name="all_file0[]" id="all_file0_0" accept="image/*" style="display:none" >
              </div>
                <div class="row">
                  
                    <div class="col-sm-12 "><!-- cbd_main_content -->

                        <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Add a New Product</h6>
                                </header>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                      <label for="usr5" class="ad_prd_lbl ">Brand *</label>
                                      <select id="subBrand" name="subBrand" class="custom-select w-100 product_need" autocomplete="off" label="Brand"  >
                                        <option value="">Select one...</option>
                                         @if(!empty($data['brandlist']))
                                          @foreach($data['brandlist'] as $brand)
                                              <option value="{!! $brand['brandId'] !!}">{!! ucfirst($brand['brandName']) !!}</option>
                                          @endforeach
                                        @endif
                                      </select>
                                      <span id="subBrand_error" style="color:red"></span>
                                    </div>
                                
                                   <div class='form-group col-sm-6'>
                                    <label for='sku5' class='ad_prd_lbl'>Product Type *</label>
                                    <select id='producttype' name='producttype' class='custom-select w-100 product_need' autocomplete='off' label='Product Type' >
                                     @if(!empty($data['ProductType']))
                                          @foreach($data['ProductType'] as $Producttype)
                                              <option value="{!! $Producttype['producttypeid'] !!}">{!! ucfirst($Producttype['producttypeName']) !!}</option>
                                          @endforeach
                                        @endif
                                    
                                    </select>
                                    <span id="producttype_error" style="color:red"></span>
                                  </div>
                              </div>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl">Product Name *</label>  
                                    <input class="form-control need product_need" id="styleName" placeholder="Product Name" type="text" name="styleName" label="Style Name"/>
                                    <span id="styleName_error" style="color:red"></span>
                                  </div>
                                  <div class="form-group col-md-6">
                                      <label for="usr5" class="ad_prd_lbl">Factory ID *</label>
                                     <input class="form-control need product_need" id="factoryId" placeholder="Factory Id" type="text" name="factoryId" style="text-transform:uppercase" label="Factory ID"/>
                                       <span id="factoryId_error" style="color:red"></span>
                                  </div>
                                </div>
                                <div class="form-row">
                                  
                                  <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl ">Category 1*</label>
                                    <select id="category" name="category" class="custom-select w-100 need product_need" autocomplete="off" label="Category 1"  >
                                      <option value="">Select one...</option>
                                      @if(!empty($data['categorylist']))
                                        @foreach($data['categorylist'] as $category)
                                            <option value="{!! $category['CategoryID'] !!}">{!! ucfirst($category['CategoryName']) !!}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                    <span id="category_error" style="color:red"></span>
                                  </div>
                                   <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl ">Category 2 </label>
                                    <select id="subCategory" name="subCategory" class="custom-select w-100 " autocomplete="off" label="Sub Category 2"  >
                                      <option value="">Select one...</option>
                                      @if(!empty($data['categorylist']))
                                        @foreach($data['categorylist'] as $category)
                                        <option value="{!! $category['CategoryID'] !!}">{!! ucfirst($category['CategoryName']) !!}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                    <span id="subCategory_error" style="color:red"></span>
                                  </div>
                                </div>
                                <div class="form-row">
                                 <div class="form-group col-md-6">
                                    <label for="usr5" class="ad_prd_lbl">Season *</label>
                                    <select id="season" name="season" class="custom-select w-100 need product_need" autocomplete="off" label="Season" >
                                     <option value="">Select one...</option>
                                      @if(!empty($data['season']))
                                        @foreach($data['season'] as $seasonData)
                                            <option value="{!! $seasonData['Id'] !!}">{!! ucfirst($seasonData['Name']) !!}</option>
                                        @endforeach
                                      @endif
                                      <!-- <option value="Summer">Summer</option>
                                      <option value="Winter">Winter</option> -->
                                    </select>
                                    <span id="season_error" style="color:red"></span>
                                  </div>                                  
                                </div> 
                                <div class="form-row">
                                  <div class="form-group col-md-12">
                                      <label for="prz5" class="ad_prd_lbl">Description *</label>
                                      <div class="browse_field" id="summernote1_form">
                                          <div id="summernote1" class="summernote " name="productDesc"></div>
                                      </div>
                                      <span id="summernote1_error" style="color:red"></span>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                          <div class="order_details">
                            <header class="prd_hdr clearfix">
                              <h6>Display Properties</h6>
                            </header>
                              <div class="form-row">
                              <div class="form-group col-md-2">
                                  <label for="usr5" class="ad_prd_lbl">Is Active </label>
                                  <input type="checkbox" class="col-md-1" id="IsActive" name="IsActive" label="Is Active" autocomplete="off" value='1'>
                                   <span id="IsActive_error" style="color:red"></span>
                              </div>
                               <div class="form-group col-md-2">
                                  <label for="usr5" class="ad_prd_lbl">Is Pre-Order </label>
                                  <input type="checkbox" class="col-md-1" id="IsPreOrder" name="IsPreOrder" label="Is Active" autocomplete="off" value='1'>
                                   <span id="IsActive_error" style="color:red"></span>
                              </div>
                              <div class="form-group col-md-2">
                                  <label for="usr5" class="ad_prd_lbl">Is New Arrival </label>
                                  <input type="checkbox" class="col-md-1" id="IsNew" name="IsNew" label="Is New Arrival" autocomplete="off" value='1'>
                                   <span id="IsActive_error" style="color:red"></span>
                              </div>
                              <!-- <div class="form-group col-md-4">
                                  <label for="usr5" class="ad_prd_lbl">Is On Sale </label>
                                  <input type="checkbox" class="col-md-1" id="IsSale" name="IsSale" label="Is On Sale" autocomplete="off" value='1'>
                                   <span id="IsActive_error" style="color:red"></span>
                              </div> -->
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="usr5" class="ad_prd_lbl">Display Priority * (Note: should be a number e.g 100)</label>
                                <input type="text" class="form-control isNumberKey need product_need" id="displayOrder" name="displayOrder" value="" label="Display Order" placeholder="" autocomplete="off">
                                <span id="displayOrder_error" style="color:red"></span>
                              </div>
                             <!--  <div class="form-group col-md-6">
                                <label for="prz5" class="ad_prd_lbl col-md-6">Store *</label>
                                <select id="store" name="store[]" class="custom-select w-100 need" autocomplete="off" label="Store" multiple >
                                    @if(!empty($data['store']))
                                      @foreach($data['store'] as $store)
                                        <option value="{!! $store['StoreId'] !!}" @if(!empty($selectStoreId) && ($store['StoreId'] == $selectStoreId)){!! 'selected' !!}@endif>{!! $store['StoreName'].' ('.$store['CountryName'].')' !!}</option>
                                      @endforeach
                                    @endif
                                </select>
                                <span id="store_error" style="color:red"></span>
                              </div> -->
                            </div>
                           <!--  <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="usr5" class="ad_prd_lbl ">Start Date *(Product Display Start date)</label>
                                <input type='text' id="startDate" name="startDate" class="form-control date need" label="Start Date" autocomplete="off" readonly/>
                                <span id="startDate_error" style="color:red"></span>
                              </div>
                              <div class="form-group col-md-6">
                                <label for="usr5" class="ad_prd_lbl ">End Date *(Product Display End date)</label>
                                <input type='text' id="endDate" name="endDate" class="form-control date need" label="End Date" autocomplete="off" readonly/>
                                <span id="endDate_error" style="color:red"></span>
                              </div>
                            </div> -->
                          
                            </div>
                        </div>
                        <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Pricing</h6>
                                </header>
                                <div class="form-row">
                                    
                                    <!-- @if(!empty($data['store']))
                                      @foreach($data['store'] as $store)
                                    <div class="form-group col-md-6">
                                        <label for="prz5" class="ad_prd_lbl"><?php echo $store['CurrencyCode'].' Retail Price'?></label>
                                        <input type="text" class="form-control isNumberKey" id="{!! $store['CurrencyCode'].'Retail Price' !!}" placeholder="AUD 0.00">
                                    </div>
                                      @endforeach
                                    @endif -->
                                    <div class="form-group col-md-4">
                                        <label for="prz5" class="ad_prd_lbl"> Retail Price (AUD)</label>
                                        <input type="text" class="form-control isNumberKey" id="Retail Price" placeholder="0.00">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="prz5" class="ad_prd_lbl"  style="width:100%">
                                          <span>Discounted Retail Price (AUD) </span> <div style="float:right"> Enable <input type="checkbox"  id="IsSale" name="IsSale" label="Is On Sale" autocomplete="off" value='1'></div>
                                          <span></span>
                                          <span></span>
                                        </label>
                                        <input type="text" class="form-control isNumberKey" id="prz5" placeholder="0.00">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="comp_prz5" class="ad_prd_lbl">Cost price (AUD)</label>
                                        <input type="text" class="form-control isNumberKey" id="comp_prz5" placeholder="0.00">
                                    </div>
                                </div>
                               <!--  <div class="form-row">
                                    
                                </div> -->
                            </div>
                        </div>


                        <div class="box">
                            <div class="order_details">
                              <header class="prd_hdr clearfix">
                                <h6>Variants</h6>
                                  <!-- <button class="txt_btn fl_left" id="tgl_div1">Add variant</button> -->
                                    <a href="javascript:void(0)" class="txt_btn fl_left" id="tgl_div1">Add variant</a>
                                    <a href="javascript:void(0)" class="txt_btn fl_left txt_btn_hide" id="tgl_div2">Close</a>

                                  <!-- <input type="submit" class="txt_btn fl_left" id="tgl_div1" value="Add variant"> -->
                                <!-- <button class="txt_btn fl_left txt_btn_hide" id="tgl_div2">close</button> -->
                              </header>
                              <div class="form-group">
                                <p>Add variants if this product comes in multiple versions, like different colors.</p>
                              </div>
                            </div>
                            <div class="clearfix dropdown_div">
                              <div class="order_details gry_of_bg">
                                <div class="clearfix form-result_outer" id="form-result_cst">
                                  <div class="prd_hdr clearfix">
                                    <h4 class="ad_prd_lbl">Modify the variants to be created:</h4>
                                  </div>
                                  <div class="cbd_table_outer custmclstableouter" >
                                    <div id="allVariant" class="advariant">
                                      <div class="form-row ">
                                        <div class="form-group col-sm-2">
                                          <label for="sku5" class="ad_prd_lbl">Barcode *</label>
                                          <input type="text" id="barcode" class="form-control w-100 varneed minthirteen isNumberKey product_need" name="barcode[]" value="" label="Barcode">
                                          <span id="barcode_error" style="color:red"></span>
                                        </div>
                                        <div class="form-group col-sm-2">
                                          <label for="sku5" class="ad_prd_lbl">Colour 1 *</label>
                                          <select id="colour_0" name="colour[]" class="custom-select w-100 varneed product_need" autocomplete="off" label="Colour 1" >
                                             @if(!empty($data['variants_colour']))
                                                @foreach($data['variants_colour'] as $colour)
                                                <option value="{!! $colour['Id'] !!}">{!! $colour['Name'] !!}</option>
                                                @endforeach
                                              @endif
                                           
                                          </select>
                                          <span id="colour_0_error" style="color:red"></span>
                                        </div>
                                        <div class="form-group col-sm-2">
                                          <label for="sku5" class="ad_prd_lbl">Colour 2</label>
                                          <select id="colour2_0" name="colour2[]" class="custom-select w-100 " autocomplete="off" label="Colour 2" >
                                             @if(!empty($data['variants_colour']))
                                                @foreach($data['variants_colour'] as $colour)
                                                <option value="{!! $colour['Id'] !!}">{!! $colour['Name'] !!}</option>
                                                @endforeach
                                              @endif
                                           
                                          </select>
                                        </div>
                                       
                                        <div class="form-group col-sm-2">
                                          <label for="sku5" class="ad_prd_lbl">Material 1 *</label>
                                          <select id="material0" name="material[]" class="custom-select w-100 varneed product_need" autocomplete="off" label="Material 1" >
                                            @if(!empty($data['variants_materials']))
                                                @foreach($data['variants_materials'] as $material)
                                                <option value="{!! $material['Id'] !!}">{!! $material['Name'] !!}</option>
                                                @endforeach
                                            @endif                                          
                                          </select>
                                          <span id="material0_error" style="color:red"></span>
                                        </div>
                                        <div class="form-group col-sm-2">
                                          <label for="sku5" class="ad_prd_lbl">Material 2</label>
                                          <select id="material20" name="material2[]" class="custom-select w-100 " autocomplete="off" label="Material 2" >
                                            @if(!empty($data['variants_materials']))
                                                @foreach($data['variants_materials'] as $material)
                                                <option value="{!! $material['Id'] !!}">{!! $material['Name'] !!}</option>
                                                @endforeach
                                            @endif
                                          
                                          </select>
                                        </div>
                                        <div class="form-group col-sm-2">
                                          <label for="sku5" class="ad_prd_lbl">Size*</label>
                                          <select id="size0" name="size[]" class="custom-select w-100 varneed product_need" autocomplete="off" label="Size" >
                                           <!--  @if(!empty($data['variants_size']))
                                                @foreach($data['variants_size'] as $vsize)
                                                <option value="{!! $vsize['Id'] !!}">{!! $vsize['Name'] !!}</option>
                                                @endforeach
                                            @endif -->
                                          
                                          </select>
                                          <span id="size0_error" style="color:red"></span>
                                        </div>
                                        <div class="form-group col-sm-1" style="display:none">
                                          <label for="sku5" class="ad_prd_lbl">Size(USA)*</label>
                                          <select id="size20" name="size2[]" class="custom-select w-100 " autocomplete="off" label="Size" >
                                           <!--  @if(!empty($data['variants_size']))
                                                @foreach($data['variants_size'] as $vsize)
                                                <option value="{!! $vsize['Id'] !!}">{!! $vsize['Name'] !!}</option>
                                                @endforeach
                                            @endif -->
                                          
                                          </select>
                                        </div>
                                      </div>
                                      
                                      <div class="form-row">
                                        <div class="form-group col-sm-8">
                                          <label for="sku5" class="ad_prd_lbl">Image :</label>
                                        
                                          <!-- <a href="javascript:void(0)" class="dropzone_btn" data-toggle="modal" data-target="#manage_upload_img">Add image from URL</a> -->
                                          <a href="javascript:void(0)" class="dropzone_btn by_file" id="by_file0" onclick="UploadImage(0)">Upload image</a>
                                        </div>
                                       <!--  <div class="form-group col-sm-4">
                                          <label for="usr5" class="ad_prd_lbl">Is Active </label>
                                          <input type="checkbox" class="col-md-1 " id="IsActive0" name="IsActive[]" label="Is Active" autocomplete="off" value='1'>
                                        </div> -->
                                      </div>
                                        
                                        <ul class=" clearfix" id="dropzone_imgs_outer_ul0" style="padding-left: 0px;"></ul>

                                        
                                      <div class="form-row">  
                                        <div class='form-group col-sm-6'>
                                          <a href="javascript:void(0)" class='but_all btn_rmv removeVariant' id="removeVariant0" style="display:none"> 
                                              <svg id='delete-minor' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'>
                                                  <path d='M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z'></path>
                                              </svg>
                                          </a>                                         
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-row">
                                      <div class="form-group col-sm-12">
                                        <!-- <button class="but_all" type="button" id="add_more_varient">Add more variants</button> -->
                                        <a href="javascript:void(0)" class="but_all"  id="add_more_varient"> Add more variants </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          
                          
                          <div class="box">
                            <div class="order_details">
                                <header class="prd_hdr clearfix">
                                    <h6>Search engine listing preview</h6>
                                    <!-- <button class="txt_btn fl_left" id="tgl2_div1">Edit website SEO</button>
                                    <button class="txt_btn fl_left txt_btn_hide" id="tgl2_div2">close</button> -->
                                    <a href="javascript:void(0)" class="txt_btn fl_left" id="tgl2_div1">Edit website SEO</a>
                                    <a href="javascript:void(0)" class="txt_btn fl_left txt_btn_hide" id="tgl2_div2">Close</a>

                                </header>
                                <div class="form-group">
                                    <p>Add a title and description to see how this product might appear in a search engine listing.</p>
                                </div>
                            </div>
                            <div class="dropdown_div btm_border_cst order_details clearfix" id="">
                                <div class="row  ">
                                    <div class="form-group col-sm-12">
                                        <label for="inpState5" class="ad_prd_lbl">Page title</label>
                                        <span class="fl_left">0 of 70 characters used</span>
                                        <input type="text" class="form-control" id="inpState5" placeholder="">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inpStatetxt5" class="ad_prd_lbl">Meta description</label>
                                        <span class="fl_left">0 of 320 characters used</span>
                                        <textarea class="form-control" id="inpStatetxt5" placeholder=""></textarea>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inpStateurl5" class="ad_prd_lbl">URL and handle</label>
                                        <input type="text" class="form-control" id="inpStateurl5" placeholder="http://new.aliasmae.com/">
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>

                </div>
               
                <div class="row">
                    <div class="save_but">
                        <!-- <button class="mailbut_all float-right disabled" type="button" disabled="disabled">Save product</button> -->
                        <button class="mailbut_all float-right" type="button" id="saveproduct">Save product</button>

                        <button type="button" id="cancel_update" class=" but_all  float-right">Cancel</button>
                    </div>
                </div>
          {!!Form::close()!!} 
        </div>

            </div>

        <!-- ############ Main END-->
          <!----------------- modal to add image from url ---------------->
        
        <div class="modal fade" id="manage_upload_img" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Add image from URL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-row">
                  <div class="form-group col-sm-12">
                        <label class="ad_prd_lbl">Paste image URL</label>
                        <input type="text" class="form-control" id="frm_eip1" placeholder="http://">
                        <span id="frm_eip1_error" style="color:red"></span>
                    </div>
                </div>
              </div>
              <div class="modal-footer pop_foot_but">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="addImageUrl">Add image</button>
              </div>
            </div>
          </div>
        </div>
        <!--end-->   
        <!-- include summernote js -->
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js') !!}
            {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') !!}

            <!-- <script src="http://new.aliasmae.com/scripts/datepicker.min.js"></script> -->
        <!-- include dropzone js -->
            <!--{!! HTML::script('scripts/plugins/dropzone.min.js') !!}-->

            {!! HTML::script('scripts/plugins/bootstrap-tagsinput.min.js') !!}
        <!-- include dropzone js -->
            <!-- <script type="text/javascript" src="scripts/plugins/dropzone.min.js"></script>
            <script src="scripts/plugins/bootstrap-tagsinput.min.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<!-- Fetch category details by product type -->
<script type="text/javascript">
$(document).ready(function(){
 
  var producttype = $("#producttype").val();//:selected


  fetchcategory(producttype);
   fetchcolour(producttype);
  fetchmaterial(producttype);
  fetchSize(producttype);
  //alert(var_count);
});

  $("#producttype").change(function(){

//=================== product type change remove all previous variant =======
      $( ".advariant" ).children().remove(  );
      $("#add_more_varient").click();
//=================== product type change remove all previous variant =======

  var producttype = $("#producttype").val();//:selected
  fetchcategory(producttype);
  fetchcolour(producttype);
  fetchmaterial(producttype);
  fetchSize(producttype);
  //$("#StoreSizeHtml0").html();
  //alert(var_count);
});
// ==== Fetch category details by product type 
function fetchcategory(producttype)
{
  //alert(producttype);
  var urldata_catDta = "{!! url('/') !!}/oms/fetchCategoryDta";
  $.ajax({
            url : urldata_catDta,
            type: 'POST',
            data : {'producttype':producttype},
            async: false,
            }).done(function(response){
               $("#category").html(response);
               $("#subCategory").html(response);
            //}
          }) 
}

// ==== Fetch category details by product type 
// ==== Fetch colour details by product type 
function fetchcolour(producttype)
{
  //alert(producttype);
  var urldata_catDta = "{!! url('/') !!}/oms/fetchColourDta";
  $.ajax({
            url : urldata_catDta,
            type: 'POST',
            data : {'producttype':producttype},
            async: false,
            }).done(function(response){
               console.log(response);

               var var_countVal = var_count;
               console.log(var_countVal);
                $("#colour_"+var_countVal).html(response);
                $("#colour2_"+var_countVal).html(response);
            //}
          }) 
}

// ==== Fetch colour details by product type 
// ==== Fetch material details by product type 
function fetchmaterial(producttype)
{
  //alert(producttype);
  var urldata_catDta = "{!! url('/') !!}/oms/fetchMaterialDta";
  $.ajax({
            url : urldata_catDta,
            type: 'POST',
            data : {'producttype':producttype},
            async: false,
            }).done(function(response){
               
              var var_countVal = var_count;
                $("#material"+var_countVal).html(response);
               $("#material2"+var_countVal).html(response);
            //}
          }) 
}

// ==== Fetch material details by product type 
//======== fetch size details by product type ====
function fetchSize(producttype){

  //============fetch eu size ========
  var urldata_catDta = "{!! url('/') !!}/oms/fetchSizeDtaEU";
  $.ajax({
            url : urldata_catDta,
            type: 'POST',
            data : {'producttype':producttype},
            async: false,
            }).done(function(response){
               
              var var_countVal = var_count;
                $("#size"+var_countVal).html(response);
                //$("#size2"+var_countVal).html(response);
            //}
          }) 

   //============fetch usa size ========         
   var urldata_catDta = "{!! url('/') !!}/oms/fetchSizeDtaUSA";
  $.ajax({
            url : urldata_catDta,
            type: 'POST',
            data : {'producttype':producttype},
            async: false,
            }).done(function(response){
               
              var var_countVal = var_count;
                //$("#size"+var_countVal).html(response);
                $("#size2"+var_countVal).html(response);
            //}
          }) 


}

//======== fetch size details by product type ====
</script>

<script>
  var_count = 0;
  image_count = 0;
  image_url = 0;
  currect_imgurl = 0;
$(document).ready(function(){
 
  var storevalue = $("#store").val();//:selected
  //alert('storeId='+storevalue);
  setFootSize(storevalue);
});

$('#store').change(function(){
  var storevalue = $("#store").val();
  setFootSize(storevalue);
});


function setFootSize(storevalue)
{
        var urldata_shoesize = "{!! url('/') !!}/oms/fetchStoreShoeSize";
//alert(storevalue+'===='+urldata_shoesize);
            $.ajax({
            url : urldata_shoesize,
            type: 'POST',
            data : {'shoevalue':storevalue,'varcount':var_count},
            async: false,
            }).done(function(response){
            // console.log('here'+response);
            //location.reload();
            //alert(response);
            //$("#selectstoreSize").css("display","block");
            //$("#ShoeSizeHtml").html(response);
            /*if(var_count == 1)
            {
              $("#StoreSizeHtml0").html(response);
            
            }
            else{*/
              $("#StoreSizeHtml"+var_count).html(response);
            //}
          }) 
}
</script>
