<?php

namespace App\Http\Controllers\site_controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
//use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\site_controller\MyController as MyController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Pagination\Paginator;  /*for pagination*/
use Illuminate\Support\Facades\DB;
use App\Mail\sendMyMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;  /*for sending mail*/
use Redirect;
use Session;


class StoreManager extends MyController
{
    
    //===================== LOAD ALL STORE LISTING ===================//
    public function index()
    {
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        if($userId != '')
        {
            $page='';
			$lastRow='';
			$limit=10;
			
			/*if(isset($_REQUEST['page']) && $_REQUEST['page'] != '')
			{
				$page=$_REQUEST['page'];
			}
			
            if($page == '' || $page == 0)
			{
				$page=1;
			}
			$lastRow = ($page-1) * $limit;*/

            // echo 
            
            //======== GET STORE LIST FROM API =========//
            $curlUrl = 'api/StoreManager';
            $curlPostData = array(
                            'mode' => 'fetch_store_ManagerList',
                            'lastrow' => $lastRow,
                            'flag' => 1,
                            'userId' => $userId,
                            'StoreManagerId'=>''
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
			$store = $this->fireCurl($url,$curlPostData);
            $store_array = json_decode($store,1);
			/* echo "<pre>";
           print_r($store_array);die;*/
            if(!empty($store_array) && $store_array['status'] == 1)
            {
                $data['store-manager'] = $store_array['data']['store-manager'];
				$hasMore=$store_array['data']['pagination']['hasMore'];
				
				$data['pagination']=$store_array['data']['pagination']['totalpage'];
				$numOfTotal=$store_array['data']['pagination']['totaldata'];
				
				$pegi= $this->myPagination($numOfTotal,$limit,$page,$url='?',"");
				$data['pegi']=html_entity_decode($pegi);
                
            }
            else
            {
                $data['store-manager'] = array();
                $data['pegi']='';
                $data['pagination']=array();
                
            }
            
            // print_r($data);
            
            $data['title'] = 'Store Manager';
            $data['viewPage'] = 'control/storemanager/store_manager_list';
            return view('control/includes/master_view',compact('data'));
        }
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
    }
    
    //================ LOAD ADD NEW STORE MANAGER PAGE =======================//
    public function addStoreManager()
    {
    	$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        if($userId != '')
        {
        	$lastRow='';
        //================= get all pages =============//
        //$pages = $this->getAllData('page_management',array('status'=>'1'),'page_name','asc');
        //$data['pages'] = $pages;
        //======== GET Country LIST FROM API =========//
            $curlUrl = 'api/StoreManagement';
            $curlPostData = array(
                            'mode' => 'fetch_country_details',
                            'lastrow' => $lastRow,
                            'userId' => $userId,
                            'countryid'=>'',
                            'flag' =>1
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
			$country = $this->fireCurl($url,$curlPostData);
            $country_array = json_decode($country,1);
            if(!empty($country_array) && $country_array['status'] == 1)
            {
            	$data['CountryDetails'] = $country_array['data']['country'];
            }
            else{
                $data['CountryDetails'] =array();
            }
            //======== GET STORE LIST FROM API =========//
            $curlUrl = 'api/StoreManagement';
            $curlPostData = array(
                            'mode' => 'fetch_store',
                            'lastrow' => $lastRow,
                            'flag' => 1,
                            'userId' => $userId,
                            'storeid'=>''
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
			$store = $this->fireCurl($url,$curlPostData);
            $store_array = json_decode($store,1);
			 //echo "<pre>";
           //print_r($store_array);die;
            if(!empty($store_array) && $store_array['status'] == 1)
            {
                $data['store'] = $store_array['data']['store'];
            }else{
            	$data['store'] = array();
            }
            //========= FETCH ADMIN MANAGEMENT LIST ========//
              $curlUrl = 'api/StoreManager';
            $curlPostData = array(
                            'mode' => 'fetch_AdminManagement_List', 
                            'userId' => $userId                            
                            );
            $url = $baseUrl.$curlUrl;
			$adminmanagementData = $this->fireCurl($url,$curlPostData);
            $adminmanagementData_array = json_decode($adminmanagementData,1);
			 //echo "<pre>";
           //print_r($adminmanagementData_array);die;
            if(!empty($adminmanagementData_array) && $adminmanagementData_array['status'] == 1)
            {
                $data['adminmanagement'] = $adminmanagementData_array['data']['Admin_Management'];
            }else{
            	$data['adminmanagement'] = array();
            }


        $data['title'] = 'add-storemanager';
        $data['viewPage'] = 'control/storemanager/add_storemanager';
        return view('control/includes/master_view',compact('data'));
    	}
    	else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }

    }
	
	//================================insert store manager details===========================//
	public function storemanagerInsert(Request $request)
	{
		
		$firstName=$request->post('firstName');
		$lastName=$request->post('lastName');
		$email=$request->post('email');
		$password=$request->post('password');
		$username=$request->post('username');
		$IsActive=$request->post('IsActive');
		$storeaccess=$request->post('storeaccess');

		$rolesVal = $request->post('roles');
		$pageaccess = $request->post('pageaccess');

		if($rolesVal == ''){
			$roles = '0';
		}else{
			$roles = $rolesVal;
		}

		if(!empty($pageaccess))
		{
			$strpageAccess=implode(',', $pageaccess);
		}
		else{
			$strpageAccess='';
		}

		if(!empty($storeaccess))
		{
			$strAccess=implode(',', $storeaccess);
		}
		else{
			$strAccess='';
		}

		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== insert store =========//
		$curlUrl = 'api/StoreManager';
		//echo "<pre>";echo $userId.'=='.$storeName.'=='.$storeLocation.'=='.$country.'=='.$IsActiveVal.'=='.$IsActive;//die();
		// echo "<pre>";print_r($_FILES);die();

		if($userId !='' && $firstName!='' && $lastName!='' && $email!='' && $password!='' && $username!='' && $IsActiveVal!='')
		{	
			$curlPostData = array(
						'mode' => 'Add_new_store_manager',
						'userId' =>$userId,
						'ManagerFirstName' =>$firstName,
						'ManagerLastName' =>$lastName,
						'ManagerEmail' =>$email,
						'ManagerPassword' =>$password,
						'ManagerAccessStore' =>$strAccess,
						'UserName' =>$username,
						'status' =>$IsActiveVal,
						'pageaccess'=>$strpageAccess,
						'roles'=>$roles
						);
			$url = $baseUrl.$curlUrl;
				
			// echo "<pre>";
			// print_r($curlPostData);
			// die();
				$store = $this->fireCurl($url,$curlPostData);
				$store_array = json_decode($store,1);
				//echo "<pre>";
			//print_r($store_array);
			//die();
				if($store_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$store_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$store_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/storemanager/store-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/storemanager/store-list');
		}
		
		
	}
	
	//================================change status===================================//
	public function changeStatus(Request $request)
	{
		$storeid=$request->segment(4);
		$status=$request->segment(5);
		
		$whereArr=array('StoreManagerId'=>$storeid);
		$updatearr=array('status'=>$status);
		
        $updatebannerDetails=$this->update('StoreManager',$whereArr,$updatearr);
		
		$request->session()->put('success_msg','Status updated successfully!');
		
		 return Redirect::to('/oms/storemanager/store-list');
	}
	
	//==============load store manager details edit page=========//
	
	public function editStoreManager(Request $request)
	{
		//echo $id;die();
		$storeid=base64_decode($request->segment(4));
    	$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
        if($userId != '')
        {
        	$lastRow='';
        //================= get all pages =============//
        //$pages = $this->getAllData('page_management',array('status'=>'1'),'page_name','asc');
        //$data['pages'] = $pages;
        //======== GET STORE LIST FROM API =========//
            $curlUrl = 'api/StoreManagement';
            $curlPostData = array(
                            'mode' => 'fetch_country_details',
                            'lastrow' => $lastRow,
                            'userId' => $userId,
                            'countryid'=>'',
                            'flag' =>1
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
			$country = $this->fireCurl($url,$curlPostData);
            $country_array = json_decode($country,1);
            if(!empty($country_array) && $country_array['status'] == 1)
            {
            	$data['CountryDetails'] = $country_array['data']['country'];
            }
            else{

                $data['CountryDetails'] =array();
            }
             //======== GET STORE DETAILS FROM API =========//
            $curlUrl = 'api/StoreManagement';
            $curlPostData = array(
                            'mode' => 'fetch_store',
                            'lastrow' => $lastRow,
                            'flag' => 1,
                            'userId' => $userId,
                            'storeid'=>''
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
			$store = $this->fireCurl($url,$curlPostData);
            $store_array = json_decode($store,1);
			 //echo "<pre>";
           //print_r($store_array);die;
            if(!empty($store_array) && $store_array['status'] == 1)
            {
                $data['store'] = $store_array['data']['store'];
            }else{
            	$data['store'] =array();
            }
            //======== GET STORE LIST FROM API =========//
            $curlUrl = 'api/StoreManager';
            $curlPostData = array(
                            'mode' => 'fetch_store_ManagerList',
                            'lastrow' => $lastRow,
                            'flag' => 0,
                            'userId' => $userId,
                            'StoreManagerId'=>$storeid
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
			$store = $this->fireCurl($url,$curlPostData);
            $store_array = json_decode($store,1);
			/* echo "<pre>";
           print_r($store_array);die;*/
            if(!empty($store_array) && $store_array['status'] == 1)
            {
                $data['storemanager'] = $store_array['data']['store-manager'][0];
                $storeaccess=$store_array['data']['store-manager'][0]['AccessStore'];
                $pageaccess=$store_array['data']['store-manager'][0]['page_access'];
                
                if($storeaccess != '')
                {
                	$strAccess=explode(',', $storeaccess);
                	$data['storeaccess'] = $strAccess;
                }
                else{
                	$data['storeaccess'] = array();
                }
                if($pageaccess != '')
                {
                	$strpageAccess=explode(',', $pageaccess);
                	$data['pageaccess'] = $strpageAccess;
                }
                else{
                	$data['pageaccess'] = array();
                }
                
            }else
            {
            	 $data['storemanager'] =array();
            	 $data['storeaccess'] = array();
            	 $data['pageaccess'] = array();
            }

             //========= FETCH ADMIN MANAGEMENT LIST ========//
              $curlUrl = 'api/StoreManager';
            $curlPostData = array(
                            'mode' => 'fetch_AdminManagement_List', 
                            'userId' => $userId                            
                            );
            $url = $baseUrl.$curlUrl;
			$adminmanagementData = $this->fireCurl($url,$curlPostData);
            $adminmanagementData_array = json_decode($adminmanagementData,1);
			 //echo "<pre>";
           //print_r($adminmanagementData_array);die;
            if(!empty($adminmanagementData_array) && $adminmanagementData_array['status'] == 1)
            {
                $data['adminmanagement'] = $adminmanagementData_array['data']['Admin_Management'];
            }else{
            	$data['adminmanagement'] = array();
            }


        $data['title'] = 'edit-store';
        $data['viewPage'] = 'control/storemanager/edit_storemanager';
        //echo "<pre>";
      	//print_r($data);die;
        return view('control/includes/master_view',compact('data'));
    	}
    	else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/oms');
        }
        
	}
	
	//==============Update store manager details edit page=========//

	public function storemanagerUpdate(Request $request)
	{
		$storemanagerId     =$request->post('storemanagerId');
		$firstName          =$request->post('firstName');
		$lastName           =$request->post('lastName');
		$email              =$request->post('email');
		$password           =$request->post('password');
		$username           =$request->post('username');
		$IsActive           =$request->post('IsActive');
		$storeaccess        =$request->post('storeaccess');

		$rolesVal 			= $request->post('roles');
		$pageaccess         = $request->post('pageaccess');

		if($rolesVal == ''){
			$roles = '0';
		}else{
			$roles = $rolesVal;
		}

		if(!empty($pageaccess))
		{
			$strpageAccess=implode(',', $pageaccess);
		}
		else{
			$strpageAccess='';
		}

		if(!empty($storeaccess))
		{
			$strAccess=implode(',', $storeaccess);
		}
		else{
			$strAccess='';
		}

		// $file=$_FILES['image'];
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = Session::get('userId');
		
		$url = \Config::get('app.url');
		if($IsActive =='')
		{
			$IsActiveVal='0';
		}
		else{
			$IsActiveVal=$IsActive;
		}
		
		//======== insert store =========//
		$curlUrl = 'api/StoreManager';
		//echo "<pre>";echo $userId.'=='.$storeName.'=='.$storeLocation.'=='.$country.'=='.$IsActiveVal.'=='.$IsActive;//die();
		// echo "<pre>";print_r($_FILES);die();

		if($userId !=''  && $storemanagerId!='' && $firstName!='' && $lastName!='' && $email!='' && $password!='' && $username!='' && $IsActiveVal!='')
		{	
			$curlPostData = array(
						'mode' => 'Edit_new_store_manager',
						'userId' =>$userId,
						'StoreManagerId' => $storemanagerId,
						'ManagerFirstName' =>$firstName,
						'ManagerLastName' =>$lastName,
						'ManagerEmail' =>$email,
						'ManagerPassword' =>$password,
						'ManagerAccessStore' =>$strAccess,
						'UserName' =>$username,
						'status' =>$IsActiveVal,
						'pageaccess'=>$strpageAccess,
						'roles'=>$roles
						);
			$url = $baseUrl.$curlUrl;
				
			//echo "<pre>";
			//print_r($curlPostData);
			//die();
				$store = $this->fireCurl($url,$curlPostData);
				$store_array = json_decode($store,1);
			//echo "<pre>";
			//print_r($store_array);
			//die();
				if($store_array['status'] == 1)
				{	
					$request->session()->put('success_msg',$store_array['message']);
				}
				else
				{
					$request->session()->put('error_msg',$store_array['message']);
				}
				//echo $banner_array['message'].'@@@'.$promo_array['status'];
				return Redirect::to('/oms/storemanager/store-list');
		}
		else
		{
			$request->session()->put('error_msg','Please provide valid details.');
				//echo 'provide valid details'.'@@@'.'0';
			return Redirect::to('/oms/storemanager/store-list');
		}
	}
//==================================user add with mail send start==============================//
    public function userUpdate(Request $request){

        //echo '<pre>';
        //print_r($_REQUEST);

        $f_name = $request->input('f_name');
        $l_name = $request->input('l_name');
        $u_name = $request->input('u_name');
        $email = $request->input('email');
        $password = $request->input('password');
		//$signifi_url = $request->input('signifi_url');
		$store_name = $request->input('store_name');
		$sub_domain = $request->input('sub_domain');
		$sub_domain = preg_replace('/[^\w-]/', '', $sub_domain);
        $pages = array();
		$pages_all = $this->getAllData('page_management',array('status'=>'1'),'page_name','asc');
		if(!empty($pages_all))
		{
			foreach($pages_all as $page_row)
			{
				array_push($pages,$page_row['id']);
			}
		}
		if($f_name !='' && $l_name !='' && $u_name!='' && $email!='' && $password !='' && $store_name !='' && $sub_domain != '')
		{
			$userId = Session::get('userId');
			
			$pages = implode(', ', $pages);
	
			$passwordhash = hash('sha256',$password);
			$realpassword =$password;
	
			$curlUrl = 'api/Home';
			$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
				$curlPostData = array(
								'mode' => 'addedit_user_details',
								'f_name' => $f_name,
								'l_name' => $l_name,
								'u_name' => $u_name,
								'email' => $email,
								'password' => $passwordhash,
								'real_password'=>$realpassword,
								'storeName'=>$store_name,
								'sub_domain' =>$sub_domain,
								'pages' => $pages,
								'useridval'=>'0',
								'addeditflag'=>'0',
								'addedby'=>"0",
								'flag'=>'2'
								);//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
				//echo "<pre>";
				//print_r($curlPostData);
				//die();
				
				$url = $baseUrl.$curlUrl;
				$user = $this->fireCurl($url,$curlPostData);
				$user_array = json_decode($user);
				
				if($user_array->status == 1 && $user_array->data->status== 1)
				{
					
					$userId = $user_array->data->user->userId;
					//======== create_function css and js file for each user ========//
					$oldCssfile = public_path().'/assets/css/app.css';
					$newCssfile = public_path().'/userTemplate/css/app_'.$userId.'.css';
					
					$oldCssfile1 = public_path().'/assets/css/style.css';
					$newCssfile1 = public_path().'/userTemplate/css/style_'.$userId.'.css';
					
					$oldJsfile = public_path().'/scripts/app.js';
					$newJsfile = public_path().'/userTemplate/js/app_'.$userId.'.js';
					
					copy($oldCssfile, $newCssfile);
					copy($oldCssfile1, $newCssfile1);
					copy($oldJsfile, $newJsfile);
					//======== create_function css and js file for each user ========//
					
					//======== insert common pages for this user =======//
					$pageCode1 = substr(strtoupper(str_shuffle(uniqid('Contact-Us'.$userId))),0,10);
					$pageCode2 = substr(strtoupper(str_shuffle(uniqid('About-Us'.$userId))),0,10);
					$pageCode3 = substr(strtoupper(str_shuffle(uniqid('Privacy-Policy'.$userId))),0,10);
					$pageCode4 = substr(strtoupper(str_shuffle(uniqid('Terms-and-Conditions'.$userId))),0,10);
					$pageCode5 = substr(strtoupper(str_shuffle(uniqid('FAQ'.$userId))),0,10);
					
					$data_to_store = array(
											"userId" => $userId,
											"defaultPages" =>  array( 
																	 0 => array(
																				"PageTitle" => "Contact Us",
																				"PageCode" => $pageCode1,
																				"PageUrl" => 'Contact-Us/'.$pageCode1,
																				"Content" => 'abcd',
																				"Status" => '1',
																				"Section" => array(
																									"businessHour" => '1',
																									"officeId" => '',
																									"displayMap" => '1',
																									"displayEmail" => '1'
																									),
																				),
																	 1 => array(
																				"PageTitle" => "About Us",
																				"PageCode" => $pageCode2,
																				"PageUrl" => 'About-Us/'.$pageCode2,
																				"Content" => 'abcd',
																				"Status" => '1'
																			   ),
																	 2 => array(
																				"PageTitle" => "Privacy Policy",
																				"PageCode" => $pageCode3,
																				"PageUrl" => 'Privacy-Policy/'.$pageCode3,
																				"Content" => 'abcd',
																				"Status" => '1'
																				),
																	 3 => array(
																				"PageTitle" => "Terms and Conditions",
																				"PageCode" => $pageCode4,
																				"PageUrl" => 'Terms-and-Conditions/'.$pageCode4,
																				"Content" => 'abcd',
																				"Status" => '1'
																			   ),
																	 4 => array(
																				"PageTitle" => "FAQ",
																				"PageCode" => $pageCode5,
																				"PageUrl" => 'FAQ/'.$pageCode5,
																				"Content" => 'abcd',
																				"Status" => '1'
																			   )
																	)
											);
					DB::connection('mongodb')->collection('template')->insert($data_to_store);
					//======== insert common pages for this user =======//
					
					
					$request->session()->put('success_msg',"User details added successfully");
				}
				else
				{
					//$request->session()->put('error_msg',"Sorry! Failed to add user details");
				}
	
				echo json_encode($user_array);

		}
    }
//=====================================user add with mail send end===============================//
    public function userUpdatesave($id){

        // echo base64_decode($id);die;

        $userdetails = $this->getAllData('admin',array('id'=>base64_decode($id)));
        $pages = $this->getAllData('page_management',array('status'=>'1'),'page_name','asc');
        
        $state_name='';
        $product_name='';
        $fullfillment_partner_name='';
//         echo '<pre>';
//         print_r($userdetails);
//		 die();
		if(!empty($userdetails))
		{
            $user_state_id=$userdetails[0]['business_state'];
            $user_producttype_id=$userdetails[0]['product_type'];
            $user_fullfillment_id=$userdetails[0]['fullfillment_partner'];
            
            $getState=$this->getAllData('state',array('state_id'=>$user_state_id));
            $getproductType=$this->getAllData('product_type',array('id'=>$user_producttype_id));
            $getfullfillment=$this->getAllData('fullfillment_partner',array('id'=>$user_fullfillment_id));
            
            if(!empty($getState))
            {
                $state_name=$getState[0]['state_name'];
            }
            if(!empty($getproductType))
            {
                $product_name=$getproductType[0]['name'];
            }
            if(!empty($getfullfillment))
            {
                $fullfillment_partner_name=$getfullfillment[0]['name'];
            }
            
            
            
			$data['pages'] = $pages;
			$data['userdetails']=$userdetails;
            
			$data['title'] = 'edit-user';
			$data['viewPage'] = 'user/edit_user';
			return view('includes/master_view',compact('data','state_name','fullfillment_partner_name','product_name'));
		}
        else
		{
			Session::put('error_msg', "Invalid user");
			return redirect('admin/user/user-list');
		}
    }

    public function saveedit(Request $request){
		$userId = Session::get('userId');
        $f_name = $request->input('f_name');
        $l_name = $request->input('l_name');
		$store_name = $request->post('store_name');
        $subdomaun = $request->post('subdomain');
		$subdomaun = preg_replace('/[^\w-]/', '', $subdomaun);
        $domain = $request->post('domain');
        $domain = preg_replace('/[^\w-]/', '', $domain);
		$adeditid = $request->input('adeditid');
		$signifi_url = $request->input('signifi_url');
		$email = $request->post('email');
		$password = $request->post('password');
	    $username= $request->post('u_name');
		
		if($password != '')
		{
			$password = hash('sha256', $password);
		}
		//======= get all page =====//
		$pages = array();
		$pages_all = $this->getAllData('page_management',array('status'=>'1'),'page_name','asc');
		if(!empty($pages_all))
		{
			foreach($pages_all as $page_row)
			{
				array_push($pages,$page_row['id']);
			}
		}
        $pages = implode(', ', $pages);
		//========= get all pages ========//

		$userStatus = $request->input('userStatus');
        $whereStore = array('storeName'=>$store_name,
                            'addedBy' => 0);
        $whereSub = array('subDomain'=>$subdomaun,
                            'addedBy' => 0);
        $whereDomain = array('domain'=>$domain,
                            'addedBy' => 0);
		
        //======== check store name =========//
        $checkStore = $this->checkAdminDetails('admin',$whereStore,'id',$adeditid);
        $checkSubDomain = $this->checkAdminDetails('admin',$whereSub,'id',$adeditid);
        $checkDomain = $this->checkAdminDetails('admin',$whereDomain,'id',$adeditid);
        $checkEamil = $this->checkAdminDetails('admin',array('email'=>$email),'id',$adeditid);
        if($checkStore > 0)
        {
            $user_array['data']['status'] = 2;
        }
        else if($checkSubDomain > 0)
        {
            $user_array['data']['status'] = 3;
        }
        else if($domain != '' && $checkDomain > 0)
        {
            $user_array['data']['status'] = 4;
        }
		else if($checkEamil > 0)
        {
            $user_array['data']['status'] = 5;
        }
		else
		{
			//$pages = $request->input('pages');
					
			$curlUrl = 'api/Home';
			$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
				$curlPostData = array(
								'mode' => 'addedit_user_details',
								'f_name' => (string)$f_name,
								'l_name' => (string)$l_name,
								'signifi_url'=>$signifi_url,
								'pages' => (string)$pages,
								'useridval'=>(string)$adeditid,
								'status'=>(string)$userStatus,
								'storeName'=> $store_name,
								'sub_domain'=> $subdomaun,
								'domain'=> $domain,
								'email'=> $email,
								'password' => $password,
								'username'=>$username,
								'addeditflag'=>'1',
								'flag'=>'2'
								);
				
				//print_r($curlPostData);die;
				$url = $baseUrl.$curlUrl;
				$user = $this->fireCurl($url,$curlPostData);
				$user_array = json_decode($user);
				if($user_array->status == 1)
				{
					//echo $subdomaun;die();
					
                    //==============activity for insert user status start==============//
                    $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
                    if($userStatus == 1)
                    {
                        //***********************activity for insert new registartion request start***********************//
                        
                        $insert_activity_msg="Activated one user for registartion.User name <b class='text-primary'>".$username."</b>";
                        $curlUrl = 'api/insert-site-activity';
                        $curlPostData = array(
                        'storeid'=>1,
                        'message'=>$insert_activity_msg,
                        'icon'=>'fa fa-user-circle',
                        );
                        $url = $baseUrl.$curlUrl;
                        
                        $activity = $this->fireCurl($url,$curlPostData);
                        $activity_array = json_decode($activity,1);
                    
                        //***********************activity for insert new registartion request end*************************//
                    }
                    if($userStatus == 2)
                    {
                        //***********************activity for insert new registartion request start***********************//
                       
                        $insert_activity_msg_block="Blocked one user for registartion.User name <b class='text-primary'>".$username."</b>";
                        $curlUrl_block = 'api/insert-site-activity';
                        $curlPostData_block = array(
                        'storeid'=>1,
                        'message'=>$insert_activity_msg_block,
                        'icon'=>'fa fa-user-circle',
                        );
                        $url = $baseUrl.$curlUrl_block;
                        
                        $activity_block = $this->fireCurl($url,$curlPostData_block);
                        $activity_array_block = json_decode($activity_block,1);
                    
                        //***********************activity for insert new registartion request end*************************//
                    }
                    //===============activity for insert user status end==============//
					
					$updateArr = array('domain'=>$domain,
                               'subDomain' => $subdomaun,
                               'storeName'=> $store_name,
                               );
					$updateAll = $this->update('admin',array('addedBy'=>$adeditid),$updateArr);
                    
                    
					$request->session()->put('success_msg',"User details updated successfully");
				}
				else
				{
					$request->session()->put('error_msg',"Sorry! Failed to update user details");
				}
		}
		echo json_encode($user_array);

    }

    public function logincheck(Request $request){

        $userId = Session::get('userId');        
        $loginUserId = Session::get('loginUserId');        
        $userdetails = $this->getAllData('admin',array('id'=>$loginUserId));
        
        $returnarray['usertype'] = $userdetails[0]['its_superadmin'];
        $returnarray['user_page_access'] = $userdetails[0]['user_page_access'];
        

        // print_r($returnarray);

        echo json_encode($returnarray);


    }
	
	// ============== load change password view ============== //
	public function changePasswordView(){
		$userId = Session::get('loginUserId');
		$data['userId'] = $userId;
		$data['title'] = 'change-password';
        $data['viewPage'] = 'user/change_password';
        return view('includes/master_view',compact('data'));
	}
	
	// ============= update change password ================= //
	public function changePasswordUpdate(Request $request){
		$userId = Session::get('loginUserId');
        $postUserId = $request->input('userId');
		$newPassword = $request->input('newPassword_change');
		$confirmPassword = $request->input('confirmPassword_change');
		$oldPassword = hash('sha256', $request->input('oldPassword'));

		if($userId == $postUserId){
			if($newPassword == $confirmPassword){
				$userdetails = $this->getAllData('admin',array('id'=>$userId));
				if(!empty($userdetails)){
					$userPassword = $userdetails[0]['password'];
					if($oldPassword == $userPassword){
						$update = $this->update('admin',array('id'=>$userId),array('password'=>hash('sha256', $newPassword)));
						$request->session()->put('success_msg',"Password is changed!");
						return Redirect::to('/admin/dashboard/change-password');
					}else{
						$request->session()->put('error_msg',"Old password is wrong!");
						return Redirect::to('/admin/dashboard/change-password');
					}
				}else{
					$request->session()->put('error_msg',"Something is wring!");
					return Redirect::to('/admin/dashboard/change-password');
				}
			}else{
				$request->session()->put('error_msg',"New password and confirm password should be same !");
				return Redirect::to('/admin/dashboard/change-password');
			}
		}else{
			$request->session()->put('error_msg',"Something is wring!");
			return Redirect::to('/');
		}
	}
	
	public function blockSubadmin(Request $request)
	{
		$subadminId = $request->input('subadminId');
		$status = $request->input('status');
		
		$update = $this->update('admin',array('id'=>$subadminId),array('status'=>$status));
		if($status == 2)
		{
			$request->session()->put('success_msg',"User has been blocked successfully!");
		}
		else if($status == 1)
		{
			$request->session()->put('success_msg',"User has been unblocked successfully!");
		}
		echo true;
	}
	
	//============== get all product af a user ==============//
	public function userProduct(Request $request)
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = base64_decode($request->segment(4));
        if($userId != '')
        {
            //======== GET OTHER DETAILS =======//
            $data = $this->getOtherDetailsUser($userId);
			$data['userId'] = $userId;
			$getUser = $this->getAllData('admin',array('id'=>$userId),'','','','','','first_name');
			$data['userName'] = $getUser[0]['first_name'];
           //=================== PRODUCT LISTING WITH SEARCH =========================// 
            $data['selected_main'] = '';
            $data['selected_kiosk'] = '';
            $data['selected_type'] = '';
            $data['selected_vendor'] = '';
            $brandUniqueCode = $product_type = $kiosk = '';
            if ($request->isMethod('post')) {
                $mainSearch = $request->post('main_filter');
                //echo "<pre>";
                //print_r($_REQUEST);die;
                $data['selected_main'] = $mainSearch;
                if($mainSearch == '1')
                {
                    $kiosk = $request->post('kiosk_list');
                    $data['selected_kiosk'] = $kiosk;
                    
                }
                else if($mainSearch == '2')
                {
                    $product_type = $request->post('product_type_list');
                    $data['selected_type'] = $product_type;
                }
                else if($mainSearch == '3')
                {
                    $brandUniqueCode = $request->post('product_vendor_list');
                    $data['selected_vendor'] = $brandUniqueCode;
                }
            }
            //======== GET PRODUCT DETAILS FROM API =========//
            $curlUrl = 'api/Cbd_product';
            $curlPostData = array(
                            'mode' => 'get_product_list',
                            'brandUniqueCode' => $brandUniqueCode,
                            'product_type' => $product_type,
                            'kiosk' => $kiosk,
                            'lastRow' => 'all',
                            'admin' => $userId,
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
            $product = $this->fireCurl($url,$curlPostData);
            $product_array = json_decode($product,1);
            if(!empty($product_array) && $product_array['status'] == 1)
            {
                $data['product'] = $product_array['data']['data'];
            }
            else
            {
                $data['product'] = array();
            }
           // echo "<pre>";
            //print_r($data['product']);die;
            $data['title'] = 'user-product';
            $data['viewPage'] = 'product/user-product';
            //$data['js'] = 'product.js';
            return view('includes/master_view',compact('data'));
        }
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/');
        }
	}
	 //========= get other details ==========//
    public function getOtherDetailsUser($userId)
    {
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = $userId;
        //======== GET VENDOR LIST FROM API =========//
            $curlUrl = 'api/Cbd_product';
            $url = $baseUrl.$curlUrl;
            $curlPostData = array(
                            'mode' => 'get_all_brand',
                            'lastRow' => 'all',
                            //'admin' => $userId,
                            'admin' => 1,
                            'is_list'=> '1'
                            );//if u sent admin = session user id then the API will return all vendor at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            
            $product_vendor = $this->fireCurl($url,$curlPostData);
            $product_vendor_array = json_decode($product_vendor,1);
            if(!empty($product_vendor_array) && $product_vendor_array['status'] == 1)
            {
                $data['product_vendor'] = $product_vendor_array['data']['data'];
            }
            else
            {
                $data['product_vendor'] = array();
            }
            //======== GET PRODUCT TYPE LIST FROM API =========//
            $curlPostDataType = array(
                            'mode' => 'get_product_type',
                            'lastRow' => 'all',
                            //'admin' => $userId,
                            'admin' => 1,
                            'is_list'=> '1'
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $product_type = $this->fireCurl($url,$curlPostDataType);
            $product_type_array = json_decode($product_type,1);
            if(!empty($product_type_array) && $product_type_array['status'] == 1)
            {
                $data['product_type'] = $product_type_array['data']['data'];
            }
            else
            {
                $data['product_type'] = array();
            }
            //======== GET PRODUCT KIOSK LIST FROM API =========//
            $curlUrl = 'api/Machine';
            $url = $baseUrl.$curlUrl;
            $curlPostDataKiosk = array(
                            'mode' => 'get_kiosk_list',
                            //'lastRow' => 'all',
                            'userId' => $userId,
                            'is_list'=> '1'
                            );
            $product_kiosk = $this->fireCurl($url,$curlPostDataKiosk);
            $product_kiosk_array = json_decode($product_kiosk,1);
            if(!empty($product_kiosk_array) && $product_kiosk_array['status'] == 1)
            {
                $data['product_kiosk'] = $product_kiosk_array['data']['data'];
            }
            else
            {
                $data['product_kiosk'] = array();
            }
            return $data;
    }
	
	//================ edit user product ==============//
	public function edit_product(Request $request){
        $baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = base64_decode($request->segment(4));
        $curlUrl = 'api/Cbd_product';
        $productid = $request->segment(6);
        $curlPostData = array(
                        'mode' => 'get_product_list',
                        'admin' => $userId,
                        'productid' =>$productid
                        );
        $url = $baseUrl.$curlUrl;
        $product = $this->fireCurl($url,$curlPostData);
        $product_array = json_decode($product,1);
        $data = $this->getOtherDetailsUser($userId);
		$data['userId'] = $userId;
        if(!empty($product_array) && $product_array['status'] == 1){
            $data['product'] = $product_array['data']['data'];
        }else{
            $data['product'] = array();
        }
        //echo "<pre>";
        //print_r($data['product'][0]);die;
        $data['title'] = 'Product details';
        $data['viewPage'] = 'product/userproduct_edit';
        $data['js'] = 'product.js';
        return view('includes/master_view',compact('data'));
    }
	//===================== update product details ===========================//
	 //=========== add product ============//
    public function addRecord(Request $request)
    {
        $userId = base64_decode($request->post('user_id'));
        $all_file = $request->file('all_file');
        $product_id = $request->post('product_id');
        $title = $request->post('title');
		$variant = $request->post('v_size');
        $varientPrice = $request->post('v_price');
        $varientSku = $request->post('v_sku');
        $varientBar = $request->post('v_bar');
		$varientInventory = $request->post('v_inventory');
        $charge_tax = $request->post('charge_tax');
        $all_file_url = $request->post('all_file_url');
		$track = $request->post('track');
        $inventory = 0;
        if($track == '1' && $request->post('inventory') != '')
        {
            $inventory = $request->post('inventory');
        }
        
        if($charge_tax == '')
        {
            $charge_tax = 'off';
        }
        $shipping = $request->post('shipping');
        if($shipping == '')
        {
            $shipping = 'off' ;
        }
		if($userId !='' && $title!='' && $request->post('body_html') !='' && $request->post('vendor') != '' && $request->post('product_type') != '' && $request->post('price') !='' && is_numeric($request->post('price')))
		{
			$date = date('Y-m-d H:i:s');
			$dateStr = strtotime($date);
			$data = array(
				'id' => '0',
				'title' => stripcslashes($title),
				'body_html' => stripcslashes($request->post('body_html')),
				'vendor' => stripcslashes($request->post('vendor')),
				'product_type' => stripcslashes($request->post('product_type')),
				'handle' => stripcslashes($title),
				'updated_at' => (string)$date,
				'published_at' => (string)$date,
				'published_scope' => 'global',
				'tags' => "",
				'price' => $request->post('price'),
				'comp_price' => $request->post('comp_price'),
				'charge_tax' => $request->post('charge_tax'),
				'sku' => $request->post('sku'),
				'barcode' => $request->post('barcode'),
				'track' => $track,
				'shipping' => $request->post('shipping'),
				'weight' => $request->post('weight'),
				'weight_unit' => $request->post('weight_unit'),
				'status' => '1',
				'deleted' => '0',
			);
			//================ create seo title ============//
			$seo = str_replace(' ', '-', $title); // Replaces all spaces with hyphens.
			$seo = preg_replace('/[^A-Za-z0-9\-]/', '', $seo); // Removes special chars.
			///========= seo title unique checking ===========/////
			$chechkSeoStatus = 1;
			while($chechkSeoStatus > 0)
			{
				$chechkSeo = DB::connection('mongodb')->collection('cbd_products')->where(array('product_seo_title'=>$seo,'userId' => $userId));   
				if($product_id != '')
				{
					$chechkSeo = $chechkSeo->where('_id','!=',$product_id);
				}
				$chechkSeo = $chechkSeo->count();
				if($chechkSeo > 0)
				{
					$seo = $seo.'-'.rand(0,99);
				}
				else
				{
					$chechkSeoStatus = 0;
				}
			}
			$data['product_seo_title'] = $seo; 
			$img_count = 0;
			$data['images'] = array();
			if($product_id != '')
			{
			   $all_pre_file = $request->post('allPreImage');
				//===================== unset variant ====================//
				DB::connection('mongodb')->collection('cbd_products')->where('_id', $product_id)->unset('variants');
				//===================== unset images ====================//
				DB::connection('mongodb')->collection('cbd_products')->where('_id', $product_id)->unset('images');
				//===================== unset image ====================//
				DB::connection('mongodb')->collection('cbd_products')->where('_id', $product_id)->unset('image');
				//===================== unset options ====================//
				DB::connection('mongodb')->collection('cbd_products')->where('_id', $product_id)->unset('options');
				//===================== unset options ====================//
				DB::connection('mongodb')->collection('cbd_products')->where('_id', $product_id)->unset('machine');
				
				 //================== image array ==============//
				
				if(is_array($all_pre_file) && count($all_pre_file) > 0)
				{
					for($img_count == 0; $img_count < count($all_pre_file); $img_count++)
					{
						$img_position = $img_count;
						$fileName = $all_pre_file[$img_count];
						$url = \Config::get('app.url');//defined in config.php
						$imgNormal = $url.'storage/productImage/normal/'.$fileName;
						
						list($width,$height) = getimagesize($imgNormal);
						$data['images'][$img_count]['id'] = '';
						$data['images'][$img_count]['product_id'] = '';
						$data['images'][$img_count]['position'] = $img_position + 1;
						$data['images'][$img_count]['created_at'] = (string)$date;
						$data['images'][$img_count]['updated_at'] = (string)$date;
						$data['images'][$img_count]['alt'] = '';
						$data['images'][$img_count]['width'] = $width;
						$data['images'][$img_count]['height'] = $height;
						$data['images'][$img_count]['src'] = $fileName;
						$data['images'][$img_count]['variant_ids'] = array();
					}
				}  
			}
			else
			{
				$dateFull = date('Y-m-d');
				$year = date('Y');
				$month = date('m');
				$data['created_at'] = (string)$date;
				$data['created_date'] = (string)$dateFull;
				$data['year'] = (string)$year;
				$data['month'] = (string)$month;
				$img_count = 0;
			}
			$data['machine'] = explode(',',$request->post('all_machine_string'));
			//================ check for varient =================//
			
			$data['variants'] = array();
			$var_count = 0;
			for($i = 0; $i < count($variant); $i++)
			{
				if($variant[$var_count] != '' && $varientPrice[$var_count] && $varientSku[$var_count] !='')
				{
					$position = $var_count;
					$position = $position + 1;
					$data['variants'][$var_count]['id'] = 0;
					$data['variants'][$var_count]['product_id'] = 0;
					$data['variants'][$var_count]['title'] = $variant[$var_count];
					$data['variants'][$var_count]['price'] = (string)$varientPrice[$var_count];
					$data['variants'][$var_count]['sku'] = $varientSku[$var_count];
					$data['variants'][$var_count]['barcode'] = $varientBar[$var_count];
					$data['variants'][$var_count]['position'] = $position;
					$data['variants'][$var_count]['inventory_policy'] = '';
					$data['variants'][$var_count]['compare_at_price'] = (string)$request->post('comp_price');
					$data['variants'][$var_count]['grams'] = (string)$request->post('weight');
					if($track == '1' && $varientInventory[$var_count] != '')
					{
						$v_inventory = $varientInventory[$var_count];
					}
					else
					{
						$v_inventory = 0;
					}
					$data['variants'][$var_count]['inventory_quantity'] = $v_inventory;
					$data['variants'][$var_count]['weight'] = (string)$request->post('weight');
					$data['variants'][$var_count]['weight_unit'] = (string)$request->post('weight_unit');
					$var_count++;
				}
			}
			if(empty($data['variants']))
			{
				$data['variants'][0]['id'] = 0;
				$data['variants'][0]['product_id'] = 0;
				$data['variants'][0]['title'] = 'Default Title';
				$data['variants'][0]['price'] = (string)$request->post('price');
				$data['variants'][0]['sku'] = $request->post('sku');
				$data['variants'][0]['barcode'] = $request->post('barcode');
				$data['variants'][0]['position'] = 1;
				$data['variants'][0]['inventory_policy'] = $track;
				$data['variants'][0]['compare_at_price'] = (string)$request->post('comp_price');
				$data['variants'][0]['grams'] = 0;
				$data['variants'][0]['inventory_quantity'] = $inventory;
				$data['variants'][0]['weight'] = (string)$request->post('weight');
				$data['variants'][0]['weight_unit'] = (string)$request->post('weight_unit');
			}
			//=================== check SKU no ================//
			$all_sku = array();
			$sku_error = 0;
			foreach($data['variants'] as $sku)
			{
			   if(!in_array($sku['sku'],$all_sku))
			   {
					//$sku_error = $this->createSKU($sku['sku'],'');
					array_push($all_sku,$sku['sku']);
			   }
			}
			//============== check for file ==============//
			
			//================ upload by file ==========//
			if(!empty($all_file))
			{
				foreach($all_file as $image)
				{
					if(strpos($image->getClientMimeType(),'image/') >= 0)
					{
						$img_position = $img_count;
						$fileName = time().rand(0,999). '.' . $image->getClientOriginalExtension();
						list($width,$height) = getimagesize($image);
						$image_content = file_get_contents($image);
						$path = 'productImage/normal/';
						$thumb_path = 'productImage/thumbnail/';
						$fileNormal = $path.$fileName;
						$fileThumb = $thumb_path.$fileName;
						$this->uploadImage('public',$fileNormal,$image_content);
						$this->uploadThumbnail('public',$fileThumb,$image_content,200,200);
						$data['images'][$img_count]['id'] = '';
						$data['images'][$img_count]['product_id'] = '';
						$data['images'][$img_count]['position'] = $img_position + 1;
						$data['images'][$img_count]['created_at'] = (string)$date;
						$data['images'][$img_count]['updated_at'] = (string)$date;
						$data['images'][$img_count]['alt'] = '';
						$data['images'][$img_count]['width'] = $width;
						$data['images'][$img_count]['height'] = $height;
						$data['images'][$img_count]['src'] = $fileName;
						$data['images'][$img_count]['variant_ids'] = array();
						$img_count++;
					}
				}
			}
			//=========== upload by url ===============//
			if(!empty($all_file_url))
			{
				foreach($all_file_url as $url)
				{
					$file_headers = @get_headers($url);
					if(is_array($file_headers) && $file_headers[2] !='' && strpos($file_headers[2],'image/') == true)
					{
						$img_position = $img_count;
						$image = file_get_contents($url);
						$path = 'productImage/normal/';
						$thumb_path = 'productImage/thumbnail/';
						$fileName = time().rand(0,99).'.jpg';
						$fileNormal = $path.$fileName;
						$fileThumb = $thumb_path.$fileName;
						list($width,$height) = getimagesize($url);
						$this->uploadImage('public',$fileNormal,$image);
						$this->uploadThumbnail('public',$fileThumb,$image,200,200);
						$data['images'][$img_count]['id'] = '';
						$data['images'][$img_count]['product_id'] = '';
						$data['images'][$img_count]['position'] = $img_position + 1;
						$data['images'][$img_count]['created_at'] = (string)$date;
						$data['images'][$img_count]['updated_at'] = (string)$date;
						$data['images'][$img_count]['alt'] = '';
						$data['images'][$img_count]['width'] = $width;
						$data['images'][$img_count]['height'] = $height;
						$data['images'][$img_count]['src'] = $fileName;
						$data['images'][$img_count]['variant_ids'] = array();
						$img_count++;
					}
				}
			}
			$data['image']['id'] = '';
			$data['image']['product_id'] = '';
			$data['image']['position'] = '';
			$data['image']['created_at'] = '';
			$data['image']['updated_at'] = '';
			$data['image']['alt'] = '';
			$data['image']['width'] = '';
			$data['image']['height'] = '';
			$data['image']['src'] = '';
			$data['image']['variant_ids'] = array();
			if(count($data['images']) > 0)
			{
				$data['image']['id'] = $data['images'][0]['id'];
				$data['image']['product_id'] = $data['images'][0]['product_id'];
				$data['image']['position'] = $data['images'][0]['position'];
				$data['image']['created_at'] = $data['images'][0]['created_at'];
				$data['image']['updated_at'] = $data['images'][0]['updated_at'];
				$data['image']['alt'] = $data['images'][0]['alt'];
				$data['image']['width'] = $data['images'][0]['width'];
				$data['image']['height'] = $data['images'][0]['height'];
				$data['image']['src'] = $data['images'][0]['src'];
				$data['image']['variant_ids'] = array();
			}
			//======= seo details ======//
			$data['seo_title'] = (string)$request->post('seo_title');
			$data['seo_desc'] = (string)$request->post('seo_desc');
			$data['seo_url'] = (string)$request->post('seo_url');
			$data['addedon'] = (string)$dateStr;
			$data['userId'] = (string)$userId;
			$data['sort_price'] = (int)$data['variants'][0]['price'];
			if($product_id == '')
			{
				$data['sellingNo'] = '0';
				$insert = $this->insertDocument('cbd_products',$data);
				if($insert)
				{
					$request->session()->put('success_msg','Product added successfully!');
				}
				else
				{
					$request->session()->put('error_msg','Failed to add product.');
				}
			}
			else
			{
				$insert = $this->updateDocument('cbd_products',$data,array('_id'=>$product_id));
				$request->session()->put('success_msg','Product updated successfully!');
			}
			
		}	
		return Redirect::to('/admin/user/product/'.base64_encode($userId));
    }
	
	//=========================function for subadmin customer list===================//
	public function userCustomer(Request $request)
	{
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
		$userId = base64_decode($request->segment(4));
       
        if($userId != '')
        {
            $page='';
			$lastRow='';
			$limit=10;
			
			if(isset($_REQUEST['page']) && $_REQUEST['page'] != '')
			{
				$page=$_REQUEST['page'];
			}
			
            if($page == '' || $page == 0)
			{
				$page=1;
			}
			$lastRow = ($page-1) * $limit;
			
			$start_date='';
			$end_date='';
			$status='';
			$country='';
			
			//*************************generate serach start****************************//
			if(isset($_REQUEST['select_customer_by']) && $_REQUEST['select_customer_by'] !='')
			{
				
				if($_REQUEST['select_customer_by'] == 1)
				{
					if(isset($_REQUEST['serch_customer']) && $_REQUEST['serch_customer'] !='')
					{
						if($_REQUEST['serch_customer'] == 'last_week')
						{
							$start_date=date('Y-m-d', strtotime("-1 week")); //1 week ago
							$end_date=date('Y-m-d');
						}
						else if($_REQUEST['serch_customer'] == 'last_month')
						{
							$start_date=date('Y-m-d', strtotime("-1 Months"));
							$end_date=date('Y-m-d');
						}
						else if($_REQUEST['serch_customer'] == 'last_3_month')
						{
							$start_date=date('Y-m-d', strtotime("-3 Months"));
							$end_date=date('Y-m-d');
						}
						else if($_REQUEST['serch_customer'] == 'last_year')
						{
							$start_date=date('Y-m-d', strtotime("-1 year"));
							$end_date=date('Y-m-d');
						}
						else if($_REQUEST['serch_customer'] == 'before_date')
						{
							$start_date='';
							$end_date=$_REQUEST['created_date'];
						}
						else if($_REQUEST['serch_customer'] == 'after_date')
						{
							$start_date=$_REQUEST['created_date'];
							$end_date='';
						}
					}
					
				}
				else if($_REQUEST['select_customer_by'] == 2)
				{
					if(isset($_REQUEST['serch_customer']) && $_REQUEST['serch_customer'] !='')
					{
						
						$status=$_REQUEST['serch_customer'];
					}				
				}
				else if($_REQUEST['select_customer_by'] == 3)
				{
					if(isset($_REQUEST['serch_customer']) && $_REQUEST['serch_customer'] !='')
					{
						
						$country=$_REQUEST['serch_customer'];
					}	
				}
			}
			
			//********************************generate search end******************************//
            
            //======== GET CUSTOMER LIST FROM API =========//
            $curlUrl = 'api/Customer';
            $curlPostData = array(
                            'mode' => 'get_customer_list',
                            'lastRow' => $lastRow,
                            'admin' => $userId,
							'userid'=>$userId,
							'start_date'=>$start_date,
							'end_date'=>$end_date,
							'status'=>$status,
							'country'=>$country
                            );//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
            $url = $baseUrl.$curlUrl;
            $customer = $this->fireCurl($url,$curlPostData);
            $customer_array = json_decode($customer,1);
			
            if(!empty($customer_array) && $customer_array['status'] == 1)
            {
                $data['customer'] = $customer_array['data']['customer'];
				$hasMore=$customer_array['data']['pagination']['hasMore'];
				
				$data['pagination']=$customer_array['data']['pagination']['totalpage'];
				$numOfTotal=$customer_array['data']['pagination']['totaldata'];
				
				//echo $numOfTotal.'@@@@@@'.$data['pagination'];die();
				$pegi= $this->myPagination($numOfTotal,$limit,$page,$url='?',"");
				$data['pegi']=html_entity_decode($pegi);
                
            }
            else
            {
                $data['customer'] = array();
                $data['pegi']='';
                $data['pagination']=array();
                
            }
            
            $data['title'] = 'customer';
            $data['viewPage'] = 'user/customers_list_subadmin';
            return view('includes/master_view',compact('data'));
        }
        else
        {
            Session::put('error_msg','Please login first!');
            return Redirect::to('/');
        }
	}
	
	//===================================customer details====================================//
    public function getDetails(Request $request)
    {
		//echo $id;die();
		$customerid=$request->segment(4);
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        //$userId = Session::get('userId');
		$userId = base64_decode($request->segment(5));
		
		//======== GET CUSTOMER LIST FROM API =========//
		$curlUrl = 'api/Customer';
		$curlPostData = array(
						'mode' => 'get_customer_list',
						'lastRow' => '',
						'customerid'=>$customerid,
						'admin' => $userId,
						'userid'=>$userId,
						);//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
		//echo "<pre>";
		//print_r($curlPostData);
		
		
		$url = $baseUrl.$curlUrl;
		$customer = $this->fireCurl($url,$curlPostData);
		$customer_array = json_decode($customer,1);
		//echo "<pre>";
		//print_r($customer_array);
		//die();
		
		$customerarr=array();
		//if($customer_array['status'] == 1)
		//{
			$customerarr=$customer_array['data']['customer'][0];
		//}
		
	
		$ConditionCounrty=array('id'=>$customerarr['country']);
		$fetchcountry=$this->getAllData('countries',$ConditionCounrty,'id','ASC');
		
		//echo "<pre>";
		//print_r($fetchcountry);
		//die();
		
		$country_name=$fetchcountry[0]['name'];
		
		
		$ConditionState=array('id'=>$customerarr['state']);
		$fetchstate=$this->getAllData('states',$ConditionState,'id','ASC');
		$state_name=$fetchstate[0]['name'];
		
		//$ConditionCity=array('id'=>$customerarr['city']);
		//$fetchcity=$this->getAllData('cities',$ConditionCity,'id','DESC');
		//$city_name=$fetchcity[0]['name'];
		
		$fetchcountryAll=$this->getAllData('countries',$Counrtyarray=array(),'id','ASC');
		$fetchstateAll=$this->getAllData('states',$Statearray=array(),'id','ASC');
		
		//if($customer_array['data']['customer'])
		//echo "<pre>";
		//print_r($customer_array['data']['customer'][0]);
		//die();
		
		$data['title'] = 'customer';
        $data['viewPage'] = 'user/subadmin_customers_details';
        return view('includes/master_view',compact('data','customerarr','country_name','state_name','fetchcountryAll','fetchstateAll'));
    }
	
	//===============================export customer==============================//
	
	public function exportCustomer(Request $request){
		
	    $userId =(int)base64_decode($request->segment(4));
		
		//echo "<pre>";
		//print_r($_REQUEST);
		//die();
		$page='';
			$lastRow='';
			$limit=10;
			
			if(isset($_REQUEST['export_page']) && $_REQUEST['export_page'] != '')
			{
				$page=$_REQUEST['export_page'];
			}
			
            if($page == '' || $page == 0)
			{
				$page=1;
			}
			$lastRow = ($page-1) * $limit;
			
			$start_date='';
			$end_date='';
			$status='';
			$country='';
				
			//*************************generate serach start****************************//
			if(isset($_REQUEST['export_select_customer_by']) && $_REQUEST['export_select_customer_by'] !='')
			{
				
				if($_REQUEST['export_select_customer_by'] == 1)
				{
					if(isset($_REQUEST['export_serch_customer']) && $_REQUEST['export_serch_customer'] !='')
					{
						if($_REQUEST['export_serch_customer'] == 'last_week')
						{
							$start_date=date('Y-m-d', strtotime("-1 week")); //1 week ago
							$end_date=date('Y-m-d');
						}
						else if($_REQUEST['export_serch_customer'] == 'last_month')
						{
							$start_date=date('Y-m-d', strtotime("-1 Months"));
							$end_date=date('Y-m-d');
						}
						else if($_REQUEST['export_serch_customer'] == 'last_3_month')
						{
							$start_date=date('Y-m-d', strtotime("-3 Months"));
							$end_date=date('Y-m-d');
						}
						else if($_REQUEST['export_serch_customer'] == 'last_year')
						{
							$start_date=date('Y-m-d', strtotime("-1 year"));
							$end_date=date('Y-m-d');
						}
						else if($_REQUEST['export_serch_customer'] == 'before_date')
						{
							$start_date='';
							$end_date=$_REQUEST['created_date'];
						}
						else if($_REQUEST['export_serch_customer'] == 'after_date')
						{
							$start_date=$_REQUEST['export_created_date'];
							$end_date='';
						}
					}
					
				}
				else if($_REQUEST['export_select_customer_by'] == 2)
				{
					if(isset($_REQUEST['export_serch_customer']) && $_REQUEST['export_serch_customer'] !='')
					{
						
						$status=$_REQUEST['export_serch_customer'];
					}				
				}
				else if($_REQUEST['export_select_customer_by'] == 3)
				{
					if(isset($_REQUEST['export_serch_customer']) && $_REQUEST['export_serch_customer'] !='')
					{
						
						$country=$_REQUEST['export_serch_customer'];
					}	
				}
			}
		
		$whereCondition=array('userid'=>$userId);
		
		
		if($start_date !='')
		{
			$whereCondition = array(array('userid',$userId) , array('added_on' ,'>=', $start_date)); 
			
			//$whereCondition = "added_on >= $start_date";
		   
		}
		if($end_date !='')
		{
			$whereCondition = array(array('userid',$userId) , array('added_on' ,'<=', $end_date)); 
			//$whereCondition= "added_on <= $end_date";
		}
		if($status !='')
		{
		   $whereCondition['status'] = (int)$status; 
		}
		if($country !='')
		{
			$whereCondition['country'] = $country; 
		}
		
		//echo "<pre>";
		//print_r($whereCondition);
		//die();
		//echo $limit.'@@@@@@'.$lastRow;die();
		
		//$allcustomer = $this->getAllDocumentCommon('customer',$whereCondition,'_id','DESC',$limit,$lastRow);
		
		$allcustomer = $this->getAllDocument('customer',$whereCondition,'_id','DESC',$limit,$lastRow);
		
		//echo "<pre>";
		//print_r($allcustomer);
		//echo "</pre>";
		//die();
		
		
		$returnArr=array();
		if(!empty($allcustomer))
		{
			$rowsCount = 0;
                    foreach($allcustomer as $rows)
                    {
							if($rows['status'] == 1)
							{
								$status='Active';
							}
							else
							{
								$status='Inactive';
							}
							$ConditionCounrty=array('id'=>$rows['country']);
							$fetchcountry=$this->getAllData('countries',$ConditionCounrty,'id','ASC');
							$country_name=$fetchcountry[0]['name'];
							
							$ConditionState=array('id'=>$rows['state']);
							$fetchstate=$this->getAllData('states',$ConditionState,'id','ASC');
							$state_name=$fetchstate[0]['name'];
							
							$ConditionAdmin=array('id'=>$rows['userid']);
							$fetchAdmin=$this->getAllData('admin',$ConditionAdmin,'id','ASC');
							$admin_name=$fetchAdmin[0]['uname'];
							
                            //$returnArr[$rowsCount]['customer_id']= (string)$rows['_id'];
                            $returnArr[$rowsCount]['admin']= $admin_name;
                            $returnArr[$rowsCount]['basic_name']= $rows['basic_first_name'].' '.$rows['basic_last_name'];
                            $returnArr[$rowsCount]['basic_first_name']= $rows['basic_first_name'];
                            $returnArr[$rowsCount]['basic_last_name']= $rows['basic_last_name'];
                            $returnArr[$rowsCount]['email']= $rows['email'];
                            $returnArr[$rowsCount]['basic_phone']= $rows['basic_phone'];
                           
                            $returnArr[$rowsCount]['adrs_name']= $rows['adrs_first_name'].' '.$rows['adrs_last_name'];
                            $returnArr[$rowsCount]['adrs_first_name']= $rows['adrs_first_name'];
                            $returnArr[$rowsCount]['adrs_last_name']= $rows['adrs_last_name'];
                            $returnArr[$rowsCount]['company_name']= $rows['company_name'];
                            $returnArr[$rowsCount]['address1']= $rows['address1'];
                            $returnArr[$rowsCount]['address2']= $rows['address2'];
                            $returnArr[$rowsCount]['city']= $rows['city'];
                            $returnArr[$rowsCount]['zip']= $rows['zip'];
                            $returnArr[$rowsCount]['country']= $country_name;
                            $returnArr[$rowsCount]['state']= $state_name;
                            $returnArr[$rowsCount]['address_phone']= $rows['address_phone'];
                            //$returnArr[$rowsCount]['image']= $rows['image'];
                            $returnArr[$rowsCount]['customer_note']= $rows['customer_note'];
							
                            $returnArr[$rowsCount]['status']= $status;
                            $returnArr[$rowsCount]['added_on']= $rows['added_on'];
                           
                            $rowsCount++;
                    }
		}
			
			
			//$headers=array('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			//echo "<pre>";
			//print_r($returnArr);
			//die();
			return \Excel::create('customer', function($excel) use ($returnArr) {
				$excel->sheet('sheet name', function($sheet) use ($returnArr)
				{
					$sheet->fromArray($returnArr);
				});
			})->download('csv');

        
    }
	
	//***************************************export user list***************************************//
	
	public function exportUserList(){
		
		$userId = Session::get('userId');
		
		//echo "<pre>";
		//print_r($_REQUEST);
		//die();
			$page='';
			$lastRow='';
			$limit=10;
			
			if(isset($_REQUEST['export_page']) && $_REQUEST['export_page'] != '')
			{
				$page=$_REQUEST['export_page'];
			}
			
            if($page == '' || $page == 0)
			{
				$page=1;
			}
			$lastRow = ($page-1) * $limit;
			
			$start_date='';
			$end_date='';
			$status='';

		
			
			//if($sub_admin != '')
			//{
			//	$whereCondition = array('id' => $sub_admin);
			//}
			//else
			//{
				$whereCondition = array('addedBy'=>0);
			//}
			
			//echo $page.'@@@'.$limit.'@@@'.$lastRow;die();
			$alluser = $this->getAllData('admin',$whereCondition,'id','DESC',$limit,$lastRow);
			
			//echo "<pre>";
			//print_r($alluser);
			//die();
		
			$returnArr=array();
			if(!empty($alluser))
			{
				
				$rowCount = 0;
				foreach($alluser as $row)
				{
					$whereCondition_customer=array('userid'=>$row['id']);
					$customer = $this->getAllDocument('customer',$whereCondition_customer,'_id','DESC');
					
					if($row['status'] == 1)
					{
						$status="Active";
					}
					if($row['status'] == 0)
					{
						$status="Inactive";
					}
					
					if(!empty($customer))
					{
						$count_customer=count($customer);
					}
					else
					{
						$count_customer=0;
					}
					
					$returnArr[$rowCount]['uname'] = $row['uname'];
					$returnArr[$rowCount]['email'] = $row['email'];
					//$returnArr[$rowCount]['its_superadmin'] = $row['its_superadmin'];
					$returnArr[$rowCount]['first_name'] = $row['first_name'];
					$returnArr[$rowCount]['last_name'] = $row['last_name'];
					$returnArr[$rowCount]['image'] = $row['image'];
					//$returnArr[$rowCount]['addedBy'] = $row['addedBy'];
					$returnArr[$rowCount]['addedOn'] = $row['addedOn'];
					$returnArr[$rowCount]['no_customer'] = $count_customer;
					$returnArr[$rowCount]['status'] = $status;
					if($row['image'] !='')
					{
						$returnArr[$rowCount]['image'] = $row['image'];
					}
					else
					{
						$returnArr[$rowCount]['image'] = '';
					}
				
					$rowCount++;
				}
			}
			
			
			//$headers=array('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			
			return \Excel::create('userlist', function($excel) use ($returnArr) {
				$excel->sheet('sheet name', function($sheet) use ($returnArr)
				{
					$sheet->fromArray($returnArr);
				});
			})->download('csv');

        
    }
	
	//=============================send mail======================================//
	public function sendMymail(Request $request)
	{
		$receiver='gaurav.singh@esolzmail.com';
		
		
		$objDemo = new \stdClass();
        $objDemo->demo_one = 'Demo One Value';
        $objDemo->demo_two = 'Demo Two Value';
        $objDemo->sender = 'check';
        $objDemo->receiver = 'Gaurav';
		$objDemo->file = url('/').'/storage/customerImage/thumbnail/1524028525827.jpg';
		$objDemo->document = url('/').'/storage/testinstruction.docx';
		
		$mail_send=Mail::to($receiver)->send(new sendMyMail($objDemo));
		 
		echo "success";
		
	}
	public function userKiosk(Request $request){
		$baseUrl = \Config::get('app.url');//this is the base url defined in config.php file
        $userId = base64_decode($request->segment(4));
        if($userId != ''){
			$data['title'] = 'kiosks';
			$data['viewPage'] = 'kiosks/user_kiosks';
			$curlUrl = 'api/Machine';
			$curlPostData = array(
							'mode' => 'get_kiosk_list',
							'userId' => (string)$userId
							);//if u sent admin = session user id then the API will return all products at a time if you want pagination then remove 'admin' parameter and sent 'lastRow' = 0
			$url = $baseUrl.$curlUrl;
			$kioskDataJson = $this->fireCurl($url,$curlPostData);
			$kioskData = json_decode($kioskDataJson,1);
			$mapLatlongArr = array();
			
			$curlUrlonline_offline = 'api/Machine';
			$curlPostDataonline_offline = array(
							'mode' => 'get_koisk_online_offline',
							'userId' => (string)$userId
							);
			$urlonline_offline = $baseUrl.$curlUrlonline_offline;
			$kioskonline_offlineDataJson = $this->fireCurl($urlonline_offline,$curlPostDataonline_offline);
			$kioskonline_offlineData = json_decode($kioskonline_offlineDataJson,1);
			if($kioskonline_offlineData['status'] == 1){
                $kioskDataRowCount =0;
                foreach($kioskonline_offlineData['data'] as $kioskDataRowKey=>$kioskDataRowValue){
                    $dataKioskOnline[$kioskDataRowCount]=array($kioskDataRowKey,(int)$kioskDataRowValue);
                    $kioskDataRowCount++;
                }
                $data['kioskonline_offline'] = json_encode($dataKioskOnline);
            }else{
				$data['kioskonline_offline'] = json_encode(array());
			}
			if(!empty($kioskData) && $kioskData['status'] == 1){
				$data['kioskData'] = $kioskData['data']['data'];
				$kioskDataRowCount = 0 ;
				foreach($data['kioskData'] as $kioskDataRow){
					if(!empty($kioskDataRow['is_online'])){
						$mapLatlongArr[$kioskDataRowCount]['longitude'] = $kioskDataRow['longitude'];
						$mapLatlongArr[$kioskDataRowCount]['latitude'] = $kioskDataRow['latitude'];
						$mapLatlongArr[$kioskDataRowCount]['name'] = $kioskDataRow['name'];
						$kioskDataRowCount++;
					}
				}
				$data['mapLatlongData'] = json_encode($mapLatlongArr);
			}else{
				$data['kioskData'] = array();
				$data['mapLatlongData'] = json_encode(array());
			}
			return view('includes/master_view',compact('data'));
		}else{
			Session::put('error_msg','Sorry! Please select any user!');
            return Redirect::to('/');
		}
	}
}
?>